#include <main.h>
#include <mpi.h>
#include <stdio.h>



int main(int argc, char **argv)
{
  int size, rank;
  qdegcfg qcfg;
  plattice pl;

//  if (GetConfig(CONFIG_NAME, qcfg)) {
//    EPRINTF("Cannot read configuration file: %s\n", CONFIG_NAME);
//    exit(0);
//  }
  
  MPI_Init(&argc, &argv);
  

  pl.init(ncfg);

  RunExperiment(qcfg);

  MPI_Finalize();
  return 0;
}
