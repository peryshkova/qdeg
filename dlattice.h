#ifndef DLATTICE_H
#define DLATTICE_H

#include <stdlib.h>
#include <emmintrin.h>
#include <xmmintrin.h>
#include <unistd.h>
#include <string.h>

#include <debug.h>
#include <optimization.h>


#define ALL_Z(s,e)     for (z = e; z < s; z++)       // z может быть любым
#define ALL_Y(s,e)     for (y = ((int)z%2)+s; y < e; y+=2)  // x должно иметь ту же чётность, что и z
#define ALL_X(s,e)     for (x = ((int)z%2)+2*(((int)z/2+(int)x/2)%2)+s; x < e; x+=4)  // y идёт через 4

#define dir_number 16
static int dx_neighbor[dir_number]  __attribute__ ( (aligned(16)) ) = { 1, 1,-1,-1, 0, 2, 2, 0, 2, 2, 0,-2,-2, 0,-2,-2}; // x-координаты соседей
static int dy_neighbor[dir_number]  __attribute__ ( (aligned(16)) )= { 1,-1, 1,-1, 2, 0, 2,-2, 0,-2, 2, 0, 2,-2, 0,-2}; // y-координаты соседей
static int dz_neighbor[dir_number]  __attribute__ ( (aligned(16)) )= { 1,-1,-1, 1, 2, 2, 0,-2,-2, 0,-2,-2, 0, 2, 2, 0}; // z-координаты соседей
static int pr_neighbor[dir_number]  __attribute__ ( (aligned(16)) )= {-1,-1,-1,-1, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3}; // номер предыдущего

typedef struct {
  int x[dir_number] __attribute__ ( (aligned(16)) );
  int y[dir_number] __attribute__ ( (aligned(16)) );
  int z[dir_number] __attribute__ ( (aligned(16)) );
} neighbors_t;

#define LPTR(type,LX,LY,newname,oldname) type (*newname)[LY][LX] __attribute__ ( (aligned(16)) ) = (type (*)[LY][LX])oldname

template<typename Elem> 
class dlattice_t{
private:
  Elem *lat;
  ulong Px, Py, Pz; // Packed lattice dimensions  
public:
  inline ulong lsize(){
    return Px*Py*Pz*sizeof(Elem);
  }

//------------------------ Initializations ------------------------------------//
  dlattice_t()
  {
    Px = 0;
    Py = 0;
    Pz = 0;
    lat = NULL;
  }
  
  int init(int Lx, int Ly, int Lz)
  {
    if( (Lx % 4) || (Ly % 4) || (Lz % 4) ){
      EPRINTF("(Lx %% 4 != 0) OR (Ly %% 4 != 0) OR (Lz %% 4 != 0): (%d,%d,%d)\n", 
          Lx, Ly, Lz);
      return -1;
    }
    Px = Lx;
    Py = Ly/2;
    Pz = Lz/4;
    if( posix_memalign((void**)&lat,16,lsize()) ){
      EPRINTF("Cannot allocate %lu bytes\n", lsize());
      return -1;
    }
    reset();
    return 0;
  }

  void reset()
  {
    if( lat )
      memset(lat, 0, lsize());
  }

	inline int maxx(){ return Px; }
	inline int maxy(){ return Py; }
	inline int maxz(){ return Pz; }

//--------------------- Element access ---------------------------------------//

  inline Elem &operator () (uint x, uint y, uint z) 
  {
    // indexes of corresponding elementary 
    // cube in the grid
    int lx, ly, lz;
    LPTR(Elem,Px,Py,lattice,lat);
#ifdef DEBUG_ON
    if( (y-z%2)%2 || ( x - (z%2+2*((z/2 + y/2)%2)) )%4 )
        DPRINTF("Access to the wrong element: (%d,%d,%d)",x,y,z);
#endif
    convert(x, y, z, lx, ly, lz);
    return lattice[lz][ly][lx];
  }

  inline Elem &l(int x, int y, int z) 
  {
    // access to elements using packed coordinates
    if( unlikely(y>Py || z>Pz) ){
      EPRINTF("Use original coordinates instead of packed!\n");
      abort();
    }
    LPTR(Elem,Px,Py,lattice,lat);
    return lattice[z][y][x];
  }

  inline  Elem &l(int c[3]) 
  {
    // access to elements using packed coordinates
    LPTR(Elem,Px,Py,lattice,lat);
    return lattice[c[2]][c[1]][c[0]];
  }

//------------------ Compute neighbors coordinates ----------------------------//

  int neighbors_serial(int x, int y, int z, neighbors_t &nbs, neighbors_t &nbs_p)
  {
    int factor, dir;
    if ( z%2==0 )
      factor=1;
    else 
      factor=-1;
  
    for (dir=0; dir<dir_number; dir++) 
    {
      nbs.x[dir] = x+factor*dx_neighbor[dir];
      nbs.y[dir] = y+factor*dy_neighbor[dir];
      nbs.z[dir] = z+factor*dz_neighbor[dir];
      convert(nbs.x[dir], nbs.y[dir], nbs.z[dir], nbs_p.x[dir], nbs_p.y[dir], nbs_p.z[dir]);
    }
    return factor;
  }

  int neighbors_vect(int x, int y, int z, neighbors_t &nbs, neighbors_t &nbs_p)
  {
    int factor, i;

    if ( z%2==0 )
      factor=1;
    else 
      factor=-1;

	  __m128i fact_v = _mm_set1_epi32(factor);
	  __m128i x_v = _mm_set1_epi32(x);
	  __m128i y_v = _mm_set1_epi32(y);
	  __m128i z_v = _mm_set1_epi32(z);
  
    for (i=0; i<dir_number/4; i++) 
    {
		  __m128i *ptr = (__m128i*)nbs.x + i;
		  *ptr = x_v;
		  *ptr = _mm_add_epi32(*ptr,mul4x32bit(fact_v,*((__m128i*)dx_neighbor + i) ) );

		  ptr = (__m128i*)nbs.y + i;
		  *ptr = y_v;
		  *ptr = _mm_add_epi32(*ptr,mul4x32bit(fact_v,*((__m128i*)dy_neighbor + i) ) );

		  ptr = (__m128i*)nbs.z + i;
		  *ptr = z_v;
		  *ptr = _mm_add_epi32(*ptr,mul4x32bit(fact_v,*((__m128i*)dz_neighbor + i) ) );
    }
    
    convert_v(nbs,nbs_p);
    
    return factor;
  }


//------------------ Original - Packed coordinates conversion ----------------------------//

  static inline void convert(int x, int y, int z, int &x1, int &y1, int &z1)
  {
  	// binary conversion seems to be faster
		convert_bin(x,y,z,x1,y1,z1);
  }

  static inline void convert_bin(int x, int y, int z, int &x1, int &y1, int &z1)
  {
    x1 = x;
    y1 = ( (y >> 2) << 1) + ((y & 0x3) >> 1);
    z1 = z >> 2;
  }

  static inline void convert_v(neighbors_t &nbs, neighbors_t &nbs_p)
  {
		for(int i=0;i<dir_number/4;i++){
  		*( (__m128i*)nbs_p.x + i) = *( (__m128i*)nbs.x + i);
			*( (__m128i*)nbs_p.y + i) = _mm_srai_epi32(*( (__m128i*)nbs.y + i),1);
			*( (__m128i*)nbs_p.z + i) = _mm_srai_epi32(*( (__m128i*)nbs.z + i),2);
		}
  }


  static inline void convert_arithm(int x, int y, int z, int &x1, int &y1, int &z1)
  {
    x1 = x;

    if( unlikely(y < 0) )
      y--;
    y1 = (y/4)*2 + (y%4)/2;
    
    if( unlikely(z < 0) ) 
      z -= 3;
    z1 = z/4;
  }


//------------------ Packed - Original coordinates conversion ----------------------------//


  static inline void r_convert(int x1, int y1, int z1, int &x, int &y, int &z)
  {
    // coordinates of x, y, z inside elementary cube
    int ix, iy, iz;
    // shifts of coordinates to put elementary cube to (0,0,0)
    int sx=x1, sy = y1;
   
    // if cube has negative coordinates it has to be shifted one 
    // cube more sinse -3/4 = 0 and 3/4 = 0
    // cube with negative X=-3 has number -1
    // cube with positive X=3 has number 0
    if( sx < 0 )
      sx -= 3;
    sx = (sx/4)*4;
    
    if( sy < 0 )
      sy -= 1;
    sy = (sy/2)*2;
    
    // Count incube x coordinate
    ix = x1 - sx;
    x = ix;
    // Count incube y coordinate
    // y is reduced by 2 (y/2) for packing purposes so we loose 
    // y%2 component. But knowing that x and y have equal parity
    // we can use x%2 to find out y%2
    //  ecube     y   y/2  x%2  [(y/2)*2 + x%2]
    // (0,0,0)    0    0    0          0
    // (0,2,2)    2    1    0          2
    // (1,1,1)    1    0    1          1
    // (1,3,3)    3    1    1          3
    // (2,0,2)    0    0    0          0
    // (2,2,0)    2    1    0          2
    // (3,1,3)    1    0    1          1
    // (3,3,1)    3    1    1          3
    iy = y1 - sy;
    y = 2*(iy%2) + ix%2;
    
    // Diamond lattice elementary cube has strong
    // dep's between (x,y) coordinates and z coord
    // on odd-z level coordinates are also odd
    // on even level - they are even
    // Accordint to the table below for z=0,1 (x-y) is zero
    // for z=2,3 (x-y) = +-2 (for module do (x-y)(x-y)/2
    // next to consider [+1] for z=1,3 we can use parity of one
    // if coordinates, for example x%2
    //  ecube     z   x-y  [ (x-y)(x-y)/2 + x%2 ]
    // (0,0,0)    0    0            0
    // (0,2,2)    2   -2            2
    // (1,1,1)    1    0            1
    // (1,3,3)    3   -2            3
    // (2,0,2)    2    2            2
    // (2,2,0)    0    0            0
    // (3,1,3)    3    2            3
    // (3,3,1)    1    0            1
    
    z = (y-x)*(y-x)/2 + x%2;
    
    // shift all coordinates to appropriate positions
    x += sx;
    y += sy*2;
    z += z1*4;
  }
};

#endif