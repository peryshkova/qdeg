/* 
 * File:   xyiterator.h
 * Author: artpol
 *
 * Created on 16 Июнь 2013 г., 12:54
 */

#ifndef ITERATOR_YX_H
#define	ITERATOR_YX_H

#include <iterator_zyx.h>

class yxiterator : public zyxiterator {
public:

    yxiterator()
    {
    }

    yxiterator(const zyxiterator &op) : zyxiterator(op)
    {
    }

    void __init(lPoint _max)
    {
        // fix z coordinate
        max[2] = _max.z() + 1;
        x[2] = _max.z();
        offs_s[2] = x[2];
        // setup the rest coordinatesd
        x[1] = _start_1();
        x[0] = _start_0();
    }

    yxiterator(lPoint _max, lPoint ghost) :
    zyxiterator(_max, ghost)
    {
        __init(_max);
    }

    yxiterator(coord_t _max[3], coord_t ghost = 0) :
    zyxiterator(_max, ghost)
    {
        lPoint max(_max);
        __init(max);
    }

    void init(lPoint _max, lPoint ghost = lPoint(0,0,0))
    {
        ((zyxiterator*)this)->init(_max, ghost);
        __init(_max);
    }

    void init(coord_t _max[3], coord_t ghost = 0)
    {
        ((zyxiterator*)this)->init(_max, ghost);
        __init(_max);
    }

    inline void offsets(lPoint p)
    {
        coord_t *_off = p.get_v();
        for(int i = 0; i < 2; i++ ) {
            offs_s[map[i]] = offs_e[map[i]] = _off[map[i]];
        }
        _reset_coords();
    }

    void soffsets(lPoint p)
    {
        coord_t *_off = p.get_v();
        for(int i = 0; i < 2; i++ ) {
            offs_s[map[i]] = _off[map[i]];
        }
        _reset_coords();
    }

    inline void eoffsets(lPoint p)
    {
        coord_t *_off = p.get_v();
        for(int i = 0; i < 2; i++ ) {
            offs_e[map[i]] = _off[map[i]];
        }
        _reset_coords();
    }

    yxiterator &operator =(const zyxiterator &op)
    {
        zyxiterator *t = (zyxiterator*)this;
        *t = op;
        return *this;
    }

    inline bool operator==(yxiterator r)
    {
        zyxiterator *lhs = this, *rhs = &r;
        return(*lhs == *rhs);
    }

    inline bool operator<(yxiterator r)
    {
        zyxiterator *lhs = this, *rhs = &r;
        return(*lhs < *rhs);
    }

    inline bool operator>(yxiterator r)
    {
        zyxiterator *lhs = this, *rhs = &r;
        return(*lhs > *rhs);
    }

    inline bool operator>=(yxiterator r)
    {
        zyxiterator *lhs = this, *rhs = &r;
        return(*lhs >= *rhs);
    }

    inline yxiterator end()
    {
        yxiterator tmp = (yxiterator) ((zyxiterator*)this)->end();
        return tmp;
    }

    inline yxiterator begin()
    {
        yxiterator tmp = ((zyxiterator*)this)->begin();
        return tmp;
    }
};

#endif	/* YXITERATOR_H */

