#include <sstream>
#include <sPlan.h>
#include <list>
#include <stddef.h>

#include <pLattice.h>
#include <dlNeighbors.h>
#include <iterator_yx.h>

// ----------- Use one sPlan instance in the whole program ----------------//
sPlan *sPlanInstance = NULL;

void sPlan::init(MPI_Comm c, branch_role r)
{
    sPlanInstance = new sPlan(c, r);
}

sPlan &sPlan::instance()
{
    if(sPlanInstance == NULL) {
        PQDEG_ABORT("sPlanInstance wasn't initialized!");
    }
    return *sPlanInstance;
}
// ----------- Use one sPlan instance in the whole program ----------------//

void sPlan::precord2mpi()
{
    MPI_Datatype t;
    char type_name[MPI_MAX_OBJECT_NAME] = "pRecord";
    int blens[] = {1, 1, 1, 6, 1, 1, 1, 1};
    MPI_Aint displ[] = {offsetof(pRecord, type), offsetof(pRecord, status), offsetof(pRecord, synced),
        offsetof(pRecord, p1), offsetof(pRecord, p1_t), offsetof(pRecord, prio), offsetof(pRecord, rank),
        offsetof(pRecord, rdouble)};
    MPI_Datatype types[] = {MPI_CHAR, MPI_CHAR, MPI_CHAR, MPI_INT, MPI_CHAR, MPI_UNSIGNED_LONG,
        MPI_INT, MPI_DOUBLE};

    // check that all arrays was initialized with equal number of parameters
    int size = sizeof (blens) / sizeof (blens[0]);
    int size2 = sizeof (displ) / sizeof (displ[0]);
    int size3 = sizeof (types) / sizeof (types[0]);
    if(unlikely(size != size2 || size != size3)) {
        PQDEG_ABORT("Number of members of blens, displ & types are different!");
    }

    MPI_Type_create_struct(size, blens, displ, types, &pRecord_mpi);
    MPI_Type_commit(&pRecord_mpi);

    // Double check that everything created right!
    int rsize = sizeof (pRecord);
    MPI_Aint extent, lb;
    MPI_Type_get_extent(pRecord_mpi, &lb, &extent);
    if(unlikely(rsize != extent)) {
        EPRINTF("WARNING: pRecord_mpi.extent=%u, sizeof(pRecord)=%u",
                (unsigned int)extent, (unsigned int)rsize);
    }
}

/*
 * Check that the distance between any two points of plan is more than 4
 * (not 2-nd oreder neighbors)
 */
bool sPlan::check_plan()
{
    decomp &d = decomp::instance();
    std::list<lPoint> pl, nl, dupd;
    pl.clear();
    nl.clear();
    for(int i = 0; i < pSize; i++) {
        if(plan[i].status == sPlan::Removed || plan[i].status == sPlan::Disabled)
            continue;
        lPoint p[2];
        int pcnt = 1;
        p[0].set_v(plan[i].p1);
        if(plan[i].type == Jump) {
            p[1].set_v(plan[i].p2);
            pcnt++;
        }
        std::list<lPoint> tmp;
        for(int j = 0; j < pcnt; j++) {
            neighbors_t nbs;
            lPoint bounds = d.gdims();
            neighbors_vect(p[j], bounds, nbs, true);
            for(int dir = 0; dir < dir_number; dir++) {
                bool include = true;
                lPoint np(nbs.x[dir], nbs.y[dir], nbs.z[dir]);
                for(int k = 0; k < pcnt; k++) {
                    if(p[k] == np) {
                        include = false;
                    }
                }
                if(include)
                    tmp.push_back(np);
            }
            pl.push_back(p[j]);

            std::stringstream ss;
            std::list<lPoint>::iterator pit = tmp.begin();
            ss << "Fill lists for " << p[j] << ": ";
            for(; pit != tmp.end(); pit++) {
                ss << (*pit) << ", ";
            }
            DPRINTF(DBG_DISAB, "%s", ss.str().c_str());
        }
        nl.merge(tmp);
    }

    std::list<lPoint>::iterator pit, nit;

    std::stringstream s;
    s.str("");
    s << std::endl << "Points list:" << std::endl;
    for(pit = pl.begin(); pit != pl.end(); pit++) {
        s << *pit << "; ";
    }
    s << std::endl << "Neighbors list:" << std::endl;
    for(nit = nl.begin(); nit != nl.end(); nit++) {
        s << *nit << "; ";
    }
    s << std::endl;
    DPRINTF(DBG_DISAB, "%s", s.str().c_str());

    for(pit = pl.begin(); pit != pl.end(); pit++) {
        nit = pit;
        int cnt = 1;
        for(nit++; nit != pl.end(); nit++) {
            if(*pit == *nit) {
                cnt++;
            }
        }
        if(cnt > 1) {
            nit = dupd.begin();
            bool already = false;
            for(; nit != dupd.end(); nit++) {
                if(*pit == *nit)
                    already = true;
            }
            if(!already) {
                dupd.push_back(*pit);
                std::stringstream s;
                s.str("");
                s << "Point " << *pit << " is duplicated in plan " << cnt <<
                        " times!";
                DPRINTF(DBG_DISAB, "%s", s.str().c_str());
            }
        }

    }

    for(pit = pl.begin(); pit != pl.end(); pit++) {
        for(nit = nl.begin(); nit != nl.end(); nit++) {
            if(*pit == *nit) {
                std::stringstream s;
                s.str("");
                s << "Point " << *pit << " from plan is presented in " <<
                        "full neighbor list!";
                DPRINTF(DBG_DISAB, "%s", s.str().c_str());
            }
        }
    }
}

std::string sPlan::precord2string(pRecord &p, pLattice<atom_info> *l)
{
    decomp &dcmp = decomp::instance();
    std::stringstream st;
    st.str("");
    st << "{ ";
    if(p.type == None)
        st << "N";
    else if(p.type == Depo)
        st << "D";
    else
        st << "J";
    st << "; ";

    switch(p.status) {
    case sPlan::Disabled:
        st << "Dis; ";
        break;
    case sPlan::Active:
        st << "Act; ";
        break;
    case sPlan::Inactive:
        st << "Inact; ";
        break;
    case sPlan::Removed:
        st << "Rem; ";
        break;
    }

    switch(p.synced) {
    case sPlan::NotSynced:
        st << "nsync; ";
        break;
    case sPlan::Synced:
        st << "sync; ";
        break;
    }

    if( role != Master ){
        dPoint gp(lPoint(p.p1));
        st << gp.glob();
        if(l && (p.status == Active)) {
            pLattice<atom_info> &lat = *l;
            st << "[" << type2str(lat(gp).type) << "]";
        }
        st << "; ";

        if(p.type == Jump) {
            gp.glob(lPoint(p.p2));
            st << gp.glob();
            if(l && (p.status == Active)) {
                pLattice<atom_info> &lat = *l;
                st << "[" << type2str(lat(gp).type) << "]";
            }
            if( p.rank >= 0 ){
                st << "; r=" << p.rank;
            }
            st << "; rdbl=" << p.rdouble << "; ";
        }
    }
    st << "rank=" << p.rank;
    st << " }";
    return st.str();
}

void sPlan::dbg_print(std::stringstream &ss, pLattice<atom_info> *l)
{
    static decomp &dcmp = decomp::instance();

    ss << "Plan output:" << std::endl;
    for(int i = 0; i < pSize; i++) {
        pRecord &p = plan[i];
        ss << "\tplan[" << i << "]: " << precord2string(p, l);
        if(role != Master) {
            if(p.type == Depo) {
                dPoint gp(lPoint(p.p1));
                if( gp.is_local() ){
                    ss << " mine.";
                } else if(gp.is_related()){
                    ss << " related.";
                }
            } else if(p.type == Jump) {
                if(p.rank >= 0) {
                    int rank;
                    MPI_Comm_rank(bComm,&rank);
                    ss << ". mine = " << ( p.rank == rank );
                } else {
                    dPoint from(lPoint(p.p1)), to(lPoint(p.p2));
                    ss << ". from: ";
                    if( from.is_local()){
                        ss << "mine;";
                    }else if( from.is_related() ){
                        ss << "related;";
                    }
                    ss << " to: ";
                    if( to.is_local() ){
                        ss << "mine.";
                    }else if( to.is_related() ){
                        ss << "related.";
                    }
                }
            }
        }
        ss << std::endl;
    }
}


void sPlan::generate_draft()
{
    if(!is_root()) {
        PQDEG_ABORT("Only Root should call this function!");
    }

    static qConfig &cfg = qConfig::instance();
    static rGen &gen = rGen::instance();


//    // Test #1. Sequential filling
//    pSize = 1;
//    printf("WARNING: generate draft is in debug state! Uniform deposition is performed!\n");
//    DPRINTF(DBG_TRACE, "WARNING: generate draft is in debug state! Uniform deposition is performed!");
//    lPoint max(cfg.Gx(),cfg.Gy(),32);
//    zyxiterator tmp = zyxiterator(max);
//    tmp.soffsets(lPoint(0,0,21));
//    static zyxiterator it = tmp;
//    plan[0].type = sPlan::Depo;
//    plan[0].p1[0] = (*it).x();
//    plan[0].p1[1] = (*it).y();
//    it++;

//    // Test #2. Parallel filling
//    static int init = 0;
//    pSize = cfg.Nx()*cfg.Ny();
//    static zyxiterator its[100][100];

//    if( !init ){
//        for(int y = 0; y<cfg.Ny();y++){
//            for(int x = 0; x < cfg.Ny(); x++){
//                lPoint max(cfg.Gx(),cfg.Gy(),cfg.Gz());
//                its[y][x] = zyxiterator(max);
//                int xstep = cfg.Gx()/cfg.Nx();
//                int xsoffs = xstep*x;
//                int xeoffs  =cfg.Gx() - (xsoffs + xstep);
//                int ystep = cfg.Gy()/cfg.Ny();
//                int ysoffs = ystep*y;
//                int yeoffs  =cfg.Gy() - (ysoffs + ystep);

//                lPoint soffs(xsoffs,ysoffs,21);
//                lPoint eoffs(xeoffs,yeoffs,0);
//                its[y][x].soffsets(soffs);
//                its[y][x].eoffsets(eoffs);
//                its[y][x] = its[y][x].begin();
//            }
//        }
//        init = 1;
//    }
//    printf("WARNING: generate draft is in debug state! Uniform deposition is performed!\n");
//    DPRINTF(DBG_TRACE, "WARNING: generate draft is in debug state! Uniform deposition is performed!");
//    for(int y=0;y<cfg.Ny();y++){
//        for(int x = 0; x < cfg.Nx(); x++){
//            int idx = y*cfg.Nx() + x;
//            plan[idx].type = sPlan::Depo;
//            lPoint lp = *(its[y][x]);
//            plan[idx].p1[0] = lp.x();
//            plan[idx].p1[1] = lp.y();
//            (its[y][x])++;
//        }
//    }



//    // Test #3. Random filling by 1 atom per step
//    static int init = 0;
//    pSize = 1; // gen.rand01() * (mSize - 1) + 1;;
//    printf("WARNING: generate draft is in debug state! Uniform deposition is performed!\n");
//    DPRINTF(DBG_TRACE, "WARNING: generate draft is in debug state! Uniform deposition is performed!");
//    plan[0].type = sPlan::Depo;
//    coord_t x = gen.rand01() * cfg.Gx();
//    coord_t y = gen.rand01() * cfg.Gy();
//    // Correct x so it would be correct dimond lattice coordinates
//    coord_t x1 = ((x % 4) / 2)*2 + y % 2;
//    x = (x / 4) * 4 + x1;
//    plan[0].p1[0] = x;
//    plan[0].p1[1] = y;

//    // Test #4. Random filling by psize atoms per step
//    static int init = 0;
//    pSize = gen.rand01() * (mSize - 1) + 1;
//    printf("WARNING: generate draft is in debug state! Uniform deposition is performed!\n");
//    DPRINTF(DBG_TRACE, "WARNING: generate draft is in debug state! Uniform deposition is performed!");
//    for(int i = 0; i < pSize; i++) {
//        // Choose jump or deposition
//        memset(plan + i, 0, sizeof (plan[i]));
//        plan[i].type = sPlan::Depo;
//        coord_t x = gen.rand01() * cfg.Gx();
//        coord_t y = gen.rand01() * cfg.Gy();
//        // Correct x so it would be correct dimond lattice coordinates
//        coord_t x1 = ((x % 4) / 2)*2 + y % 2;
//        x = (x / 4) * 4 + x1;
//        plan[i].p1[0] = x;
//        plan[i].p1[1] = y;
//        plan[i].rdouble = gen.rand01();
//        plan[i].prio = gen.random_();
//        plan[i].status = sPlan::Disabled;
//    }


//    // Test #5. Parallel filling
//    static int init = 0;
//    pSize = cfg.Nx()*cfg.Ny();
//    static zyxiterator its[100][100];
//    static yxiterator yxits[100][100];
//    static int curzs[100][100];

//    if( !init ){
//        for(int y = 0; y<cfg.Ny();y++){
//            for(int x = 0; x < cfg.Ny(); x++){
//                lPoint max(cfg.Gx(),cfg.Gy(),cfg.Gz());
//                its[y][x] = zyxiterator(max);
//                int xstep = cfg.Gx()/cfg.Nx();
//                int xsoffs = xstep*x;
//                int xeoffs  =cfg.Gx() - (xsoffs + xstep);
//                int ystep = cfg.Gy()/cfg.Ny();
//                int ysoffs = ystep*y;
//                int yeoffs  = cfg.Gy() - (ysoffs + ystep);

//                lPoint soffs(xsoffs,ysoffs,21);
//                lPoint eoffs(xeoffs,yeoffs,0);
//                its[y][x].soffsets(soffs);
//                its[y][x].eoffsets(eoffs);
//                its[y][x] = its[y][x].begin();
//                curzs[y][x] = -1;
//            }
//        }
//        init = 1;
//    }
//    printf("WARNING: generate draft is in debug state! Uniform deposition is performed!\n");
//    DPRINTF(DBG_TRACE, "WARNING: generate draft is in debug state! Uniform deposition is performed!");
//    for(int y=0;y<cfg.Ny();y++){
//        for(int x = 0; x < cfg.Nx(); x++){
//            if( x % 2 != 0){
//                //                int i = 1;
//                //                while(i){
//                //                    sleep(1);
//                //                }

//                if( (*its[y][x]).z() > curzs[y][x] ){
//                    curzs[y][x] = (*its[y][x]).z();
//                    lPoint max(cfg.Gx(),cfg.Gy(),curzs[y][x]);
//                    yxits[y][x].init(max);
//                    int xstep = cfg.Gx()/cfg.Nx();
//                    int xsoffs = xstep*x;
//                    int xeoffs  =cfg.Gx() - (xsoffs + xstep);
//                    int ystep = cfg.Gy()/cfg.Ny();
//                    int ysoffs = ystep*y;
//                    int yeoffs  = cfg.Gy() - (ysoffs + ystep);

//                    lPoint soffs(xsoffs,ysoffs,21);
//                    lPoint eoffs(xeoffs,yeoffs,0);
//                    yxits[y][x].soffsets(soffs);
//                    yxits[y][x].eoffsets(eoffs);
//                    yxits[y][x] = yxits[y][x].end();
//                }
//            }
//            int idx = y*cfg.Nx() + x;
//            plan[idx].type = sPlan::Depo;
//            lPoint lp = *(its[y][x]);
//            if( x % 2 != 0 ){
//                lp = *(yxits[y][x]);
//            }
//            plan[idx].p1[0] = lp.x();
//            plan[idx].p1[1] = lp.y();
//            (its[y][x])++;
//            if( x % 2 != 0 ){
//                (yxits[y][x])--;
//            }
//        }
//    }


/*
 *Archive: first version of plan draft generation. Disadvantages: most of the work was given to
 *the worker with highest jump probability.

    pSize = gen.rand01() * (mSize - 1) + 1;
    for(int i = 0; i < pSize; i++) {
        // Choose jump or deposition
        memset(plan + i, 0, sizeof (plan[i]));
        double Px = prob_total * gen.rand01();

        if(Px < cfg.depProb()) { // deposition
            plan[i].type = sPlan::Depo;
            coord_t x = gen.rand01() * cfg.Gx();
            coord_t y = gen.rand01() * cfg.Gy();
            // Correct x so it would be correct dimond lattice coordinates
            coord_t x1 = ((x % 4) / 2)*2 + y % 2;
            x = (x / 4) * 4 + x1;
            plan[i].p1[0] = x;
            plan[i].p1[1] = y;
        } else { // jump
            plan[i].type = sPlan::Jump;
            Px -= cfg.depProb();
            int size;
            MPI_Comm_size(bComm,&size);
            double cum = 0;
            int r;
            for(r = 1; r < size && cum < Px; r++) {
                cum += prob[r];
                plan[i].rank = r;
            }
            prob[r-1] -=
            DPRINTF(DBG_TRACE, "it's Jump: Px= %lf, cum = %lf, rank = %d",
                    Px, cum, plan[i].rank);
        }
        plan[i].rdouble = gen.rand01();
        plan[i].prio = gen.random_();
        plan[i].status = sPlan::Disabled;
    }

*/

    //pSize = gen.rand01() * (mSize - 1) + 1;

//        int flag = 1;
//        while(flag){
//            sleep(1);
//        }
    pSize = mSize;
    unsigned pIdx = 0;
    int size;
    MPI_Comm_size(bComm,&size);
    while( pIdx < pSize ){
        for(int rank = 1; rank < size && pIdx < pSize; rank++) {
            // Choose jump or deposition
            memset(plan + pIdx, 0, sizeof (plan[pIdx]));

            double depProb = cfg.depProb()/cfg.Nx()/cfg.Ny();
            double fullProb = prob[rank] + depProb;
            
            double Px = fullProb * gen.rand01();

            if( Px < depProb ) { // deposition
                plan[pIdx].type = sPlan::Depo;
            } else { // jump
                plan[pIdx].type = sPlan::Jump;
            }
            plan[pIdx].rdouble = gen.rand01();
            plan[pIdx].status = sPlan::Disabled;
            plan[pIdx].rank = rank;
            pIdx++;
        }
    }


    std::stringstream ss;
    dbg_print(ss);
    DPRINTF(DBG_TRACE, "%s", ss.str().c_str());
}

static bool pRecord_cmp(sPlan::pRecord *left, sPlan::pRecord * right)
{
    return (left->prio > right->prio);
}

void sPlan::clear_one_point(lPoint &p, std::list<pRecord*>::iterator &it,
                            std::list<pRecord*> &l)
{
    static decomp &d = decomp::instance();
    neighbors_t nbs;

    lPoint bounds = d.gdims();
    neighbors_vect(p, bounds, nbs,true);

    std::list<pRecord*>::iterator it1 = it;
    it1++;
    for(; it1 != l.end(); it1++) {
        if((*it1)->type != Depo && (*it1)->type != Jump) {
            EPRINTF("WARNING: Plan has records of incorrect type! Skip.");
            (*it1)->status = sPlan::Removed;
            continue;
        }

        if((*it1)->status == sPlan::Disabled) {
            // Skip, since this record wasn't updated
            continue;
        }

        int lpp_cnt = 0;
        lPoint lpp[2]; // less priority points
        lpp_cnt++;
        lpp[0].set_v((*it1)->p1);
        if((*it1)->type == Jump) {
            lpp_cnt++;
            lpp[1].set_v((*it1)->p2);
        }

        bool next_iter = true;
        for(int i = 0; i < lpp_cnt && next_iter; i++) {
            if(lpp[i] == p) {
                (*it1)->status = sPlan::Removed;
                next_iter = false;

                {
                    std::stringstream s;
                    s.str("");
                    s << "it1's point #" << i << "=" << lpp[i] <<
                            " conflicts with it point" << p <<
                            ". Disable it1 record.";
                    DPRINTF(DBG_TRACE, "\t\t%s", s.str().c_str());
                }


                continue;
            }
            for(int dir = 0; (dir < dir_number) && next_iter; dir++) {
                lPoint np(nbs.x[dir], nbs.y[dir], nbs.z[dir]);

                if( p == lPoint(43,63,21) || p == lPoint(45,1,21) || p == lPoint(40,62,22) || p == lPoint(42,64,22) ){
                    dPoint gp(p), gp1(np);
                    std::stringstream s;
                    s.str("");
                    s << "clear_one_point: Check base point " << gp << " against neighbor "<< gp1;
                    DPRINTF(DBG_TRACE, "\t\t%s", s.str().c_str());
                }
                if(lpp[i] == np) {
                    (*it1)->status = sPlan::Removed;
                    next_iter = false;

                    {
                        std::stringstream s;
                        s.str("");
                        s << "it1's point #" << i << "=" << lpp[i] <<
                                " conflicts with it neighbor point #" << dir <<
                                "=" << np <<
                                ". Disable it1 record.";
                        DPRINTF(DBG_DISAB, "\t\t%s", s.str().c_str());
                    }

                    break;
                }
            }
        }
    }
}

void sPlan::clear_conflicts()
{
    if(unlikely(st != Ready)) {
        PQDEG_ABORT("Cannot clean not Ready plan!");
    }

    std::list<pRecord*> l;
    for(int i = 0; i < pSize; i++) {
        l.push_back(plan + i);
    }
    l.sort(pRecord_cmp);

//    DPRINTF(1, "\nPre-final version of plan gathered (sorted):");
//    std::list<pRecord*>::iterator it = l.begin();
//    decomp &d = decomp::instance();
//    for(int i = 0; it != l.end(); it++, i++) {
//        lPoint p((*it)->p1);
//        DPRINTF(1, "plan[%d]: %s, mine = %d, related = %d",
//                i, precord2string(*(*it)).c_str(),
//                d.is_local(p),
//                d.is_neighbor(p));
//    }
//    DPRINTF(1, "\n");
    DPRINTF(1, "Clear plan from duplications:");
    std::list<pRecord*>::iterator it = l.begin();
    for(int i = 0; it != l.end(); it++, i++) {
        if((*it)->type != Depo && (*it)->type != Jump) {
            std::string str = precord2string(*(*it));
            EPRINTF("WARNING: Plan has record (%s) of incorrect type! Skip.",
                    str.c_str());
            (*it)->status = sPlan::Removed;
        }
        if((*it)->status == sPlan::Disabled) {
            // Skip, since this record wasn't updated
            continue;
        }
        lPoint p((*it)->p1);
        clear_one_point(p, it, l);
        if((*it)->type == Jump) {
            p.set_v((*it)->p2);
            clear_one_point(p, it, l);
        }
    }
}

sPlan::sPlan(MPI_Comm c, branch_role r)
{
    qConfig &cfg = qConfig::instance();
    bComm = c;
    mSize = cfg.stepSize();
    DPRINTF(1, "msize=%d", mSize);
    st = Zero;
    Gx = cfg.Gx();
    Gy = cfg.Gy();
    Gz = cfg.Gz();

    pRecord tmp = {None};
    empty = tmp;
    role = r;
    if(r == Master) {
        plan = new pRecord[mSize];
        int size;
        MPI_Comm_size(bComm,&size);
        prob = new double[size];
    } else {
        plan = new pRecord[mSize];
        prob = new double[1];
    }
    precord2mpi();
}

double sPlan::prepare_draft()
{
    if(!is_root()) {
        PQDEG_ABORT("Only Root should call this function!");
    }
    st = Zero;

    // Receive cumulative jump probabilitys from worker nodes
    prob[0] = 0;
    MPI_Status status;
    MPI_Gather(prob, 1, MPI_DOUBLE, prob, 1, MPI_DOUBLE, 0, bComm);
    MPI_Reduce(prob, &prob_jump, 1, MPI_DOUBLE, MPI_SUM, 0, bComm);

#ifdef DEBUG_ON
    {
        int size;
        MPI_Comm_rank(bComm, &size);
        DPRINTF(DBG_TRACE, "Branch probabilities:");
        for(int i = 1; i < size; i++)
            DPRINTF(DBG_TRACE, "\trank[%d] prob is %lf", i, prob[i]);
    }
#endif

    generate_draft();

    MPI_Bcast(&pSize, 1, MPI_UNSIGNED, 0, bComm);
    MPI_Bcast(plan, pSize, pRecord_mpi, 0, bComm);
    st = Draft;
    return prob_jump;
}

void sPlan::prepare_draft(double p)
{
    if(is_root()) {
        PQDEG_ABORT("Only Workers should call this function!");
    }
    int rank;
    MPI_Comm_rank(bComm, &rank);
    static decomp &dcmp = decomp::instance();
    st = Zero;
    *prob = p;
    // Send jump probability sum
    MPI_Gather(prob, 1, MPI_DOUBLE, prob, 1, MPI_DOUBLE, 0, bComm);
    MPI_Reduce(prob, &prob_jump, 1, MPI_DOUBLE, MPI_SUM, 0, bComm);
    // Receive step plan
    MPI_Bcast(&pSize, 1, MPI_UNSIGNED, 0, bComm);
    MPI_Bcast(plan, pSize, pRecord_mpi, 0, bComm);

    st = Draft;
    for(int i = 0; i < pSize; i++) {
        if((plan[i].type == Depo) && plan[i].rank == rank) {
            // TODO: process each local line of plan
            plan[i].prio = rGen::instance().generate();
            plan[i].status = sPlan::Active;
        } else if((plan[i].type == Jump) && plan[i].rank == rank) {
            plan[i].prio = rGen::instance().generate();
            plan[i].status = sPlan::Active;
        }
    }

#ifdef DEBUG_ON
    /* TODO: DEBUG - comment */
    DPRINTF(DBG_TRACE, "Plan draft gathered:");
    std::stringstream ss;
    dbg_print(ss);
    DPRINTF(DBG_TRACE, "%s",ss.str().c_str());
    /* TODO: DEBUG - comment */
#endif
}

void sPlan::exch_with_neigh(pRecord *n_plans_raw, int ranks[8])
{
    int nranks[3][3]; // left and right neighbors ranks by x,y axes
    PTR_to_2DARRAY(pRecord, pSize, n_plans, n_plans_raw);
    decomp &dcmp = decomp::instance();
    MPI_Comm c = dcmp.comm();
    MPI_Status status;

    dcmp.nbr_rankmap(nranks);

    // exchange x borders
    if(dcmp[0] > 1) {
        DPRINTF(DBG_DISAB, "%d <-> %d <-> %d", nranks[0][1], nranks[1][1], nranks[2][1]);

        MPI_Sendrecv(plan, pSize, pRecord_mpi, nranks[0][1], 1,
                n_plans[0], pSize, pRecord_mpi, nranks[2][1], 1,
                c, &status);
        ranks[0] = nranks[2][1];

        MPI_Sendrecv(plan, pSize, pRecord_mpi, nranks[2][1], 1,
                n_plans[1], pSize, pRecord_mpi, nranks[0][1], 1,
                c, &status);
        ranks[1] = nranks[0][1];
    }
    // exchange y borders
    // TODO: check if possible!! m.pdims()[0] = 1;

    if(dcmp[1] > 1) {
        DPRINTF(DBG_DISAB, "%d <-> %d <-> %d", nranks[1][0], nranks[1][1], nranks[1][2]);

        MPI_Sendrecv(plan, pSize, pRecord_mpi, nranks[1][0], 1,
                n_plans[2], pSize, pRecord_mpi, nranks[1][2], 1,
                c, &status);
        ranks[2] = nranks[1][2];
        MPI_Sendrecv(plan, pSize, pRecord_mpi, nranks[1][2], 1,
                n_plans[3], pSize, pRecord_mpi, nranks[1][0], 1,
                c, &status);
        ranks[3] = nranks[1][0];
    }

    if(dcmp[0] > 1 && dcmp[1] > 1) {

        DPRINTF(DBG_DISAB, "%d <-> %d <-> %d", nranks[0][0], nranks[1][1], nranks[2][2]);
        MPI_Sendrecv(plan, pSize, pRecord_mpi, nranks[0][0], 1,
                n_plans[4], pSize, pRecord_mpi, nranks[2][2], 1,
                c, &status);
        ranks[4] = nranks[2][2];
        MPI_Sendrecv(plan, pSize, pRecord_mpi, nranks[2][2], 1,
                n_plans[5], pSize, pRecord_mpi, nranks[0][0], 1,
                c, &status);
        ranks[5] = nranks[0][0];

        DPRINTF(DBG_DISAB, "%d <-> %d <-> %d", nranks[0][0], nranks[1][1], nranks[2][2]);
        MPI_Sendrecv(plan, pSize, pRecord_mpi, nranks[0][2], 1,
                n_plans[6], pSize, pRecord_mpi, nranks[2][0], 1,
                c, &status);
        ranks[6] = nranks[2][0];
        MPI_Sendrecv(plan, pSize, pRecord_mpi, nranks[2][0], 1,
                n_plans[7], pSize, pRecord_mpi, nranks[0][2], 1,
                c, &status);
        ranks[7] = nranks[0][2];
    }

}

void sPlan::prepare_final(pLattice<atom_info> *l)
{
    if(unlikely(is_root())) {
        PQDEG_ABORT("Root shouldn't participate in this step");
    }
    if(unlikely(st == Zero)) {
        PQDEG_ABORT("No plan draft was generated!");
    }
    if(unlikely(st == Ready)) {
        DPRINTF(1, "Final version of plan already prepared!");
        return;
    }

    decomp &dcmp = decomp::instance();

    pRecord plan_n[8][pSize];
    int ranks[8];
    exch_with_neigh((pRecord*)plan_n, ranks);

    for(int j = 0; j < 8; j++) {
        for(int i = 0; i < pSize; i++) {
            // There can be 3 posibilitys:
            // 1. it is Jump, p1 belongs to neighbor and p2 is local
            // 2. the point p1 belongs to us but this was desided on our 
            //    neighbor's side during deposition
            // 3. we don't care about this point
            dPoint p1(lPoint(plan_n[j][i].p1));

            int ret;

            if(plan_n[j][i].status != sPlan::Active || plan[i].synced == sPlan::Synced)
                continue;
            else {
                plan[i] = plan_n[j][i];
                plan[i].status = sPlan::Inactive;
                plan[i].synced = sPlan::Synced;
            }
            // Check No. 1
            if(plan_n[j][i].type == sPlan::Jump) {
                dPoint p2(lPoint(plan_n[j][i].p2));
                if( p1.is_related() || p2.is_related() ) {
                    // TODO: process each local line of plan
                    plan[i].status = sPlan::Active;
                    DPRINTF(DBG_DISAB, "sync neighbor %d plan[%d]: %s",
                            j, i, precord2string(plan[i]).c_str());
                }
            } else if(plan_n[j][i].type == sPlan::Depo) {
                if( p1.is_related() ) {
                    plan[i].status = sPlan::Active;
                    DPRINTF(DBG_DISAB, "sync neighbor %d plan[%d]: %s",
                            j, i, precord2string(plan[i]).c_str());
                }
            }
        }
    }

    // DEBUG:
    //poison_plan();

    st = Ready;
    DPRINTF(DBG_DISAB, "Pre-final version of plan gathered:");
    //dbg_print(l);

    DPRINTF(DBG_DISAB, "Check plan _before_ clear");
    check_plan();

    clear_conflicts();

    DPRINTF(DBG_DISAB, "Check plan _after_ clear");
    check_plan();

    DPRINTF(DBG_DISAB, "\n");
    DPRINTF(1, "Final version of plan gathered:");
    std::stringstream ss;
    dbg_print(ss, l);
    DPRINTF(DBG_TRACE, "%s", ss.str().c_str());

}

void sPlan::poison_plan()
{
    // Debug function. Adds 2 new entries that duplicate others    
    neighbors_t nbs;
    plan[pSize] = plan[1];
    lPoint tlp(plan[pSize].p1);
    lPoint bounds = decomp::instance().gdims();

    neighbors_vect(tlp, bounds, nbs, true);
    plan[pSize].p1[0] = nbs.x[10];
    plan[pSize].p1[1] = nbs.y[10];
    plan[pSize].p1[2] = nbs.z[10];
    plan[pSize].p2[0] += 16;
    plan[pSize].prio -= 100;
    pSize++;

    plan[pSize] = plan[0];
    tlp = lPoint(plan[pSize].p1);
    neighbors_vect(tlp, bounds, nbs, true);
    plan[pSize].p1[0] = nbs.x[10];
    plan[pSize].p1[1] = nbs.y[10];
    plan[pSize].p1[2] = nbs.z[10];
    plan[pSize].p2[0] += 16;
    plan[pSize].prio += 100;
    pSize++;
}
