#include <iostream>
#include <sstream>
#include <rGen.h>
#include <epitaxy.h>
#include <iterator_zyx.h>
#include <pLattice.h>
#include <qTypes.h>

void epitaxy_w::dyn_range(int &start, int &end, bool verbose)
{
    if( verbose){
	int count = 0;
        for(start = 0; start < zjump.size(); start++){
    	    if( zjump[start] > 0 ){
        	DPRINTF(DBG_TRACE,"start: zjump[%d] = %ld",start,zjump[start]);
        	count++;
    	    }
        }
        if( count == 0 ){
    	    DPRINTF(DBG_TRACE,"WARNING: No non-zero zjumps found!!");
        }
    }
    for(start = 0; start < zjump.size() && zjump[start] == 0; start++);
    for(end = zjump.size() - 1; end > start && zjump[end] == 0; end--);
    end++;
    DPRINTF(DBG_DISAB, "start = %d, end = %d", start, end);
}

void epitaxy_w::calc_jump_prob(lPoint p)
{
    qConfig &simcfg = qConfig::instance();
    nConfig &ncfg = nConfig::instance();

    float E_a;
    float old_jmprob = jmprob(p);

    if( nodparam(p).type == atomType::None ) {
        if( old_jmprob > 0 ){
            zjump[p.z()]--;
        }
        jmprob(p) = 0;
        return;
    }

    if(unlikely((nodparam(p).defect_f.x != 0) ||
                (nodparam(p).defect_f.y != 0) ||
                (nodparam(p).defect_f.z != 0))) {
        std::stringstream ss;
        ss << "Error!!! atom " << p << " near defect is on the surface!";
        PQDEG_ABORT("%s", ss.str().c_str());
    }

    {
        // TODO: remove, DEBUG.
        neighbors_t nbs;
        nodparam.neighbors(p, nbs);
        int dir = 0, n1cnt = 0, n2cnt = 0, njmps = 0;
        while( dir < 4 ){
            lPoint nb(nbs.x[dir], nbs.y[dir], nbs.z[dir]);
            if( (nodparam(nb).type != atomType::None) ){
                n1cnt++;
            }else{
                njmps++;
            }
            dir++;
        }

        while( dir < dir_number ){
            lPoint nb(nbs.x[dir], nbs.y[dir], nbs.z[dir]);
            if( (nodparam(nb).type != atomType::None) ){
                n2cnt++;
            }else{
                njmps++;
            }
            dir++;
        }
        if( ncfg.n1_config(nodparam(p).config) != n1cnt ||
                ncfg.n2_config(nodparam(p).config) != n2cnt ){
            std::stringstream ss;
            ss << "Misconfiguration of the node " << p
               << ": conf=" << std::hex << nodparam(p).config << std::dec << std::endl
               << "n1_config=" << (int)ncfg.n1_config(nodparam(p).config)
               << ", n2_config=" << (int)ncfg.n2_config(nodparam(p).config) << std::endl
               << "n1cnt=" << n1cnt << ", n2cnt=" << n2cnt << std::endl
               << "jnum=" << (int)nodejmp(p).jnum << ", njmps=" << njmps;

            PQDEG_ABORT("%s",ss.str().c_str());
        }
    }

    E_a = simcfg.E1() * ncfg.n1_config(nodparam(p).config)
            + simcfg.E2() * ncfg.n2_config(nodparam(p).config)
            - nodparam(p).Edef;
    if(E_a < 0)
        E_a = 0;
    jmprob(p) = simcfg.p0() * exp(-E_a / simcfg.T()) * nodejmp(p).jnum;
    if(jmprob(p) > 0 ) {
        if( !(old_jmprob > 0) ){
            zjump[p.z()]++;
        }
    }else{
        jmprob(p) = 0;
        if( old_jmprob > 0 ){
            zjump[p.z()]--;
        }
    }

#ifdef DEBUG_ON
    if(jmprob(p) > 0 ){
        std::stringstream ss;
        dPoint gp;
        gp.local(p);
        int jn = (int)nodejmp(p).jnum;
        ss << gp << ": E_a = " << E_a <<
              ", E1 = " << simcfg.E1() <<
              ", n1_conf = " << (int)ncfg.n1_config(nodparam(p).config) <<
              ", E2 = " << simcfg.E2() <<
              ", n2_conf = " << (int)ncfg.n2_config(nodparam(p).config) <<
              ", woEdef = " << simcfg.E1() * ncfg.n1_config(nodparam(p).config)
              + simcfg.E2() * ncfg.n2_config(nodparam(p).config) <<
              ", Edef = " << nodparam(p).Edef <<
              ", p0 = " << simcfg.p0() <<
              ", T = " << simcfg.T() <<
              ", nj = " << jn <<
              ", P = " << jmprob(p);
        DPRINTF(DBG_DISAB, "%s", ss.str().c_str());
    }
#endif
}

void epitaxy_w::set_jump_info(lPoint p, bool debug)
{
    decomp &d = decomp::instance();
    nConfig &aconf = nConfig::instance();

#ifdef DEBUG_ON
    dPoint gp;
    gp.local(p);
    if( !gp.is_correct() ) {
        std::stringstream ss;
        ss << "Bad point " << gp;
        PQDEG_ABORT("%s", ss.str().c_str());
    }
#endif

    if(nodparam(p).type == atomType::None || aconf.n1_config(nodparam(p).config) == 4
            || p.z() < 2 || p.z() >= d.ldim_z() - 2) {
        nodejmp(p).jnum = 0;
        nodejmp(p).jmap = 0;

#ifdef DEBUG_ON
        if(debug ){
            dPoint gp;
            gp.local(p);
            // TODO: Quite verbose debug, disable after check and REMOVE
            std::stringstream ss;
            ss << gp << ": jnum = " << (int)nodejmp(p).jnum <<
                    ", jmap = " << std::hex << nodejmp(p).jmap;
            DPRINTF(DBG_TRACE, "%s", ss.str().c_str());
        }
#endif        
        return;
    }
    int n_jumps = 0;
    int j_array[dir_number];
    for(int dir = 0; dir < dir_number; dir++)
        j_array[dir] = 0;

    neighbors_t nbs;
    nodparam.neighbors(p, nbs);
    for(int dir = 0; dir < dir_number; dir++) {
        lPoint nb(nbs.x[dir], nbs.y[dir], nbs.z[dir]);
        if((nodparam(nb).type == atomType::None) && (aconf.n1_config(nodparam(nb).config) > (dir < 4)) ) {
            j_array[dir] = 1;
            n_jumps++;
        }
    }

    nodejmp(p).jnum = n_jumps;
    nodejmp(p).jmap = aconf.array2config(j_array);

#ifdef DEBUG_ON
    if( debug ){
        std::stringstream ss;
        neighbors_t nbs;
        nodparam.neighbors(p, nbs);
        dPoint gp;
        gp.local(p);
        ss << "from=" << gp << "[" << type2str(nodparam(gp).type) << "], to ";
        for(int dir = 0; dir < dir_number; dir++) {
            dPoint gnb;
            gnb.local(lPoint(nbs.x[dir], nbs.y[dir], nbs.z[dir]));
            bool sign = (nodparam(gnb).type == atomType::None) && (aconf.n1_config(nodparam(gnb).config) > (dir < 4));
            ss << gnb << ( sign ? "[+]" : "[-]" ) << ", ";
        }

        ss <<  "| jnum=" << n_jumps << ", jmap=" << std::hex << nodejmp(p).jmap;
        DPRINTF(DBG_TRACE,"%s", ss.str().c_str());
    }
#endif
}

double epitaxy_w::sum_jump_prob_opt(bool verbose)
{
    // Skip empty layers
    double result = 0;
    int start, end;
    dyn_range(start, end,verbose);

    zyxiterator it = travInit(start, end);
    for(it = it.begin(); it <= it.end(); it++) {
        result += jmprob(*it);
    }

#ifdef DEBUG_ON
    if( verbose ){
        std::stringstream ss;
        lPoint ibegin = *(it.begin()), iend = *(it.end());
        ss << "Trace: begin=" << ibegin << " end=" << iend << std::endl;
        for(it = it.begin(); it <= it.end(); it++) {
            lPoint p = *it;
            dPoint gp;
            gp.local(p);
            ss << "jumps" << gp << ".jprob = " << jmprob(*it) << std::endl;
        }
        DPRINTF(DBG_TRACE, "%s", ss.str().c_str());
    }
#endif



    return result;
}

double epitaxy_w::sum_jump_prob_full(bool verbose)
{
    double result = 0;
    zyxiterator it = travInit();

    std::stringstream ss;
    for(; it <= it.end(); it++) {
        lPoint p = *it;
        result += jmprob(*it);
    }

#ifdef DEBUG_ON
    if( verbose ){
        {
            std::stringstream ss;
            ss << "Nodes with huge jump probability" << std::endl;
            for(; it <= it.end(); it++) {
                lPoint p = *it;
                if(jmprob(*it) > 10000) {
                    ss << "\tjumps(" << p << ").jprob = " << jmprob(*it) << std::endl;
                }
            }
            DPRINTF(DBG_TRACE, "%s", ss.str().c_str());
        }
        {
            std::stringstream ss;
            lPoint ibegin = *(it.begin()), iend = *(it.end());
            ss << "Trace: begin=" << ibegin << " end=" << iend << std::endl;
            for(it = it.begin(); it <= it.end(); it++) {
                lPoint p = *it;
                ss << "jumps(" << p << ").jprob = " << jmprob(*it) << std::endl;
            }
            DPRINTF(DBG_TRACE, "%s", ss.str().c_str());
        }
    }
#endif
    return result;
}

double epitaxy_w::sum_jump_prob()
{
    double opt = sum_jump_prob_opt();

#ifdef DEBUG_ON
    {
        static int inform_debug = 1;
        if(inform_debug == 1) {
            DPRINTF(DBG_TRACE, "sum_jump_prob_opt is checked against sum_jump_prob_full");
            inform_debug = 0;
        }
    }
    double full = sum_jump_prob_full();
    DPRINTF(1, " full = %lf, opt = %lf", full, opt);
    if(opt != full) {
        sum_jump_prob_opt(true);
        sum_jump_prob_full(true);
        PQDEG_ABORT("ERROR: full_prob = %lf <> opt_prob = %lf", full, opt);
    }
#endif

    return opt;
}

bool epitaxy_w::jump_isok(dPoint from)
{
    static nConfig &cfg = nConfig::instance();
    static decomp &dcmp = decomp::instance();
    neighbors_t nbs;
    bool result = true;

    nodparam.neighbors(from, nbs);
    for(int dir = 0; dir < dir_number; dir++) {
        dPoint nb;
        nb.local(lPoint(nbs.x[dir], nbs.y[dir], nbs.z[dir]));
        unsigned short conf = cfg.set_config_wout(nb, from, nodparam);
        bool good = nodparam(nb).type == atomType::None || cfg.good_config(conf);
        result = result && good;
    }

    return result;
}

void epitaxy_w::jump_gen(double jprob, sPlan::pRecord &r)
{
    static decomp &dcmp = decomp::instance();
    static nConfig &cfg = nConfig::instance();

    double Px = jprob * r.rdouble;
    double P = 0;

    // Skip empty layers
    double result = 0;
    int start, end;
    dyn_range(start, end);
    zyxiterator it = travInit(start, end);

    double check = sum_jump_prob_opt();

    dPoint from;
    from.local(*(it.begin()));
    bool flag = true;
    for(it = it.begin(); it <= it.end() && flag; it++) {
        P += jmprob(*it);
#ifdef DEBUG_ON
        {
            dPoint gp;
            gp.local(*it);
            // TODO: remove debug 
            std::stringstream ss;
            ss << "Node: " << gp << " " << type2str(nodparam(gp).type)
               << ", jprob = " << jmprob(gp);
            DPRINTF(DBG_DISAB, "%s", ss.str().c_str());
        }
#endif
        if(P >= Px && (nodparam(*it).type != atomType::None) && (jmprob(*it) > 0)) {
            dPoint gp;
            gp.local(*it);
            from = gp;
            bool ret;
            if(ret = jump_isok(from)) {
                flag = false;
            }else{
                break;
            }
#ifdef DEBUG_ON
            {
                // TODO: remove debug
                std::stringstream ss;
                ss << "Potential node: " << gp << "(" << type2str(nodparam(gp).type)
                   << "), check = " << ret;
                DPRINTF(DBG_DISAB, "%s", ss.str().c_str());
            }
#endif
        }
    }

    if(flag) {
        DPRINTF(DBG_TRACE, "Can't find node to jump from, Px = %lf", Px);
        r.status = sPlan::Disabled;
        return;
    }
    if(unlikely(P < Px)) {
        PQDEG_ABORT("Error!!! in choose_jump() P=%f Px=%f P_jump_sum=%f\n",
                    P, Px, jprob);
    }

    if(unlikely(nodejmp(from).jnum <= 0)) {
        PQDEG_ABORT("Error!!! in choose_jump()..\n");
    }


    int j_array[dir_number];
    cfg.config2array(j_array, nodejmp(from).jmap);

    int nx = 1 + rGen::instance().random_(r.prio, nodejmp(from).jnum);
    int count = 0, dir = 0;
    flag = false;
    bool occupied = true;
    DPRINTF(DBG_DISAB, "nx = %d", nx);
    do {

        count += j_array[dir];
        // Check if target node will have good config
        dPoint to;
        nodparam.one_neighbor(from, dir, to);
        unsigned short conf = cfg.set_config_wout(to, from, nodparam);
        bool good_cfg = cfg.good_config(conf);

        flag = (count >= nx) && good_cfg && j_array[dir];

#ifdef DEBUG_ON
        if( flag && nodparam(to).type != atomType::None ){
            std::stringstream ss;
            unsigned short jmap_old = nodejmp(from).jmap;
            set_jump_info(from.local());
            unsigned short jmap_new = nodejmp(from).jmap;
            ss << "ERROR: configuration for " << from << " is outdated: old="
               << std::hex << jmap_old << ", new=" << jmap_new;
            DPRINTF(DBG_TRACE,"%s", ss.str().c_str());
            int j_array[dir_number];
            cfg.config2array(j_array,jmap_old);
            ss.str("jmap_old: ");
            ss << std::dec;
            for(int i=0;i<dir_number;i++){
                ss << j_array[i] << " ";
            }
            DPRINTF(DBG_TRACE,"%s", ss.str().c_str());
            cfg.config2array(j_array,jmap_new);
            ss.str("jmap_new: ");
            for(int i=0;i<dir_number;i++){
                ss << j_array[i] << " ";
            }
            DPRINTF(DBG_TRACE,"%s", ss.str().c_str());
        }
#endif

        if(count >= nx && !flag) {
            DPRINTF(DBG_TRACE, "\t!Can jump in %d but conf is bad", dir);
        }

#ifdef DEBUG_ON
        {
            std::stringstream ss;
            ss << "\tCheck " << dir << " direction " << to << " "
               << type2str(nodparam(to).type)
               << ". Count = " << count <<
                  ", conf = " << std::hex << conf << std::dec;
            DPRINTF(DBG_DISAB, "%s", ss.str().c_str());
        }
#endif

        dir++;
    } while(dir < dir_number && !flag);

    if(!flag) {
        r.status = sPlan::Disabled;
        std::stringstream ss;
        ss << "Can't perform jump from " << from <<
                ". No suitable target node found." <<
                "r.status = Disabled";
        DPRINTF(DBG_TRACE, "%s", ss.str().c_str());
        return;
    }

    dir--;


    // Get target point
    dPoint to;
    nodparam.one_neighbor(from, dir, to);
    memcpy(r.p1, from.glob().get_v(), from.glob().vsize());
    r.p1_t = nodparam(from).type;
    memcpy(r.p2, to.glob().get_v(), to.glob().vsize());
    r.rank = -1;
#ifdef DEBUG_ON
    std::stringstream ss;
    ss << "Choose " << from  << "[" << type2str(nodparam(from).type)
       << "] as jumping node, Px=" << Px << " P=" << P;
    DPRINTF(DBG_DISAB, "%s", ss.str().c_str());
    ss.str("");
    ss << "jump in " << dir << " direction";
    ss << ", to= " << to << "[" << type2str(nodparam(to).type)
       << "]" << std::endl;
    ss << "p2 = {" << r.p2[0] << "," << r.p2[1] << "," << r.p2[2] << "}";
    DPRINTF(DBG_DISAB, "%s", ss.str().c_str());
#endif
}

// INFO: we pass pl here only for debug
void epitaxy_w::jump(sPlan::pRecord & r, sPlan &pl)
{
    dPoint from(lPoint(r.p1)), to(lPoint(r.p2));
    std::list<dPoint> l, l1;
    std::list<dPoint>::iterator it;
    neighbors_t nbs;

    l.clear();
    l1.clear();

#ifdef DEBUG_ON
    {
        std::stringstream ss;
        ss << "Jump from " << from
           << " [" << type2str(nodparam(from).type)
           << ((int)from.is_local() ? ", mine" : "") << "]"
           << " to " << to << " [" << type2str(nodparam(to).type)
           << ((int)to.is_local() ? ", mine" : "") << "]";
        DPRINTF(DBG_TRACE, "%s", ss.str().c_str());
    }
#endif

    if(!(from.is_related() || to.is_related()))
        return;

    if(from.is_local() && to.is_local()) {
        state.jump_local++;
    }

    if(to.is_related()) {
        state.jump_to_rel++;
        if(unlikely(nodparam(to).type != atomType::None)) {
            std::stringstream ss;
            ss << "ERROR: jump to occupied site: " << from << " -> " << to;
            PQDEG_ABORT("%s", ss.str().c_str());
        }

        nodparam(to).type = r.p1_t;
        if(to.is_local()) {
            state.jump_to++;
            l.push_back(to);
            zproj[to.local().z()]++;
            zyproj[to.local().z()][to.local().y()]++;
        }
        // Put all local neighbors of "to" node into the list
        nodparam.neighbors_glob(to, nbs);
        for(int dir = 0; dir < dir_number; dir++) {
            dPoint nb(lPoint(nbs.x[dir], nbs.y[dir], nbs.z[dir]));
            if(nb.is_local())
                l.push_back(nb);
        }
    }

    if(from.is_related()) {
        state.jump_from_rel++;
        if(unlikely(nodparam(from).type == atomType::None)) {
            std::stringstream ss;
            ss << "ERROR: jump from empty site: " << from << " -> " << to;
            PQDEG_ABORT("%s", ss.str().c_str());
        }

        nodparam(from).type = atomType::None;
        if(from.is_local()) {
            // Update service information
            state.jump_from++;
            zproj[from.local().z()]--;
            zyproj[from.local().z()][from.local().y()]--;
            l.push_back(from);
        }
        // Put all local neighbors of "from" node into the list
        nodparam.neighbors_glob(from,nbs);
        for(int dir = 0; dir < dir_number; dir++) {
            dPoint nb(lPoint(nbs.x[dir], nbs.y[dir], nbs.z[dir]));
            if(nb.is_local())
                l.push_back(nb);
        }
    }

    // Setup related nodes configurations
    for(it = l.begin(); it != l.end(); it++) {
        static nConfig &cfg = nConfig::instance();
        lPoint p = (*it).local();
        set_config(p);

        if(unlikely( nodparam(p).type != atomType::None &&
                     !cfg.good_config(nodparam(p).config))) {
            dPoint gp = *it;
            std::stringstream ss;
            ss << "Error!!! bad config of atom " << gp << " "
               << std::hex << nodparam(p).config << std::dec << std::endl;
            ss << "Jump was from " << from << " to " << to << std::endl;
            ss << "Neighbors:" << std::endl << "Global\t\tAbsolut\t\tRelative\tType" << std::endl;
            neighbors_t nbs;
            int factor = nodparam.neighbors(p, nbs);
            for(int dir = 0; dir < dir_number; dir++) {
                lPoint nb(nbs.x[dir], nbs.y[dir], nbs.z[dir]);
                dPoint gnb;
                gnb.local(nb);
                int type = nodparam(nb).type;
                ss << gnb.glob() << "\t" << gnb.local() << "\t";
                lPoint d = nb - p;
                ss << d << "\t";
                ss << type << std::endl;
            }

            ss << "Plan was: " << std::endl;
            pl.dbg_print(ss);
            PQDEG_ABORT("Abort.\n%s",ss.str().c_str());
        }
    }

    // Setup B0 vectors for the related nodes

    for(it = l.begin(); it != l.end(); it++) {
        lPoint p = (*it).local();
        calc_B0(p);
    }

    if( to.is_local() ){
        axyz(to.local());
    }

    // Put all local neighbors of nodes from the list
    // into the new list
    for(it = l.begin(); it != l.end(); it++) {
        neighbors_t nbs;
        nodparam.neighbors_glob((*it), nbs);
        for(int dir = 0; dir < dir_number; dir++) {
            dPoint nb(lPoint(nbs.x[dir], nbs.y[dir], nbs.z[dir]));
            if(nb.is_local()){
                dPoint gp = *it;
                l1.push_back(nb);
            }
        }
    }
    // Merge those two lists in one and put them into update
    // map excluding duplication
    l.merge(l1);
    for(it = l.begin(); it != l.end(); it++) {
       lPoint p = (*it).local();
        set_jump_info(p);
        if( nodejmp(p).jnum == 0 ){
            calc_jump_prob(p);
        }
        upd_q[p] = true;
    }

    DPRINTF(DBG_TRACE, "jump_local=%lf, jump_from=%lf, jump_to=%lf, "
            "jump_from_rel=%lf, jump_to_rel=%lf",
            state.jump_local, state.jump_from, state.jump_to,
            state.jump_from_rel, state.jump_to_rel);
}
