/* 
 * File:   distrPoint.h
 * Author: artpol
 *
 * Created on 11 Март 2013 г., 17:24
 */

#ifndef DPOINT_H
#define	DPOINT_H

#include <lPoint.h>
#include <decomp.h>

class dPoint {
private:
    lPoint _local, _global;
    bool is_local_cached, is_related_cached;
public:

    dPoint()
    {
        is_local_cached = false;
        is_related_cached = false;
    }

    dPoint(lPoint p)
    {
        is_local_cached = false;
        is_related_cached = false;
        glob(p);
    }

    void glob(lPoint p)
    {
        static decomp &d = decomp::instance();
        _global = p;
        _local = _global;
        d.glob2local(_global, _local); // will touch only x & y!!
        is_local_cached = d.is_local(_global);
        is_related_cached = d.is_related(_global);

#ifdef DEBUG_ON
        std::stringstream ss;
        ss << "Set dPoint: glob=" << _global << ", loc=" <<_local;
        //DPRINTF(DBG_TRACE,"%s",ss.str().c_str());
#endif
    }

    void glob(coord_t x, coord_t y, coord_t z)
    {
        lPoint lp(x, y, z);
        glob(lp);
    }

    void glob(coord_t *_v)
    {
        lPoint lp(_v);
        glob(lp);
    }

    lPoint &glob()
    {
        return _global;
    }

    dPoint local(lPoint p)
    {
        static decomp &d = decomp::instance();
        _local = p;
        _global = _local;
        d.local2glob(_local, _global); // will touch only x & y!!
        is_local_cached = d.is_local(_global);
        is_related_cached = d.is_related(_global);
        return *this;
    }

    dPoint local(coord_t x, coord_t y, coord_t z)
    {
        lPoint lp(x,y,z);
        return local(lp);
    }


    dPoint local(coord_t *_v)
    {
        static decomp &d = decomp::instance();
        lPoint lp(_v);
        return local(lp);
    }

    lPoint &local()
    {
        return _local;
    }

    inline bool is_local()
    {
        return is_local_cached;
    }

    inline bool is_related()
    {
        return is_related_cached;
    }

    inline bool is_internal(){
        static decomp &d = decomp::instance();
        return (is_local() && d.is_internal(_global));
    }

    inline bool is_correct()
    {
        return _local.is_correct(local_bounds());
    }

    inline lPoint local_bounds()
    {
        static decomp &d = decomp::instance();
        return d.ldims_wgh();
    }

    inline bool loc_is_correct()
    {
        return local().is_correct(local_bounds());
    }

    inline bool operator==(dPoint &rp)
    {
        return((*this).glob() == rp.glob());
    }

    inline bool operator<(dPoint &rp)
    {
        return((*this).glob() < rp.glob());
    }

    inline bool operator!=(dPoint &rp)
    {
        return !this->operator ==(rp);
    }
    inline bool operator<(const dPoint rp) const
    {
        return _global < rp._global;
    }

    inline bool operator=(const lPoint rp) const
    {
        int rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        std::cout << rank << " Cannot assign dPoint to lPoint" << std::endl;
#ifdef DEBUG_ON
        static int hang = 1;
        debug_hang(hang);
#endif
    }
};

inline static std::ostream& operator <<(std::ostream& o, dPoint& p)
{
    lPoint glob = p.glob();
    lPoint loc = p.local();
    o << "glob:" << glob << "; loc:" << loc;
    return o;
}

#endif	/* DISTRPOINT_H */

