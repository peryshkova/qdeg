/* 
 * File:   cartOps.h
 * Author: artpol
 *
 * Created on 12 Март 2013 г., 9:41
 */

#ifndef CARTMESH_H
#define	CARTMESH_H

#include <mpi.h>
#include <qTypes.h>
#include <debug.h>

#include <lPoint.h>
#include <mpi.h>

template <int n>
class cartmesh {
private:
    MPI_Comm bcomm; // Base communicator (e.g. MPI_COMM_WORLD)
    MPI_Comm _comm; // Workers communicator

    int _size, _rank;

    /* User prefer to have least significant dimension (like x) in dims[0], but 
     MPI_Cart_create waits for the most significant dimension (like z) there*/
    int dims[n]; // original dimensions from user
    int mpidims[n]; // dimensions for MPI_Cart_create
    int mpicoords[n], _coords[n];

    void reverse(int src[], int dst[]);

public:

    cartmesh()
    {
    }

    int init(MPI_Comm c, int _dims[]);
    void coords(int c[]);
    int coords2rank(int c[]);
    void rank2coords(int r, int c[]);

    inline MPI_Comm comm()
    {
        return _comm;
    }

    inline int rank()
    {
        return _rank;
    }

    inline int size()
    {
        return _size;
    }

    inline int operator [](int idx) {
        return(int) dims[idx];
    }

    inline int *pdims()
    {
        return dims;
    }

};

template <int n>
void cartmesh<n>::reverse(int src[], int dst[])
{
    for( int i = 0; i < n; i++ ) {
        dst[i] = src[n - i - 1];
    }
}

template <int n>
int cartmesh<n>::init(MPI_Comm c, int _dims[])
{
    int periods[n];
    int chksize = 1;
    bcomm = c;
    
    for( int i = 0; i < n; i++ ) {
        periods[i] = true;
        dims[i] = _dims[i];
        mpidims[n - i - 1] = dims[i];
        chksize *= dims[i];
    }

#ifdef DEBUG_ON
    int osize, orank;
    MPI_Comm_rank(bcomm, &orank);
    MPI_Comm_size(bcomm, &osize);
    if( osize != chksize && !orank ) {
        EPRINTF("Size of the old communicator (%d) doesn't correspond to the"
                " multiply of dimensions (%d)\n", osize, chksize);
        MPI_Abort(bcomm, 0);
    }
#endif

    // TODO: handle error
    MPI_Cart_create(bcomm, n, mpidims, periods, 1, &_comm);
    MPI_Comm_rank(_comm, &_rank);
    MPI_Comm_size(_comm, &_size);
    MPI_Cart_coords(_comm,_rank,n,mpicoords);

    reverse(mpicoords, _coords);
    return 0;
}

template <int n>
void cartmesh<n>::coords(int c[])
{
    for( int i = 0; i < n; i++ ) {
        c[i] = _coords[i];
    }
}

template <int n>
int cartmesh<n>::coords2rank(int c[])
{
    int tmp[n], r;
    reverse(c, tmp);
    int rank;
    MPI_Cart_rank(_comm,tmp,&rank);
    return rank;
}

template <int n>
void cartmesh<n>::rank2coords(int r, int c[])
{
    int tmp[n];
    MPI_Cart_coords(_comm,r,n,tmp);
    reverse(tmp, c);
}

#endif	/* CARTMESH_H */

