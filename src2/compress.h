#ifndef COMPRESS_H
#define COMPRESS_H

#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
extern int errno;
#include <list>
#include <string>

#include "debug.h"


class ckpt_compressor {
private:
    enum { MAX_FILE_PATH = 1024 };
    pid_t cpid;
    int freq;
    int rfd, wfd;

    int add_to_tar(char *tarname, char *ckptname){
        char command[2*MAX_FILE_PATH+10]; // approximately MAX size of the command
        sprintf(command,"tar -rvf %s %s 2>/dev/null 1>/dev/null",tarname, ckptname);
        int status = system(command);
        if(  WEXITSTATUS(status) != 0 ){
            printf("ckpt_compressor: Error adding ckpt %s into the tar %s. Keep ckpt file.", ckptname, tarname);
            return -1;
        }

        if( unlink(ckptname) ){
            char buf[MAX_FILE_PATH + 100];
            sprintf(buf,"ckpt_compressor: unlink(%s)",ckptname);
            perror(buf);
            return -1;
        }

        return 0;
    }

    int gzip_tar(char *tarname){
        char command[2*MAX_FILE_PATH+10]; // approximately MAX size of the command
        sprintf(command,"gzip -fq %s 1>/dev/null 2>/dev/null",tarname);
        int status = system(command);
        if(  WEXITSTATUS(status) != 0 ){
            printf("ckpt_compressor: Error compressing tar %s.", tarname);
            return -1;
        }

        return 0;
    }

    void compressor_process(){
        std::list<std::string> l;
        char buf[MAX_FILE_PATH];
        int ckpt_cnt = 0;
        int offset = 0;
        int cnt;
        char tar_name[MAX_FILE_PATH];

        while( (cnt = read(rfd, buf + offset, MAX_FILE_PATH - offset) ) > 0 ){
            char *fname = buf;
            char *tar_name_cur = NULL;
            int i;
            offset += cnt;
            if( buf[offset - 1] != '\0' ){
                // read more data
                continue;
            }

            for(i = 0; i < offset; i++){
                if(buf[i] == '\0' ){
                    tar_name_cur = buf + i + 1;
                    break;
                }
            }

            if( (i + 1) == offset ){
                // read only one string, read more data
                continue;
            }

            // Next time read from the beginning of the buffer
            offset = 0;

            // Setup tar name if this is firt checkpoint
            if( ckpt_cnt == 0 ){
                char tmp_buf[MAX_FILE_PATH];

                // set tar_name
               strncpy(tar_name, tar_name_cur, sizeof(tar_name) - 1);
               tar_name[sizeof(tar_name) - 1] = '\0';

               // Cleanup stalled files if any
               sprintf(tmp_buf,"%s.gz",tar_name);
               if( unlink(tar_name) ){
                   if( errno != ENOENT ){
                       char buf[MAX_FILE_PATH + 100];
                       sprintf(buf,"ckpt_compressor: unlink(%s)",tar_name);
                       perror(buf);
                   }
               }
               if( unlink(tmp_buf) ){
                   if( errno != ENOENT ){
                       char buf[MAX_FILE_PATH + 100];
                       sprintf(buf,"ckpt_compressor: unlink(%s)",tmp_buf);
                       perror(buf);
                   }
               }

            }

            // Add a checkpoint into tar
            if( add_to_tar(tar_name, fname) ){
                fprintf(stderr, "%s:%d [%s]: Cannot add ckpt %s to the tar %s. Skip.\n",
                        DEBUG_IN_FILE, __LINE__, __FUNCTION__, fname, tar_name);
                continue;
            }

            ckpt_cnt++;
            if( ckpt_cnt >= freq ){
                fprintf(stderr, "Compress tar %s\n", tar_name);
                gzip_tar(tar_name);
                // Switch to the next tar
                ckpt_cnt = 0;

            }
        }

        if( ckpt_cnt ){
            // Compress remaining ckpts
            gzip_tar(tar_name);
        }
        exit(0);
    }

    bool state_ok;
public:
    ckpt_compressor(){
        cpid = -1;
        state_ok = false;
    }

    int init(int _freq)
    {
        freq = _freq;
        int chan[2];

        if( pipe(chan) ){
            perror("Compressor pipe");
            state_ok = false;
            return -1;
        }
        rfd = chan[0];
        wfd = chan[1];

        cpid = fork();
        if( cpid < 0 ){
            perror("Compressor fork");
            state_ok = false;
            return -1;
        }
        if( cpid == 0 ){
            close(wfd);
            compressor_process();
            // Exit after we done
            exit(0);
        }
        close(rfd);
        state_ok = true;
        return 0;
    }

    int notify(const char *tarname, const char *filename)
    {
        int ret;
        char *ptr;

        if( !state_ok ){
            return -1;
        }

        // Send filename
        size_t size = strlen(filename) + 1 /* '\0' */;
        if( size > MAX_FILE_PATH ){
            fprintf(stderr,"%s:%d [%s]: filename \"%s\" is too long: %d, expect %d\n",
                    DEBUG_IN_FILE, __LINE__, __FUNCTION__, filename, MAX_FILE_PATH, (int)strlen(filename) + 1);
            return -1;
        }

        ptr = (char*)filename;
        while( size > 0 ){
            if( (ret = write(wfd, ptr, size) ) < 0 ){
                fprintf(stderr,"%s:%d [%s]: filename write error: %d(%s)\n",
                        DEBUG_IN_FILE, __LINE__, __FUNCTION__, errno, strerror(errno));
                return -1;
            }
            size -= ret;
            ptr += ret;
        }

        // Send tarname
        size = strlen(tarname) + 1 /* '\0' */;
        if( size > MAX_FILE_PATH ){
            fprintf(stderr,"%s:%d [%s]: tarname \"%s\" is too long: %d, expect %d\n",
                    DEBUG_IN_FILE, __LINE__, __FUNCTION__, filename, MAX_FILE_PATH, (int)strlen(filename) + 1);
            return -1;
        }

        ptr = (char*)tarname;
        while( size > 0 ){
            if( (ret = write(wfd, ptr, size) ) < 0 ){
                fprintf(stderr,"%s:%d [%s]: tarname write error: %d(%s)\n",
                        DEBUG_IN_FILE, __LINE__, __FUNCTION__, errno, strerror(errno));
                return -1;
            }
            size -= ret;
            ptr += ret;
        }
    }

    int finish(){
        // compressor_thread will notice that fd was closed and exit
        if( state_ok ){
            close(wfd);
            int status;
            wait(&status);
        }
    }
};

#endif // COMPRESS_H
