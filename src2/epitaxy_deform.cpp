#include "dPoint.h"
#include <dlNeighbors.h>
#include <nConfig.h>
#include <epitaxy.h>

static void add_B0_term1(float* B, char* nbconf, int i);
static void add_B0_term2(float* B, char* nbconf, int i, int j);
static void add_B0_term3(float* B, char* nbconf, int i, int j);
static float E_term1(int* nbconf, float nb_a[][3], int i);
static float E_term2(int* nbconf, float nb_a[][3], int i, int j);
static float E_term3(int* nbconf, float nb_a[][3], int i, int j);
static void add_A_term1(float* A, int* nbconf, int i);
static void add_A_term2(float* A, int* nbconf, int i, int j);
static void add_A_term3(float* A, int* nbconf, int i, int j);
static void add_B_term1(float* B, int* nbconf, int i);
static void add_B_term2(float* Bi, float* Bj, int* nbconf, int i, int j);
static void add_B_term3(float* Bi, float* Bj, int* nbconf, int i, int j);

#define n 3
static void jacobi(float a[n + 1][n + 1], float d[n + 1], float v[n + 1][n + 1]);
#undef n

void epitaxy_w::set_defects()
{
    //int x, y, z, n, dx, dy, dz, x2, y2, z2, sum_dx, sum_dy, sum_dz, sum;
    decomp &dcmp = decomp::instance();
    qConfig &cfg = qConfig::instance();

    for(int n = 0; n < cfg.defectNum(); n++) {
        std::stringstream out;
        dPoint gp;
        gp.glob(lPoint(cfg.getDefect(n)));

        out << "Try to set defect at " << gp.glob() << ": ";

        if(!gp.is_related()) {
            out << "not related";
            DPRINTF(DBG_SHORT, "%s", out.str().c_str());
            continue;
        }

        if(!gp.loc_is_correct()) {
            std::stringstream ss;
            ss << "Error!!! bad defect position: " << gp << ". Point is not correct";
            PQDEG_ABORT("%s", ss.str().c_str());
        }

        int sum_dx = 0, sum_dy = 0, sum_dz = 0, sum = 0;
        for(int dx = -1; dx <= 1; dx += 2) {
            for(int dy = -1; dy <= 1; dy += 2) {
                for(int dz = -1; dz <= 1; dz += 2) {
                    dPoint gp2;
                    gp2.local(gp.local() + lPoint(dx, dy, dz));
                    if(!gp2.is_local()) {
                        continue;
                    }

                    if(nodparam(gp2).type == atomType::None) {
                        std::stringstream ss;
                        ss << "Error!!! bad defect position: " << gp <<
                                ", point" << gp2 << " is \"None\"";
                        PQDEG_ABORT("%s", ss.str().c_str());
                    }

                    // add defect contribution to the force affecting atoms(p2)
                    nodparam(gp2).defect_f.x += dx;
                    nodparam(gp2).defect_f.y += dy;
                    nodparam(gp2).defect_f.z += dz;
                    sum_dx += dx;
                    sum_dy += dy;
                    sum_dz += dz;
                    sum++;

                    out << "apply defect to " << gp2;
                    DPRINTF(DBG_TRACE, "%s", out.str().c_str());
                }
            }
        }
        if(sum_dx != 0 || sum_dy != 0 || sum_dz != 0 || sum != 4) {

            std::stringstream ss;
            ss << "Error!!! bad defect position: " << gp.glob();
            PQDEG_ABORT("%s", ss.str().c_str());
        }
    }
}


void epitaxy_w::update_Edef_cached()
{
#ifdef DEBUG_ON
    std::stringstream ss;
#endif
    updIter it = upd_q.begin();
    for(; it != upd_q.end(); it++) {
        lPoint p = it->first;
        if(it->second && p.z() >= 2) {
            calc_Edef(p);
            calc_jump_prob(p);
#ifdef DEBUG_ON
            ss << "\tUpdate " << p << "\n";
#endif
        }
    }
    upd_q.clear();
#ifdef DEBUG_ON
    DPRINTF(DBG_DISAB, "%s", ss.str().c_str());
#endif
}



void epitaxy_w::calc_AA()
{
    nConfig &aconf = nConfig::instance();
    int i;
    unsigned short cfg;
    int nbconf[dir_number];
    float A_temp[6];
    unsigned int it;
    for(cfg = 0, it = 0; it < nConfig::Nconfig; it++, cfg++) {
        aconf.config2array(nbconf, cfg);

        for(i = 0; i < 6; i++)
            A_temp[i] = 0;
        add_A_term1(A_temp, nbconf, 0);
        add_A_term1(A_temp, nbconf, 1);
        add_A_term1(A_temp, nbconf, 2);
        add_A_term1(A_temp, nbconf, 3);
        add_A_term2(A_temp, nbconf, 0, 1);
        add_A_term2(A_temp, nbconf, 0, 2);
        add_A_term2(A_temp, nbconf, 0, 3);
        add_A_term2(A_temp, nbconf, 1, 2);
        add_A_term2(A_temp, nbconf, 1, 3);
        add_A_term2(A_temp, nbconf, 2, 3);
        add_A_term3(A_temp, nbconf, 0, 4);
        add_A_term3(A_temp, nbconf, 0, 5);
        add_A_term3(A_temp, nbconf, 0, 6);
        add_A_term3(A_temp, nbconf, 1, 7);
        add_A_term3(A_temp, nbconf, 1, 8);
        add_A_term3(A_temp, nbconf, 1, 9);
        add_A_term3(A_temp, nbconf, 2, 10);
        add_A_term3(A_temp, nbconf, 2, 11);
        add_A_term3(A_temp, nbconf, 2, 12);
        add_A_term3(A_temp, nbconf, 3, 13);
        add_A_term3(A_temp, nbconf, 3, 14);
        add_A_term3(A_temp, nbconf, 3, 15);

        for(i = 0; i < 6; i++) {
            AA[cfg][i] = A_temp[i];
        }
    }
}

void epitaxy_w::calc_BB()
{
    nConfig &aconf = nConfig::instance();
    int k, i;
    unsigned short cfg;
    int nbconf[dir_number];
    float B_temp[dir_number][9];
    unsigned int it;
    for(cfg = 0, it = 0; it < nConfig::Nconfig; it++, cfg++) {
        aconf.config2array(nbconf, cfg);
        for(i = 0; i < dir_number; i++)
            for(k = 0; k < 9; k++)
                B_temp[i][k] = 0;
        add_B_term1(B_temp[0], nbconf, 0);
        add_B_term1(B_temp[1], nbconf, 1);
        add_B_term1(B_temp[2], nbconf, 2);
        add_B_term1(B_temp[3], nbconf, 3);
        add_B_term2(B_temp[0], B_temp[1], nbconf, 0, 1);
        add_B_term2(B_temp[0], B_temp[2], nbconf, 0, 2);
        add_B_term2(B_temp[0], B_temp[3], nbconf, 0, 3);
        add_B_term2(B_temp[1], B_temp[2], nbconf, 1, 2);
        add_B_term2(B_temp[1], B_temp[3], nbconf, 1, 3);
        add_B_term2(B_temp[2], B_temp[3], nbconf, 2, 3);
        add_B_term3(B_temp[0], B_temp[ 4], nbconf, 0, 4);
        add_B_term3(B_temp[0], B_temp[ 5], nbconf, 0, 5);
        add_B_term3(B_temp[0], B_temp[ 6], nbconf, 0, 6);
        add_B_term3(B_temp[1], B_temp[ 7], nbconf, 1, 7);
        add_B_term3(B_temp[1], B_temp[ 8], nbconf, 1, 8);
        add_B_term3(B_temp[1], B_temp[ 9], nbconf, 1, 9);
        add_B_term3(B_temp[2], B_temp[10], nbconf, 2, 10);
        add_B_term3(B_temp[2], B_temp[11], nbconf, 2, 11);
        add_B_term3(B_temp[2], B_temp[12], nbconf, 2, 12);
        add_B_term3(B_temp[3], B_temp[13], nbconf, 3, 13);
        add_B_term3(B_temp[3], B_temp[14], nbconf, 3, 14);
        add_B_term3(B_temp[3], B_temp[15], nbconf, 3, 15);
        for(i = 0; i < dir_number; i++) {
            for(k = 0; k < 9; k++) {
                BB[cfg][i][k] = B_temp[i][k];
            }
        }
    }
}

void epitaxy_w::calc_B0(lPoint p)
{
#ifdef DEBUG_ON
    dPoint gp;
    gp.local(p);
    if(!gp.is_correct()) {
        std::stringstream ss;
        ss << "Bad point " << gp;
        PQDEG_ABORT("%s", ss.str().c_str());
    }
#endif

    qConfig &cfg = qConfig::instance();
    int dir, factor;
    float B[3];
    char nbconf[dir_number + 1];
    if(nodparam(p).type == atomType::None) {
        //atoms(x,y,z).B0.x = atoms(x,y,z).B0.y = atoms(x,y,z).B0.z = 0; 
        memset(&nodparam(p).B0, 0, sizeof (nodparam(0, 0, 0).B0));
        return;
    }

    neighbors_t nbs;
    factor = nodparam.neighbors(p, nbs);
    for(dir = 0; dir < dir_number; dir++) {
        nbconf[dir] = nodparam(nbs.x[dir], nbs.y[dir], nbs.z[dir]).type;
    }
    nbconf[dir_number] = nodparam(p).type;

    B[0] = B[1] = B[2] = 0;
    add_B0_term1(B, nbconf, 0);
    add_B0_term1(B, nbconf, 1);
    add_B0_term1(B, nbconf, 2);
    add_B0_term1(B, nbconf, 3);
    add_B0_term2(B, nbconf, 0, 1);
    add_B0_term2(B, nbconf, 0, 2);
    add_B0_term2(B, nbconf, 0, 3);
    add_B0_term2(B, nbconf, 1, 2);
    add_B0_term2(B, nbconf, 1, 3);
    add_B0_term2(B, nbconf, 2, 3);
    add_B0_term3(B, nbconf, 0, 4);
    add_B0_term3(B, nbconf, 0, 5);
    add_B0_term3(B, nbconf, 0, 6);
    add_B0_term3(B, nbconf, 1, 7);
    add_B0_term3(B, nbconf, 1, 8);
    add_B0_term3(B, nbconf, 1, 9);
    add_B0_term3(B, nbconf, 2, 10);
    add_B0_term3(B, nbconf, 2, 11);
    add_B0_term3(B, nbconf, 2, 12);
    add_B0_term3(B, nbconf, 3, 13);
    add_B0_term3(B, nbconf, 3, 14);
    add_B0_term3(B, nbconf, 3, 15);
    nodparam(p).B0.x = B[0] * factor - cfg.defectF() * nodparam(p).defect_f.x;
    nodparam(p).B0.y = B[1] * factor - cfg.defectF() * nodparam(p).defect_f.y;
    nodparam(p).B0.z = B[2] * factor - cfg.defectF() * nodparam(p).defect_f.z;
}

void epitaxy_w::calc_Edef(lPoint p)
{

    if(nodparam(p).type == atomType::None || nodejmp(p).jnum == 0 ) {
        nodparam(p).Edef = 0;
        return;
    }
    if(nodparam(p).defect_f.x != 0 || nodparam(p).defect_f.y != 0 ||
            nodparam(p).defect_f.z != 0) {
        nodparam(p).Edef = 0;
        return;
    }

    unsigned short conf = nodparam(p).config;
    float *ptr = AA_[conf];
    float A_xx = ptr[0], A_yy = ptr[1], A_zz = ptr[2],
            A_xy = ptr[3], A_xz = ptr[4], A_yz = ptr[5];

    float Bx = nodparam(p).B0.x;
    float By = nodparam(p).B0.y;
    float Bz = nodparam(p).B0.z;

    dPoint gp;
    gp.local(p);
    DPRINTF(DBG_DISAB,"Cacl Edef (%d,%d,%d):",gp.glob().x(), gp.glob().y(), gp.glob().z());
    DPRINTF(DBG_DISAB,"\t0: B = (%.9f,%.9f,%.9f)",Bx,By,Bz);

    neighbors_t nbs;
    int factor = nodparam.neighbors(p, nbs);
    int nbconf[dir_number + 1];
    float nb_a[dir_number + 1][3];
    for(int dir = 0; dir < dir_number; dir++) {
        lPoint np(nbs.x[dir], nbs.y[dir], nbs.z[dir]);
        float ax = displs(np).x;
        float ay = displs(np).y;
        float az = displs(np).z;
        ptr = BB[conf][dir];
        float Bxx = ptr[0];
        float Bxy = ptr[1];
        float Bxz = ptr[2];
        float Byx = ptr[3];
        float Byy = ptr[4];
        float Byz = ptr[5];
        float Bzx = ptr[6];
        float Bzy = ptr[7];
        float Bzz = ptr[8];
        Bx += Bxx * ax + Bxy * ay + Bxz*az;
        By += Byx * ax + Byy * ay + Byz*az;
        Bz += Bzx * ax + Bzy * ay + Bzz*az;
        // Count
        nbconf[dir] = nodparam(np).type;
        nb_a[dir][0] = ax * factor;
        nb_a[dir][1] = ay * factor;
        nb_a[dir][2] = az * factor;
    }
    float ax_ = 0, ay_ = 0, az_ = 0;
    //random_displacements(&ax_,&ay_,&az_,config_);
    ax_ -= 0.5 * (A_xx * Bx + A_xy * By + A_xz * Bz);
    ay_ -= 0.5 * (A_xy * Bx + A_yy * By + A_yz * Bz);
    az_ -= 0.5 * (A_xz * Bx + A_yz * By + A_zz * Bz);

    nbconf[dir_number] = nodparam(p).type;
    nb_a[dir_number][0] = ax_*factor;
    nb_a[dir_number][1] = ay_*factor;
    nb_a[dir_number][2] = az_*factor;

    float E = 0;
    E += E_term1(nbconf, nb_a, 0);
    E += E_term1(nbconf, nb_a, 1);
    E += E_term1(nbconf, nb_a, 2);
    E += E_term1(nbconf, nb_a, 3);
    E += E_term2(nbconf, nb_a, 0, 1);
    E += E_term2(nbconf, nb_a, 0, 2);
    E += E_term2(nbconf, nb_a, 0, 3);
    E += E_term2(nbconf, nb_a, 1, 2);
    E += E_term2(nbconf, nb_a, 1, 3);
    E += E_term2(nbconf, nb_a, 2, 3);
    E += E_term3(nbconf, nb_a, 0, 4);
    E += E_term3(nbconf, nb_a, 0, 5);
    E += E_term3(nbconf, nb_a, 0, 6);
    E += E_term3(nbconf, nb_a, 1, 7);
    E += E_term3(nbconf, nb_a, 1, 8);
    E += E_term3(nbconf, nb_a, 1, 9);
    E += E_term3(nbconf, nb_a, 2, 10);
    E += E_term3(nbconf, nb_a, 2, 11);
    E += E_term3(nbconf, nb_a, 2, 12);
    E += E_term3(nbconf, nb_a, 3, 13);
    E += E_term3(nbconf, nb_a, 3, 14);
    E += E_term3(nbconf, nb_a, 3, 15);
    if(!(E>-1e100 && E < 1e100)) {
        std::stringstream ss;
        ss << "Error!!! E" << p << " = " << E;
        PQDEG_ABORT("%s", ss.str().c_str());
    }
    nodparam(p).Edef = E;
}

void epitaxy_w::calc_AA_()
{ // заполняем массивы AA_ и TR
    int ii, ip, iq, ir;
    unsigned short cfg;
    nConfig &aconf = nConfig::instance();
    float a[4][4] = {0}, d[4], v[4][4];
    float a_[4][4], tr[4][4], d1[4], d2[4], dmax;

    unsigned int i;
    for(cfg = 0, i = 0; i < nConfig::Nconfig; i++, cfg++) {
        for(ii = 0; ii < 6; ii++)
            AA_[cfg][ii] = TR[cfg][ii] = 0;
    }

    // fill AA_ and TR only for "good" configurations
    unsigned int it;
    for(cfg = 0, it = 0; it <= nConfig::Nconfig; it++, cfg++) {
        if(aconf.good_config(cfg)) {
            a[1][1] = AA[cfg][0]; // считываем инф. из массива AA в матрицу a
            a[2][2] = AA[cfg][1];
            a[3][3] = AA[cfg][2];
            a[1][2] = a[2][1] = AA[cfg][3];
            a[1][3] = a[3][1] = AA[cfg][4];
            a[2][3] = a[3][2] = AA[cfg][5];

            // reduce the matrix "a" to diagonal form
            // d - eigenvalues of a
            // v - transformation matrix
            // a - no longer valid
            jacobi(a, d, v);

            std::stringstream ss;

            // check the non-degeneracy of the matrix and calculate
            // inverse values to d1, d2[i] = sqrt(d1[i])
            dmax = 0;
            for(ii = 1; ii <= 3; ii++) {
                if(d[ii] > dmax)
                    dmax = d[ii];
            }
            if(dmax == 0) {
                PQDEG_ABORT("Error!!! matrix AA[%d] is zero\n", cfg);
            }
            for(ii = 1; ii <= 3; ii++) {
                if(d[ii]<-1e-3 * dmax) {
                    PQDEG_ABORT("Error!!! negative eigenvalue of AA[%d]\n", cfg);
                }
                if(d[ii] < 1e-3 * dmax)
                    d1[ii] = d2[ii] = 0;
                else {
                    d1[ii] = 1.0 / d[ii];
                    d2[ii] = sqrt(1.0 / d[ii]);
                }
            }

            // count a_=a^(-1) & tr=a^(-1/2)
            for(ip = 1; ip <= 3; ip++)
                for(iq = 1; iq <= 3; iq++)
                    a_[ip][iq] = tr[ip][iq] = 0;
            for(ip = 1; ip <= 3; ip++) {
                for(iq = 1; iq <= 3; iq++) {
                    for(ir = 1; ir <= 3; ir++) {
                        a_[ip][iq] += v[ip][ir] * d1[ir] * v[iq][ir];
                        tr[ip][iq] += v[ip][ir] * d2[ir] * v[iq][ir];
                    }
                }
            }

            // create AA_[cfg] & TR[cfg]
            AA_[cfg][0] = a_[1][1];
            AA_[cfg][1] = a_[2][2];
            AA_[cfg][2] = a_[3][3];
            AA_[cfg][3] = a_[1][2];
            AA_[cfg][4] = a_[1][3];
            AA_[cfg][5] = a_[2][3];
            TR[cfg][0] = tr[1][1];
            TR[cfg][1] = tr[2][2];
            TR[cfg][2] = tr[3][3];
            TR[cfg][3] = tr[1][2];
            TR[cfg][4] = tr[1][3];
            TR[cfg][5] = tr[2][3];
        }
    }
}

void epitaxy_w::axyz(lPoint p)
{

    float ax2, ay2, az2, A_xx, A_yy, A_zz, A_xy, A_xz, A_yz;
    float Bx, By, Bz, Bxx, Bxy, Bxz, Byx, Byy, Byz, Bzx, Bzy, Bzz;
    int factor;
    unsigned short int config_;
    float *ptr;

    if(nodparam(p).type == atomType::None)
        return;
    config_ = nodparam(p).config;

    ptr = AA_[config_];
    A_xx = ptr[0];
    A_yy = ptr[1];
    A_zz = ptr[2];
    A_xy = ptr[3];
    A_xz = ptr[4];
    A_yz = ptr[5];

    Bx = nodparam(p).B0.x;
    By = nodparam(p).B0.y;
    Bz = nodparam(p).B0.z;

    dPoint dp;
    dp.local(p);

    // Optimization to reduce amount of work dPoint construction
    if( dp.is_internal() ){
        neighbors_t nbs;
        factor = nodparam.neighbors(p, nbs);
        // Vectorised version end
        for(int dir = 0; dir < dir_number; dir++) {
            lPoint np(nbs.x[dir], nbs.y[dir], nbs.z[dir]);

            ax2 = displs(np).x;
            ay2 = displs(np).y;
            az2 = displs(np).z;

            ptr = BB[config_][dir];
            Bxx = ptr[0];
            Bxy = ptr[1];
            Bxz = ptr[2];
            Byx = ptr[3];
            Byy = ptr[4];
            Byz = ptr[5];
            Bzx = ptr[6];
            Bzy = ptr[7];
            Bzz = ptr[8];
            Bx += Bxx * ax2 + Bxy * ay2 + Bxz*az2;
            By += Byx * ax2 + Byy * ay2 + Byz*az2;
            Bz += Bzx * ax2 + Bzy * ay2 + Bzz*az2;
            upd_q[np] = true;
        }
    } else {
        neighbors_t nbs;
        factor = nodparam.neighbors(p, nbs);
        // Vectorised version end
        for(int dir = 0; dir < dir_number; dir++) {
            dPoint np;
            np.local(nbs.x[dir], nbs.y[dir], nbs.z[dir]);

            ax2 = displs(np).x;
            ay2 = displs(np).y;
            az2 = displs(np).z;

            ptr = BB[config_][dir];
            Bxx = ptr[0];
            Bxy = ptr[1];
            Bxz = ptr[2];
            Byx = ptr[3];
            Byy = ptr[4];
            Byz = ptr[5];
            Bzx = ptr[6];
            Bzy = ptr[7];
            Bzz = ptr[8];
            Bx += Bxx * ax2 + Bxy * ay2 + Bxz*az2;
            By += Byx * ax2 + Byy * ay2 + Byz*az2;
            Bz += Bzx * ax2 + Bzy * ay2 + Bzz*az2;
            if( np.is_local() ){
                upd_q[np.local()] = true;
            }
        }
    }

    upd_q[p] = true;

    displ_t f;
    rand_displ(f, config_);
    f.x -= 0.5 * (A_xx * Bx + A_xy * By + A_xz * Bz);
    f.y -= 0.5 * (A_xy * Bx + A_yy * By + A_yz * Bz);
    f.z -= 0.5 * (A_xz * Bx + A_yz * By + A_zz * Bz);

    displs(p) = f;
}

void epitaxy_w::do_all_axyz()
{
    decomp &dcmp = decomp::instance();
    lPoint max = dcmp.ldims_wgh();
    max.z() = count_max_Z();
    zyxiterator it(max, dcmp.ghosts());
    // Skip first 4 layers
    it.offsets(lPoint(0,0,2));
    // Recalculate amount of points in the lattice
    for(it = it.begin(); it <= it.end(); it++) {
        axyz(*it);
    }
}


void epitaxy_w::do_many_axyz(int displ_per_ml)
{
    decomp &dcmp = decomp::instance();
    rGen &prng = rGen::instance();
    coord_t total = count_mobile_points();
    int i,N,n;
    N = displ_per_ml*(total*8.0/(dcmp.ldim_x()*dcmp.ldim_y()));
    for (int i = 0; i < N; i++) {
      n = prng.random_(total);
      //n = i;
      lPoint lp = point_by_no(n);
      axyz(lp);
    }
}

void epitaxy_w::rand_displ(displ_t &f, unsigned short config_)
{
    qConfig &cfg = qConfig::instance();
    float a1, a2, a3, a4, coeff;
    float* p;
    coeff = sqrt(0.5 * cfg.T());
    rGen::instance().randn2(a1, a2);
    rGen::instance().randn2(a3, a4);
    p = TR[config_];
    f.x = coeff * (p[0] * a1 + p[3] * a2 + p[4] * a3);
    f.y = coeff * (p[3] * a1 + p[1] * a2 + p[5] * a3);
    f.z = coeff * (p[4] * a1 + p[5] * a2 + p[2] * a3);
}

static void add_B0_term1(float* B, char* nbconf, int i)
{
    qConfig &cfg = qConfig::instance();
    float eps, coeff;
    if(nbconf[i] == atomType::None) return;
    eps = cfg.eps() * 0.5 * (nbconf[i] + nbconf[dir_number] - 2);
    coeff = 24 * cfg.A() * eps;
    B[0] += coeff * dx_neighbor[i];
    B[1] += coeff * dy_neighbor[i];
    B[2] += coeff * dz_neighbor[i];
}

static void add_B0_term2(float* B, char* nbconf, int i, int j)
{
    qConfig &cfg = qConfig::instance();
    float sum_eps, coeff;
    if(nbconf[i] == atomType::None || nbconf[j] == atomType::None)
        return;
    sum_eps = cfg.eps() * 0.5 * (nbconf[i] + nbconf[j] + 2 * nbconf[dir_number] - 4);
    coeff = -2 * cfg.B() * sum_eps;
    B[0] += coeff * (dx_neighbor[i] + dx_neighbor[j]);
    B[1] += coeff * (dy_neighbor[i] + dy_neighbor[j]);
    B[2] += coeff * (dz_neighbor[i] + dz_neighbor[j]);
}

static void add_B0_term3(float* B, char* nbconf, int i, int j)
{
    qConfig &cfg = qConfig::instance();
    float sum_eps, coeff;
    if(nbconf[i] == atomType::None || nbconf[j] == atomType::None)
        return;
    sum_eps = cfg.eps() * 0.5 * (2 * nbconf[i] + nbconf[j] + nbconf[dir_number] - 4);
    coeff = 2 * cfg.B() * sum_eps;
    B[0] += coeff * (dx_neighbor[j] - dx_neighbor[i]);
    B[1] += coeff * (dy_neighbor[j] - dy_neighbor[i]);
    B[2] += coeff * (dz_neighbor[j] - dz_neighbor[i]);
}

static float E_term1(int* nbconf, float nb_a[][3], int i)
{
    qConfig &cfg = qConfig::instance();
    float eps, in_brackets;
    if(nbconf[i] == atomType::None) return 0;
    eps = cfg.eps() * 0.5 * (nbconf[i] + nbconf[dir_number] - 2);
    in_brackets = 2 * (dx_neighbor[i]*(nb_a[i][0] - nb_a[dir_number][0])
            + dy_neighbor[i]*(nb_a[i][1] - nb_a[dir_number][1])
            + dz_neighbor[i]*(nb_a[i][2] - nb_a[dir_number][2])
            - 3 * eps);
    return cfg.A() * in_brackets*in_brackets;
}

static float E_term2(int* nbconf, float nb_a[][3], int i, int j)
{
    qConfig &cfg = qConfig::instance();
    float sum_eps, in_brackets;
    int dxi, dyi, dzi, dxj, dyj, dzj;
    if(nbconf[i] == 0 || nbconf[j] == 0) return 0;
    sum_eps = cfg.eps() * 0.5 * (nbconf[i] + nbconf[j] + 2 * nbconf[dir_number] - 4);
    dxi = dx_neighbor[i];
    dxj = dx_neighbor[j];
    dyi = dy_neighbor[i];
    dyj = dy_neighbor[j];
    dzi = dz_neighbor[i];
    dzj = dz_neighbor[j];
    in_brackets = dxi * nb_a[j][0] + dxj * nb_a[i][0] - (dxi + dxj) * nb_a[dir_number][0]
            + dyi * nb_a[j][1] + dyj * nb_a[i][1] - (dyi + dyj) * nb_a[dir_number][1]
            + dzi * nb_a[j][2] + dzj * nb_a[i][2] - (dzi + dzj) * nb_a[dir_number][2]
            + sum_eps;
    return cfg.B() * in_brackets*in_brackets;
}

static float E_term3(int* nbconf, float nb_a[][3], int i, int j)
{
    qConfig &cfg = qConfig::instance();
    float sum_eps, in_brackets;
    int dxi, dyi, dzi, dxj, dyj, dzj;
    if(nbconf[i] == 0 || nbconf[j] == 0) return 0;
    sum_eps = cfg.eps() * 0.5 * (2 * nbconf[i] + nbconf[j] + nbconf[dir_number] - 4);
    dxi = dx_neighbor[i];
    dxj = dx_neighbor[j];
    dyi = dy_neighbor[i];
    dyj = dy_neighbor[j];
    dzi = dz_neighbor[i];
    dzj = dz_neighbor[j];
    in_brackets = (2 * dxi - dxj) * nb_a[i][0] - dxi * nb_a[j][0] + (dxj - dxi) * nb_a[dir_number][0]
            + (2 * dyi - dyj) * nb_a[i][1] - dyi * nb_a[j][1] + (dyj - dyi) * nb_a[dir_number][1]
            + (2 * dzi - dzj) * nb_a[i][2] - dzi * nb_a[j][2] + (dzj - dzi) * nb_a[dir_number][2]
            + sum_eps;
    return cfg.B() * in_brackets*in_brackets;
}

static void add_A_term1(float* A, int* nbconf, int i)
{
    qConfig &cfg = qConfig::instance();
    int dx, dy, dz;
    if(nbconf[i] == 0)
        return;
    dx = 2 * dx_neighbor[i];
    dy = 2 * dy_neighbor[i];
    dz = 2 * dz_neighbor[i];
    A[0] += cfg.A() * dx*dx;
    A[1] += cfg.A() * dy*dy;
    A[2] += cfg.A() * dz*dz;
    A[3] += cfg.A() * dx*dy;
    A[4] += cfg.A() * dx*dz;
    A[5] += cfg.A() * dy*dz;
}

static void add_A_term2(float* A, int* nbconf, int i, int j)
{
    qConfig &cfg = qConfig::instance();
    int dx, dy, dz;
    if(nbconf[i] == 0 || nbconf[j] == 0)
        return;
    dx = dx_neighbor[i] + dx_neighbor[j];
    dy = dy_neighbor[i] + dy_neighbor[j];
    dz = dz_neighbor[i] + dz_neighbor[j];
    A[0] += cfg.B() * dx*dx;
    A[1] += cfg.B() * dy*dy;
    A[2] += cfg.B() * dz*dz;
    A[3] += cfg.B() * dx*dy;
    A[4] += cfg.B() * dx*dz;
    A[5] += cfg.B() * dy*dz;
}

static void add_A_term3(float* A, int* nbconf, int i, int j)
{
    qConfig &cfg = qConfig::instance();
    int dx, dy, dz;
    if(nbconf[i] == 0 || nbconf[j] == 0) return;
    dx = dx_neighbor[j] - dx_neighbor[i];
    dy = dy_neighbor[j] - dy_neighbor[i];
    dz = dz_neighbor[j] - dz_neighbor[i];
    A[0] += cfg.B() * dx*dx;
    A[1] += cfg.B() * dy*dy;
    A[2] += cfg.B() * dz*dz;
    A[3] += cfg.B() * dx*dy;
    A[4] += cfg.B() * dx*dz;
    A[5] += cfg.B() * dy*dz;
}

static void add_B_term1(float* B, int* nbconf, int i)
{
    qConfig &cfg = qConfig::instance();
    int dx, dy, dz;
    if(nbconf[i] == 0) return;
    dx = dx_neighbor[i];
    dy = dy_neighbor[i];
    dz = dz_neighbor[i];
    B[0] += -8 * cfg.A() * dx*dx;
    B[1] += -8 * cfg.A() * dx*dy;
    B[2] += -8 * cfg.A() * dx*dz;
    B[3] += -8 * cfg.A() * dy*dx;
    B[4] += -8 * cfg.A() * dy*dy;
    B[5] += -8 * cfg.A() * dy*dz;
    B[6] += -8 * cfg.A() * dz*dx;
    B[7] += -8 * cfg.A() * dz*dy;
    B[8] += -8 * cfg.A() * dz*dz;
}

static void add_B_term2(float* Bi, float* Bj, int* nbconf, int i, int j)
{
    qConfig &cfg = qConfig::instance();
    int dx, dy, dz, dxi, dyi, dzi, dxj, dyj, dzj;
    if(nbconf[i] == 0 || nbconf[j] == 0) return;
    dx = dx_neighbor[i] + dx_neighbor[j];
    dy = dy_neighbor[i] + dy_neighbor[j];
    dz = dz_neighbor[i] + dz_neighbor[j];
    dxi = dx_neighbor[j];
    dxj = dx_neighbor[i];
    dyi = dy_neighbor[j];
    dyj = dy_neighbor[i];
    dzi = dz_neighbor[j];
    dzj = dz_neighbor[i];
    Bi[0] += -2 * cfg.B() * dx*dxi;
    Bj[0] += -2 * cfg.B() * dx*dxj;
    Bi[1] += -2 * cfg.B() * dx*dyi;
    Bj[1] += -2 * cfg.B() * dx*dyj;
    Bi[2] += -2 * cfg.B() * dx*dzi;
    Bj[2] += -2 * cfg.B() * dx*dzj;
    Bi[3] += -2 * cfg.B() * dy*dxi;
    Bj[3] += -2 * cfg.B() * dy*dxj;
    Bi[4] += -2 * cfg.B() * dy*dyi;
    Bj[4] += -2 * cfg.B() * dy*dyj;
    Bi[5] += -2 * cfg.B() * dy*dzi;
    Bj[5] += -2 * cfg.B() * dy*dzj;
    Bi[6] += -2 * cfg.B() * dz*dxi;
    Bj[6] += -2 * cfg.B() * dz*dxj;
    Bi[7] += -2 * cfg.B() * dz*dyi;
    Bj[7] += -2 * cfg.B() * dz*dyj;
    Bi[8] += -2 * cfg.B() * dz*dzi;
    Bj[8] += -2 * cfg.B() * dz*dzj;
}

static void add_B_term3(float* Bi, float* Bj, int* nbconf, int i, int j)
{
    qConfig &cfg = qConfig::instance();
    int dx, dy, dz, dxi, dyi, dzi, dxj, dyj, dzj;
    if(nbconf[i] == 0 || nbconf[j] == 0) return;
    dx = dx_neighbor[j] - dx_neighbor[i];
    dy = dy_neighbor[j] - dy_neighbor[i];
    dz = dz_neighbor[j] - dz_neighbor[i];
    dxi = 2 * dx_neighbor[i] - dx_neighbor[j];
    dxj = -dx_neighbor[i];
    dyi = 2 * dy_neighbor[i] - dy_neighbor[j];
    dyj = -dy_neighbor[i];
    dzi = 2 * dz_neighbor[i] - dz_neighbor[j];
    dzj = -dz_neighbor[i];
    Bi[0] += 2 * cfg.B() * dx*dxi;
    Bj[0] += 2 * cfg.B() * dx*dxj;
    Bi[1] += 2 * cfg.B() * dx*dyi;
    Bj[1] += 2 * cfg.B() * dx*dyj;
    Bi[2] += 2 * cfg.B() * dx*dzi;
    Bj[2] += 2 * cfg.B() * dx*dzj;
    Bi[3] += 2 * cfg.B() * dy*dxi;
    Bj[3] += 2 * cfg.B() * dy*dxj;
    Bi[4] += 2 * cfg.B() * dy*dyi;
    Bj[4] += 2 * cfg.B() * dy*dyj;
    Bi[5] += 2 * cfg.B() * dy*dzi;
    Bj[5] += 2 * cfg.B() * dy*dzj;
    Bi[6] += 2 * cfg.B() * dz*dxi;
    Bj[6] += 2 * cfg.B() * dz*dxj;
    Bi[7] += 2 * cfg.B() * dz*dyi;
    Bj[7] += 2 * cfg.B() * dz*dyj;
    Bi[8] += 2 * cfg.B() * dz*dzi;
    Bj[8] += 2 * cfg.B() * dz*dzj;
}


#define ROTATE(a,i,j,k,l){ \
g=a[i][j]; \
h=a[k][l]; \
a[i][j]=g-s*(h+g*tau); \
a[k][l]=h+s*(g-h*tau); \
}
#define n 3

static void jacobi(float a[n + 1][n + 1], float d[n + 1], float v[n + 1][n + 1])
{
    // Диагонализация матрицы a методом Якоби, см. Numerical Recipes, параграф 11.1, стр. 467-468.
    // На входе - матрица a, на выходе - массив собств. значений d, матрица поворота v.
    int nrot, j, iq, ip, i;
    float tresh, theta, tau, t, sm, s, h, g, c;
    float b[n + 1], z[n + 1];
    int normal_exit; // для проверки нормального выхода из цикла

    for(ip = 1; ip <= n; ip++) { //Initialize to the identity matrix.
        for(iq = 1; iq <= n; iq++) v[ip][iq] = 0.0;
        v[ip][ip] = 1.0;
    }
    for(ip = 1; ip <= n; ip++) { //Initialize b and d to the diagonal
        b[ip] = d[ip] = a[ip][ip]; // of a.
        z[ip] = 0.0; // This vector will accumulate terms
        // of the form tapq as in equation (11.1.14).
    }
    nrot = 0;
    normal_exit = 0;
    for(i = 1; i <= 50; i++) {
        sm = 0.0;
        for(ip = 1; ip <= n - 1; ip++) { // Sum off-diagonal elements.
            for(iq = ip + 1; iq <= n; iq++)
                sm += fabs(a[ip][iq]);
        }
        if(sm == 0.0) { // The normal return, which relies
            // on quadratic convergence to machine underflow.
            normal_exit = 1;
            break;
        }
        if(i < 4)
            tresh = 0.2 * sm / (n * n); // ...on the first three sweeps.
        else
            tresh = 0.0; // ...thereafter.
        for(ip = 1; ip <= n - 1; ip++) {
            for(iq = ip + 1; iq <= n; iq++) {
                g = 100.0 * fabs(a[ip][iq]);
                // After four sweeps, skip the rotation if the off-diagonal element is small.
                if(i > 4 && (float)(fabs(d[ip]) + g) == (float)fabs(d[ip])
                        && (float)(fabs(d[iq]) + g) == (float)fabs(d[iq]))
                    a[ip][iq] = 0.0;
                else if(fabs(a[ip][iq]) > tresh) {
                    h = d[iq] - d[ip];
                    if((float)(fabs(h) + g) == (float)fabs(h))
                        t = (a[ip][iq]) / h; // t = 1/(2 theta)
                    else {
                        theta = 0.5 * h / (a[ip][iq]); // Equation (11.1.10).
                        t = 1.0 / (fabs(theta) + sqrt(1.0 + theta * theta));
                        if(theta < 0.0) t = -t;
                    }
                    c = 1.0 / sqrt(1 + t * t);
                    s = t*c;
                    tau = s / (1.0 + c);
                    h = t * a[ip][iq];
                    z[ip] -= h;
                    z[iq] += h;
                    d[ip] -= h;
                    d[iq] += h;
                    a[ip][iq] = 0.0;
                    for(j = 1; j <= ip - 1; j++) { // Case of rotations 1 <= j < p.
                        ROTATE(a, j, ip, j, iq)
                    }
                    for(j = ip + 1; j <= iq - 1; j++) { // Case of rotations p < j  < q.
                        ROTATE(a, ip, j, j, iq)
                    }
                    for(j = iq + 1; j <= n; j++) { // Case of rotations q < j <= n.
                        ROTATE(a, ip, j, iq, j)
                    }
                    for(j = 1; j <= n; j++) {
                        ROTATE(v, j, ip, j, iq)
                    }
                    ++(nrot);
                }
            }
        }
        for(ip = 1; ip <= n; ip++) {
            b[ip] += z[ip];
            d[ip] = b[ip]; // Update d with the sum of ta_pq ,
            z[ip] = 0.0; // and reinitialize z.
        }
    }
    if(normal_exit == 0) {
        fprintf(stderr, "Error!!! Too many iterations in jacobi\n");
        exit(0);
    }
    // Теперь в массиве d лежат собственные значения матрицы a, а в матрице v - матрица преобразования.
    // Матрица a при этом портится.
}
#undef n
#undef ROTATE
