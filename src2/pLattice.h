/* 
 * File:   pLattice.h
 * Author: artpol
 *
 * Created on 28 Май 2013 г., 16:21
 */

#ifndef PLATTICE_H
#define	PLATTICE_H

#include <mpi.h>
#include <qConfig.h>
#include <qTypes.h>
#include <dLattice.h>

#include <lPoint.h>
#include <dPoint.h>
#include <decomp.h>
#include <iterator_zyx.h>
#include <string.h>

#include <debug.h>

template<typename Elem>
class pLattice {
private:
    dLattice<Elem> lat;
    MPI_Comm wcomm;
    MPI_Datatype mpi_type;
    MPI_Datatype dump_type, xcomm_type, ycomm_type;

    void create_dump_types();
    void create_comm_types();
    void surf_comm_type_x(MPI_Datatype *t, int count);
    void surf_comm_type_y(MPI_Datatype *t, int count);


public:

    // lattice initialization

    pLattice()
    {
    }

    pLattice(MPI_Datatype mpi_t)
    {
        init(mpi_t);
    }

    void init(MPI_Datatype mpi_type);

    // dump
    void dump(FILE *f);

    //-------------------------- Distributed operations --------------------------//
    void sync();
    void sync_surf(int z0, int z);
    void sync_neigh(dPoint a);
    void mpi_dump(const char *fname);

    //------------------------ Access to class elements --------------------------//

    inline Elem &operator () (uint x, uint y, uint z)
    {
        return lat(x, y, z);
    }

    inline dLattice<Elem> &lattice()
    {
        return lat;
    }

    inline Elem &operator () (lPoint p)
    {
        return lat(p.x(), p.y(), p.z());
    }

    inline Elem &operator () (dPoint p)
    {
        return this->operator ()(p.local());
    }


    inline Elem &l(int x, int y, int z)
    {
        return lat.l(x, y, z);
    }

    inline void one_neighbor(dPoint &a, int dir, dPoint &n)
    {
        lPoint neigh;
        lat.one_neighbor(a.local(), dir, neigh);
        n.local(neigh);
    }

    inline void one_neighbor_glob(dPoint &a, int dir, dPoint &n)
    {
        lPoint neigh;
        lat.one_neighbor(a.glob(), dir, neigh);
        n.glob(neigh);
    }

    inline int neighbors(lPoint a, neighbors_t &nb)
    {
        static decomp &d = decomp::instance();
        return neighbors_vect(a, d.ldims_wgh(), nb);
    }

    inline int neighbors(dPoint a, neighbors_t &nb)
    {
        return neighbors_vect(a.local(), a.local_bounds(), nb);
    }

    inline int neighbors_glob(dPoint a, neighbors_t &nb)
    {
        lPoint bounds = decomp::instance().gdims();
        return neighbors_vect(a.glob(), bounds, nb, true);
    }

    //    inline int neighbors(lPoint a, neighbors_t &nb, bool wrap = false)
    //    {
    //        static decomp &d = decomp::instance();
    //        return neighbors_vect(a, d.ldims_wgh(), nb, wrap);
    //    }

    //    inline int neighbors(lPoint a, lPoint bnds, neighbors_t &nb, bool wrap = false)
    //    {
    //        static decomp &d = decomp::instance();
    //        return neighbors_vect(a, bnds, nb, wrap);
    //    }

    inline zyxiterator begin()
    {
        qConfig &qcfg = qConfig::instance();
        decomp &dcmp = decomp::instance();
        lPoint p = dcmp.ldims_wgh();
        lPoint gh = dcmp.ghosts();
        return zyxiterator(p, gh);
    }

    inline zyxiterator end()
    {
        qConfig &qcfg = qConfig::instance();
        decomp &dcmp = decomp::instance();
        lPoint p = dcmp.ldims_wgh();
        lPoint gh = dcmp.ghosts();
        zyxiterator it(p, gh);
        return it.end();
    }

    // --------- Iterators fot debug purposes: iterate through all nodes -----//

    inline zyxiterator _begin()
    {
        qConfig &qcfg = qConfig::instance();
        decomp &dcmp = decomp::instance();
        lPoint p = dcmp.ldims_wgh();
        lPoint gh(0, 0, 0);
        return zyxiterator(p, gh);
    }

    inline zyxiterator _end()
    {
        qConfig &qcfg = qConfig::instance();
        decomp &dcmp = decomp::instance();
        lPoint p = dcmp.ldims_wgh();
        lPoint gh(0, 0, 0);
        zyxiterator it(p, gh);
        return it.end();
    }
};

template<typename Elem>
void pLattice<Elem>::init(MPI_Datatype mpi_t)
{
    decomp &dcmp = decomp::instance();
    qConfig &qcfg = qConfig::instance();
    // Create local lattice.
    mpi_type = mpi_t;
    MPI_Aint ext, lb;
    MPI_Type_get_extent(mpi_t,&lb, &ext);

    if( sizeof(Elem) != ext ) {
        PQDEG_ABORT("Size of Elem do not match MPI type extent!");
    }
    lPoint p = dcmp.ldims_wgh();
    if( lat.init(p.x(), p.y(), qcfg.Gz()) ) {
        PQDEG_ABORT("Cannot initialize lattice");
    }
    create_dump_types();
    create_comm_types();
}

template<typename Elem>
void pLattice<Elem>::create_dump_types()
{
    /*
    decomp &dcmp = decomp::instance();
    lPoint c = dcmp.gcubs();
    // Important! this array should be initialized in MPI (and C array declaration) order
    // (most significant dimension first) rather that C usual order: least significant value first.
    int gsizes[decomp::n] = {c.y() * 2, c.x()};
    int distribs[decomp::n] = {MPI_DISTRIBUTE_BLOCK, MPI_DISTRIBUTE_BLOCK};
    int dargs[decomp::n] = {MPI_DISTRIBUTE_DFLT_DARG, MPI_DISTRIBUTE_DFLT_DARG};

    int tsize;
    MPI_Aint ext;

    cartmesh<decomp::n> &m = dcmp.mesh();
    dumpType = mpiType.Create_darray(m.size(), m.rank(), decomp::n, gsizes,
            distribs, dargs, m.pdims(), MPI_ORDER_C);
    dumpType.Commit();
    int lb;
    dumpType.Get_extent(lb, ext);
    tsize = dumpType.Get_size();
    DPRINTF(1, "ext = %d, size = %d\n", ext, tsize);
*/ 
}

template<typename Elem>
void pLattice<Elem>::create_comm_types()
{
    decomp &dcmp = decomp::instance();
    qConfig &qcfg = qConfig::instance();
    lPoint d = dcmp.ldims_wgh();
    lPoint gh = dcmp.ghosts();

    MPI_Type_vector( (d.y() / 2)*(d.z() / 4), gh.x(), d.x(),mpi_type,&xcomm_type);
    MPI_Type_commit(&xcomm_type);

    MPI_Type_vector(d.z() / 4, d.x()*(gh.y() / 2), d.x()*(d.y() / 2),mpi_type, &ycomm_type );
    MPI_Type_commit(&ycomm_type);
}

template<typename Elem>
void pLattice<Elem>::surf_comm_type_x(MPI_Datatype *t, int count)
{
    decomp &dcmp = decomp::instance();
    qConfig &qcfg = qConfig::instance();
    lPoint d = dcmp.ldims_wgh();
    lPoint gh = dcmp.ghosts();

    MPI_Type_vector( count * (d.y() / 2), gh.x(), d.x(), mpi_type, t);
    MPI_Type_commit(t);
}


template<typename Elem>
void pLattice<Elem>::surf_comm_type_y(MPI_Datatype *t, int count)
{
    decomp &dcmp = decomp::instance();
    qConfig &qcfg = qConfig::instance();
    lPoint d = dcmp.ldims_wgh();
    lPoint gh = dcmp.ghosts();

    MPI_Type_vector(count, d.x()*(gh.y() / 2), d.x()*(d.y() / 2), mpi_type, t );
    MPI_Type_commit( t );
}


template<typename Elem>
void pLattice<Elem>::sync()
{
    int myrank;
    int nranks[3][3]; // left and right neighbors ranks by y axe
    MPI_Status status;
    decomp &dcmp = decomp::instance();

    lPoint d = dcmp.ldims_wgh();
    lPoint g = dcmp.ghosts();
    dcmp.nbr_rankmap(nranks);

    // exchange x borders
    if( dcmp[0] > 1 ) {
        MPI_Status status;
        //DPRINTF("lat = %p, lat.l(4,0,0)=%p, lat.l(_lx-4,0,0)=%p",
        //   &lat(0, 0, 0), &lat(4, 0, 0), &lat(dims[0] - 4, 0, 0));
        MPI_Sendrecv(&lat.l(g.x(), 0, 0), 1, xcomm_type, nranks[0][1], 1,
                &lat.l(d.x() - g.x(), 0, 0), 1, xcomm_type, nranks[2][1], 1,
                dcmp.comm(), &status);
        MPI_Sendrecv(&lat.l(d.x() - 2 * g.x(), 0, 0), 1, xcomm_type,
                     nranks[2][1], 1, &lat.l(0, 0, 0), 1, xcomm_type, nranks[0][1], 1,
                dcmp.comm(), &status);

    }
    // exchange y borders
    if( dcmp[1] > 1 ) {

        MPI_Sendrecv(&lat.l(0, g.y() / 2, 0), 1, ycomm_type,
                     nranks[1][0], 1, &lat.l(0, (d.y() - g.y()) / 2, 0), 1,
                ycomm_type, nranks[1][2], 1, dcmp.comm(), &status);

        MPI_Sendrecv(&lat.l(0, (d.y() - 2 * g.y()) / 2, 0), 1,
                     ycomm_type, nranks[1][2], 1, &lat.l(0, 0, 0), 1,
                ycomm_type, nranks[1][0], 1, dcmp.comm(), &status);
    }
}

template<typename Elem>
void pLattice<Elem>::sync_surf(int z0, int zmax)
{
    int myrank;
    int nranks[3][3]; // left and right neighbors ranks by y axe
    MPI_Status status;
    decomp &dcmp = decomp::instance();
    int count = zmax - z0 + 1 ;
    int pz0 = z0/4;
    int pcount = count / 4 + ((count % 4) ? 1 : 0);
    lPoint d = dcmp.ldims_wgh();
    lPoint g = dcmp.ghosts();
    dcmp.nbr_rankmap(nranks);

    DPRINTF(DBG_TRACE,"z0 = %d, zmax = %d, count = %d, pz0 = %d, pcount = %d\n", z0, zmax, count, pz0, pcount);
    // exchange x borders
    if( dcmp[0] > 1 ) {
        MPI_Status status;
        MPI_Datatype surf_x;
        surf_comm_type_x(&surf_x, pcount);

        MPI_Sendrecv(&lat.l(g.x(), 0, pz0), 1, surf_x,
                     nranks[0][1], 1,
                &lat.l(d.x() - g.x(), 0, pz0), 1, surf_x,
                nranks[2][1], 1, dcmp.comm(), &status);
        MPI_Sendrecv(&lat.l(d.x() - 2 * g.x(), 0, pz0), 1, surf_x,
                     nranks[2][1], 1, &lat.l(0, 0, pz0), 1, surf_x,
                nranks[0][1], 1, dcmp.comm(), &status);
        MPI_Type_free(&surf_x);
    }

    // exchange y borders
    if( dcmp[1] > 1 ) {
        MPI_Datatype surf_y;
        surf_comm_type_y(&surf_y, pcount);
        MPI_Sendrecv(&lat.l(0, g.y() / 2, pz0), 1, surf_y,
                     nranks[1][0], 1,
                &lat.l(0, (d.y() - g.y()) / 2, pz0), 1, surf_y,
                nranks[1][2], 1,
                dcmp.comm(), &status);

        MPI_Sendrecv(&lat.l(0, (d.y() - 2 * g.y()) / 2, pz0), 1, surf_y,
                     nranks[1][2], 1,
                &lat.l(0, 0, pz0), 1, surf_y,
                nranks[1][0], 1,
                dcmp.comm(), &status);
        MPI_Type_free(&surf_y);
    }

#ifdef DEBUG_ON
    {
        qConfig &qcfg = qConfig::instance();
        dLattice<Elem> lat_chk;
        lPoint p = dcmp.ldims_wgh();
        if( lat_chk.init(p.x(), p.y(), qcfg.Gz()) ) {
            PQDEG_ABORT("Cannot initialize duplicating lattice");
        }

        // Save for future comparison
        memcpy(&lat_chk.l(0,0,0),&lat.l(0,0,0), lat.lsize());
		sync();

        zyxiterator it = _begin();
        zyxiterator end = _end();
		it.soffsets(lPoint(0,0,z0-4));
        DPRINTF(DBG_TRACE,"DEBUG sync_surf");
        for( ; it < end; it++){
            void *l1 = (void*)&lat(*it);
            void *l2 = (void*)&lat(*it);
            int ret = memcmp(l1, l2, sizeof(Elem));

            lPoint lp = *it;
            dPoint gp;
            gp.local(lp);

//            {
//                std::stringstream ss;
//                ss << "Check " << gp << ". OK = " << !ret << std::endl;
//                DPRINTF(DBG_TRACE, "%s", ss.str().c_str());
//            }

            if( ret != 0 ){
                std::stringstream ss;
                ss << "\n\n\n\n Mismatch in " << gp << "!!!\n\n\n\n" ;
                printf("%s",ss.str().c_str());
                int flag = 1;
                while( flag ){
                    std::stringstream ss;
                    ss << "Found mismatch between sync_surf and sync for " << gp <<". Sleep(1)";
                    DPRINTF(DBG_TRACE, "%s", ss.str().c_str());
                    sleep(1);
                }
            }
        }
    }
#endif

}

template<typename Elem>
void pLattice<Elem>::sync_neigh(dPoint dp)
{
    decomp &dcmp = decomp::instance();
    cartmesh<decomp::n> &m = dcmp.mesh();
    decomp::relmap_t cmap;
    lPoint d = dcmp.ldims_wgh();
    lPoint g = dcmp.ghosts();
    int i, j;

    if( dcmp.comm_pattern(dp.glob(), cmap) ) {
        DPRINTF(0, "I am related");
        int crd[2];
        for( i = 0; i < 3; i += 2 ) {
            m.coords(crd);
            if( cmap[1][i] == 2 ) {
                uint offset = (!!i)*(d.x() - g.x());
                crd[0] += i - 1;
                int rank = m.coords2rank(crd);
                MPI_Status status;
                DPRINTF(0, "MPI_Recv(%d)", rank);
                MPI_Recv(&lat.l(offset, 0, 0), 1, xcomm_type, rank, 1, m.comm(), &status);
            }
        }

        for( i = 0; i < 3; i += 2 ) {
            int crd[2];
            m.coords(crd);
            if( cmap[i][1] == 2 ) {
                uint offset = (!!i)*(d.y() - g.y()) / 2;
                crd[1] += i - 1;
                int rank = m.coords2rank(crd);
                MPI_Status status;
                DPRINTF(0, "MPI_Recv(%d)", rank);
                MPI_Recv(&lat.l(0, offset, 0), 1, ycomm_type, rank, 1, m.comm(), &status);
            }
        }

        for( i = 0; i < 3; i += 2 ) {
            int crd[2];
            m.coords(crd);
            if( cmap[1][i] == 1 ) {
                uint offset = g.x() + (i / 2)*(d.x() - 3 * g.x());
                crd[0] += i - 1;
                int rank = m.coords2rank(crd);
                DPRINTF(0, "MPI_Send(%d)", rank);
                MPI_Send(&lat.l(offset, 0, 0), 1, xcomm_type, rank, 1, dcmp.comm());
            }
        }

        for( i = 0; i < 3; i += 2 ) {
            int crd[2];
            m.coords(crd);
            if( cmap[i][1] == 1 ) {
                uint offset = (g.y() + (i / 2)*(d.y() - 3 * g.y())) / 2;
                crd[1] += i - 1;
                int rank = m.coords2rank(crd);
                DPRINTF(0, "MPI_Send(%d)", rank);
                MPI_Send(&lat.l(0, offset, 0), 1, ycomm_type, rank, 1, dcmp.comm());
            }
        }
    }
}

template<typename Elem>
void pLattice<Elem>::dump(FILE *f)
{
    /*
    int x, y, z;
    fprintf(f, "------------ Start dump------------------\n");
    int out;
    zyxiterator it(dcmp().ldimwgh_x(), 0, dcmp().ldimwgh_y(), 0,
            qcfg().Lz());
    it.zoffs(0);

    y = -1;
    z = -1;

    for(; it < it.end();) {

        if(z != (*it).z()) {
            if(z > -1)
                fprintf(f, "\n\n");
            z = (*it).z();
        }

        if(y != (*it).y()) {
            fprintf(f, "{ ");
            y = (*it).y();
        }

        int out = (*this)(*it);
        fprintf(f, "%3d ", out);

        it++;
        if(y != (*it).y()) {
            fprintf(f, "}\n");
        }
    }
    fprintf(f, "\n-----------------End dump---------------------\n\n");
     */
}

template<typename Elem>
void pLattice<Elem>::mpi_dump(const char *fname)
{
    MPI_Info info;
    MPI_File fh;
    MPI_Datatype t;
    int tsize;
    decomp &d = decomp::instance();
    MPI_Comm comm = d.comm();
    int rank;
    /*
            MPI_Comm_rank(comm, &rank);
            MPI_Barrier(comm);

            DPRINTF("Open file \"%s\"", fname);

            MPI_File_open(comm, (char*) fname, MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &fh);

            DPRINTF("File \"%s\" is opened successfuly", fname);

            if(rank == 0) {
                dumpTypes t;
                if(mpiType == MPI_CHAR)
                    t = CHAR;
                else if(mpiType == MPI_SHORT)
                    t = SHORT;
                else if(mpiType == MPI_INT)
                    t = INT;
                else if(mpiType == MPI_FLOAT)
                    t = FLOAT;
                else if(mpiType == MPI_DOUBLE)
                    t = DOUBLE;
                int tmp = (int) t;
                MPI_File_write(fh, &tmp, 1, MPI_INT, MPI_STATUS_IGNORE);
                ulong buf[] = {dcmp().gcub_x(), 2 * dcmp().gcub_y()};
                MPI_File_write(fh, buf, 2, MPI_LONG, MPI_STATUS_IGNORE);
            }

            //MPI_Barrier(comm);

            MPI_File_set_view(fh, sizeof(int) + 2 * sizeof(long), mpiType, dumpType,
                    (char*) "native", MPI_INFO_NULL);

            int z;
            coord_t locx = dcmp().lcub_x();
            coord_t locy = dcmp().lcub_y()*2;
            zyxiterator it = (*this).begin();

            coord_t zstart = it.startz();
            coord_t zmax = it.maxz();

            for(z = zstart; z < zmax; z++) {
                it.fix(z);
                Elem buf[locy][locx];
                char outstr[1024] = "";
                int x1 = 0, y1 = 0;
                for(; it < it.end(); it++) {
                    lPoint p = *it;
                    lPoint p1 = it.local();
                    if(rank == 0) {
                        DPRINTF("z = " LPSPEC "; buf[" LPSPEC "][" LPSPEC "] = %d", p1.z(), p1.y() / 2, p1.x() / 4, (*this)(*it));
                    }
                    buf[y1][x1] = (*this)(*it);
                    x1++;
                    if(x1 >= locx) {
                        x1 = 0;
                        y1++;
                    }
                    sprintf(outstr, "%s %d", outstr, buf[p1.y() / 2][p1.x() / 4]);
                }
                if(rank == 0)
                    DPRINTF("---------------------------------");
                sprintf(outstr, "%s\n", outstr);
                DPRINTF("z=%d localx=%d, localy=%d\n%s",
                        z, locx, locy, outstr);
                MPI_File_write_all(fh, buf, locx*locy, mpiType, MPI_STATUS_IGNORE);
            }

            MPI_File_close(&fh);
            DPRINTF("exit");
     */
}

#endif	/* PLATTICE_H */

