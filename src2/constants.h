/* 
 * File:   constants.h
 * Author: artpol
 *
 * Created on 2 Август 2013 г., 12:53
 */

#ifndef CONSTANTS_H
#define	CONSTANTS_H

#define ECUBE 4

#define PTR_to_2DARRAY(type,LX,newname,oldname) type (*newname)[LX] \
       = (type (*)[LX])oldname

#define PTR_to_3DARRAY(type,LX,LY,newname,oldname) type (*newname)[LY][LX] \
       = (type (*)[LY][LX])oldname

#define PTR_to_3DARRAY_ALIGN(type,LX,LY,newname,oldname) type (*newname)[LY][LX] _sse_align \
       = (type (*)[LY][LX])oldname


#endif	/* CONSTANTS_H */

