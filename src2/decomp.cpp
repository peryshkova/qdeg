/* 
 * File:   decomp.cpp
 * Author: artpol
 *
 * Created on 15 Март 2013 г., 16:13
 */

#include <sstream>
#include <decomp.h>

static decomp *decompInstance = NULL;

decomp &decomp::instance()
{
    if(decompInstance == NULL) {
        decompInstance = new decomp();
    }
    return *decompInstance;
}

void decomp::distribute(int mesh_crd, coord_t mesh_max, coord_t lat_dimsz, coord_t &blk, coord_t &off)
{
    coord_t modulo = lat_dimsz % mesh_max;
    blk = lat_dimsz / mesh_max;
    off = mesh_crd * blk;
    if(mesh_crd < modulo) {
        blk += 1;
        off += mesh_crd;
    } else {
        off += modulo;
    }
}

void decomp::init(MPI_Comm comm, uint _gw)
{
    qConfig &qcfg = qConfig::instance();
    int grid[2] = {qcfg.Nx(), qcfg.Ny()};
    int lattice[lPoint::dim] = {qcfg.Gx(), qcfg.Gy(), qcfg.Gz()};
    bcomm = comm;

    _mesh.init(comm, grid);

    gwidth = _gw; // number of ghost elementary cubes
    // Check input data
    for(int i = 0; i < lPoint::dim; i++) {
        lcubes[i] = lattice[i] / ecube;
    }

    // distribute available space between workers
    int crd[n];
    _mesh.coords(crd);
    for(int i = 0; i < n; i++) {
        distribute(crd[i], _mesh[i], lcubes[i], blksz[i], offs[i]);
    }

    DPRINTF(DBG_TRACE, "gwidth = %d, lcubes[0] = %d, lcubes[1] = %d\n"
            "blksz[0] = %d, blksz[1] = %d, offs[0] = %d, offs[1] = %d",
            gwidth, lcubes[0], lcubes[1], blksz[0], blksz[1], offs[0], offs[1]);
}

void
decomp::glob2local(lPoint g, lPoint &l, bool verbose)
{
    coord_t *gv = g.get_v();
    lPoint gmax = gdims();
    coord_t *gmaxv = gmax.get_v();
    l = g;
    coord_t *lv = l.get_v();

    for(int i = 0; i < n; i++) {
        lv[i] = (gv[i] - offset(i));

        if(_mesh[i] > 1) {
            lv[i] = lv[i] + _ghost(i);
            if(lv[i] >= gmaxv[i])
                lv[i] -= gmaxv[i];
            else if(lv[i] < 0)
                lv[i] += gmaxv[i];
        }
        if(verbose) {
            std::stringstream ss;
            ss << "glob2local: gv=(" << gv[0] << "," << gv[1] << "), offs=(" <<
                    offset(0) << "," << offset(1) << ")"
                    << "), gmaxv=(" <<
                    gmaxv[0] << "," << gmaxv[1] <<
                    "), lv[" << i << "]=" << lv[i];
            DPRINTF(DBG_TRACE, "%s", ss.str().c_str());
        }
    }
}

void decomp::nbr_rankmap(int ranks[3][3])
{
    int crd[2];
    char out[1024] = "";
    int i, j;

    _mesh.coords(crd);
    sprintf(out, "(%d,%d):\n", crd[0], crd[1]);

    for(i = -1; i < 2; i++) {
        for(j = -1; j < 2; j++) {
            _mesh.coords(crd);
            crd[0] += i;
            crd[1] += j;
            ranks[1 + i][1 + j] = _mesh.coords2rank(crd);
            sprintf(out, "%s(%d,%d)->%d\n", out, crd[0], crd[1], ranks[1 + i][1 + j]);
        }
    }

    for(i = -1; i < 2; i++) {
        for(j = -1; j < 2; j++) {
            sprintf(out, "%s%d ", out, ranks[1 + i][1 + j]);
        }
        sprintf(out, "%s\n", out);
    }
    //DPRINTF(1, "%s", out);
}

void
decomp::local2glob(lPoint l, lPoint &g)
{
    coord_t *gv = g.get_v();
    coord_t *lv = l.get_v();
    lPoint p = gdims();
    for(int i = 0; i < n; i++) {
        gv[i] = lv[i] + offset(i);
        if(_mesh[i] > 1) {
            gv[i] -= _ghost(i);
        }
        gv[i] = (gv[i] + p.get_v()[i]) % p.get_v()[i];
        DPRINTF(DBG_DISAB, "lv[i]=%d, offs[i]=%d, ghost(i)=%d", lv[i], offset(i), _ghost(i));
    }
}

bool decomp::is_local(lPoint p)
{
    lPoint max = gdims();
    coord_t *m = max.get_v();
    coord_t *v = p.get_v();
    bool local;

#ifdef DEBUG_ON
/*
    local = true;
    std::stringstream ss;
    ss << "Check locality for " << p << ": ";
    for(int i = 0; i < n; i++) {
        ss << offset(i) << " <= " << v[i] << " <= " <<
                offset(i) + blocksize(i) << "; ";
        v[i] = v[i] % m[i];
        if(!(v[i] >= offset(i) && v[i] < (offset(i) + blocksize(i))))
            local = false;
    }
    ss << "local = " << (int)local;
    //DPRINTF(DBG_TRACE, "%s", ss.str().c_str());
*/
#endif
    for(int i = 0; i < n; i++) {
        v[i] = v[i] % m[i];
        if(!(v[i] >= offset(i) && v[i] < (offset(i) + blocksize(i))))
            return false;
    }
    return true;
}

// This function tells us if this point is local and its neighbors too
bool decomp::is_internal(lPoint p)
{
    lPoint max = gdims();
    coord_t *m = max.get_v();
    coord_t *v = p.get_v();
    bool local;

    for(int i = 0; i < n; i++) {
        coord_t tmp = ( v[i] - 4 + m[i]) % m[i];
        if(!(tmp >= offset(i) && tmp < (offset(i) + blocksize(i))))
            return false;
        tmp = ( v[i] + 4 + m[i]) % m[i];
        if(!(tmp >= offset(i) && tmp < (offset(i) + blocksize(i))))
            return false;
    }


    return true;
}

bool decomp::is_neighbor(lPoint p, bool verbose)
{
    lPoint loc;
    this->glob2local(p, loc, verbose);
    bool ret = true;
    coord_t *v = loc.get_v();
    lPoint d = ldims_wgh();
    coord_t *max = d.get_v();

    std::stringstream s;
    s << "point = " << p << ", local= " << loc << ", max = " << d << "; ";

    for(int i = 0; i < n; i++) {
        if(!(v[i] >= 0 && v[i] < max[i])) {
            ret = false;
            if(verbose) {
                this->glob2local(p, loc, true);
                int rank;
                MPI_Comm_rank(MPI_COMM_WORLD, &rank);
                std::cout << "is_neighbor (fail): rank="
                          << rank << " (" << p.x() << "," << p.y()
                          << "), i=" << i << " v=(" << v[0] << ","
                          << v[1] << "), max[i]=" << max[i] << std::endl;
            }
        }

    }
    s << "result = " << (ret && !is_local(p)) << std::endl;
    DPRINTF(0, "%s", s.str().c_str());

    return ret && !is_local(p);
}

/*
 * Relative MAP has following views:
 *   1) 0 0 0   2) 0 0 0  3) 0 0 0
 * 	    0 1 0      1 0 0     0 0 0
 * 	    0 0 0      0 0 0     0 0 1
 * 1) means this processor owns the point p
 * 2) means owner is at the left side relative to this processor
 * 3) means owner is down and right relative to this processor
 * -) if map is zeroed - the point is too far from this processor
 */
bool decomp::is_related(lPoint p, relmap_t &map)
{
    lPoint loc;
    this->glob2local(p, loc);
    bool ret = false;
    lPoint max = gdims();
    lPoint lmax = this->ldims_wgh();
    max.z() = 1;
    loc = loc + max;
    loc = loc.mod(max);

    bool mapdims[n][3];
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < 3; j++) {
            mapdims[i][j] = false;
        }
        coord_t *v = loc.get_v();
        coord_t *mv = lmax.get_v();
        if(v[i] >= 0 && v[i] < _ghost(i)) {
            mapdims[i][0] = true;
        } else if(v[i] >= _ghost(i) && v[i] < mv[i] - _ghost(i)) {
            mapdims[i][1] = true;
        } else if(v[i] < mv[i]) {
            mapdims[i][2] = true;
        }
    }

    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {

            map[i][j] = mapdims[1][i] * mapdims[0][j];
            ret = ret || map[i][j];
        }
    }
    /*
    if(ret)
        outputMap(map, "MAP:");
     */
    return ret;
}

bool decomp::is_neighbor_no(lPoint p, int rank)
{
    bool ret = false;
    relmap_t map;
    is_related(p, map);
    int crd[2];
    _mesh.coords(crd);
    for(int y = 0; y < 3; y++) {
        for(int x = 0; x < 3; x++) {
            if(map[y][x]) {
                int ncrd[2];
                ncrd[0] = crd[0] + x - 1;
                ncrd[1] = crd[1] + y - 1;
                int nrank = _mesh.coords2rank(ncrd);
                if(nrank == rank)
                    ret = true;
            }
        }
    }

#ifdef DEBUG_ON
    {
        std::stringstream ss;
        ss.str("");
        ss << p << std::endl;
        int crd[2];
        _mesh.coords(crd);
        for(int y = 0; y < 3; y++) {
            for(int x = 0; x < 3; x++) {
                ss << (int)map[y][x];
                if(map[y][x]) {
                    int ncrd[2];
                    ncrd[0] = crd[0] + x - 1;
                    ncrd[1] = crd[1] + y - 1;
                    int nrank = _mesh.coords2rank(ncrd);
                    ss << "(" << nrank << ") ";
                } else {
                    ss << "(-) ";
                }
            }
            ss << std::endl;
        }
        DPRINTF(DBG_DISAB, "%s\nret = %d", ss.str().c_str(), ret);
    }
#endif
    return ret;
}

/*
 * Neighbour MAP has following views (only orthogonal):
 *   1) 0 1 0   2) 0 0 0  3) 0 0 0
 *  	1 0 0      0 0 1     0 0 0
 * 	    0 0 0      0 0 0     0 0 0
 * 1) the point of interest also affects 2 neighbors (x-1,y) and (x,y-1)
 * 2) the point of interest also affects 1 neighbor (x+1,y)
 * 3) the point of interest didn't affect no neighbors (should not happen)
 */
void decomp::nbr_map(lPoint p, relmap_t nmap)
{
    lPoint loc;
    this->glob2local(p, loc);
    bool ret = false;
    lPoint lmax = ldims_wgh();
    lmax.z() = 1;

    std::stringstream s;

    s << "Point = " << p << ". local = " << loc << ". max = " << lmax << std::endl;
    /*
        DPRINTF("local: (" LPSPEC "," LPSPEC "," LPSPEC ")",
                loc.x(), loc.y(), loc.z());
        DPRINTF("max: (" LPSPEC "," LPSPEC "," LPSPEC ")",
                lmax.x(), lmax.y(), lmax.z());
     */
    loc = loc + lmax;
    s << "loc = (loc + lmax) = " << loc << std::endl;

    loc = loc.mod(lmax);
    s << "loc = loc.mod(lmax) = " << loc << std::endl;

    s << "mapdims:" << std::endl;
    coord_t *v = loc.get_v();
    coord_t *mv = lmax.get_v();
    uint mapdims[n][3];
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < 3; j++) {
            mapdims[i][j] = false;
        }

        if(v[i] < 2 * _ghost(i)) {
            mapdims[i][0] = true;
        } else if(v[i] >= 2 * _ghost(i) && v[i] < mv[i] - 2 * _ghost(i)) {
            mapdims[i][1] = true;
        } else if(v[i] < mv[i]) {
            mapdims[i][2] = true;
        }

        for(int j = 0; j < 3; j++) {
            s << mapdims[i][j] << " ";
        }
        s << std::endl;
    }

    DPRINTF(0, "%s", s.str().c_str());

    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
            nmap[i][j] = 0;
        }
    }
    for(int i = 0; i < 3; i++) {

        nmap[1][i] = mapdims[0][i];
        nmap[i][1] = mapdims[1][i];
    }

    //outputMap(nmap, "Neighbors MAP:");
}

/*
 * Communication MAP has following view:
 * 	0 1 0 
 * 	0 0 2 
 * 	0 0 0 
 * This means we get infromation from (x+1,y) and send it to (x,y-1) neighbors
 */
void decomp::comm_map(relmap_t rmap, relmap_t nmap, relmap_t cmap)
{
    int i, j;
    int flag = 0;

    if(rmap[1][1]) {
        for(i = 0; i < 3; i++) {
            for(j = 0; j < 3; j++) {
                cmap[i][j] = nmap[i][j];
                flag += cmap[i][j];
            }
        }
        return;
    }

    for(i = 0; i < 3; i++) {
        for(j = 0; j < 3; j++) {
            cmap[i][j] = rmap[i][j] + nmap[i][j];
            flag += cmap[i][j];
        }
    }
    if(cmap[0][1] == 2 || cmap[2][1] == 2) {
        cmap[1][0] = cmap[1][2] = 0;
    } else {
        int flag = 0;
        for(i = 0; i < 3; i++) {
            for(j = 0; j < 3; j++) {
                if(cmap[i][j] == 2)
                    flag = 1;
            }
        }
        if(!flag) {

            cmap[0][1] *= 2;
            cmap[2][1] *= 2;
            cmap[1][0] = cmap[1][2] = 0;
        }
    }
}

bool decomp::comm_pattern(lPoint p, relmap_t map)
{
    bool ret;
    relmap_t rmap, nmap;
    if(ret = is_related(p, rmap)) {

        outputMap(rmap, "REL MAP:");
        nbr_map(p, nmap);
        outputMap(nmap, "NBR MAP:");
        comm_map(rmap, nmap, map);
        outputMap(map, "COMM MAP:");
    }
    return ret;
}

void decomp::outputMap(relmap_t map, std::string prefix)
{
    std::stringstream s;

    s << prefix << std::endl << "\t";
    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
            s << (int)map[i][j] << " ";
        }
        s << std::endl << "\t";
    }
    DPRINTF(0, "%s", s.str().c_str());
}
