/* 
 * File:   randgen.h
 * Author: artpol
 *
 * Created on 5 Август 2013 г., 17:21
 */

#ifndef RANDGEN_H
#define	RANDGEN_H

#include <stdlib.h>
#include <sys/time.h>

class rGen {
private:

public:

    rGen()
    {
        struct timeval tv;
        //gettimeofday(&tv, NULL);
        //srandom(tv.tv_usec);
    }

    long int generate()
    {
        return random();
    }

    int random_()
    { // случайное число от 0 до n-1
        return rand(); // it's not very good!!!!!!!!!!!!!!!!!!!!!
    }

    int random_(int n)
    { // случайное число от 0 до n-1
        return rand() % n; // it's not very good!!!!!!!!!!!!!!!!!!!!!
    }

    int random_(unsigned int rnd, int n)
    { // случайное число от 0 до n-1
        return rnd % n; // it's not very good!!!!!!!!!!!!!!!!!!!!!
    }

    float rand01(void)
    { // случайное число между 0 и 1
        return((float) rand() / RAND_MAX + (float) rand()) / (RAND_MAX + 1.);
    }

    static rGen &instance();
    void randn2(float &x1, float &x2);
};


#endif	/* RANDGEN_H */

