/* 
 * File:   debug.h
 * Author: artpol
 *
 * Created on 28 Май 2013 г., 13:30
 */

#ifndef DEBUG_H
#define	DEBUG_H
#include <stdlib.h>
#include <unistd.h>
#include <mpi.h>

//#define DEBUG_ON // Define it in each file separately now!!!!

#define DBG_TRACE 10
#define DBG_SHORT 5
#define DBG_DISAB 0
#include <mpi.h>

extern FILE *__parqdeg_dbgout__;
extern int __rank__;
extern MPI_Comm __dbg_comm__;

static void debug_hang(int i){
    while( i ){
        sleep(1);
    }
}

#define DEBUG_IN_FILE (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#define EPRINTF(fmt,args...)\
{ \
    static int __r__; \
    MPI_Comm_rank(MPI_COMM_WORLD, &__r__); \
    fprintf(stderr, "[%d]%s:%d(%s) " fmt "\n", __r__, \
            DEBUG_IN_FILE, __LINE__, __FUNCTION__, \
            ## args  ); \
    fflush(stderr); \
}

#define PQDEG_ABORT(fmt,args...) { \
    EPRINTF("ParQDEG ABORT: " fmt, ## args ); \
    MPI_Abort(MPI_COMM_WORLD, 0); \
}


#ifdef DEBUG_ON

static void init_debug(MPI_Comm comm, FILE *out = NULL)
{
    // TODO: error handling!?
    MPI_Comm_rank(__dbg_comm__, &__rank__);
    ::__parqdeg_dbgout__ = out;
    ::__dbg_comm__ = comm;
    if(::__parqdeg_dbgout__ == NULL) {
        char tmp[256];
        sprintf(tmp, "rank%d.log", ::__rank__);
        ::__parqdeg_dbgout__ = fopen(tmp, "w");
        if(::__parqdeg_dbgout__ == NULL) {
            MPI_Abort(__dbg_comm__, 0);
        }
    }
}

#undef  DPRINTF
#define DPRINTF(en,fmt,args...)\
{ \
   if( __rank__ < 0 ){ \
     fprintf(__parqdeg_dbgout__, \
        "[%d]:%s(%s):%d: DPRINTF is used before debug_init! Abort.\n", \
        __rank__, DEBUG_IN_FILE , __FUNCTION__, __LINE__); \
     exit(0); \
   } else {\
     if( en ) { \
         fprintf(__parqdeg_dbgout__, "[%d]%s:%d(%s): " fmt "\n",       \
             __rank__, DEBUG_IN_FILE, __LINE__, __FUNCTION__, ## args  ); \
         fflush(__parqdeg_dbgout__); \
     } \
   } \
}

#else

static void init_debug(MPI_Comm comm, FILE *out = NULL)
{
}

#undef DPRINTF

#define DPRINTF(fmt,args...) { }

#endif

#endif	/* DEBUG_H */

