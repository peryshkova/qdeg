#include <nConfig.h>

static nConfig *nConfigInstance = NULL;

nConfig &nConfig::instance()
{
    if(nConfigInstance == NULL) {
        nConfigInstance = new nConfig;
    }
    return *nConfigInstance;
}

nConfig::nConfig()
{
    unsigned int conf;
    int nb_type[dir_number];
    int n1; // number of direct neighbors
    int n2; // number of 2-hop neighbors that are CONNECTED
    int n2_all; // number of non-zero 2-hop neighbors
    DPRINTF(DBG_DISAB, "Start");
    for(conf = 0; conf < Nconfig; conf++) {
        n1 = n2 = n2_all = 0;
        for(int i = 0; i < dir_number; i++)
            if(pr_neighbor[i] < 0 && (conf & (1 << (15 - i))))
                n1++;

        for(int i = 0; i < dir_number; i++)
            if(pr_neighbor[i] >= 0) {
                int j = pr_neighbor[i];
                if(conf == 0x9e4f)
                    DPRINTF(DBG_DISAB,
                            "conf=0x%04x, i = %d, j = %d, parent = %d, child #%d = %d",
                            conf, i, j,
                            conf & (1 << (15 - j)),
                            i,
                            conf & (1 << (15 - i))  );
                if((conf & (1 << (15 - i))) && (conf & (1 << (15 - j))))
                    n2++;
                if(conf & (1 << (15 - i)))
                    n2_all++;
            }

        if(conf == 0x9e4f)
            DPRINTF(DBG_TRACE,
                    "n1=%d, n2=%d", n1, n2);
        n1_conf[conf] = n1;
        n2_conf[conf] = n2_all;
        good_conf[conf] = (n2 >= 2);
    }
}

unsigned short
nConfig::set_config(lPoint p, pLattice<atom_info> &l, bool debug)
{
    int dir, x2, y2, z2, factor = 1;
    int nb_type[dir_number];
    neighbors_t nbs;
    ushort conf = 0;

    factor = l.neighbors(p, nbs);

#ifdef DEBUG_ON
    if( debug ){
        dPoint gp;
        gp.local(p);
        std::stringstream ss;
        ss << "Neighbors of " << gp << ":" << std::endl;
        for(dir = 0; dir < dir_number; dir++) {
            lPoint p1(nbs.x[dir], nbs.y[dir], nbs.z[dir]);
            dPoint gp1;
            gp1.local(p1);
            ss << "\t" << gp1 << ".type=" << (int)l(p1).type << std::endl;
        }
        DPRINTF(DBG_TRACE,"%s",ss.str().c_str());
    }
#endif

    for(dir = 0; dir < dir_number; dir++) {
        lPoint p1(nbs.x[dir], nbs.y[dir], nbs.z[dir]);
        conf |= (l(p1).type != atomType::None) << (15 - dir);
    }
    return conf;
}

ushort nConfig::set_config_wout(lPoint &p, lPoint &excl, pLattice<atom_info> &l)
{
    int dir, x2, y2, z2, factor = 1;
    int nb_type[dir_number];
    neighbors_t nbs;
    ushort conf = 0;
    std::stringstream ss;

    ss << "point=" << p << " excl=" << excl << ": ";
    if(p.z() % 2 != 0)
        factor = -1;
    l.neighbors(p, nbs);

    for(dir = 0; dir < dir_number; dir++) {
        lPoint p1(nbs.x[dir], nbs.y[dir], nbs.z[dir]);
        ss << p1 << " " << type2str(l(p1).type);
        if(p1 != excl)
            conf |= (l(p1).type != atomType::None) << (15 - dir);
        ss << " " << std::hex << conf << std::dec << "; ";
    }
    DPRINTF(DBG_DISAB, "%s", ss.str().c_str());
    return conf;
}

void nConfig::config2array(int* nb_type, unsigned short _conf)
{
    for(int i = 0; i < dir_number; i++) {
        nb_type[i] = ((_conf & 0x8000) != 0);
        _conf <<= 1;
    }
}

unsigned short nConfig::array2config(int* nb_type)
{
    unsigned short conf = 0;
    for(int i = 0; i < dir_number; i++) {
        conf <<= 1;
        conf += (nb_type[i] != 0);
    }
    return conf;
}
