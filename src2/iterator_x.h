#ifndef ITERATOR_X_H
#define ITERATOR_X_H

#include <iterator_yx.h>
#include <sstream>

class xiterator : public yxiterator {
public:

    xiterator()
    {
    }

    xiterator(const yxiterator &op) : yxiterator(op)
    {
    }

    xiterator(const zyxiterator &op) : yxiterator(op)
    {
    }

    void __init(lPoint _max)
    {
        // fix y coordinate
        max[1] = _max.y() + 1;
        x[1] = _max.y();
        offs_s[1] = x[1];
        ghostw[1] = 0;
        // setup x coordinate
        x[0] = _start_0();
    }

    xiterator(lPoint _max, lPoint ghost) :
        yxiterator(_max, ghost)
    {
        __init(_max);
    }

    xiterator(coord_t _max[3], coord_t ghost = 0) :
        yxiterator(_max, ghost)
    {
        lPoint max(_max);
        __init(max);
    }

    void init(lPoint _max, lPoint ghost = lPoint(0,0,0))
    {
        ((yxiterator*)this)->init(_max, ghost);
        __init(_max);
    }

    void init(coord_t _max[3], coord_t ghost = 0)
    {
        ((yxiterator*)this)->init(_max, ghost);
        __init(_max);
    }

    inline void offsets(lPoint p)
    {
        coord_t *_off = p.get_v();
        for(int i = 0; i < 2; i++ ) {
            offs_s[map[i]] = offs_e[map[i]] = _off[map[i]];
        }
        _reset_coords();
    }

    void soffsets(lPoint p)
    {
        coord_t *_off = p.get_v();
        for(int i = 0; i < 2; i++ ) {
            offs_s[map[i]] = _off[map[i]];
        }
        _reset_coords();
    }

    inline void eoffsets(lPoint p)
    {
        coord_t *_off = p.get_v();
        for(int i = 0; i < 2; i++ ) {
            offs_e[map[i]] = _off[map[i]];
        }
        _reset_coords();
    }


    inline bool operator==(xiterator r)
    {
        yxiterator *lhs = this, *rhs = &r;
        return(*lhs == *rhs);
    }

    inline bool operator<(xiterator r)
    {
        yxiterator *lhs = this, *rhs = &r;
        return(*lhs < *rhs);
    }

    inline bool operator>(xiterator r)
    {
        yxiterator *lhs = this, *rhs = &r;
        return(*lhs > *rhs);
    }

    inline bool operator>=(xiterator r)
    {
        yxiterator *lhs = this, *rhs = &r;
        return(*lhs >= *rhs);
    }

    inline xiterator end()
    {
        xiterator tmp = (xiterator) ((yxiterator*)this)->end();
        return tmp;
    }

    inline xiterator begin()
    {
        xiterator tmp = ((yxiterator*)this)->begin();
        return tmp;
    }
};


#endif // ITERATOR_X_H
