/* 
 * File:   step_plan.h
 * Author: artpol
 *
 * Created on 5 Август 2013 г., 11:57
 */

#ifndef STEP_PLAN_H
#define	STEP_PLAN_H

#include <stdio.h>
#include <mpi.h>
#include <rGen.h>
#include <qConfig.h>
#include <sstream>
#include <string>

#include <lPoint.h>
#include <decomp.h>
#include <rGen.h>
#include <x86arch.h>
#include <list>
#include <pLattice.h>

class sPlan {
public:

    typedef enum {
        Master, Worker, Unused
    } branch_role;

    typedef enum {
        Zero, Draft, Ready, Clean
    } states;

    typedef enum {
        None, Depo, Jump
    } acttypes;

    typedef enum {
        Disabled = 0, Inactive = 1, Active = 2, Removed = 3
    } recStat;

    typedef enum {
        NotSynced = 0, Synced = 1
    } syncStat;

    typedef struct {
        char type;
        char status;
        char synced;
        int p1[3], p2[3];
        char p1_t;
        unsigned long prio;
        int rank;
        double rdouble;
    } pRecord;

private:
    branch_role role;
    MPI_Comm bComm;
    unsigned int mSize, pSize;
    states st;
    coord_t Gx, Gy, Gz;
    pRecord *plan;
    double *prob, prob_jump;
    MPI_Datatype pRecord_mpi;

    void precord2mpi();

    void generate_draft();
    void exch_with_neigh(pRecord *n_plans_raw, int ranks[8]);
    void clear_one_point(lPoint &p, std::list<pRecord*>::iterator &it,
            std::list<pRecord*> &l);
    void clear_conflicts();
    bool check_plan();
    pRecord empty;

    // DEBUG
    std::string precord2string(pRecord &p, pLattice<atom_info> *l = NULL);
    void poison_plan();
public:
    void dbg_print(std::stringstream &ss, pLattice<atom_info> *l = NULL);

    static void init(MPI_Comm c, branch_role r);
    static sPlan &instance();

    inline states state()
    {
        return st;
    }

    pRecord &operator [](int index) {
        if( index > pSize )
            return empty;
        return plan[index];
    }

    inline unsigned int size()
    {
        return pSize;
    }

    inline unsigned int msize()
    {
        return mSize;
    }

    inline bool is_root()
    {
        int rank;
        MPI_Comm_rank(bComm, &rank);
        if( rank == 0 )
            return true;
        return false;
    }

    sPlan(MPI_Comm c, branch_role r);

    ~sPlan()
    {
        delete [] plan;
        delete [] prob;
    }

    double prepare_draft();
    void prepare_draft(double p);
    void prepare_final( pLattice<atom_info> *l = NULL);
    //    void fix_final();

};

#endif	/* STEP_PLAN_H */

