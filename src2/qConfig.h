/* 
 * File:   qConfig.h
 * Author: artpol
 *
 * Created on 28 Май 2013 г., 10:13
 */

#ifndef QCONFIG_H
#define	QCONFIG_H

#include <mpi.h>
#include <string>
#include <qTypes.h>
#include <debug.h>
#include <constants.h>
#include <math.h>
#include <x86arch.h>


// Config parse functions
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>

#include "lPoint.h"

#define DEFAULT_CONFIG "pqdeg.cfg"

class qConfig {
public:
    enum DIM {
        x = 0, y = 1, z = 2
    };

    enum odiff{ time, step, amount };

    struct io_freq_t {
        double amount;
        int steps;
        int sync;
    };

    struct io_t{
        char load;
        std::string input;
        std::string pref, suf;
        odiff diff;
        std::string stat_fname;
        int compress;
        int ckpt_per_tar;
    };

    struct control_t{
        double start, speed;
        char type;
        double temp;
    };

    struct core_config {
        // Lattice settings
        coord_t _G[3]; // Global lattice dimensions
        coord_t zSurf; // Initial surface level
        // MPI settings
        coord_t _N[2]; // Node grid dimensions
        unsigned int gw; // number of ghost cubes
        unsigned int _stepSize;
        double simTime, simAmount;
        unsigned int ctrl_size;
        unsigned int ctrl_cur;
        int disp_freq_l;   // local displacement frequency
        double disp_perml_l;        // fraction per ML
        int disp_freq_s;   // surface displacement frequency
        double disp_perml_s;        // fraction per ML
        unsigned int disp_deep_s; // surface layer depth
        int disp_freq_f;   // full displacement frequency
        double disp_perml_f;        // fraction per ML
        io_freq_t io_freq;
    };

private:

    MPI_Comm comm;
    // Core config
    core_config conf;
    // IO settings
    io_t io;
    // Simulation settings
    std::vector<control_t> ctrlv;
    std::vector<lPoint> defs;


    int parseConfig(std::string name);
    void bcast();
    void _init(MPI_Comm _comm, std::string &name);

public:

    void printfConfig();

    void setup_ctrl(double time){
        int cur = 0;
        int count = ctrlv.size();
        if( unlikely( count == 0 ) ){
            PQDEG_ABORT("No control vector!");
        }
        for(int i =0; i < count; i++){
            if( ctrlv[i].start < time )
                cur = i;
        }
        conf.ctrl_cur = cur;
    }

    static void initialize(std::string &name);
    static qConfig &instance();

    inline qConfig(std::string &name)
    {
        _init(MPI_COMM_WORLD, name);
    }
    inline qConfig(MPI_Comm _comm, std::string &name){
        _init(_comm, name);
    }

    void print(std::stringstream & ss)
    {
        ss << "ParQDEG configuration:" <<
                "\n\tGx = " << Gx() << ", Gy = " << Gy() << ", Gz = " <<
                Gz() << std::endl;
        ss << "\tNx = " << Nx() << ", Ny = " << Ny() << std::endl;
    }

    // Access to lattise parameters

    inline coord_t Gx()
    {
        return conf._G[x];
    }

    inline coord_t Gy()
    {
        return conf._G[y];
    }

    inline coord_t Gz()
    {
        return conf._G[z];
    }

    inline coord_t Nx()
    {
        return conf._N[0];
    }

    inline coord_t Ny()
    {
        return conf._N[1];
    }

    inline double mlSize()
    {
        return (float)conf._G[0] * conf._G[1] / 8.0;
    }

    // Simulation parameters

    inline unsigned int stepSize()
    {
        return conf._stepSize;
    }

    double simTime()
    {
        return conf.simTime;
    }

    double simAmount()
    {
        return conf.simAmount;
    }

    inline io_freq_t ioFreq(){
        return conf.io_freq;
    }

    int initLayer()
    {
        return conf.zSurf;
    }

    float defectF()
    {
        // TODO: Once stable change sqrt(3) to calculated value!
        static double sqrt3 = sqrt(3.0);
        return (5 * 1.5 * 11800) / sqrt3 ;
        //return (5 * 1.5 * 11800) / sqrt(3.0) /* 1.732050808*/;
    }

    inline double A()
    {
        return (48.5 * 1.36e-10 * 1.36e-10 / 1.6e-19 * 11800 / 8.0);
    }

    inline float B()
    {
        return 38 * 1.36e-10 * 1.36e-10 / 1.6e-19 * 11800 / 8.0;
    }

    inline float eps()
    {
        return 0.04;
    }

    inline float T()
    {
        control_t c = ctrlv[conf.ctrl_cur];
        return c.temp;
    }

    inline float E1()
    {
        return 0.35 * 11800;
    }

    inline float E2()
    {
        return 0.15 * 11800;
    }

    inline float p0()
    {
        return 1e13;
    }

    inline unsigned int defectNum()
    {
        return defs.size();
    }

    inline lPoint getDefect(int n)
    {
        if( n >= defs.size() ) {
            PQDEG_ABORT("Wrong defect #%d requested, total count is %u",
                    n + 1, (unsigned int)defs.size())
        }
        return defs[n];
    }

    inline double depProb()
    {
        control_t c = ctrlv[conf.ctrl_cur];
        return c.speed * conf._G[0] * conf._G[1] / 8.0;
    }
    
    inline char depType()
    {
        control_t c = ctrlv[conf.ctrl_cur];
        return c.type;
    }

    inline unsigned int localDispFreq(){
        return conf.disp_freq_l;
    }

    inline unsigned int localDispPerML(){
        return conf.disp_perml_l * mlSize();
    }

    inline unsigned int surfDispFreq(){
        return conf.disp_freq_s;
    }

    inline unsigned int surfDispPerML(){
        return conf.disp_perml_s * mlSize();
    }

    inline unsigned int surfDispDepth(){
        return conf.disp_deep_s;
    }

    inline unsigned int fullDispFreq(){
        return conf.disp_freq_f;
    }

    inline unsigned int fullDispPerML(){
        return conf.disp_perml_f * mlSize();
    }

    inline std::string getBaseName(double time, double amount, int step){
        std::stringstream ret;
        if( io.diff == qConfig::time ){
            ret << io.pref << std::setiosflags(std::ios::fixed) << std::setprecision(2)
                << time;
        }else if( io.diff == qConfig::amount ){
            ret << io.pref << std::setiosflags(std::ios::fixed) << std::setprecision(2)
                << amount;
        }else{
            ret << io.pref << step;
        }
        return ret.str();
    }

    inline std::string getCkptName(double time, double amount, int step){
        std::stringstream ret;
        ret << getBaseName(time, amount, step) << io.suf;
        return ret.str();
    }

    inline std::string getTarName(double time, double amount, int step){
        std::stringstream ret;
        ret << getBaseName(time, amount, step) << ".tar";
        return ret.str();
    }

    inline std::string stat_fname(){
        return std::string(io.stat_fname);
    }

    inline bool load_file()
	{
        if( io.load ){
			return true;
		}
		return false;
	}

    inline std::string input_name()
    {
        if( io.load ){
            return io.input;
        }
        return std::string("");
    }

    inline bool compress()
    {
        if( io.compress ){
            return true;
        }
        return false;
    }

    inline int ckpt_per_tar()
    {
        return io.ckpt_per_tar;
    }

};

#endif	/* QCONFIG_H */
