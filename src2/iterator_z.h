/* 
 * File:   zxyiterator.h
 * Author: artpol
 *
 * Created on 14 Июнь 2013 г., 10:51
 */

#ifndef ITERATOR_Z_H
#define	ITERATOR_Z_H

#include <iterator_zyx.h>

class ziterator : public zyxiterator {
public:

    ziterator(const zyxiterator &op) : zyxiterator(op)
    {
    }

    void __init(lPoint _max)
    {
        map[0] = 2; // inner loop variable is z
        map[1] = 1; // middle loop is y
        map[2] = 0; // outermost loop is x
        for( int i = 0; i < 2; i++ ) {
            offs_s[i] = 0;
            max[i] = _max.get_v()[i] + 1;
            x[i] = _max.get_v()[i];
        }
        offs_s[map[2]] = x[map[2]];
        coord_t tmp = _start_1();
        offs_s[map[1]] = x[map[1]] - tmp;
        x[map[0]] = _start_0();
    }

    ziterator(lPoint _max) :
    zyxiterator(_max, lPoint(0, 0, 0))
    {
        __init(_max);
    }

    ziterator(coord_t _max[3]) :
    zyxiterator(_max, 0)
    {
        lPoint max(_max);
        __init(_max);
    }

    void init(lPoint _max)
    {
        lPoint ghost(0, 0, 0);
        ((zyxiterator*)this)->init(_max, ghost);
        __init(_max);
    }

    void init(coord_t _max[3])
    {
        ((zyxiterator*)this)->init(_max, 0);
        lPoint max(_max);
        __init(max);
    }

    ziterator &operator =(const zyxiterator &op)
    {
        zyxiterator *t = (zyxiterator*)this;
        *t = op;
        return *this;
    }

    inline bool operator==(ziterator r)
    {
        zyxiterator *lhs = this, *rhs = &r;
        return(*lhs == *rhs);
    }

    inline bool operator<(ziterator r)
    {
        zyxiterator *lhs = this, *rhs = &r;
        return(*lhs < *rhs);
    }

    inline bool operator>(ziterator r)
    {
        zyxiterator *lhs = this, *rhs = &r;
        return(*lhs > *rhs);
    }

    inline bool operator>=(ziterator r)
    {
        zyxiterator *lhs = this, *rhs = &r;
        return(*lhs >= *rhs);
    }

    inline ziterator end()
    {
        ziterator tmp = ((zyxiterator*)this)->end();
        return tmp;
    }

    inline ziterator begin()
    {
        ziterator tmp = ((zyxiterator*)this)->begin();
        return tmp;
    }
};

#endif	/* ZITERATOR_H */

