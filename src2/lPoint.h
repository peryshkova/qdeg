/* 
 * File:   latPoint.h
 * Author: artpol
 *
 * Created on 11 Март 2013 г., 17:10
 */

#ifndef LATPOINT_H
#define	LATPOINT_H

#include <string.h>
#include <qTypes.h>
#include <iostream>
#include "debug.h"

class lPoint;
inline static std::ostream& operator <<(std::ostream& o, lPoint& p);

class lPoint {
public:
    static const int dim = 3;
protected:
    coord_t v[dim];
public:

    lPoint()
    {
        for( int i = 0; i < dim; i++ )
            v[i] = 0;
    }

    lPoint(coord_t x, coord_t y, coord_t z)
    {
        v[0] = x;
        v[1] = y;
        v[2] = z;
    }

    lPoint(coord_t *_v)
    {
        memcpy(v, _v, vsize());
    }

    inline coord_t &x()
    {
        return v[0];
    }

    inline coord_t &y()
    {
        return v[1];
    }

    inline coord_t &z()
    {
        return v[2];
    }

    inline coord_t *get_v()
    {
        return v;
    }

    inline void set_v(coord_t *_v)
    {
        memcpy(v, _v, vsize());
    }

    inline coord_t vsize()
    {
        return sizeof(v);
    }

    void set(coord_t x, coord_t y, coord_t z)
    {
        v[0] = x;
        v[1] = y;
        v[2] = z;
    }

    inline lPoint operator+(lPoint rp)
    {
        lPoint ret;
        ret.x() = this->x() + rp.x();
        ret.y() = this->y() + rp.y();
        ret.z() = this->z() + rp.z();
        return ret;
    }

    inline bool operator==(lPoint rp)
    {
        return !memcmp(v, rp.v, sizeof(v));
    }

    inline lPoint operator-(lPoint rp)
    {
        lPoint tmp;
        tmp.x() = this->x() - rp.x();
        tmp.y() = this->y() - rp.y();
        tmp.z() = this->z() - rp.z();
        return tmp;
    }

    inline bool operator<(const lPoint rp) const
    {
        // compare z
        if( this->v[2] > rp.v[2] ) {
            return false;
        } else if( this->v[2] < rp.v[2] ) {
            return true;
        }

        // Compare y
        if( this->v[1] > rp.v[1] ) {
            return false;
        } else if( this->v[1] < rp.v[1] ) {
            return true;
        }

        if( this->v[0] >= rp.v[0] ) {
            return false;
        }
        return true;
    }

    inline bool operator!=(lPoint rp)
    {
        return !this->operator ==(rp);
    }

    inline lPoint mod(lPoint rp)
    {
        lPoint ret;
        ret.x() = this->x() % rp.x();
        ret.y() = this->y() % rp.y();
        ret.z() = this->z() % rp.z();
        return ret;
    }

    bool is_dlpoint()
    {
        coord_t x = this->x();
        coord_t y = this->y();
        coord_t z = this->z();
        return (x % 2 == z % 2) && 
                ((y - (z % 2) + 2 * ((z / 2 + x / 2) % 2)) % 4 == 0);
    }

    bool is_correct(lPoint max)
    {
        coord_t x = this->x();
        coord_t y = this->y();
        coord_t z = this->z();
        bool positive = (x >= 0) && (y >= 0) && (z >= 0);
        bool inside = x < max.x() && y < max.y() && z < max.z();
//        DPRINTF(DBG_TRACE, "positive=%d, inside=%d, is_dlpoint=%d",
//                positive, inside, is_dlpoint());
        if( !(positive && inside && is_dlpoint()) )
        {
            lPoint tmp = *this;
            int rank;
            MPI_Comm_rank(MPI_COMM_WORLD,&rank);
            std::cout << "[" << rank << "] Point " << tmp <<
                         " not correct" << std::endl;
#ifdef DEBUG_ON
            static int hang = 1;
            debug_hang(hang);
#endif
        }
        return positive && inside && is_dlpoint();
    }
};

inline static std::ostream& operator <<(std::ostream& o, lPoint& p)
{
    o << "(" << p.x() << "," << p.y() << "," << p.z() << ")";
    return o;
}

#endif	/* LATPOINT_H */
