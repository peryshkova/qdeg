/* 
 * File:   configuration.h
 * Author: artpol
 *
 * Created on 6 Февраль 2013 г., 11:02
 */

#ifndef NODECONFIG_H
#define	NODECONFIG_H

#include <pLattice.h>
#include <qTypes.h>

#include <dPoint.h>

class nConfig {
public:
    static const int Nconfig = 65536; // 2^16 - 1
private:
    bool good_conf[Nconfig];
    uchar n1_conf[Nconfig];
    uchar n2_conf[Nconfig];
public:

    static nConfig &instance();

    nConfig();
    ushort set_config(lPoint p, pLattice<atom_info> &l, bool debug = false);
    ushort set_config_wout(lPoint &p, lPoint &excl,pLattice<atom_info> &l);
    inline ushort set_config(dPoint p, pLattice<atom_info> &l, bool debug = false){
        return set_config(p.local(),l, debug);
    }
    inline ushort set_config_wout(dPoint &p, dPoint &excl,pLattice<atom_info> &l)
    {
        return set_config_wout(p.local(),excl.local(),l);
    }

    uchar n1_config(ushort conf)
    {
        return n1_conf[conf];
    }

    uchar n2_config(ushort conf)
    {
        return n2_conf[conf];
    }

    bool good_config(ushort conf)
    {
        return good_conf[conf];
    }
    void config2array(int* nb_type, unsigned short _conf);
    unsigned short array2config(int* nb_type);
};



#endif	/* CONFIGURATION_H */

