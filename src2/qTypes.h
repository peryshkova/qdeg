/* 
 * File:   qTypes.h
 * Author: artpol
 *
 * Created on 28 Май 2013 г., 10:14
 */

#ifndef QTYPES_H
#define	QTYPES_H

#include <string>
#include <mpi.h>

typedef unsigned long ulong;
typedef unsigned int uint;
typedef unsigned char uchar;
typedef unsigned short ushort;
typedef int coord_t;
const MPI_Datatype mpi_coord_t = MPI_INT;

enum dumpTypes {
    CHAR, SHORT, INT, LONG, FLOAT, DOUBLE
};

class atomType {
public:
    enum {None = 0, Si = 1, Ge = 2};
};

static inline std::string type2str(char type)
{
    std::string s;
    if(type == atomType::None)
        s = "None";
    else if(type == atomType::Si)
        s = "Si";
    else
        s = "Ge";
    return s;
}

typedef struct {
    float x;
    float y;
    float z;
} displ_t;

struct atom_info {
    float Edef;
    float Edef_pa;

    displ_t B0;
    
    unsigned short config;
    char type;

    struct {
        char x;
        char y;
        char z;
    } defect_f;
};

struct jump_info {
    char jnum; //  number of allowed jumps from described node
    unsigned short int jmap; //  bitmap of allowed jumps
};

#endif	/* QTYPES_H */

