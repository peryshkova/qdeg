/* 
 * File:   decomp.h
 * Author: artpol
 *
 * Created on 11 Март 2013 г., 16:01
 */

#ifndef DECOMP_H
#define	DECOMP_H

#include <debug.h>
#include <mpi.h>
#include <stdlib.h>
#include <cartmesh.h>
#include <lPoint.h>
#include <qConfig.h>

class decomp {
public:
    static const uint ecube = ECUBE; // Elementary cube dimensions
    static const uint n = 2; // dimensions
    typedef char relmap_t[3][3];
private:
    cartmesh<n> _mesh;
    MPI_Comm bcomm;

    // lattice configuration
    coord_t lcubes[lPoint::dim]; // lattice cubes in each dimension
    coord_t z_axe;

    // decomposition
    coord_t offs[n], blksz[n], gwidth;

    // decomposition of data
    void distribute(int cur, coord_t max, coord_t data, coord_t &blk,
            coord_t &off);

    inline int offset(int i)
    {
        return offs[i] * ecube;
    }

    inline int blocksize(int i)
    {
        return blksz[i] * ecube;
    }

    inline int _ghost(int dim)
    {
        return gwidth * ecube * (_mesh[dim] > 1);
    }

public:

    static decomp &instance();

    decomp()
    {
    }

    decomp(MPI_Comm &comm, uint _gw = 2)
    {
        init(comm, _gw);
    }

    void init(MPI_Comm c, uint _gw = 2);
    void glob2local(lPoint g, lPoint &l, bool verbose = false);
    void local2glob(lPoint l, lPoint &g);
    void nbr_rankmap(int ranks[3][3]);
    bool is_local(lPoint p);
    bool is_internal(lPoint p);
    bool is_neighbor(lPoint p, bool verbose = false);
    bool is_related(lPoint p, relmap_t &map);

    inline bool is_related(lPoint p)
    {
        relmap_t map;
        return is_related(p, map);
    }

    bool is_neighbor_no(lPoint p, int rank);
    void nbr_map(lPoint p, relmap_t nmap);
    void comm_map(relmap_t rmap, relmap_t nmap, relmap_t cmap);
    bool comm_pattern(lPoint p, relmap_t map);
    void outputMap(relmap_t map, std::string prefix);

    cartmesh<n> &mesh()
    {
        return _mesh;
    }

    int operator [](int idx) {
        return _mesh[idx];
    }

    inline MPI_Comm comm()
    {
        return _mesh.comm();
    }

    int dim()
    {
        return n;
    }

    inline lPoint goffset()
    {
        return lPoint(offset(0), offset(1), 0);
    }

    inline lPoint gdims()
    {
        coord_t t[lPoint::dim] = {0};
        for( int i = 0; i < n; i++ )
            t[i] = lcubes[i] * ecube;
        return lPoint(t);
    }

    coord_t gdim_x()
    {
        return lcubes[0] * ecube;
    }

    coord_t gdim_y()
    {
        return lcubes[1] * ecube;
    }

    coord_t gdim_z()
    {
        return lcubes[2] * ecube;
    }

    inline lPoint gcubs()
    {
        coord_t t[lPoint::dim] = {0};
        for( int i = 0; i < lPoint::dim; i++ )
            t[i] = lcubes[i];
        return lPoint(t);
    }

    coord_t gcub_x()
    {
        return lcubes[0];
    }

    coord_t gcub_y()
    {
        return lcubes[1];
    }

    inline lPoint ldims()
    {
        coord_t t[lPoint::dim] = {0};
        for( int i = 0; i < n; i++ )
            t[i] = blksz[i] * ecube;
        t[n] = lcubes[n] * ecube;
        return lPoint(t);
    }

    coord_t ldim_x()
    {
        return blksz[0] * ecube;
    }

    coord_t ldim_y()
    {
        return blksz[1] * ecube;
    }

    coord_t ldim_z()
    {
        return lcubes[2] * ecube;
    }

    inline lPoint lcubs()
    {
        coord_t t[lPoint::dim] = {0};
        for( int i = 0; i < n; i++ )
            t[i] = blksz[i];
        t[n] = lcubes[n];
        return lPoint(t);
    }

    coord_t lcub_x()
    {
        return blksz[0];
    }

    coord_t lcub_y()
    {
        return blksz[1];
    }

    coord_t lcub_z()
    {
        return lcubes[2];
    }

    inline lPoint ghosts()
    {
        coord_t t[lPoint::dim] = {0};
        for( int i = 0; i < n; i++ )
            t[i] = gwidth * ecube * (_mesh[i] > 1);
        t[n] = 0;
        return lPoint(t);
    }

    inline coord_t ghostx()
    {
        return gwidth * ecube * (_mesh[0] > 1);
    }

    inline coord_t ghosty()
    {
        return gwidth * ecube * (_mesh[1] > 1);
    }

    inline coord_t ghostz()
    {
        return 0;
    }

    inline lPoint ldims_wgh()
    {
        coord_t t[lPoint::dim] = {0};
        for( int i = 0; i < n; i++ )
            t[i] = blksz[i] * ecube + 2 * _ghost(i);
        t[n] = ldim_z();
        return lPoint(t);
    }

    inline coord_t ldimwgh_x()
    {
        return ldim_x() + 2 * ghostx();
    }

    inline coord_t ldimwgh_y()
    {
        return ldim_y() + 2 * ghosty();
    }

    inline coord_t ldimwgh_z()
    {
        return ldim_z() + 2 * ghostz();
    }

    inline lPoint lcubswgh()
    {
        coord_t t[lPoint::dim] = {0};
        for( int i = 0; i < n; i++ )
            t[i] = blksz[i] + 2 * _ghost(i) / ecube;
        t[n] = lcub_z();
        return lPoint(t);
    }

    inline coord_t lcubwgh_x()
    {
        return lcub_x() + 2 * ghostx() / ecube;
    }

    inline coord_t lcubwgh_y()
    {
        return lcub_y() + 2 * ghosty() / ecube;
    }

    inline coord_t lcubwgh_z()
    {
        return lcub_z() + 2 * ghostz() / ecube;
    }

    void check()
    {
        DPRINTF(1, "gwidth = %d, lcubes[0] = %d, lcubes[1] = %d\n"
                "blksz[0] = %d, blksz[1] = %d, offs[0] = %d, offs[1] = %d",
                gwidth, lcubes[0], lcubes[1], blksz[0], blksz[1], offs[0], offs[1]);
    }

};


#endif	/* DECOMP_H */

