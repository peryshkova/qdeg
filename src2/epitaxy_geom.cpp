#include <epitaxy.h>
#include <dPoint.h>
#include <iterator_z.h>

void epitaxy_w::set_config(lPoint p, bool verbose)
{
    static nConfig &cfg = nConfig::instance();
    nodparam(p).config = cfg.set_config(p,nodparam);
}

dPoint epitaxy_w::deposit_gen(sPlan::pRecord &r)
{
    static decomp &dcmp = decomp::instance();
    static nConfig &cfg = nConfig::instance();
    static rGen &gen = rGen::instance();

    // Generate deposition point
    coord_t x = gen.rand01() * dcmp.ldim_x() + dcmp.ghostx();
    coord_t y = gen.rand01() * dcmp.ldim_y() + dcmp.ghosty();
    // Fix x to form correct dimond lattice coordinates
    coord_t x1 = ((x % 4) / 2)*2 + y % 2;
    x = (x / 4) * 4 + x1;

    lPoint loc(x, y, dcmp.ldim_z());

    // TODO: remove debug
        std::stringstream ss;
        ss << "Base point is " << loc;
        DPRINTF(DBG_DISAB,"%s", ss.str().c_str());
    //-------------------------

    ziterator it(loc);
    lPoint p = *(it.end());
    for(it = it.end(); it >= it.begin(); it--) {
       if(nodparam(*it).type == atomType::None) {
            p = *it;
        }
    }

    dPoint gp;
    gp.local(p);

#ifdef DEBUG_ON
    {
        std::stringstream ss;
        ss << gp << std::endl;
        lPoint lp = *(it.end()), lp1 = *(it.begin());
        ss << "Start point: " << lp  << ", end = " << lp1;
        ss << ", start >= end: " << ( it.end() >= it.begin() ) << std::endl;
        // get closer to final position
        for(it = it.end(); it >= it.begin(); it--) {
            lp = *it;
            ss << "atom" << lp << " = " << type2str(nodparam(*it).type) << std::endl;
        }
        ss << "Place under the surface is " << gp;
        ushort conf = cfg.set_config(gp, nodparam);
        if(!cfg.good_config(conf)) {
            ss << "Cannot deposit directly. Will explore area from " << p << ":"
               << std::endl;
        }
        DPRINTF(DBG_DISAB,"%s",ss.str().c_str());
    }
#endif

    ushort conf = cfg.set_config(gp, nodparam);
    if(!cfg.good_config(conf)) {
        std::vector<lPoint> good_cfg;
        int dx, dy;
        if((p.x() % ECUBE) < 2) {
            dx = -1;
        } else {
            dx = 0;
        }
        if((p.y() % ECUBE) < 2) {
            dy = -1;
        } else {
            dy = 0;
        }

        p.x() = (p.x() / ECUBE + dx) * ECUBE;
        p.y() = (p.y() / ECUBE + dy) * ECUBE;

        // Will explore the layer of 2x2 elementary cubes in XY 
        lPoint ecmax(8, 8, 4);
        zyxiterator ecit(ecmax);
        for(; ecit <= ecit.end(); ecit++) {
            lPoint ckp = p + (*ecit);
            // Init Z-iterator to go down till first not None node
            it.init(ckp);
            lPoint p1 = *(it.end());
            for(it = it.end(); it >= it.begin(); it--) {
                lPoint tmp = *it;
                if(nodparam(tmp).type == atomType::None) {
                    p1 = tmp;
                }
            }
            conf = cfg.set_config(p1, nodparam);
            if((nodparam(p1).type == atomType::None) && cfg.good_config(conf)) {
                good_cfg.push_back(p1);
            }
        }



#ifdef DEBUG_ON
        {
            std::stringstream ss;
            for(ecit = ecit.begin(); ecit <= ecit.end(); ecit++) {
                lPoint ckp = p + (*ecit);
                ss << ckp << " ";
                // Init Z-iterator to go down till first not None node
                it.init(ckp);
                lPoint p1 = *(it.end());
                for(it = it.end(); it >= it.begin(); it--) {
                    lPoint tmp = *it;
                    ss << tmp;
                    if(nodparam(tmp).type == atomType::None) {
                        p1 = tmp;
                        // DEBUG
                        conf = cfg.set_config(p1, nodparam);
                        if(cfg.good_config(conf)) {
                            ss << "+";
                        } else {
                            ss << "*";
                        }
                        ss << " ";

                    }
                }
            }
            ss << std::endl;
            DPRINTF(DBG_DISAB, "%s", ss.str().c_str());
        }
#endif
        if(!good_cfg.size()) {
            std::stringstream abort_buf;
            abort_buf << "ERROR: cannot find the place for " << p;
            PQDEG_ABORT("%s", abort_buf.str().c_str())
        } else {
            int i = rGen::instance().random_(good_cfg.size());
            p = good_cfg[i];
        }
    }
    
    gp.local(p);

#ifdef DEBUG_ON
    {
        std::stringstream ss;
        ss.str("");
        ss << "Deposit into: " << gp;
        DPRINTF(DBG_DISAB,"%s",ss.str().c_str());
    }
#endif
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    std::cout << "Deposit into " << rank << ": " << gp.glob() << std::endl;

    return gp;
}

void epitaxy_w::deposit(sPlan::pRecord &r)
{
    static int deposited = 0;
    static nConfig &cfg = nConfig::instance();
    static qConfig &qcfg = qConfig::instance();

    std::list<dPoint> l, l1;
    std::list<dPoint>::iterator it;
    dPoint gp(lPoint(r.p1));

    if( gp.is_local() || gp.is_related() ) {
#ifdef DEBUG_ON
        std::stringstream ss;
        ss << "atom " << gp.glob() << " = " << type2str(qcfg.depType()) <<
                ". Deposited = " << ++deposited;
        DPRINTF(DBG_TRACE,"%s",ss.str().c_str());
#endif
        nodparam(gp).type = qcfg.depType();
        state.deposit_related++;
        if( gp.is_local()){
            zproj[gp.local().z()]++;
            zyproj[gp.local().z()][gp.local().y()]++;
            state.deposit_local++;
            state.deposit_since_ckpt++;
        }
    }

    neighbors_t nbs;
    lPoint bounds = decomp::instance().gdims();
    nodparam.neighbors_glob(gp, nbs);
    for(int dir = 0; dir < dir_number; dir++) {
        dPoint ngp(lPoint(nbs.x[dir], nbs.y[dir], nbs.z[dir]));

        if(!ngp.is_local())
            continue;
        lPoint p = ngp.local();
        set_config(p);
        if(unlikely( nodparam(p).type != atomType::None &&
                     !cfg.good_config(nodparam(p).config))) {
            dPoint gp = *it;
            std::stringstream ss;
            ss << "Error!!! bad config of atom " << gp << " "
               << std::hex << nodparam(p).config << std::dec << std::endl;
            ss << "Neighbors:" << std::endl << "Absolut\t\tRelative\tType" << std::endl;
            neighbors_t nbs;
            int factor = nodparam.neighbors(p, nbs);
            for(int dir = 0; dir < dir_number; dir++) {
                lPoint nb(nbs.x[dir], nbs.y[dir], nbs.z[dir]);
                int type = nodparam(nb).type;
                ss << nb << "\t";
                lPoint d = nb - p;
                ss << d << "\t";
                ss << type << std::endl;
            }
            PQDEG_ABORT("Abort.\n%s",ss.str().c_str());
        }
        l.push_back(ngp);
    }

    for(it = l.begin(); it != l.end(); it++) {
        calc_B0((*it).local());
        nodparam.neighbors_glob(*it, nbs);
        for(int dir = 0; dir < dir_number; dir++) {
            dPoint ngp(lPoint(nbs.x[dir], nbs.y[dir], nbs.z[dir]));
            if(ngp.is_local())
                l1.push_back(ngp);
        }
    }
    l.merge(l1);

    if(gp.is_local()){
        axyz(gp.local()); // set ax,ay,az for new atom
        l.push_back(gp);
    }

    for(it = l.begin(); it != l.end(); it++) {
        dPoint gp = *it;
        set_jump_info(gp.local());
        if( nodejmp(gp.local()).jnum == 0 ){
            calc_jump_prob(gp.local());
        }
        upd_q[gp.local()] = true;
    }
}
