#include <epitaxy.h>
#include <iomanip>
#include <iostream>
#include <sstream>

#include <nConfig.h>
#include <iterator_zyx.h>
#include <iterator_yx.h>
#include <iterator_x.h>
#include <mpi.h>

#include "pLattice.h"

// ----------------------- epitaxy realisation -------------------------------//

epitaxy::epitaxy()
{
    setupComm();
    sPlan::init(bComm, role);
    memset(&state, 0, sizeof(state));
    step = 0;
}

sPlan::branch_role epitaxy::role_detect()
{
    qConfig &cfg = qConfig::instance();
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    if( rank == 0) {
        DPRINTF(DBG_TRACE, "I am master");
        return sPlan::Master;
    } else if( rank <= cfg.Nx() * cfg.Ny()) {
        DPRINTF(DBG_TRACE, "I am worker");
        return sPlan::Worker;
    }
    DPRINTF(DBG_TRACE, "This process would not be used for simulation");
    return sPlan::Unused;
}

void epitaxy::setupComm()
{
    qConfig &cfg = qConfig::instance();
    role = role_detect();

    // exit Unused
    bool flag = (role == sPlan::Unused);
    MPI_Comm_split(MPI_COMM_WORLD,flag,0,&bComm);

    if(flag) {
        DPRINTF(DBG_TRACE, "Exiting ...");
        MPI_Finalize();
        exit(0);
    }

    // Split master and workers
    flag = (role == sPlan::Master);
    MPI_Comm_split(bComm,flag,0,&wComm);
    int brank, bsize;
    int wrank, wsize;
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(bComm, &brank);
    MPI_Comm_size(bComm, &bsize);
    MPI_Comm_rank(wComm, &wrank);
    MPI_Comm_size(wComm, &wsize);

    DPRINTF(DBG_TRACE, "\n\tWORLD rank/size = %d/%d"
            "\n\tBASE rank/size = %d/%d"
            "\n\tWORKERs rank/size = %d/%d",
            rank, size, brank, bsize, wrank, wsize);
}

epitaxy *epitaxy::inststance_create()
{
    qConfig &cfg = qConfig::instance();
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if( rank == 0)
        return new epitaxy_r();
    return new epitaxy_w();
}

void epitaxy::mainLoop()
{
    DPRINTF(DBG_TRACE, "Start");
    qConfig &cfg = qConfig::instance();
    decomp &dcmp = decomp::instance();

    init_stat_save();

    for( ; ; step++) {
        DPRINTF(DBG_TRACE, "Step %ld cur time = %lf, total time = %lf",
                step, state.time, cfg.simTime());

        if( role == sPlan::Master && ( (step+1)%10000) == 0 ){
            printf("------------- step %lu, time = %lf ------------\n", step, state.time );
        }

        // Do main job
        float dt = mc_step(step);

        if( state.deposit_total/cfg.mlSize() > cfg.simAmount() ){
            break;
        }

        // Update time and controls
        state.time += dt;
        cfg.setup_ctrl(state.time);

        // Perform random displacements
        if( timeForFullShake(step) ){
            DPRINTF(DBG_TRACE,"Do a full shake");
            doFullShake();
        }else if( timeForSurfShake(step) ){
            DPRINTF(DBG_TRACE,"Do surface shake");
            doSurfShake();
        }else if( timeToLocalShake(step) ){
            DPRINTF(DBG_TRACE,"Do a local shake");
            doLocalShake();
        }

        // Perform a checkpoint
        if( timeToCheckpoint(step) ){
            DPRINTF(DBG_TRACE,"save a checkpoint");
            saveCkpt(step);
        }
    }
}

bool epitaxy::timeToCheckpoint(int step){
    static int stat_save_counter = 0;

    static qConfig &cfg = qConfig::instance();
    if( ((step + 1 ) % cfg.ioFreq().sync) == 0)
    {
        int size;
        MPI_Comm_size(bComm, &size);
        unsigned int deposited;
        double in_data[] = {0, 0};
        int count = sizeof(in_data)/sizeof(in_data[0]);
        double *out_data = NULL;
        if( role == sPlan::Worker ){
            in_data[0] = state.jump_to;
            in_data[1] = state.deposit_local;
        }else if( role == sPlan::Master ){
            out_data = (double*)calloc(size*count, sizeof(double));
        }

        MPI_Allreduce(&state.deposit_since_ckpt, &deposited, 1, MPI_UNSIGNED, MPI_SUM, bComm);
        MPI_Gather(in_data, count, MPI_DOUBLE, out_data, count, MPI_DOUBLE, 0, bComm);
        if( role == sPlan::Master ){
            for(int i=1; i < size; i++){
                state.jump_vector[i] = out_data[2*i];
                state.depo_vector[i] = out_data[2*i + 1];
            }
            free(out_data);
        }

        // statistics save
        if( cfg.depProb() > 0 ){
            unsigned int treshold = cfg.mlSize()*cfg.ioFreq().amount;
            if( deposited > (treshold/10)*stat_save_counter ){
                stat_save_counter++;
                stat_save(step);
            }
            if( deposited > treshold ){
                stat_save_counter = 0;
                return true;
            }
        }
    }

    if( cfg.depProb() == 0 ){
        if( (step + 1) % (cfg.ioFreq().steps/10) == 0 ){
            stat_save(step);
        }
        if( (step + 1) % cfg.ioFreq().steps == 0 ){
            return true;
        }
    }
    return false;
}
// ---------------------- epitaxy_r realisation ------------------------------//

void epitaxy_r::initData()
{
    static qConfig &cfg = qConfig::instance();
    int size;

    create_decomp_MPI();

    if( cfg.load_file() ){
        read_lattice();
        doALLShake();
    }

    MPI_Comm_size(bComm, &size);
    state.depo_vector = (double*)calloc(size, sizeof(double));
    state.jump_vector = (double*)calloc(size, sizeof(double));

    if( cfg.compress() ){
        comp.init( cfg.ckpt_per_tar() );
    }
	saveCkpt(step++);
}

double epitaxy_r::mc_step(int count)
{
    static qConfig &cfg = qConfig::instance();
    static rGen &rgen = rGen::instance();
    double jprob_tot, prob_tot, dt, glob_state[2];
    static double time_passed = 0;
    static double cum_prob = 0;

    jprob_tot = sPlan::instance().prepare_draft();
    
    prob_tot = jprob_tot + cfg.depProb();
    dt = -log(rgen.rand01()) / prob_tot;

    glob_state[0] = dt;
    glob_state[1] = state.deposit_total;
    MPI_Bcast(&glob_state, 2, MPI_DOUBLE, 0, bComm);
    return dt;
}

void epitaxy_r::init_stat_save()
{
    static qConfig &cfg = qConfig::instance();
    std::string fn = cfg.stat_fname();
    FILE *out = fopen(fn.c_str(), "w");
    int size, tmp = 0;
    MPI_Comm_size(bComm, &size);
    int *xstart = (int*)malloc(sizeof(int)*size);
    int *ystart = (int*)malloc(sizeof(int)*size);
    int *xend = (int*)malloc(sizeof(int)*size);
    int *yend = (int*)malloc(sizeof(int)*size);

    if( out == NULL ){
        PQDEG_ABORT("Cannot open file %s for writing", cfg.stat_fname().c_str());
    }

    MPI_Gather(&tmp,1,MPI_INT,xstart,1,MPI_INT,0,MPI_COMM_WORLD);
    MPI_Gather(&tmp,1,MPI_INT,ystart,1,MPI_INT,0,MPI_COMM_WORLD);
    MPI_Gather(&tmp,1,MPI_INT,xend,1,MPI_INT,0,MPI_COMM_WORLD);
    MPI_Gather(&tmp,1,MPI_INT,yend,1,MPI_INT,0,MPI_COMM_WORLD);

    for(int i=1; i<size; i++){
        fprintf(out, "rank%d: [%d,%d]x[%d,%d]\n",
                i, xstart[i],xend[i],ystart[i],yend[i]);
    }
    fprintf(out,"\n");
    fclose(out);
    free(xstart);
    free(xend);
    free(ystart);
    free(yend);
}

void epitaxy_r::stat_save(int step)
{
    static qConfig &cfg = qConfig::instance();
    std::string fn = cfg.stat_fname();
    FILE *out = fopen(fn.c_str(), "a");
    if( out == NULL ){
        PQDEG_ABORT("Cannot open file %s for writing", cfg.stat_fname().c_str());
    }

    int size;
    MPI_Comm_size(bComm, &size);
    double depo_total = 0, jump_total = 0;
    for(int i = 1; i<size; i++){
        jump_total += state.jump_vector[i];
        depo_total += state.depo_vector[i];
    }
    state.deposit_total = depo_total;
    state.jump_to = jump_total;
    fprintf(out, "%d: time=%lf, deposited = %lf, jumps = %lf\n",
            step, state.time, depo_total, jump_total);

    for(int i = 1; i<size; i++){
        fprintf(out,"\trank%d:\tdepositions: %lf,\tjumps: %lf\n",
                i, state.depo_vector[i], state.jump_vector[i]);
    }
    fclose(out);
}

void epitaxy_r::saveCkpt(int step)
{
    static int count = 0;
    static qConfig &cfg = qConfig::instance();
    static decomp &dcmp = decomp::instance();
    int bsize;
    MPI_Comm_size(bComm,&bsize);
    decomp_info info[bsize];
    decomp_info * info_2d[cfg.Ny()][cfg.Nx()];
    int rank_2d[cfg.Ny()][cfg.Nx()];
    unsigned char layer[cfg.Gy()][cfg.Gx()];
    //displ_t displs[cfg.Gy()][cfg.Gx()];
    MPI_Datatype mtypes[cfg.Ny()][cfg.Nx()];
    //MPI_Datatype dsp_types[cfg.Ny()][cfg.Nx()]; // Remove offsets

    int max_Z = 0;
    MPI_Gather(info, 1, decomp_info_MPI, info, 1, decomp_info_MPI, 0, bComm);
    for(int i = 1; i < bsize; i++) {
        info_2d[info[i].y][info[i].x] = info + i;
        rank_2d[info[i].y][info[i].x] = i;
        if(max_Z < info[i].lz) {
            max_Z = info[i].lz;
        }
    }
    int surf_Z = max_Z;
    for(int i = 1; i < bsize; i++) {
        if(surf_Z > info[i].lz0) {
            surf_Z = info[i].lz0;
        }
    }

    int tmpbuf[2] = { max_Z, surf_Z };
    MPI_Bcast(tmpbuf, 2, MPI_INT, 0, bComm);

    unsigned int tmp = 0, points_count = 0, points_count_chk = 0;
    MPI_Reduce(&tmp, &points_count, 1, MPI_UNSIGNED, MPI_SUM, 0, bComm);
    DPRINTF(DBG_DISAB, "total point count = %u", points_count);

    int offsets[cfg.Ny()][cfg.Nx()];
    int offs = 0, offs_cur = 0;
    for(int i = 0; i < cfg.Ny(); i++) {
        int offs1 = 0;
        for(int j = 0; j < cfg.Nx(); j++) {
            offsets[i][j] = offs1 + offs_cur;
            offs1 += info_2d[i][j]->lx;
            offs += info_2d[i][j]->lx * info_2d[i][j]->ly;
        }
        offs_cur += offs;
        offs = 0;
    }

    MPI_Datatype mpi_displ_t;
    MPI_Type_vector(1, 3, 0, MPI_FLOAT, &mpi_displ_t);
    MPI_Type_commit(&mpi_displ_t);

    for(int i = 0; i < cfg.Ny(); i++) {
        for(int j = 0; j < cfg.Nx(); j++) {
            // Atom types
            MPI_Type_vector(info_2d[i][j]->ly, info_2d[i][j]->lx, cfg.Gx(), MPI_UNSIGNED_CHAR,&mtypes[i][j]);
            MPI_Type_commit(&mtypes[i][j]);

            /* TODO: Remove in future, once be obvious that it's unneeded
            // Atom displacements
            MPI_Type_vector(info_2d[i][j]->ly, info_2d[i][j]->lx, cfg.Gx(), mpi_displ_t, &dsp_types[i][j]);
            MPI_Type_commit(&dsp_types[i][j]);
            */
        }
    }

#ifdef DEBUG_ON
    for(int i = 0; i < cfg.Ny(); i++) {
        for(int j = 0; j < cfg.Nx(); j++) {
            DPRINTF(DBG_DISAB, "%d datatype conf: offset=%d, %dx%dx%d",
                    rank_2d[i][j], offsets[i][j],
                    info_2d[i][j]->lx, info_2d[i][j]->ly, info_2d[i][j]->lz);
        }
    }
#endif

    std::string fname = cfg.getCkptName(state.time, state.deposit_total/cfg.mlSize(), step);
    FILE *fp = fopen(fname.c_str(), "w");
    fprintf(fp, "%u 0.0\n\n", points_count);

    for(int z = 0; z < max_Z; z++) {
        DPRINTF(DBG_DISAB, "------------------------ z=%d -------------------------------", z);
        memset(layer, 0, sizeof (layer));
        //memset(displs, 0, sizeof (layer));

        for(int i = 0; i < cfg.Ny(); i++) {
            for(int j = 0; j < cfg.Nx(); j++) {
                MPI_Status status;
                MPI_Recv((char*)layer + offsets[i][j], 1, mtypes[i][j], rank_2d[i][j], 0, bComm, &status);
                //MPI_Recv((displ_t*)displs + offsets[i][j], 1, dsp_types[i][j], rank_2d[i][j], 0, bComm, &status);

#ifdef DEBUG_ON
                // Output temporal results of layer content
                DPRINTF(DBG_SHORT, "Recv from %d (%d,%d):", rank_2d[i][j], i, j);
                for(int k = 0; k < cfg.Gy(); k++) {
                    std::stringstream ss;
                    for(int l = 0; l < cfg.Gx(); l++) {
                        ss << (int)layer[k][l] << " ";
                    }
                    //DPRINTF(DBG_TRACE, "%s", ss.str().c_str());
                }
#endif
            }
        }


        for(int y = 0; y < cfg.Gy(); y++) {
            for(int x = 0; x < cfg.Gx(); x++) {
                if(layer[y][x]) {
                    /*
                    double ax = displs[y][x].x;
                    double ay = displs[y][x].y;
                    double az = displs[y][x].z;
                    */
                    fprintf(fp, "%d %d %d %d\n", layer[y][x], x, y, z);
                    //fprintf(fp, "%d %d %20.15lf %d %20.15lf %d %20.15lf\n", layer[y][x], x, ax, y, ay, z, az);
                    points_count_chk++;
                }
            }
        }
    }

    if(points_count != points_count_chk) {
        PQDEG_ABORT("Count = %d, count_check = %d!", points_count, points_count_chk);
    }
    fclose(fp);

    if( cfg.compress() ){
        std::string tarname = cfg.getTarName(state.time, state.deposit_total/cfg.mlSize(), step);
        comp.notify(tarname.c_str(), fname.c_str());
    }
}

int epitaxy_r::read_lattice()
{
    debug_hang(0);
    static qConfig &cfg = qConfig::instance();
    std::string fname = cfg.input_name();
    static decomp &dcmp = decomp::instance();
    int bsize;
    MPI_Comm_size(bComm,&bsize);
    decomp_info info[bsize];
    decomp_info * info_2d[cfg.Ny()][cfg.Nx()];
    int rank_2d[cfg.Ny()][cfg.Nx()];
    unsigned char layer[cfg.Gy()][cfg.Gx()];
    //displ_t displs[cfg.Gy()][cfg.Gx()];
    MPI_Datatype mtypes[cfg.Ny()][cfg.Nx()];
    //MPI_Datatype dsp_types[cfg.Ny()][cfg.Nx()];

    MPI_Gather(info, 1, decomp_info_MPI, info, 1, decomp_info_MPI, 0, bComm);
    for(int i = 1; i < bsize; i++) {
        info_2d[info[i].y][info[i].x] = info + i;
        rank_2d[info[i].y][info[i].x] = i;
    }

    int offsets[cfg.Ny()][cfg.Nx()];
    int offs = 0, offs_cur = 0;
    for(int i = 0; i < cfg.Ny(); i++) {
        int offs1 = 0;
        for(int j = 0; j < cfg.Nx(); j++) {
            offsets[i][j] = offs1 + offs_cur;
            offs1 += info_2d[i][j]->lx;
            offs += info_2d[i][j]->lx * info_2d[i][j]->ly;
        }
        offs_cur += offs;
        offs = 0;
    }

    MPI_Datatype mpi_displ_t;
    MPI_Type_vector(1, 3, 0, MPI_FLOAT, &mpi_displ_t);
    MPI_Type_commit(&mpi_displ_t);

    for(int i = 0; i < cfg.Ny(); i++) {
        for(int j = 0; j < cfg.Nx(); j++) {
            // Atom types
            MPI_Type_vector(info_2d[i][j]->ly, info_2d[i][j]->lx, cfg.Gx(), MPI_UNSIGNED_CHAR,&mtypes[i][j]);
            MPI_Type_commit(&mtypes[i][j]);
            /* Atom displacements
            MPI_Type_vector(info_2d[i][j]->ly, info_2d[i][j]->lx, cfg.Gx(), mpi_displ_t, &dsp_types[i][j]);
            MPI_Type_commit(&dsp_types[i][j]);
            */
        }
    }

    unsigned int count = 0, count_max;
    FILE *fp = fopen(fname.c_str(), "r");
    if( fp == 0 ){
        return -1;
    }
    int ret = fscanf(fp, "%u %*f", &count_max);
    if( ret != 1 ){
        fclose(fp);
        PQDEG_ABORT("Bad input file format!");
        return -1;
    }

    bool flag = true, next_layer = true;
    int z = 0;
    int next = 1;
    memset(layer, 0, sizeof (layer));
    //memset(displs, 0, sizeof (displs));
    while( next ) {
        DPRINTF(DBG_DISAB, "------------------------ z=%d -------------------------------", z);
        int type;
        int x, y, z1;
        //double ax, ay,az;
        //int ret = fscanf(fp, "%d %d %lf %d %lf %d %lf\n", &type, &x, &ax, &y, &ay, &z1, &az);
        int ret = fscanf(fp, "%d %d %d %d", &type, &x, &y, &z1);

        if( ret != 4 ){
            if( ret == EOF && count == count_max ){
                next = 0;
            } else {
                fclose(fp);
                PQDEG_ABORT("Count = %d, count_check = %d!", count_max, count);
            }
        }

        if( !next || z1 != z ){
            MPI_Bcast(&next, 1, MPI_INT, 0, bComm);
            // Send current layer to workers
            for(int i = 0; i < cfg.Ny(); i++) {
                for(int j = 0; j < cfg.Nx(); j++) {
                    MPI_Status status;
                    // Send next layer Confirm that we continue
                    MPI_Send((char*)layer + offsets[i][j], 1, mtypes[i][j], rank_2d[i][j], 0, bComm);
                    //MPI_Send((displ_t*)displs + offsets[i][j], 1, dsp_types[i][j], rank_2d[i][j], 0, bComm);
                }
            }
            // Move to the next layer
            z++;
            memset(layer, 0, sizeof (layer));
            //memset(displs, 0, sizeof (displs));
        }

        if( next ){
            // Check coordinates before applying
            if( !( y < cfg.Gy() && x < cfg.Gx() && z1 < cfg.Gz() ) ){
                PQDEG_ABORT("Input lattice don't fit the one in the config:"
                            " Lmax=(%u,%u,%u), point=(%u,%u,%u)",
                            cfg.Gx()-1, cfg.Gy()-1, cfg.Gz()-1, x, y, z1);
            }
            layer[y][x] = type;
            /*
            displs[y][x].x = ax;
            displs[y][x].y = ay;
            displs[y][x].z = az;
            */
            count++;
        }
    }

    unsigned int tmp = 0, tmp1 = 0;
    MPI_Reduce(&tmp1, &tmp, 1, MPI_UNSIGNED, MPI_SUM, 0, bComm);
    if( count != count_max || tmp != count ) {
        PQDEG_ABORT("Count = %d, count_check = %d, tmp = %d!", count_max, count, tmp);
    }

    fclose(fp);
}

void epitaxy_r::saveCkpt_wgh()
{
    static int count = 0;
    static qConfig &cfg = qConfig::instance();
    static decomp &dcmp = decomp::instance();
    int bsize;
    MPI_Comm_size(bComm, &bsize);
    decomp_info info[bsize];
    decomp_info * info_2d[cfg.Ny()][cfg.Nx()];
    int rank_2d[cfg.Ny()][cfg.Nx()];
    unsigned char layer[cfg.Gy()+16*cfg.Ny()][cfg.Gx()+16*cfg.Nx()];
    MPI_Datatype mtypes[cfg.Ny()][cfg.Nx()];
    int max_Z = 0;

    MPI_Gather(info, 1, decomp_info_MPI, info, 1, decomp_info_MPI, 0, bComm);
    for(int i = 1; i < bsize; i++) {
        info_2d[info[i].y][info[i].x] = info + i;
        rank_2d[info[i].y][info[i].x] = i;
        if(max_Z < info[i].lz) {
            max_Z = info[i].lz;
        }
    }

    max_Z += 0;
    MPI_Bcast(&max_Z, 1, MPI_INT, 0, bComm);
    unsigned int tmp = 0, points_count = 0, points_count_chk = 0;
    MPI_Reduce(&tmp, &points_count, 1, MPI_UNSIGNED, MPI_SUM, 0,bComm);
    //DPRINTF(DBG_TRACE, "total point count = %u", points_count);

    int offsets[cfg.Ny()][cfg.Nx()];
    int offs = 0, offs_cur = 0;
    for(int i = 0; i < cfg.Ny(); i++) {
        int offs1 = 0;
        for(int j = 0; j < cfg.Nx(); j++) {
            offsets[i][j] = offs1 + offs_cur;
            offs1 += info_2d[i][j]->lx;
            offs += info_2d[i][j]->lx * info_2d[i][j]->ly;
        }
        offs_cur += offs;
        offs = 0;
    }

    for(int i = 0; i < cfg.Ny(); i++) {
        for(int j = 0; j < cfg.Nx(); j++) {
            MPI_Type_vector(info_2d[i][j]->ly, info_2d[i][j]->lx, cfg.Gx()+16*cfg.Nx(),MPI_UNSIGNED_CHAR,&mtypes[i][j]);
            MPI_Type_commit(&mtypes[i][j]);

        }
    }

#ifdef DEBUG_ON
    for(int i = 0; i < cfg.Ny(); i++) {
        for(int j = 0; j < cfg.Nx(); j++) {
            DPRINTF(DBG_TRACE, "%d datatype conf: offset=%d, %dx%dx%d",
                    rank_2d[i][j], offsets[i][j],
                    info_2d[i][j]->lx, info_2d[i][j]->ly, info_2d[i][j]->lz);
        }
    }
#endif

    char fname[1024];
    sprintf(fname, "B%d.txt", count);
    FILE *fp = fopen(fname, "w");
    fprintf(fp, "%u 0.0\n\n", points_count);
    for(int z = 0; z < max_Z; z++) {
        //DPRINTF(DBG_SHORT, "------------------------ z=%d -------------------------------", z);
        // Receive layer
        memset(layer, 0, sizeof (layer));
        for(int i = 0; i < cfg.Ny(); i++) {
            for(int j = 0; j < cfg.Nx(); j++) {
                MPI_Status status;
                MPI_Recv((char*)layer + offsets[i][j], 1, mtypes[i][j], rank_2d[i][j], 0, bComm, &status);
#ifdef DEBUG_ON
                DPRINTF(DBG_SHORT, "Recv from %d (%d,%d):", rank_2d[i][j], i, j);
                for(int k = 0; k < cfg.Gy()+16*cfg.Ny(); k++) {
                    std::stringstream ss;
                    for(int l = 0; l < cfg.Gx()+16*cfg.Nx(); l++) {
                        if( layer[k][l] )
                            ss << (int)layer[k][l] << " ";
                        else
                            ss << "  ";
                    }
                    DPRINTF(DBG_TRACE, "%s", ss.str().c_str());
                }
#endif
            }
        }


        // Check layer
        int maxX = cfg.Gx()+16*cfg.Nx();
        int maxY = cfg.Gy()+16*cfg.Ny();
        std::stringstream ss;
        for(int i = 0; i < cfg.Nx(); i++){
            int xb = offsets[0][i];
            int xs1 = ((xb - 8) + maxX) % maxX;
            int xs2 = ((xb + 8) + maxX) % maxX;
            ss << "Compare x=" << xs1 << "..." << xb-1 << " and x="
               << xb << "..." <<  xs2 << std::endl;
            for(int y=0; y<maxY;y++){
                int x1,x2;
                for(x1=xs1, x2=xs2; x1-xs1 < 8; x1++, x2++){
                    int type1 = layer[y][x1];
                    int type2 = layer[y][x2];
                    if( type2 != type1 ){
                        ss << "\t(" << x1 << "," << y << "," << z << ") = " << type1 << " <> ("
                           << x2 << "," << y << "," << z << ") = " << type2 << std::endl;
                    }
                    if( layer[y][x1] != layer[y][x2]){
                        ss << "ERROR! Not equal!" << std::endl;
                        PQDEG_ABORT("%s",ss.str().c_str());
                    }
                }
            }
        }
//        DPRINTF(DBG_TRACE,"%s",ss.str().c_str());

        ss.str("");
        for(int i = 0; i < cfg.Ny(); i++){
            int yb = offsets[i][0];
            int ys1 = ((yb - 8) + maxY) % maxY;
            int ys2 = ((yb + 8) + maxY) % maxY;
            ss << "Compare y=" << ys1 << "..." << ((yb - 1) + maxY) % maxY << " and y="
               << yb << "..." <<  ys2 << std::endl;
            for(int x=0; x<maxX;x++){
                int y1,y2;
                for(y1=ys1, y2=ys2; y1-ys1 < 8; y1++, y2++){
                    int type1 = layer[y1][x];
                    int type2 = layer[y2][x];
                    ss << "\t(" << x << "," << y1 << "," << z << ") = " << type1 << " ? ("
                       << x << "," << y2 << "," << z << ") = " << type2 << std::endl;
                    if( layer[y1][x] != layer[y2][x]){
                        ss << "ERROR! Not equal!" << std::endl;
                        PQDEG_ABORT("%s",ss.str().c_str());
                    }
                }
            }
        }
//        DPRINTF(DBG_TRACE,"%s",ss.str().c_str());

        // Output layer
        for(int y = 0; y < cfg.Gy()+16*cfg.Ny(); y++) {
            for(int x = 0; x < cfg.Gx()+16*cfg.Nx(); x++) {
                if(layer[y][x]) {
                    fprintf(fp, "%d %d %f %d %f %d %f\n", layer[y][x], x, 0.0, y, 0.0, z, 0.0);
                    points_count_chk++;
                }
            }
        }
    }

    if(points_count != points_count_chk) {
        PQDEG_ABORT("Count = %d, count_check = %d!", points_count, points_count_chk);
    }
    fclose(fp);
    count++;
}

// ---------------------- epitaxy_r realisation ------------------------------//

void epitaxy_w::createTypes()
{
    char tname1[] = "aType", tname2[] = "jType";

    int ai_blens[] = {2, 3, 1, 1, 3};
    MPI_Aint ai_displ[] = {offsetof(atom_info, Edef),
        offsetof(atom_info, B0), offsetof(atom_info, config),
        offsetof(atom_info, type), offsetof(atom_info, defect_f)};
    MPI_Datatype ai_types[] = {MPI_FLOAT, MPI_FLOAT,
        MPI_UNSIGNED_SHORT, MPI_CHAR, MPI_CHAR};
    MPI_Type_create_struct(5, ai_blens, ai_displ, ai_types, &atom_info_MPI);
    MPI_Type_commit(&atom_info_MPI);
    MPI_Aint lb, ai_ext;
    MPI_Type_get_extent(atom_info_MPI, &lb, &ai_ext);
    if(ai_ext != sizeof (atom_info)) {
        PQDEG_ABORT("Size of atom_info and extent of atom_info_MPI do no match!");
    }

    int ji_blens[] = {1, 1};
    MPI_Aint ji_displ[] = {offsetof(jump_info, jnum),offsetof(jump_info, jmap)};
    MPI_Datatype ji_types[] = {MPI_CHAR, MPI_UNSIGNED_SHORT};
    MPI_Type_create_struct(2, ji_blens, ji_displ, ji_types, &jump_info_MPI);
    MPI_Type_commit(&jump_info_MPI);
    MPI_Aint ji_ext;
    MPI_Type_get_extent(jump_info_MPI, &lb, &ji_ext);
    if(ji_ext != sizeof (jump_info)) {
        PQDEG_ABORT("Size of jump_info and extent of jump_info_MPI do no match!");
    }

    int displ_blens[] = {3};
    MPI_Aint displ_displ[] = {0};
    MPI_Datatype displ_types[] = {MPI_FLOAT };
    MPI_Type_create_struct(1, displ_blens, displ_displ, displ_types, &displ_MPI);
    MPI_Type_commit(&displ_MPI);
    MPI_Aint displ_ext;
    MPI_Type_get_extent(displ_MPI, &lb, &displ_ext);
    if(displ_ext != sizeof (displ_t)) {
        PQDEG_ABORT("Size of atom_info and extent of displ_MPI do no match!");
    }

    DPRINTF(DBG_TRACE, "New types:"
            "\n\tsizeof(atom_info)=%u, extent(atom_info_MPI)=%u"
            "\n\tsizeof(jump_info)=%u, extent(jump_info_MPI)=%u"
            "\n\tsizeof(displ_info)=%u, extent(displ_info_MPI)=%u",
            (unsigned int)sizeof(atom_info), (unsigned int)ai_ext,
            (unsigned int)sizeof(jump_info), (unsigned int)ji_ext,
            (unsigned int)sizeof(displ_t), (unsigned int)displ_ext);
}

void epitaxy_w::initData()
{
    qConfig &cfg = qConfig::instance();
    decomp &dcmp = decomp::instance();
    nConfig &aconf = nConfig::instance();

    dcmp.init(wComm, 2);
    create_decomp_MPI();

    DPRINTF(DBG_TRACE, "Calculate service matrixes: AA, AA_, BB");
    calc_AA();
    DPRINTF(DBG_TRACE, "AA - ready");
    calc_AA_();
    DPRINTF(DBG_TRACE, "AA_ - ready");
    calc_BB();
    DPRINTF(DBG_TRACE, "BB - ready");


    DPRINTF(DBG_TRACE, "Prepare MPI types");
    createTypes();

    DPRINTF(DBG_TRACE, "Init diamond lattice structures");

    nodparam.init(atom_info_MPI);
    nodejmp.init(jump_info_MPI);
    jmprob.init(MPI_FLOAT);
    displs.init(displ_MPI);

    DPRINTF(DBG_TRACE, "Prepare projection arrays helping to reduce amount of "
            "work when performing lattice traversal");
    lPoint max = dcmp.ldims_wgh();
    lPoint gh = dcmp.ghosts();

    std::vector< std::vector<unsigned int> > _zyproj(max.z(),std::vector<unsigned int>(max.y(),0));
    std::vector<unsigned long> _zproj(max.z(),0);
    std::vector<unsigned long> _zjump(max.z(),0);
    zyproj = _zyproj;
    zproj = _zproj;
    zjump = _zjump;

    if( !cfg.load_file() ){
        DPRINTF(DBG_TRACE, "Fill %d initial layers of lattice", cfg.initLayer());
        int z;
        state.cur_max_Z = cfg.initLayer();
        state.cur_surf_Z = cfg.initLayer();
        zyxiterator it(dcmp.ldims_wgh());
        lPoint off(0, 0, dcmp.ldimwgh_z() - cfg.initLayer() - 1);
        it.eoffsets(off);
        for(; it <= it.end(); it++) {
            memset(&nodparam(*it), 0, sizeof (nodparam(*it)));
            nodparam(*it).type = atomType::Si;
            dPoint gp;

            gp.local(*it);
            if( gp.is_local() ){
                zproj[(*it).z()]++;
                zyproj[(*it).z()][(*it).y()]++;
            }

//#ifdef DEBUG_ON
//            {
//                // TODO: Quite verbose debug, disable after check
//                std::stringstream ss;
//                lPoint lp = *it;
//                ss << "\tSet " << lp << " to Si, zyproj[" << lp.z() <<
//                      "][" << lp.y() << "] = " << zyproj[(*it).z()][(*it).y()] <<
//                                                                                  "\tzproj[" << lp.z() << "] = " << zproj[(*it).z()];
//                //DPRINTF(DBG_TRACE, "%s", ss.str().c_str());
//            }
//#endif
        }

        DPRINTF(DBG_TRACE, "Fill empty layers of lattice");
        off = lPoint(0, 0, 0);
        it.eoffsets(off);
        off.z() = cfg.initLayer()+1;
        it.soffsets(off);
        for(; it <= it.end(); it++) {
            memset(&nodparam(*it), 0, sizeof (nodparam(*it)));
            nodparam(*it).type = atomType::None;
        }
    } else {
        read_lattice();
    }


    DPRINTF(DBG_TRACE, "Set atoms configuration");
    zyxiterator it;
    for(it = travInit(); it <= it.end(); it++) {
        static nConfig &ncfg = nConfig::instance();
        lPoint p = *it;
        set_config(p);

        if(unlikely( nodparam(p).type != atomType::None &&
                     !ncfg.good_config(nodparam(p).config))) {
            dPoint gp = *it;
            std::stringstream ss;
            ss << "Error!!! bad config of atom " << gp << " "
               << std::hex << nodparam(p).config << std::dec << std::endl;
            ss << "Neighbors:" << std::endl << "Absolut\t\tRelative\tType" << std::endl;
            neighbors_t nbs;
            int factor = nodparam.neighbors(p, nbs);
            for(int dir = 0; dir < dir_number; dir++) {
                lPoint nb(nbs.x[dir], nbs.y[dir], nbs.z[dir]);
                int type = nodparam(nb).type;
                ss << nb << "\t";
                lPoint d = nb - p;
                ss << d << "\t";
                ss << type << std::endl;
            }
            PQDEG_ABORT("Abort.\n%s",ss.str().c_str());
        }
    }

    DPRINTF(DBG_TRACE, "Check atoms configuration");
    for(it = travInit(); it <= it.end(); it++) {
        if(unlikely(nodparam(*it).type != atomType::None
                    && !aconf.good_config(nodparam(*it).config))) {
            std::stringstream ss;
            lPoint lp = *it;
            ss << "Error!! atom " << lp << " has wrong initial conf: t=" <<
                    nodparam(*it).type;
            PQDEG_ABORT("%s", ss.str().c_str());
        }
    }
    DPRINTF(DBG_TRACE, "atoms.sync()");
    nodparam.sync();

    DPRINTF(DBG_TRACE, "Set jump configuration");
    for(it = travInit(); it <= it.end(); it++) {
        set_jump_info(*it);
    }
    
    for(it = travInit(); it <= it.end(); it++) {
        if( jmprob(*it) > 0 ){
            zjump[(*it).z()]++;
        }
    }

    for(it = travInit(); it <= it.end(); it++) {
        if( jmprob(*it) > 0 ){
            zjump[(*it).z()]++;
        }
    }

    DPRINTF(DBG_TRACE, "Synchronize jump configuration");
    nodejmp.sync();
    jmprob.sync();

    DPRINTF(DBG_TRACE, "Setup defects");
    set_defects();

    DPRINTF(DBG_TRACE, "Calculate B0 impact at each node");
    for(it = travInit(); it <= it.end(); it++) {
        calc_B0(*it);
    }

    DPRINTF(DBG_TRACE, "Synchronize atoms");
    nodparam.sync();

    DPRINTF(DBG_TRACE, "Update Defect energy");
    for(it = travInit(); it <= it.end(); it++) {
        calc_Edef(*it);
        calc_jump_prob(*it);
    }

    if( cfg.load_file() ){
        doALLShake();
    }

    saveCkpt(step++);
}

void epitaxy_w::plan_refine(double jprob)
{
    static sPlan &pl = sPlan::instance();
    for(int i = 0; i < pl.size(); i++) {
        if(!pl[i].status)
            continue;
        dPoint gp(pl[i].p1);
        if( pl[i].type == sPlan::Depo ) {
            gp = deposit_gen(pl[i]);
            memcpy(pl[i].p1, gp.glob().get_v(), gp.glob().vsize());
        } else if(pl[i].type == sPlan::Jump) {
            jump_gen(jprob, pl[i]);
        }
    }

    DPRINTF(DBG_DISAB, "Plan after refine:");
    //pl.dbg_print(&nodparam);
}

double epitaxy_w::mc_step(int count)
{
    static qConfig &cfg = qConfig::instance();
    static sPlan &pl = sPlan::instance();

    double jprob = sum_jump_prob();
    sPlan::instance().prepare_draft(jprob);
    double dt, glob_state[2];
    MPI_Bcast(glob_state, 2, MPI_DOUBLE, 0,bComm);
    dt = glob_state[0];
    state.deposit_total = glob_state[1];

    plan_refine(jprob);

    pl.prepare_final(&nodparam);

    for(int i = 0; i < pl.size(); i++) {
        if(pl[i].status != sPlan::Active)
            continue;
        DPRINTF(DBG_TRACE, "Proces plan record #%d", i);
        if(pl[i].type == sPlan::Depo) {
            deposit(pl[i]);
        } else if(pl[i].type == sPlan::Jump) {
            jump(pl[i], pl);
        }
    }

    // Perform all nesessary updates of affected nodes
    update_Edef_cached();

    return dt;
}

coord_t epitaxy_w::count_max_Z()
{
    static decomp &dcmp = decomp::instance();
    coord_t z, z_ret = 0;
    for(z = 0; z < dcmp.ldim_z(); z++){
        if( zproj[z] != 0 ){
            z_ret = z;
        }
        DPRINTF(DBG_DISAB,"zproj[%d] = %lu",z,zproj[z]);
    }
    return (++z_ret); // return strict bound: any_pint.z < z_ret;
}

coord_t epitaxy_w::count_surf_Z()
{
    static decomp &dcmp = decomp::instance();
    static qConfig &cfg = qConfig::instance();
    static int ml_size = dcmp.ldim_x() * dcmp.ldim_y() / 8;

    coord_t z;
    for(z = 0; zproj[z] != 0 && z < dcmp.ldim_z(); z++){
        DPRINTF(DBG_DISAB,"zproj[%d] = %lu, mlsize = %d\n", z, zproj[z], ml_size );
        if( zproj[z] != ml_size ){
            break;
        }
    }
    return (--z);
}

unsigned int epitaxy_w::count_points(int max_Z)
{
    static decomp &dcmp = decomp::instance();
    unsigned int sum = 0;
    for(int z = 0; z < max_Z; z++) {
        sum += zproj[z];
    }
    return sum;
}

unsigned int epitaxy_w::count_mobile_points()
{
    static decomp &dcmp = decomp::instance();
    unsigned int sum = 0;
    for(int z = 4; zproj[z] != 0 && z < dcmp.ldim_z(); z++) {
        sum += zproj[z];
    }
    return sum;
}

lPoint epitaxy_w::point_by_no(unsigned no)
{
    lPoint lp = point_by_no_opt(no);
#ifdef DEBUG_ON
    point_by_no_opt_verbose(no);
    lPoint lp2 = point_by_no_full(no);
    if( lp != lp2 ){
        std::stringstream ss;
        ss << "ERROR: no=" << no << ", lp=" << lp
           << ", lp2=" << lp2;
        DPRINTF(DBG_TRACE,"%s",ss.str().c_str());
    }
#endif
    return lp;
}

void epitaxy_w::point_by_no_opt_verbose(unsigned no)
{
    static decomp &dcmp = decomp::instance();
    coord_t x, y, z;
    coord_t sum = 0;
    std::stringstream ss;
    // 1. Find z coord
    ss << "Search Z coordinate:" << std::endl;
    for(z=4; z < dcmp.ldim_z() && ((sum + zproj[z]) <= no); z++){
        sum += zproj[z];
        ss << "sum=" << sum << " zproj[" << z << "]=" << zproj[z] << std::endl;
    }
    ss << "Z = " << z << std::endl;

    // 2. find y coord
    ss << "Search Y coordinate:" << std::endl;
    for(y=0; y < dcmp.ldimwgh_y() && ((sum + zyproj[z][y]) <= no); y++){
        sum += zyproj[z][y];
        ss << "sum=" << sum << " zyproj[" << z << "][" << y << "]=" << zyproj[z][y] << std::endl;
    }
    ss << "Z = " << z << ", Y = " << y << " zyproj=" << zyproj[z][y] << std::endl;

    // 3. find x coord
    lPoint max = dcmp.ldims_wgh();
    lPoint gh = dcmp.ghosts();
    max.z() = z;
    max.y() = y;
    xiterator it(max,gh);
    lPoint ret = *(it.begin());
    lPoint end = *(it.end());
    ss << "start = " << ret << ", end = " << end << std::endl;
    for(it = it.begin(); it <= it.end() && sum != (no + 1); it++){
        lPoint lp = *it;
        ss << lp << std::endl;
        if( nodparam(*it).type != atomType::None ){
            sum++;
            ret = *it;
            ss << "ret = " << ret << std::endl;
        }
    }
    ss << "#" << no << ", p = " << ret;
    DPRINTF(DBG_DISAB,"%s",ss.str().c_str());
}

lPoint epitaxy_w::point_by_no_opt(unsigned no)
{
    static decomp &dcmp = decomp::instance();
    coord_t x, y, z;
    coord_t sum = 0;
    // 1. Find z coord
    for(z=4; z < dcmp.ldim_z() && ((sum + zproj[z]) <= no); z++){
        sum += zproj[z];
    }

    // 2. find y coord
    for(y=0; y < dcmp.ldimwgh_y() && ((sum + zyproj[z][y]) <= no); y++){
        sum += zyproj[z][y];
    }

    // 3. find x coord
    lPoint max = dcmp.ldims_wgh();
    lPoint gh = dcmp.ghosts();
    max.z() = z;
    max.y() = y;
    xiterator it(max,gh);
    lPoint ret = *(it.begin());
    lPoint end = *(it.end());
    for(it = it.begin(); it <= it.end() && sum != (no + 1); it++){
        lPoint lp = *it;
        if( nodparam(*it).type != atomType::None ){
            sum++;
            ret = *it;
        }
    }
    return ret;
}

lPoint epitaxy_w::point_by_no_full(unsigned no)
{
    static decomp &dcmp = decomp::instance();
    lPoint max = dcmp.ldims_wgh();
    lPoint gh = dcmp.ghosts();
    zyxiterator it(max,gh);
    it.soffsets(lPoint(0,0,4));
    coord_t sum = 0;
    lPoint ret = *(it.begin());
    for(it=it.begin(); it <= it.end(); it++){
        if( nodparam(*it).type != atomType::None){
            sum++;
            ret = *it;
            if( sum == (no + 1) ){
                break;
            }

        }
    }

    return ret;
}

void epitaxy_w::saveCkpt(int step)
{
    static qConfig &cfg = qConfig::instance();
    static decomp &dcmp = decomp::instance();
    decomp_info info;

    state.deposit_since_ckpt = 0;
    int crds[2];
    dcmp.mesh().coords(crds);

    info.lx = dcmp.ldim_x();
    info.ly = dcmp.ldim_y();
    info.x = crds[0];
    info.y = crds[1];
    int max_Z = count_max_Z();
    int surf_Z = count_surf_Z();
    info.lz = max_Z;
    info.lz0 = surf_Z;

    DPRINTF(DBG_TRACE, "Send info");
    MPI_Gather(&info, 1, decomp_info_MPI, &info, 1, decomp_info_MPI, 0,bComm);
    int tmp[2];
    MPI_Bcast(tmp, 2, MPI_INT, 0,bComm);
    max_Z = tmp[0];
    surf_Z = tmp[1];
    // Save this information for surface sync in future
    state.cur_max_Z = max_Z;
    state.cur_surf_Z = surf_Z;
    DPRINTF(DBG_TRACE,"max_Z = %d, surf_Z = %d",max_Z, surf_Z);

    unsigned int points_cnt = 0;
    lPoint max = dcmp.ldims_wgh();
    max.z() = max_Z;
    zyxiterator it(max, dcmp.ghosts());

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

#ifdef DEBUG_ON
#if 0
    // Force the predefined error in the lattice description
    // to check that sanity check code really works
    if( step > 1000 && rank == 2)
    {
        fprintf(stdout,"%d: Fake the latice, step = %d\n", rank, step);
        int after = (1.0*rand())/RAND_MAX * count_points();
        fprintf(stderr,"%d: Fake after %d'th point\n", rank, after);
        int search = 0;
        lPoint fake;
        for(it = it.begin(); it <= it.end(); it++) {
            dPoint gp;
            gp.local(*it);
            if(nodparam(gp).type != atomType::None) {
                points_cnt++;
            }

            if( points_cnt > after ){
                search = 1;
            }

            if( search && nodparam(gp).type == atomType::None) {
                // fake atom
                nodparam(gp).type = atomType::Ge;
                fprintf(stderr,"%d: Fake point is g(%d,%d,%d), l(%d,%d,%d)\n",rank,
                        gp.glob().x(),gp.glob().y(), gp.glob().z(),
                        gp.local().x(),gp.local().y(), gp.local().z());
                fake = gp.local();
                break;
            }
        }
        points_cnt = 0;
    }
#endif
    // Output short lattice state
    {
        lPoint strt = *(it.begin());
        lPoint end = *(it.end());
        std::stringstream ss;
        ss << "Start: " << strt << " end: " << end << " max=" << max;
        DPRINTF(DBG_TRACE, "max_Z = %d, surf_Z = %d, iter: %s",
                max_Z, surf_Z, ss.str().c_str());
    }

    // Recalculate amount of points in the lattice
    for(it = it.begin(); it <= it.end(); it++) {
        dPoint gp;
        gp.local(*it);
        if(nodparam(gp).type != atomType::None) {
            points_cnt++;
        }
    }

    // If we have a mismatch - try to find the (z,y) row that contains
    // unaccounted point to help the debug
    if(count_points(max_Z) != points_cnt) {
        int rank;
        MPI_Comm_rank(MPI_COMM_WORLD,&rank);
        int z;
        unsigned long int sum = 0;
        DPRINTF(DBG_TRACE, "%d: zproj is incorrect: count_points()=%d, points_cnt=%d, max_Z = %d\n",
                rank, count_points(max_Z), points_cnt, max_Z);

        {
            fprintf(stderr,"%d: Output etalon count:\n", rank);
            it = it.begin();
            int cur_z = (*it).z();
            int sum = 0;
            for(; it <= it.end(); it++) {
                dPoint gp;
                gp.local(*it);
                if( (*it).z() != cur_z ){
                    DPRINTF(DBG_TRACE, "%d: z = %d, etalon count = %d\n", rank, cur_z, sum);
                    cur_z = (*it).z();
                    sum = 0;
                }
                if(nodparam(gp).type != atomType::None) {
                    sum++;
                }
            }
            DPRINTF(DBG_TRACE, "%d: --------------------------------\n", rank);
        }


        points_cnt = 0;
        for(z=0; z <= max_Z+4; z++){
            sum = 0;
            lPoint max1 = dcmp.ldims_wgh();
            max1.z() = z;
            yxiterator it2(max1,dcmp.ghosts());
            for(it2 = it2.begin(); it2 <= it2.end(); it2++){
                dPoint gp;
                gp.local(*it2);
                if(nodparam(gp).type != atomType::None) {
                    sum++;
                }
            }
            DPRINTF(DBG_TRACE, "%d: z = %d, expect = %d, real = %d\n", rank, z, (int)zproj[z], (int)sum);
            if( zproj[z] != sum ){
                fprintf(stderr, "%d: z = %d, points count mismatch\n", rank, z);
                it2 = it2.begin();
                int cur_y = (*it2).y();
                sum = 0;
                for(; it2 <= it2.end(); it2++){
                    dPoint gp;
                    gp.local(*it2);
                    int y = (int)gp.local().y();
                    if( cur_y != y ){
                        if( zyproj[z][cur_y] != sum ){
                            DPRINTF(DBG_TRACE, "%d: z = %d, y = %d points mismatch: expect %d, have %d\n",
                                    rank, z, cur_y, (int)sum, zyproj[z][cur_y]);
                            {
                                int delay = 0; // change to 1 to hang for debug
                                while( delay ){
                                    sleep(1);
                                }
                                PQDEG_ABORT("exit.");
                            }
                        }
                        sum = 0;
                        cur_y = y;
                    }

                    if(nodparam(gp).type != atomType::None) {
                        sum++;
                    }
                }
            }
        }
    }

    // Check lattice points configuration
    for(it = it.begin(); it <= it.end(); it++) {
        dPoint gp;
        gp.local(*it);
        if(nodparam(gp).type != atomType::None) {
            // TODO: For debug purpose: check that goodness of nodes conf wasnt violated
            nConfig &cfg = nConfig::instance();
            if( gp.local().z() >=2 ){
                unsigned short conf = cfg.set_config(gp.local(), nodparam);
                if(conf != nodparam(gp).config) {
                    lPoint lp = *it;
                    std::stringstream ss;
                    ss << "conf=" << std::hex << conf << std::dec <<
                          ", atoms" << lp << "= " <<
                          std::hex << nodparam(gp).config;
                    DPRINTF(DBG_TRACE, "%s", ss.str().c_str());
                }
                if(!cfg.good_config(conf)) {
                    std::stringstream ss;
                    ss << "atom" << gp << " has bad config: " << std::hex << conf;
                    DPRINTF(DBG_TRACE, "%s", ss.str().c_str());
                }
            }
            // ------------------------------ end debug code -----------------------------
        }
    }

#endif

    points_cnt = count_points(max_Z);
    MPI_Reduce(&points_cnt, NULL, 1, MPI_UNSIGNED, MPI_SUM, 0,bComm);

    MPI_Datatype mpi_displ_t;
    MPI_Type_vector(1, 3, 0, MPI_FLOAT, &mpi_displ_t);
    MPI_Type_commit(&mpi_displ_t);

    for(int z = 0; z < max_Z; z++) {
        DPRINTF(DBG_DISAB, "Trace of checkpoint: z = %d", z);
        lPoint max = dcmp.ldims_wgh();
        max.z() = z;
        yxiterator it(max, dcmp.ghosts());
        unsigned char layer[info.ly][info.lx];
        //displ_t dsps[info.ly][info.lx];

        memset(layer, 0, sizeof (layer));
        std::stringstream ss;
        int count = 0;
        for(it = it.begin(); it <= it.end(); it++) {
            if( count > 0 ){
                ss << "\n\t";
            }
            count++;
            lPoint lp = *it;
            lp.y() -= dcmp.ghosty();
            lp.x() -= dcmp.ghostx();
            ss << "layer" << lp << ":";
            layer[lp.y()][lp.x()] = nodparam(*it).type;
            //dsps[lp.y()][lp.x()] = displs(*it);
            ss << " type=" << (int)nodparam(*it).type
               << " displs=(" << displs(*it).x << ","
               << displs(*it).y << "," << displs(*it).z << ")";
        }
        MPI_Send(layer, info.ly * info.lx, MPI_UNSIGNED_CHAR, 0, 0, bComm);
        //MPI_Send(dsps, info.ly * info.lx, mpi_displ_t, 0, 0, bComm);
        DPRINTF(DBG_DISAB,"send %s",ss.str().c_str());
    }

#ifdef DEBUG_ON
    for(it = travInit(); it <= it.end(); it++) {
        int bkp = nodejmp(*it).jnum;
        unsigned short conf = nodparam(*it).config;
        nConfig &cfg = nConfig::instance();
        nodparam(*it).config = cfg.set_config(*it, nodparam);
        set_jump_info(*it);
        if( bkp != nodejmp(*it).jnum ){
            dPoint gp;
            gp.local(*it);
            cfg.set_config(*it, nodparam, true);
            set_jump_info(*it, true);
            std::stringstream ss;
            lPoint p = *it;
            sleep(2);
            ss << "ERROR with " << gp << " old_jnum=" << bkp
               << ", new=" << (int)nodejmp(*it).jnum
               << std::hex << "; old_cfg=" << conf
               << ", new_cfg = " << nodparam(*it).config;
            PQDEG_ABORT("%s",ss.str().c_str());
        }
    }
#endif
}

void epitaxy_w::init_stat_save()
{
    static decomp &dcmp = decomp::instance();
    lPoint offs = dcmp.goffset();
    lPoint blks = dcmp.ldims();
    int tmp;

    tmp = offs.x();
    MPI_Gather(&tmp,1,MPI_INT,NULL,1,MPI_INT,0,MPI_COMM_WORLD);
    tmp = offs.y();
    MPI_Gather(&tmp,1,MPI_INT,NULL,1,MPI_INT,0,MPI_COMM_WORLD);
    tmp = offs.x() + blks.x() - 1;
    MPI_Gather(&tmp,1,MPI_INT,NULL,1,MPI_INT,0,MPI_COMM_WORLD);
    tmp = offs.y() + blks.y() - 1;
    MPI_Gather(&tmp,1,MPI_INT,NULL,1,MPI_INT,0,MPI_COMM_WORLD);
}

int epitaxy_w::read_lattice()
{
    static qConfig &cfg = qConfig::instance();
    static decomp &dcmp = decomp::instance();
    decomp_info info;
    std::stringstream ss;

    // Set lattice with zeroes
    zyxiterator it(dcmp.ldims_wgh());
    it.offsets(lPoint(0,0,0));
    DPRINTF(DBG_TRACE, "Clear all %d layers of lattice", cfg.initLayer());
    for(; it <= it.end(); it++) {
        memset(&nodparam(*it), 0, sizeof (nodparam(*it)));
        nodparam(*it).type = atomType::None;
        memset(&displs(*it), 0, sizeof (displs(*it)));
#ifdef DEBUG_ON
        // TODO: Quite verbose debug, disable after check
        std::stringstream ss;
        lPoint lp = *it;
        ss << "Set " << lp << " to None";
        //DPRINTF(DBG_TRACE, "%s", ss.str().c_str());
#endif
    }

    // Now read data from the master
    int crds[2];
    dcmp.mesh().coords(crds);
    info.lx = dcmp.ldim_x();
    info.ly = dcmp.ldim_y();
    info.x = crds[0];
    info.y = crds[1];
    info.lz = 0;
    info.lz0 = 0;
    DPRINTF(DBG_TRACE, "Send info");
    MPI_Gather(&info, 1, decomp_info_MPI, &info, 1, decomp_info_MPI, 0,bComm);

    MPI_Datatype mpi_displ_t;
    MPI_Type_vector(1, 3, 0, MPI_FLOAT, &mpi_displ_t);
    MPI_Type_commit(&mpi_displ_t);

    int next = 1;
    char layer[info.lx][info.ly];
    displ_t dsps[info.ly][info.lx];
    for(int z = 0; next; z++) {
        DPRINTF(DBG_DISAB, "Trace of checkpoint: z = %d", z);
        MPI_Status status;
        lPoint max = dcmp.ldims_wgh();
        max.z() = z;
        yxiterator it(max, dcmp.ghosts());
        unsigned char layer[info.ly][info.lx];
        //displ_t dsps[info.ly][info.lx];

        memset(layer, 0, sizeof (layer));
        // Confirm that we continue
        MPI_Bcast(&next, 1, MPI_INT, 0, bComm);
        MPI_Recv(layer, info.ly * info.lx, MPI_UNSIGNED_CHAR, 0, 0, bComm, &status);
        //MPI_Recv(dsps, info.ly * info.lx, mpi_displ_t, 0, 0, bComm, &status);
        DPRINTF(DBG_TRACE,"Recv %s",ss.str().c_str());

        for(it = it.begin(); it <= it.end(); it++) {
            lPoint lp = *it;
            lp.y() -= dcmp.ghosty();
            lp.x() -= dcmp.ghostx();
            nodparam(*it).type = layer[lp.y()][lp.x()];
            //displs(*it) = dsps[lp.y()][lp.x()];
            if( nodparam(*it).type != atomType::None ){
                zproj[(*it).z()]++;
                zyproj[(*it).z()][(*it).y()]++;
            }
        }
    }

    unsigned int points_cnt = count_points(count_max_Z());
    int rank;
    MPI_Comm_rank(bComm, &rank);
    MPI_Reduce(&points_cnt, NULL, 1, MPI_UNSIGNED, MPI_SUM, 0,bComm);
    nodparam.sync();
    displs.sync();
}

void epitaxy_w::saveCkpt_wgh()
{
    static qConfig &cfg = qConfig::instance();
    static decomp &dcmp = decomp::instance();
    decomp_info info;
    int max_Z;

    int crds[2];
    dcmp.mesh().coords(crds);

    info.lx = dcmp.ldimwgh_x();
    info.ly = dcmp.ldimwgh_y();
    info.x = crds[0];
    info.y = crds[1];
    int z = count_max_Z();
    info.lz = z;
    DPRINTF(DBG_DISAB, "Send info");
    MPI_Gather(&info, 1, decomp_info_MPI, &info, 1, decomp_info_MPI, 0, bComm);
    MPI_Bcast(&max_Z, 1, MPI_INT, 0, bComm);
    DPRINTF(DBG_TRACE, "max_Z = %d, local max_Z = %d", max_Z,count_max_Z());

    unsigned int points_cnt = 0;
    lPoint max = dcmp.ldims_wgh();
    max.z() = max_Z;
    zyxiterator it(max);

    for(it = it.begin(); it <= it.end(); it++) {
        if(nodparam(*it).type != atomType::None) {
//            // TODO: REMOVE!!!!
//            lPoint tmp(9,5,21);
//            dPoint dtmp;
//            dtmp.local(*it);
//            if( tmp != dtmp.glob() || !dtmp.is_local())
//            // TODO: REMOVE!!!!
            points_cnt++;
        }
    }

    MPI_Reduce(&points_cnt, NULL, 1, MPI_UNSIGNED, MPI_SUM, 0, bComm);

    for(int z = 0; z < max_Z; z++) {
        DPRINTF(DBG_DISAB, "Trace of checkpoint: z = %d", z);
        lPoint max = dcmp.ldims_wgh();
        max.z() = z;
        yxiterator it(max, lPoint(0,0,0));
        unsigned char layer[info.ly][info.lx];
        memset(layer, 0, sizeof (layer));
        std::stringstream ss;
        for(it = it.begin(); it <= it.end(); it++) {
            lPoint lp = *it;
            ss << "layer" << lp << std::endl;
//            // TODO: REMOVE!!!!
//            lPoint tmp(9,5,21);
//            dPoint dtmp;
//            dtmp.local(lp);
//            if( tmp != dtmp.glob() || !dtmp.is_local() ){
//            // TODO: REMOVE!!!!
            layer[lp.y()][lp.x()] = nodparam(*it).type;
//            // TODO: REMOVE!!!!
//            }
//            // TODO: REMOVE!!!!
        }
        MPI_Send(layer, info.ly * info.lx, MPI_UNSIGNED_CHAR, 0, 0, bComm);
        DPRINTF(DBG_DISAB,"send %s",ss.str().c_str());
    }
}

