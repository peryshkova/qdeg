#include <debug.h>
#include <pLattice.h>
#include <string>
#include <sPlan.h>
#include <rGen.h>
#include <mpi.h>

#include <epitaxy.h>

MPI_Comm create_bComm();
MPI_Comm create_wComm(MPI_Comm &bComm);

int main(int argc, char **argv)
{

    MPI_Init(&argc, &argv);

    init_debug(MPI_COMM_WORLD);

    std::string cfgfile = "config";
    qConfig::initialize(cfgfile);
    qConfig::instance().printfConfig();

    MPI_Comm bComm = create_bComm();
    MPI_Comm wComm = create_wComm(bComm);
    epitaxy *e = epitaxy::inststance_create();
    e->initData();
    e->mainLoop();

    MPI_Finalize();
    return 0;
}

MPI_Comm create_bComm()
{
    qConfig &cfg = qConfig::instance();
    bool flag;
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    flag = rank <= cfg.Nx() * cfg.Ny();
    MPI_Comm bComm;

    // TODO: error handling!
    MPI_Comm_split(MPI_COMM_WORLD,flag,0,&bComm);

    if(!flag) {
        DPRINTF(1, "I am exiting!");
        MPI_Finalize();
        exit(0);
    }
    return bComm;
}

MPI_Comm create_wComm(MPI_Comm &bComm)
{
    qConfig &cfg = qConfig::instance();
    int rank, size;
    MPI_Comm_rank(bComm, &rank);
    MPI_Comm_size(bComm, &size);
    bool flag = rank > 0;
    MPI_Comm wComm;
    // TODO: error handling!
    MPI_Comm_split(bComm,flag,0,&wComm);
    int wrank, wsize;
    MPI_Comm_rank(wComm, &wrank);
    MPI_Comm_size(wComm, &wsize);

    DPRINTF(1, "old rank = %d, new rank = %d, size = %d",
            rank, wrank, wsize);
    return wComm;
}
