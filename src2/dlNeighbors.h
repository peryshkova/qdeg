/* 
 * File:   neighbors.h
 * Author: artpol
 *
 * Created on 7 Август 2013 г., 16:27
 */

#ifndef NEIGHBORS_H
#define	NEIGHBORS_H

#include <lPoint.h>
#include <x86arch.h>
#include <sstream>


#define dir_number 16
static int dx_neighbor[dir_number] _sse_align = {1,   1, -1, -1, 0, 2, 2,  0,  2,  2,  0, -2, -2,  0, -2, -2}; // neighbors x-coordinates
static int dy_neighbor[dir_number] _sse_align = {1,  -1,  1, -1, 2, 0, 2, -2,  0, -2,  2,  0,  2, -2,  0, -2}; // neighbors y-coordinates
static int dz_neighbor[dir_number] _sse_align = {1,  -1, -1,  1, 2, 2, 0, -2, -2,  0, -2, -2,  0,  2,  2,  0}; // neighbors z-coordinates
static int pr_neighbor[dir_number] _sse_align = {-1, -1, -1, -1, 0, 0, 0,  1,  1,  1,  2,  2,  2,  3,  3,  3}; // previous node number

typedef struct {
    int x[dir_number] _sse_align;
    int y[dir_number] _sse_align;
    int z[dir_number] _sse_align;
} neighbors_t;

inline static void get_one_neighbor(int x, int y, int z, int dir,
        int &x2, int &y2, int &z2)
{
    int factor;
    if( z % 2 == 0 )
        factor = 1;
    else
        factor = -1;
    x2 = x + factor * dx_neighbor[dir];
    y2 = y + factor * dy_neighbor[dir];
    z2 = z + factor * dz_neighbor[dir];
}


inline void boundary(lPoint &p, lPoint b)
{ // изменить (x,y,z), если нужно, так чтобы попасть внутрь ящика

    if( unlikely(p.x() < 0) ) p.x() += b.x();
    if( unlikely(p.x() >= b.x()) ) p.x() -= b.x();
    if( unlikely(p.y() < 0) ) p.y() += b.y();
    if( unlikely(p.y() >= b.y()) ) p.y() -= b.y();
//    if( unlikely((z < 0) || (z >= Lz)) ) {
//        fprintf(stderr, "Error!!! z out of range\n");
//        abort();
//    }
}

//inline void boundary_v(neighbors_t &nbs, lPoint bnds) // изменить (x,y,z), если нужно, так чтобы попасть внутрь ящика
//{
//    static __m128i low_v = _mm_set1_epi32(0);
//    unsigned int Lx = bnds.x();
//    unsigned int Ly = bnds.y();
//    unsigned int Lz = bnds.z();
//    static __m128i upx_v = _mm_set_epi32(Lx, Lx, Lx, Lx);
//    static __m128i upy_v = _mm_set_epi32(Ly, Ly, Ly, Ly);
//    static __m128i upz_v = _mm_set_epi32(Lz, Lz, Lz, Lz);
//    __m128i cklow, ckup;
//    __m128i zck[4];
//    static int count = 0;
//
//    __m128i *xptr = (__m128i *) nbs.x;
//    __m128i *yptr = (__m128i *) nbs.y;
//    __m128i *zptr = (__m128i *) nbs.z;
//
//    for( int i = 0; i < 4; i++ ) {
//        // check bounds of X
//        cklow = _mm_cmplt_epi32(*(xptr + i), low_v);
//        *(xptr + i) = _mm_add_epi32(*(xptr + i), _mm_and_si128(cklow, upx_v));
//
//        ckup = _mm_cmplt_epi32(upx_v, *(xptr + i));
//        ckup = _mm_add_epi32(ckup, _mm_cmpeq_epi32(upx_v, *(xptr + i)));
//        *(xptr + i) = _mm_sub_epi32(*(xptr + i), _mm_and_si128(ckup, upx_v));
//
//        // check bounds of Y
//        cklow = _mm_cmplt_epi32(*(yptr + i), low_v);
//        *(yptr + i) = _mm_add_epi32(*(yptr + i), _mm_and_si128(cklow, upy_v));
//
//        ckup = _mm_cmplt_epi32(upy_v, *(yptr + i));
//        ckup = _mm_add_epi32(ckup, _mm_cmpeq_epi32(upy_v, *(yptr + i)));
//        *(yptr + i) = _mm_sub_epi32(*(yptr + i), _mm_and_si128(ckup, upy_v));
//    }
//}

inline static int __neighbors_ser(int x, int y, int z, neighbors_t &nbs)
{
    int factor, dir;
    if( z % 2 == 0 )
        factor = 1;
    else
        factor = -1;

    for( dir = 0; dir < dir_number; dir++ ) {
        nbs.x[dir] = x + factor * dx_neighbor[dir];
        nbs.y[dir] = y + factor * dy_neighbor[dir];
        nbs.z[dir] = z + factor * dz_neighbor[dir];
    }
    return factor;
}

inline static int neighbors_ser(lPoint p, lPoint bounds, neighbors_t &nbs)
{
    int factor = __neighbors_ser(p.x(), p.y(), p.z(), nbs);
    for(int i=0; i<16;i++){
        lPoint lp(nbs.x[i],nbs.y[i],nbs.z[i]);
        boundary(lp,bounds);
    }
    return factor;
}

inline static int __neighbors_vect(int x, int y, int z, neighbors_t &nbs)
{
    int factor, i;

    if( z % 2 == 0 )
        factor = 1;
    else
        factor = -1;

    __m128i fact_v = _mm_set1_epi32(factor);
    __m128i x_v = _mm_set1_epi32(x);
    __m128i y_v = _mm_set1_epi32(y);
    __m128i z_v = _mm_set1_epi32(z);

    for( i = 0; i < dir_number / 4; i++ ) {
        __m128i *ptr = (__m128i*) nbs.x + i;
        *ptr = x_v;
        *ptr = _mm_add_epi32(*ptr, mul4x32bit(fact_v, *((__m128i*) dx_neighbor + i)));

        ptr = (__m128i*) nbs.y + i;
        *ptr = y_v;
        *ptr = _mm_add_epi32(*ptr, mul4x32bit(fact_v, *((__m128i*) dy_neighbor + i)));

        ptr = (__m128i*) nbs.z + i;
        *ptr = z_v;
        *ptr = _mm_add_epi32(*ptr, mul4x32bit(fact_v, *((__m128i*) dz_neighbor + i)));
    }

    return factor;
}

inline static int neighbors_vect(lPoint p, lPoint bounds, neighbors_t &nbs, bool wrap = false)
{
    int factor = __neighbors_vect(p.x(), p.y(), p.z(), nbs);
    // TODO: fix boundary_v(nbs,bounds);
    for(int i=0; i<16;i++){
        lPoint lp(nbs.x[i],nbs.y[i],nbs.z[i]);
        if( wrap ){
            boundary(lp,bounds);
        } else if( (lp.x() < 0 || lp.y() < 0 || lp.z() < 0) ){
            std::stringstream ss;
            ss << "Error while counting neighbors: p=" << p <<
                  ", lp=" << lp;
            DPRINTF(DBG_TRACE,"%s",ss.str().c_str());
#ifdef DEBUG_ON
            static int hang = 1;
            debug_hang(hang);
#endif
            //PQDEG_ABORT("%s",ss.str().c_str());

        }
        nbs.x[i] = lp.x();
        nbs.y[i] = lp.y();
        nbs.z[i] = lp.z();
    }
    return factor;
}

#endif	/* NEIGHBORS_H */

