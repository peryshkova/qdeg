#include <rGen.h>
#include <math.h>

static rGen *rgenInst = NULL;

rGen& rGen::instance()
{
    if(rgenInst == NULL) {
        rgenInst = new rGen();
    }
    return *rgenInst;
}

void rGen::randn2(float &x1, float &x2)
{ // генерирует 2 нормально распределённых случайных числа
    float V1, V2, S, f, t;
    t = 1.0 / (RAND_MAX + 1.);
    while(1) {
        V1 = 2 * t * (rand() + t * rand()) - 1;
        V2 = 2 * t * (rand() + t * rand()) - 1;
        S = V1 * V1 + V2*V2;
        if(S < 1) break;
    }
    f = sqrt(-2 * log(S) / S);
    x1 = V1*f;
    x2 = V2*f;
}