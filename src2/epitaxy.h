/* 
 * File:   epitaxy.h
 * Author: artpol
 *
 * Created on 9 Август 2013 г., 13:19
 */

#ifndef EPITAXY_H
#define	EPITAXY_H

#include <qTypes.h>
#include <lPoint.h>
#include <map>
#include <vector>
#include <stddef.h>
#include <sPlan.h>
#include <nConfig.h>
#include <compress.h>

class epitaxy {
public:

    typedef struct {
        double time;
        unsigned int cur_max_Z, cur_surf_Z;
        unsigned int deposit_since_ckpt;
        double deposit_local, deposit_related;
        unsigned int n_jumps;
        double jump_from, jump_to, jump_local;
        double jump_from_rel, jump_to_rel;
        // Total statistics
        double *jump_vector, *depo_vector;
        double deposit_total;
    } state_t;

    struct decomp_info {
        int x, y; // cartesian coordinates
        unsigned int lx, ly; // local square information
        unsigned int lz, lz0; // maximum height
    };

protected:
    MPI_Comm bComm;
    MPI_Comm wComm;
    sPlan::branch_role role;
    state_t state;
    //MPI_Datatype state_t_MPI;
    MPI_Datatype decomp_info_MPI;
    ckpt_compressor comp;

    unsigned long step;

    inline void create_decomp_MPI()
    {
        int ai_blens[] = {2,4};
        MPI_Aint ai_displ[] = {offsetof(decomp_info, x), offsetof(decomp_info, lx)};
        MPI_Datatype ai_types[] = {MPI_INT, MPI_UNSIGNED};
        MPI_Type_create_struct(2, ai_blens, ai_displ, ai_types, &decomp_info_MPI);
        MPI_Type_commit(&decomp_info_MPI);
    }

    inline zyxiterator travInit()
    {
        static lPoint offs(0, 0, 2);
        static decomp &d = decomp::instance();
        static zyxiterator it(d.ldims_wgh(), d.ghosts());
        it.offsets(offs);
        return it.begin();
    }

    inline zyxiterator travInit(int start, int end)
    {
        static decomp &d = decomp::instance();
        lPoint soffs(0, 0, start), eoffs(0, 0, d.ldimwgh_z() - end);
        zyxiterator it(d.ldims_wgh(), d.ghosts());
        it.soffsets(soffs);
        it.eoffsets(eoffs);
        return it.begin();
    }

    virtual void init_stat_save() = 0;
    virtual void stat_save(int step) { }
    virtual int read_lattice() = 0;

public:
    epitaxy();
    void setupComm();

    virtual void initData() = 0;

    void mainLoop();
    virtual double mc_step(int count) = 0;

    void time_init();
    static sPlan::branch_role role_detect();
    static epitaxy *inststance_create();
    //virtual void saveRelief(std::string fname) {};    

    virtual double get_jump_num() {  return 0.0; }

    bool timeForFullShake(int step){
        static qConfig &cfg = qConfig::instance();
        if( cfg.fullDispFreq() < 0 ){
            return false;
        }

        if( (step + 1) % cfg.fullDispFreq() == 0){
            DPRINTF(DBG_TRACE,"iter = %d, modulo = %d",step,cfg.fullDispFreq());
            return true;
        }
        return false;
    }
    bool timeForSurfShake(int step){
        static qConfig &cfg = qConfig::instance();
        if( cfg.surfDispFreq() <= 0 ){
            return false;
        }

        if( (step + 1) % cfg.surfDispFreq() == 0){
            DPRINTF(DBG_TRACE,"iter = %d, modulo = %d",step,cfg.surfDispFreq());
            return true;
        }
        return false;
    }

    virtual bool timeToLocalShake(int step){
        static qConfig &cfg = qConfig::instance();
        if( cfg.localDispFreq() < 0 ){
            return false;
        }
        if( (step + 1) % cfg.localDispFreq() == 0){
            DPRINTF(DBG_TRACE,"iter = %d, modulo = %d",step,cfg.localDispFreq());
            return true;
        }
        return false;
    }

    virtual void doALLShake(){
        // master won't do anything
    }

    virtual void doFullShake() {
        // master won't do anything
    }
    virtual void doSurfShake(){
        // master won't do anything
    }
    virtual void doLocalShake(){
        // master won't do anything
    }

    bool timeToCheckpoint(int step);
    virtual void saveCkpt(int step) = 0;
    virtual void saveCkpt_wgh() = 0;
};

class epitaxy_r : public epitaxy {
public:

protected:

    bool timeToCheckpoint();
    virtual void initData();
    virtual double mc_step(int count);
    virtual void saveCkpt(int step);
    virtual void saveCkpt_wgh();
    virtual void init_stat_save();
    virtual void stat_save(int step);
    virtual int read_lattice();


public:

    epitaxy_r()
    {
    }

    ~epitaxy_r(){
        static qConfig &cfg = qConfig::instance();
        if( cfg.compress() ){
            comp.finish();
        }
    }

};

class epitaxy_w : public epitaxy {
protected:
    pLattice<atom_info> nodparam;
    pLattice<jump_info> nodejmp;
    pLattice<float> jmprob;
    pLattice<displ_t> displs;

    // Update queue
    std::map<lPoint, bool> upd_q;
    typedef std::map<lPoint, bool>::iterator updIter;

    std::vector< std::vector<unsigned int> > zyproj;
    std::vector<unsigned long> zproj;
    std::vector<unsigned long> zjump;

    // Axx,Ayy,Azz,Axy,Axz,Ayz
    float AA[nConfig::Nconfig][6];
    // (A^-1)xx ... (A^-1)yz
    float AA_[nConfig::Nconfig][6];
    // Bxx,Bxy,Bxz,Byx,Byy,Byz,Bzx,Bzy,Bzz
    float BB[nConfig::Nconfig][dir_number][9];
    float TR[nConfig::Nconfig][6]; // для random_displacements

    // deformation
    void set_defects();
    void calc_B0(lPoint p);

    void calc_AA();
    void calc_BB();
    void calc_AA_();
    void calc_Edef(lPoint p);
    void update_cached();
    void update_Edef(lPoint p);
    void update_Edef_cached();
    dPoint deposit_gen(sPlan::pRecord &r);
    void deposit(sPlan::pRecord &r);
    void axyz(lPoint p);
    void do_many_axyz(int displ_per_ml);
    void do_all_axyz();
    void rand_displ(displ_t &f, unsigned short config_);

    // jump
    void dyn_range(int &start, int &end, bool verbose = false);
    void set_jump_info(lPoint p, bool debug = false);
    void calc_jump_prob(lPoint p);
    void check_jump(lPoint p);
    double sum_jump_prob();
    double sum_jump_prob_full(bool verbose = false);
    double sum_jump_prob_opt(bool verbose = false);
    void jump(sPlan::pRecord &r, sPlan &pl);
    void jump_gen(double jprob, sPlan::pRecord &r);
    bool jump_isok(dPoint from);

    // Geometry
    void set_config(lPoint p, bool verbose = false);
    coord_t count_max_Z();
    coord_t count_surf_Z();
    unsigned int count_points(int max_Z);
    unsigned int count_mobile_points();
    lPoint point_by_no(unsigned int no);
    lPoint point_by_no_opt(unsigned int no);
    lPoint point_by_no_full(unsigned int no);
    void point_by_no_opt_verbose(unsigned no);
    
    
    // MPI types
    void createTypes();
    MPI_Datatype atom_info_MPI, jump_info_MPI, displ_MPI;

    // Prepare plan
    void plan_refine(double jprob);

    virtual void saveCkpt(int step);
    virtual void saveCkpt_wgh();
    virtual int read_lattice();
    virtual void init_stat_save();

public:

    epitaxy_w()
    {
    }
    virtual void initData();
    virtual double mc_step(int count);
    virtual double get_jump_num() { return (float)state.jump_from; }

    virtual void doALLShake(){
        static qConfig &cfg = qConfig::instance();
        for(int i = 0; i< 20 /*1000*/; i++)
        {
            displs.sync();
            do_all_axyz();
            displs.sync();
        }
    }

    virtual void doFullShake(){
        static qConfig &cfg = qConfig::instance();
        displs.sync();
        do_many_axyz(cfg.fullDispPerML());
        displs.sync();
    }

    virtual void doSurfShake(){
        static qConfig &cfg = qConfig::instance();
		int start_z = state.cur_surf_Z - cfg.surfDispDepth();
		// TODO: find the better solution. We can sync this on each step using master
		int end_z = state.cur_max_Z + 4;
        displs.sync_surf(start_z, end_z);
        do_many_axyz(cfg.surfDispPerML());
        displs.sync_surf(start_z, end_z);
    }
    virtual void doLocalShake(){
        static qConfig &cfg = qConfig::instance();
        do_many_axyz(cfg.localDispPerML());
    }


};

#endif	/* EPITAXY_H */

