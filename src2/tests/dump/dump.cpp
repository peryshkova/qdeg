#include <stdio.h>
#include <mpi.h>

int main(int argc, char **argv)
{
    char a[10][10] = {0};
    char b[5][5] = {0};

    MPI_Init(argc,argv);
    
    for(int i = 0; i < 5; i++) {
        for(int j = 0; j < 5; j++) {
            b[i][j] = 10;
        }
    }

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    if( rank == 0) {
        MPI_Datatype vect;
        MPI_Type_vector(5, 5, 10, MPI_CHAR, &vect);
        MPI_Type_commit(&vect);
        MPI_Status status;
        MPI_Recv(a, 1, vect, 1, 0, MPI_COMM_WORLD,&status);
        MPI_Recv((char*)a+55, 1, vect, 1, 0, MPI_COMM_WORLD,&status);
        for(int i = 0; i < 10; i++) {
            for(int j = 0; j < 10; j++) {
                printf("%02hhd ",a[i][j]);
            }
            printf("\n");
        }
    } else {
        MPI_Send(b, 25, MPI_CHAR, 0, 0);
        MPI_Send(b, 25, MPI_CHAR, 0, 0);
    }

    MPI_Finalize();
}
