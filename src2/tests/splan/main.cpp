#define DEBUG_ON
#include <pLattice.h>
#include <string>
#include <debug.h>
#include <sPlan.h>
#include <rGen.h>

MPI_comm create_bComm(qConfig &cfg);
MPI_comm create_wComm(MPI_comm &bComm, qConfig &cfg);

int main(int argc, char **argv)
{
    MPI_Init(argc, argv);
    init_debug(MPI_COMM_WORLD);
    
    std::string cfgfile = "inp";
    qConfig *cfg = new qConfig(cfgfile);

    MPI_comm bComm = create_bComm(*cfg);
    init_debug(bComm);
    MPI_comm wComm = create_wComm(bComm, *cfg);


    if(bComm.Get_rank() > 0) {
        decomp &dcmp = decomp::instance();
        dcmp.init(wComm, *cfg);
        sPlan::init(bComm, *cfg, sPlan::Worker);
        sPlan::instance().prepare_draft(bComm.Get_rank()*1.0);
        sPlan::instance().prepare_final();
    } else {
        sPlan::init(bComm, *cfg, sPlan::Master);
        sPlan::instance().prepare_draft();
    }

    /*        while(1) {
            DPRINTF("I am root process");
            sleep(1);
        }
     */

    MPI_Finalize();
    return 0;
}

MPI_comm create_bComm(qConfig &cfg)
{
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    bool flag = rank <= cfg.Nx() * cfg.Ny();
    MPI_comm wComm = MPI_COMM_WORLD;
    MPI_Comm_split(MPI_COMM_WORLD, flag, 0, &wComm);

    if(!flag) {
        DPRINTF(1, "I am exiting!");
        MPI_Finalize();
        exit(0);
    }
    return wComm;
}

MPI_comm create_wComm(MPI_comm &bComm, qConfig &cfg)
{
    bool flag = bComm.Get_rank() > 0;
    MPI_comm nComm = bComm.Split(flag, 0);

    DPRINTF(1, "old rank = %d, new rank = %d, size = %d",
            bComm.Get_rank(), nComm.Get_rank(), nComm.Get_size());
    return nComm;
}
