#include <dlNeighbors.h>
#include <list>

int print_nbs(lPoint &p)
{
    lPoint bounds = p;
    neighbors_t nbs;

    std::list<lPoint> rel;
    __neighbors_vect(p.x(), p.y(), p.z(), nbs);
    for( int dir = 0; dir < dir_number; dir++ ) {
        lPoint p1(nbs.x[dir], nbs.y[dir], nbs.z[dir]);
        rel.push_back(p1);
        std::cout << "add direct neighbor " << p1 << std::endl;
        neighbors_t nbs1;
        __neighbors_vect(p1.x(), p1.y(), p1.z(), nbs1);
        for( int dir1 = 0; dir1 < dir_number; dir1++ ) {
            lPoint p2(nbs1.x[dir1], nbs1.y[dir1], nbs1.z[dir1]);
	        	std::cout << "add relative neighbor" << p2 << " direct neighbor of " << p1 << std::endl;
            rel.push_back(p2);
        }
    }
    
    std::list<lPoint>::iterator it = rel.begin();
    for( ; it != rel.end(); it++){
        std::cout << (*it) << " ";
    }
    std::cout << std::endl;
    return 0;
}


int main()
{
    lPoint p1(4, 4, 4), p2(3,3,3);
    print_nbs(p1);
    print_nbs(p2);
    return 0;
}