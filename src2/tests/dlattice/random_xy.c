#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main()
{
		int i;

	for(i=0;i<100;i++){
    int x = rand() % 256;
    int y = rand() % 256;
		printf("x=%d, y=%d (ex=%d, ey=%d) -> ", x, y, x%4, y%4);
    int x1 = ((x % 4) / 2)*2 + y%2;
    x = (x / 4) * 4 + x1;
		printf("x=%d, y=%d (ex=%d, ey=%d)\n", x, y, x%4, y%4);
	}
}
