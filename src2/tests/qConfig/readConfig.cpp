#include <mpi.h>
#include<qConfig.h>
#include <iostream>

int main(int argc, char **argv)
{
    std::string s = "input";
    std::stringstream sstream;
    MPI_Init(argc, argv);
    MPI_Comm comm = MPI_COMM_WORLD;
    qdegConfig cfg(MPI_COMM_WORLD, s);
    cfg.print(sstream);
    std::cout << sstream.str();
    MPI_Finalize();
    return 0;
}
