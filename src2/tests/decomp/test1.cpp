// #define DEBUG_ON
#include <decomp.h>
#include <qConfig.h>
#include <iostream>
#include <mpi.h>
#include <vector>

using namespace std;

void try_point(decomp &d, lPoint p)
{
    int rank, size;
    decomp::relmap_t cmap;

    int i, j;
    int myrank = d.mesh().rank();
    if( d.comm_pattern(p, cmap) ) {
        char s[256] = "";
        /*
            sprintf(s, "(%d) is related:\n", rank);
            for (i = 0; i < 3; i++) {
              strcat(s, "\t");
              for (j = 0; j < 3; j++) {
                sprintf(s, "%s%d ", s, cmap[i][j]);
              }
              strcat(s, "\n");
            }
         */
        int buf = 1;
        for( i = 0; i < 3; i += 2 ) {
            int crd[2];
            d.mesh().coords(crd);
            if( cmap[1][i] == 2 ) {
                crd[0] += i - 1;
                int rank = d.mesh().coords2rank(crd);
                MPI_Status status;
                MPI_Recv(&buf, 1, MPI_INT, rank, 0, d.mesh().comm(), &status);
                buf++;
                sprintf(s, "%s MPI_Recv(%d,%d): %d\n", s, rank, myrank, buf);
            }
        }

        for( i = 0; i < 3; i += 2 ) {
            int crd[2];
            d.mesh().coords(crd);
            if( cmap[i][1] == 2 ) {
                crd[1] += i - 1;
                int rank = d.mesh().coords2rank(crd);
                MPI_Status status;
                MPI_Recv(&buf, 1, MPI_INT, rank, 0, d.mesh().comm(), &status);
                buf++;
                sprintf(s, "%sMPI_Recv(%d,%d): %d\n", s, rank, myrank, buf);
            }
        }

        for( i = 0; i < 3; i += 2 ) {
            int crd[2];
            d.mesh().coords(crd);
            if( cmap[1][i] == 1 ) {
                crd[0] += i - 1;
                int rank = d.mesh().coords2rank(crd);
                MPI_Send(&buf, 1, MPI_INT, rank, 0, d.mesh().comm());
                sprintf(s, "%sMPI_Send(%d,%d): %d\n", s, myrank, rank, buf);
            }
        }

        for( i = 0; i < 3; i += 2 ) {
            int crd[2];
            d.mesh().coords(crd);
            if( cmap[i][1] == 1 ) {
                crd[1] += i - 1;
                int rank = d.mesh().coords2rank(crd);
                MPI_Send(&buf, 1, MPI_INT, rank, 0, d.mesh().comm());
                sprintf(s, "%sMPI_Send(%d,%d): %d\n", s, myrank, rank, buf);
            }
        }

        printf("%s", s);
    }
}

int main(int argc, char **argv)
{
    MPI_Init(&argc, &argv);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

//    if(rank == 2){
//        int k = 1;
//        while( k ){
//            sleep(1);
//        }
//    }

    init_debug(MPI_COMM_WORLD, stdout);

    std::string s = "input1";
    std::stringstream sstream;
    MPI_Comm comm = MPI_COMM_WORLD;

    qConfig::initialize(s);
    qConfig &cfg = qConfig::instance();

    MPI_Comm c = MPI_COMM_WORLD;
    bool flag = rank < cfg.Nx() * cfg.Ny();
    MPI_Comm nc;
    MPI_Comm_split(c,flag, 0, &nc);


    if( flag ) {
        decomp &d = decomp::instance();
        d.init(nc);
        lPoint p(29, 19, 4);
        try_point(d, p);
    }
    MPI_Finalize();

    return 0;
}
