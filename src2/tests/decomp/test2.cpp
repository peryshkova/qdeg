#include <iostream>
#include <mpi.h>
#include <vector>

#define DEBUG_ON
#include <debug.h>
#include <decomp.h>
#include <qConfig.h>
#include <algorithm>


using namespace std;

void count_bounds(coord_t glob, coord_t nodes, int pos,
        coord_t &start, coord_t &end)
{
    coord_t cglob = glob / 4;
    coord_t base = cglob / nodes;
    start = base*pos;
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    // TODO: Remove test debug    
    if( rank == -1 )
        std::cout << "cglob=" << cglob << " base=" << base
            << " start=" << start << std::endl;

    if( pos < cglob % nodes ) {
        start += pos;
        base += 1;
    } else {
        start += cglob % nodes;
    }
    end = start + base;

    // TODO: Remove test debug
    if( rank == -1 )
        std::cout << "cglob=" << cglob << " base=" << base
            << " start=" << start << std::endl;

    start *= 4;
    end *= 4;
}

void prepare_matrix(std::vector< std::vector<int> > &matrix, qConfig &cfg,
        bool verbose)
{
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    std::stringstream ss;
    for( int ny = 0, count = 0; ny < cfg.Ny(); ny++ ) {
        for( int nx = 0; nx < cfg.Nx(); nx++ ) {
            coord_t xstart, xend;
            coord_t ystart, yend;
            count_bounds(cfg.Gx(), cfg.Nx(), nx, xstart, xend);
            count_bounds(cfg.Gy(), cfg.Ny(), ny, ystart, yend);

            // TODO: Remove test debug
            if( rank == -1 ) {
                std::cout << "(" << nx << "," << ny << ") -> (" << xstart << ","
                        << ystart << ")x(" << xend << "," << yend << ")" << std::endl;
            }

            // TODO: Remove test debug
            if( 0 && verbose )
                std::cout << "Rank " << count << ":" <<
                    "x=(" << xstart << "," << xend << "); y=("
                    << ystart << "," << yend << ")" << std::endl;

            for( int i = ystart; i < yend; i++ ) {
                for( int j = xstart; j < xend; j++ ) {
                    matrix[i][j] = count;
                }
            }
            count++;
        }
    }

    if( verbose && rank== 0 ) {
        ss << rank << ": " << std::endl;
        for( int i = 0; i < cfg.Gy(); i++ ) {
            if( i < 10 )
                ss << " ";
            ss << i << ": ";
            for( int j = 0; j < cfg.Gx(); j++ ) {
                if( matrix[i][j] < 10 )
                    ss << " ";
                ss << matrix[i][j] << " ";
            }
            ss << endl;
        }
        std::cout << ss.str();
    }
}

int check_self(std::vector< std::vector<int> > &matrix, decomp &d)
{
    int ret = 1, rret;
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    for( int i = 0; i < matrix.size(); i++ ) {
        for( int j = 0; j < matrix[0].size(); j++ ) {
            lPoint p(j, i, 10);
            bool decomp_res = d.is_local(p);
            bool simple_res = (matrix[i][j] == rank);
            if( decomp_res != simple_res ) {
                std::cout << rank << ": Wrong self result: (" << j << "," <<
                        i << "), decomp=" << decomp_res << ", simple=" <<
                        simple_res << std::endl;
                ret = false;
            }
        }
    }

    MPI_Allreduce(&ret, &rret, 1, MPI_INT, MPI_LAND,d.mesh().comm());
    return rret;
}

bool check_point(std::vector< std::vector<int> > &matrix, qConfig &cfg,
        int i, int j)
{
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    bool ret = false;
    if( matrix[i][j] != rank ) {
        /* Check all points in -4 ... 4 */
        for( int k1 = -8; k1 <= 8; k1++ ) {
            for( int k2 = -8; k2 <= 8; k2++ ) {

                coord_t i1 = ((i + k1) + cfg.Gy()) % cfg.Gy();
                coord_t j1 = ((j + k2) + cfg.Gx()) % cfg.Gx();

                if( rank == -1 && i == 16 && j == 0 ) {
                    std::cout << "need " << rank << " (" << j << "," << i <<
                            "), k=" << k1 << ", " << k2 << " -> (" << j1 << "," << i1 <<
                            ") matrix[" << i1 << "][" << j << "]=" << matrix[i1][j] <<
                            ", matrix[" << i << "][" << j1 << "]=" << matrix[i][j1] <<
                            ", matrix[" << i1 << "][" << j1 << "]=" << matrix[i1][j1] <<
                            ", simple_res = " << ret <<
                            std::endl;
                }

                if( matrix[i1][j] == rank )
                    ret = true;
                if( matrix[i][j1] == rank )
                    ret = true;
                if( matrix[i1][j1] == rank )
                    ret = true;

            }
        }
    }
    return ret;
}

bool check_neigh(std::vector< std::vector<int> > &matrix, decomp &d, qConfig &cfg)
{
    int ret = 1, rret;
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    for( int i = 0; i < matrix.size(); i++ ) {
        for( int j = 0; j < matrix[0].size(); j++ ) {
            lPoint p(j, i, 10);
            bool simple_res = false;
            bool verb = false;

            simple_res = check_point(matrix, cfg, i, j);

            if( rank == -1 && i == 16 && j == 0 ) {
                verb = true;
            }
            bool decomp_res = d.is_neighbor(p, verb);

            if( decomp_res != simple_res ) {
                std::cout << rank << ": Wrong neighbor result: (" << j << "," <<
                        i << "), decomp=" << decomp_res << ", simple=" <<
                        simple_res << std::endl;
                ret = false;
            }
        }
    }

    MPI_Allreduce(&ret, &rret, 1, MPI_INT, MPI_LAND,d.mesh().comm());
    return rret;
}

bool check_related(std::vector< std::vector<int> > &matrix, decomp &d,
        qConfig &cfg)
{
    int ret = 1, rret;
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    for( int i = 0; i < matrix.size(); i++ ) {
        for( int j = 0; j < matrix[0].size(); j++ ) {
            lPoint p(j, i, 10);
            decomp::relmap_t map;
            d.is_related(p, map);
            for( int k = 0; k < 3; k++ ) {
                for( int l = 0; l < 3; l++ ) {
                    if( map[k][l] ) {
                        coord_t ni, nj;
                        ni = ((i - (k - 1) * d.ghosty()) + cfg.Gy()) % cfg.Gy();
                        nj = ((j - (l - 1) * d.ghosty()) + cfg.Gx()) % cfg.Gx();

                        lPoint p1(nj, ni, 1);
                        //                        {
                        //                            std::stringstream s;
                        //                            s << "point = " << p << ", related local point: " << p1;
                        //                            s << ", matrix[" << ni << "][" << nj << "] = " << matrix[ni][nj];
                        //                            DPRINTF(1, "%s", s.str().c_str());
                        //                        }
                        if( matrix[ni][nj] != rank ) {
                            ret = false;

                            std::cout << "Related test failed for rank=" <<
                                    rank << ", point=" << p <<
                                    ", neigh point=" << p1 << ", Map: " << std::endl;
                            for( int k1 = 0; k1 < 3; k1++ ) {
                                for( int l1 = 0; l1 < 3; l1++ ) {
                                    std::cout << (int) map[k1][l1] << " ";
                                }
                                std::cout << std::endl;
                            }
                            std::cout << std::endl;
                        }
                    }
                }
            }
        }
    }
    MPI_Allreduce(&ret, &rret, 1, MPI_INT, MPI_LAND, d.mesh().comm());
    return rret;
}

bool check_nbrmap(std::vector< std::vector<int> > &matrix, decomp &d,
        qConfig &cfg)
{
    int ret = true, rret;
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    vector<int> nbs_matr, nbs_decomp;
    int c[2];
    d.mesh().coords(c);

    for( int i = 0; i < matrix.size(); i++ ) {
        for( int j = 0; j < matrix[0].size(); j++ ) {
            lPoint p(j, i, 10);
            // Form vector of neighbors
            decomp::relmap_t map;
            d.is_related(p, map);
            nbs_decomp.clear();
            for( int k = 0; k < 3; k++ ) {
                for( int l = 0; l < 3; l++ ) {
                    if( map[k][l] ) {
                        int nc[2];
                        nc[0] = c[0] + (l - 1);
                        nc[1] = c[1] + (k - 1);
                        nbs_decomp.push_back(d.mesh().coords2rank(nc));
                    }
                }
            }

            nbs_matr.clear();
            for( int k = 0; k < 3; k++ ) {
                coord_t ni = ((i - (k - 1) * d.ghosty()) + cfg.Gy()) % cfg.Gy();
                coord_t nj = ((j - (k - 1) * d.ghostx()) + cfg.Gx()) % cfg.Gx();
                if( matrix[i][nj] == rank ) {
                    coord_t ni1 = ((i - d.ghosty()) + cfg.Gy()) % cfg.Gy();
                    coord_t ni2 = ((i + d.ghosty()) + cfg.Gy()) % cfg.Gy();
                    if( matrix[ni1][nj] != rank && matrix[ni1][nj] != matrix[i][j] ) {
                        bool flag = true;
                        for( int f = 0; f < nbs_matr.size(); f++ ) {
                            if( nbs_matr[f] == matrix[ni1][nj] )
                                flag = false;
                        }
                        if( flag )
                            nbs_matr.push_back(matrix[ni1][nj]);
                    } else if( matrix[ni2][nj] != rank && matrix[ni2][nj] != matrix[i][j] ) {
                        bool flag = true;
                        for( int f = 0; f < nbs_matr.size(); f++ ) {
                            if( nbs_matr[f] == matrix[ni2][nj] )
                                flag = false;
                        }
                        if( flag )
                            nbs_matr.push_back(matrix[ni2][nj]);
                    }
                }
            }

            // Compare results
            // 1. Check that all nodes in nbs_decomp are in nbs_matr
            for( int k = 0; k < nbs_decomp.size(); k++ ) {
                std::vector<int>::iterator it;
                it = std::find(nbs_matr.begin(), nbs_matr.end(), nbs_decomp[k]);
                if( it != nbs_matr.end() ) {
                    ret = false;
                }
            }

        }
    }
    MPI_Allreduce(&ret, &rret, 1, MPI_INT, MPI_LAND,d.mesh().comm());
    return rret;
}

void check_commmap_r(qConfig cfg)
{
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    /* Single test 
      int point[2] = {0, 32};

    MPI_Bcast(point, 2, MPI_INT, rank, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
     */
    /**/
    std::cout << "i = ";
    for( int i = 0; i < cfg.Gy(); i++ ) {
        std::cout << i << " ";
        std::cout.flush();
        
        for( int j = 0; j < cfg.Gx(); j++ ) {
            int point[2] = {j, i};
            MPI_Bcast(point, 2, MPI_INT, rank,MPI_COMM_WORLD);
            MPI_Barrier(MPI_COMM_WORLD);

        }
    }
    std::cout << std::endl;
    /**/

}

bool check_commap_w(std::vector< std::vector<int> > &matrix, decomp &d,
        qConfig &cfg, int root)
{
    bool ret = true;
    MPI_Status status;
    for( int k = 0; k < cfg.Gx() * cfg.Gy() /*1*/; k++ ) {
        int points[2];
        MPI_Bcast(points, 2, MPI_INT, root, MPI_COMM_WORLD);
        lPoint p(points[0], points[1], 10);

        // ------------------- DEBUG -------------------------//
        stringstream s;
        s << "k=" << k << ", point = " << p;
        DPRINTF(0, "%s", s.str().c_str());
        s.str("");
        // ------------------- DEBUG (end) -------------------//
        
        
        decomp::relmap_t cmap;
        if( d.comm_pattern(p, cmap) ) {
            cartmesh<decomp::n> &m = d.mesh();

            // ------------------- DEBUG -----------------------//
            s << "I am related. Map:" << std::endl;
            for( int k1 = 0; k1 < 3; k1++ ) {
                for( int l1 = 0; l1 < 3; l1++ ) {
                    s << (int) cmap[k1][l1] << " ";
                }
                s << std::endl;
            }
            DPRINTF(0, "%s",s.str().c_str());
            // ------------------- DEBUG (end) -------------------//

            int crd[2];
            int point = -1;
            if( d.is_local(p) )
                point = matrix[p.y()][p.x()];

            // Post receive
            for( int i = 0; i < 3; i += 2 ) {
                m.coords(crd);
                if( cmap[1][i] == 2 ) {
                    crd[0] += i - 1;
                    int rank = m.coords2rank(crd);
                    DPRINTF(0, "MPI_Recv(%d)", rank);
                    MPI_Recv(&point, 1, MPI_INT, rank, 1,m.comm(),&status);
                }
            }

            for( int i = 0; i < 3; i += 2 ) {
                m.coords(crd);
                if( cmap[i][1] == 2 ) {
                    crd[1] += i - 1;
                    int rank = m.coords2rank(crd);
                    DPRINTF(0, "MPI_Recv(%d)", rank);
                    MPI_Recv(&point, 1, MPI_INT, rank, 1, m.comm(),&status);
                }
            }

            for( int i = 0; i < 3; i += 2 ) {
                m.coords(crd);
                if( cmap[1][i] == 1 ) {
                    crd[0] += i - 1;
                    int rank = m.coords2rank(crd);
                    DPRINTF(0, "MPI_Send(%d)", rank);
                    MPI_Send(&point, 1, MPI_INT, rank, 1, m.comm());
                }
            }

            for( int i = 0; i < 3; i += 2 ) {
                m.coords(crd);
                if( cmap[i][1] == 1 ) {
                    crd[1] += i - 1;
                    int rank = m.coords2rank(crd);
                    DPRINTF(0, "MPI_Send(%d)", rank);
                    MPI_Send(&point, 1, MPI_INT, rank, 1, m.comm());
                }
            }
            if( point != matrix[p.y()][p.x()] ) {
                ret = false;
                int rank;
                MPI_Comm_rank(MPI_COMM_WORLD,&rank);
                std::cout << "Communication map test failed for rank=" <<
                        rank << ", point=" << p << std::endl;
            } else if( 0 ) {
                int rank;
                MPI_Comm_rank(MPI_COMM_WORLD,&rank);
                std::cout << "[" << rank << "]: point "
                        << p << " = " << point << std::endl;
            }
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }
    return ret;
}

int main(int argc, char **argv)
{
    MPI_Init(&argc, &argv);
    init_debug(MPI_COMM_WORLD);
    bool verbose = false;

    if( argc > 1 )
        verbose = true;
    std::string s = "input2";

    MPI_Comm comm = MPI_COMM_WORLD;
    qConfig::initialize(s);
    qConfig &cfg = qConfig::instance();
    std::vector< std::vector<int> > matrix(cfg.Gy(), std::vector<int>(cfg.Gx()));
    prepare_matrix(matrix, cfg, verbose);

    decomp d;
    MPI_Comm c = MPI_COMM_WORLD;
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    bool flag = rank < cfg.Nx() * cfg.Ny();
    MPI_Comm nc;
    MPI_Comm_split(c,flag,0, &nc);

    int root_rank = cfg.Nx() * cfg.Ny();
    if( flag ) {
        int rank;
        MPI_Comm_rank(MPI_COMM_WORLD,&rank);
        decomp &d = decomp::instance();
        d.init(nc);

        // ------------------ Self points -------------------------
        if( rank == 0 )
            std::cout << "Check points that belongs to this process";
        if( !check_self(matrix, d) ) {
            if( rank == 0 )
                std::cout << " [FAILED!]" << std::endl;
            goto exit;
        } else if( rank == 0 ) {
            std::cout << " [OK]" << std::endl;
        }

        // ------------------ Neighbour points -------------------------
        if( rank == 0 )
            std::cout << "Check neighbour points";
        if( !check_neigh(matrix, d, cfg) ) {
            if( rank == 0 )
                std::cout << " [FAILED!]" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, 0);
        } else if( rank == 0 ) {
            std::cout << " [OK]" << std::endl;
        }

        // ------------------ Related maps -------------------------
        if( rank == 0 ) {
            std::cout << "Check related maps";
        }
        if( !check_related(matrix, d, cfg) ) {
            if( rank == 0 )
                std::cout << " [FAILED!]" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, 0);
        } else if( rank == 0 ) {
            std::cout << " [OK]" << std::endl;
        }

        // ------------------ Comm maps -------------------------
        MPI_Barrier(MPI_COMM_WORLD);
        if( rank == 0 )
            std::cout << "Check communication maps";
        if( !check_commap_w(matrix, d, cfg, root_rank) ) {
            if( rank == 0 )
                std::cout << " [FAILED!]" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, 0);
        } else if( rank == 0 ) {
            std::cout << " [OK]" << std::endl;
        }

    } else {
        MPI_Barrier(MPI_COMM_WORLD);
        check_commmap_r(cfg);
    }

exit:
    MPI_Finalize();

    return 0;
}
