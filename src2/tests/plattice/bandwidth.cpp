#include <string>
#include <iostream>
#include <pLattice.h>
#include <sstream>
#include <list>
#include <vector>

#include <iterator_yx.h>
#include <iterator_z.h>
#include <iterator_zyx.h>

#include <constants.h>
#define COUNT 4000

int Gx, Gy, Gz;

using namespace std;

double measure_time(pLattice<float> &p){
	zyxiterator it = p.begin();

	// initialize local lattice
    MPI_comm c = MPI_COMM_WORLD;
	for(it = p.begin();it <= p.end(); it++){
		p(*it) =c.Get_rank();
	}

	double start = MPI_Wtime();

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    printf("%d: start measure", rank);
/*
int i=1;
while( i == 1){
	sleep(1);
}
*/
	for(int i=0;i<COUNT;i++){
		if( c.Get_rank() == 0 && i%100 == 0){
			printf("Iteration #%d\n",i);
		}
		p.sync();
	}

	double end = MPI_Wtime();
	return (end - start);
}

int main(int argc, char **argv)
{
    MPI_Init(argc, argv);
    init_debug(MPI_COMM_WORLD);
    decomp &d = decomp::instance();
    pLattice<float> p;

    std::string s = "input";
    qConfig::initialize(s);
    qConfig &cfg = qConfig::instance();

    MPI_comm c = MPI_COMM_WORLD;
    if( c.Get_size() !=  cfg.Nx() * cfg.Ny() + 1){
        printf("Need %d CPUs, act number is %d\n",
        	cfg.Nx() * cfg.Ny(), c.Get_size());
        MPI_Finalize();
        exit(0);
    }


    bool flag = c.Get_rank() < cfg.Nx() * cfg.Ny();
    MPI_comm nc = c.Split(flag, 0);
    if( flag ){
    	printf("%d: Init decomposition\n",c.Get_rank());
    	fflush(stdout);
    	int i = 1;
    	d.init(nc,2);
    	printf("%d: Init plattice\n",c.Get_rank());
    	fflush(stdout);


    	int size = p.init(MPI_INT);
    	printf("%d: start measure\n",c.Get_rank());
    	fflush(stdout);
    	double time = measure_time(p);
    	lPoint dim = d.lcubswgh();
      double cubeline = 8*(dim.x()+dim.y());
      double cubelayer = cubeline*2*2;
      double cubewell = cubelayer*dim.z();
    	double one_proc_size = sizeof(float)*cubewell;
    	double dsize = one_proc_size*d[0]*d[1]*COUNT;
    	double bw = dsize/time/1e6;
    	printf("%d, measured time is %lf, one_proc=%lf (%lf), total=%lf, bw=%lf\n",
    				c.Get_rank(), time, one_proc_size, size*1.0, dsize, bw);
    }


    MPI_Finalize();
    return (0);
}
