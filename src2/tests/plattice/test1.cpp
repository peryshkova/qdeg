#include <iostream>
#include <mpi.h>
#include <vector>

#define DEBUG_ON
#include <debug.h>
#include <decomp.h>
#include <qConfig.h>
#include <pLattice.h>
#include <algorithm>


using namespace std;

void count_bounds(coord_t glob, coord_t nodes, int pos,
        coord_t &start, coord_t &end)
{
    coord_t cglob = glob / 4;
    coord_t base = cglob / nodes;
    start = base*pos;

    // TODO: Remove test debug
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    if( rank == -1 )
        std::cout << "cglob=" << cglob << " base=" << base
            << " start=" << start << std::endl;

    if( pos < cglob % nodes ) {
        start += pos;
        base += 1;
    } else {
        start += cglob % nodes;
    }
    end = start + base;

    // TODO: Remove test debug
    if( rank == -1 )
        std::cout << "cglob=" << cglob << " base=" << base
            << " start=" << start << std::endl;

    start *= 4;
    end *= 4;
}

void prepare_matrix(std::vector< std::vector<int> > &matrix, qConfig &cfg,
        bool verbose)
{
    std::stringstream ss;
    for( int ny = 0, count = 0; ny < cfg.Ny(); ny++ ) {
        for( int nx = 0; nx < cfg.Nx(); nx++ ) {
            coord_t xstart, xend;
            coord_t ystart, yend;
            count_bounds(cfg.Gx(), cfg.Nx(), nx, xstart, xend);
            count_bounds(cfg.Gy(), cfg.Ny(), ny, ystart, yend);

            // TODO: Remove test debug
            int rank;
            MPI_Comm_rank(MPI_COMM_WORLD,&rank);
            if( rank == -1 ) {
                std::cout << "(" << nx << "," << ny << ") -> (" << xstart << ","
                        << ystart << ")x(" << xend << "," << yend << ")" << std::endl;
            }

            // TODO: Remove test debug
            if( 0 && verbose )
                std::cout << "Rank " << count << ":" <<
                    "x=(" << xstart << "," << xend << "); y=("
                    << ystart << "," << yend << ")" << std::endl;

            for( int i = ystart; i < yend; i++ ) {
                for( int j = xstart; j < xend; j++ ) {
                    matrix[i][j] = count;
                }
            }
            count++;
        }
    }

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    if( verbose && rank == 0 ) {
        ss << rank << ": " << std::endl;
        for( int i = 0; i < cfg.Gy(); i++ ) {
            if( i < 10 )
                ss << " ";
            ss << i << ": ";
            for( int j = 0; j < cfg.Gx(); j++ ) {
                if( matrix[i][j] < 10 )
                    ss << " ";
                ss << matrix[i][j] << " ";
            }
            ss << endl;
        }
        std::cout << ss.str();
    }
}

int main(int argc, char **argv)
{
    MPI_Init(argc, argv);
    init_debug(MPI_COMM_WORLD);
    bool verbose = false;

    if( argc > 1 )
        verbose = true;
    std::string s = "input1";

    MPI_Comm comm = MPI_COMM_WORLD;
    qConfig::initialize(s);
    qConfig &cfg = qConfig::instance();
    std::vector< std::vector<int> > matrix(cfg.Gy(), std::vector<int>(cfg.Gx()));

    prepare_matrix(matrix, cfg, verbose);

//    std::stringstream ss;
//    ss.str("");
//    ss << "Matrix:" << std::endl;
//    for( int i = 0; i < matrix.size(); i++ ) {
//        for( int j = 0; j < matrix[i].size(); j++ ) {
//            if( matrix[i][j] < 10)
//                ss << " ";
//            ss << matrix[i][j] << " ";
//        }
//        ss << std::endl;
//    }
//    DPRINTF(1, "%s", ss.str().c_str());

    MPI_Comm c = MPI_COMM_WORLD;
    bool flag = c.Get_rank() < cfg.Nx() * cfg.Ny();
    MPI_comm nc = c.Split(flag, 0);
    int root_rank = cfg.Nx() * cfg.Ny();
    if( flag ) {
        int rank;
        MPI_Comm_rank(MPI_COMM_WORLD,&rank);
        decomp &d = decomp::instance();
        d.init(nc);
        pLattice<int> p(MPI_INT);

        //        int tmp = 1;
        //        while(tmp == 1){
        //            sleep(1);
        //        }

        zyxiterator it = p.begin();
        for( it = p.begin(); it <= p.end(); it++ ) {
//            std::stringstream ss;
//            lPoint lp = *it;
//            lPoint ep = *p.end();
//            ss << lp << " < " << ep;
//            DPRINTF(1, "%s", ss.str().c_str());
            p(*it) = nc.Get_rank();
        }

        p.sync();

        zyxiterator prev = it;
        std::stringstream ss1, ss2;
        ss1 << "My part of the matrix:" << std::endl;
        for( it = p._begin(), prev = it; it <= p._end(); it++ ) {
            if( (*prev).y() < (*it).y() ) {
                ss1 << std::endl;
                if( (*prev).z() < (*it).z() )
                    ss1 << std::endl << std::endl;
                prev = it;
            }
            if( (*prev).z() < (*it).z() ) {
                ss1 << std::endl << std::endl;
                prev = it;
            }
            
            if( p(*it) < 10)
                ss1 << " ";
            ss1 << p(*it) << " ";
        }
        DPRINTF(1, "%s", ss1.str().c_str());

        for( it = p._begin(); it < p._end(); it++ ) {
            std::stringstream ss3;
            lPoint glob;
            lPoint loc = *it;
            decomp &d = decomp::instance();
            d.local2glob(loc, glob);

            glob.x() = (glob.x() + d.gdim_x()) % d.gdim_x();
            glob.y() = (glob.y() + d.gdim_y()) % d.gdim_y();

            if( matrix[glob.y()][glob.x()] != p(loc) ) {
                lPoint glob1;
                d.local2glob(loc, glob1);
                ss3 << "Test FAILED on point (loc=" << loc << ", glob=" << glob
                        << "[" << glob1 << "]) matrix[p] = " << matrix[glob.y()][glob.x()] <<
                        " lattice(p) = " << p(loc) << std::endl;
                DPRINTF(1, "%s", ss3.str().c_str());
            }
        }
    }

exit:
    MPI_Finalize();

    return 0;
}
