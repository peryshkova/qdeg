#include <iostream>
#include <iterator_yx.h>
#include <list>

#define Lx 1024
#define Ly 1024
#define Lz 1024

#define ALL_Z(s,e)     for (z = s; z < e; z++)       // z may take all values
#define ALL_Y(s,e)     for (y = ((int)z%2)+s; y < e; y+=2)  // y & z - same parity
#define ALL_X(s,e)     for (x = ((int)z%2)+2*(((int)z/2+(int)y/2)%2)+s; x < e; x+=4)

int main()
{
    coord_t maxx[] = {Lx, Ly, Lz};
    int x, y, z;
    yxiterator t(maxx, 0);
    lPoint ep1, ep2;


    std::cout << "Test increment functions:" << std::endl;

    std::cout << "1. Ghost nodes width=4, postfix increment (based on prefix)" << std::endl;
    {
        // Zero ghost nodes

        ALL_Z(0, Lz)
        {
            coord_t yxmax[] = {Lx, Ly, z};
            t.init(yxmax, 4);

            ALL_Y(4, Ly - 4) ALL_X(4, Lx - 4)
            {
                lPoint p = (*t);
                lPoint p1(x, y, z);

                if(x != p.x() || y != p.y() || z != p.z()) {
                    std::cout << "Z=" << z << " FAILED: " << ep2 << " <-> "
                            << ep1 << std::endl;
                }
                t++;
            }
        }
    }

    std::cout << "2. Ghost nodes width=4, postfix decncrement" << std::endl;
    { // Ghost nodes width = 4

        ALL_Z(0, Lz)
        {
            lPoint ep1, ep2;
            bool flag = false;
            coord_t yxmax[] = {Lx, Ly, z};
            t.init(yxmax, 4);
            std::list<lPoint> lst;
            std::list<lPoint>::iterator it;
            lst.clear();

            ALL_Y(4, Ly - 4) ALL_X(4, Lx - 4)
            {
                lPoint p1(x, y, z);
                lst.push_front(p1);
            }

            it = lst.begin();
            t = t.end();
            while(t >= t.begin()) {
                lPoint p = *t, p1 = *it;
                if(p != p1) {
                    //std::cout << p1 << " <-> " << p << " [ERROR]" << std::endl;
                    ep1 = p;
                    ep2 = p1;
                    flag = true;
                }
                it++;
                t--;
            }
            if(flag) {
                std::cout << "Z=" << z << " FAILED: " << ep2 << " <-> "
                        << ep1 << std::endl;
                continue;
            } 
        }
    }

    return 0;
}

