#include <stdio.h>
#include <mpi.h>

int main(int argc, char **argv)
{
    MPI_Init(argc,argv);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	
	float send[3] = {(rank+1)*1.0, (rank+1)*2.0, (rank+1)*3.0};
	float recv[3];
	
    MPI_Reduce(send, recv, 3, MPI_FLOAT, MPI_SUM,0, MPI_COMM_WORLD);
	
	if( rank == 0 ){
		printf("Reduce: %f, %f, %f\n", recv[0], recv[1], recv[2]);
	}
	
    MPI_Finalize();
	return 0;
}
