#include <stdio.h>
#include <mpi.h>
#include <string>

typedef struct {
    char type;
    unsigned int x1, y1, z1;
    unsigned int x2, y2, z2;
    unsigned long prio;    
} pRecord;

MPI_Datatype construct_dtype()
{
    MPI_Datatype t;
	char type_name[MPI_MAX_OBJECT_NAME] = "pRecord";
	int blens[3] = { 1, 6, 1};
	MPI_Aint displ[3] = {offsetof(pRecord,type),offsetof(pRecord,x1), offsetof(pRecord,prio)};
	printf("Displs= { %d, %d, %d }\n",displ[0],displ[1],displ[2]);
    MPI_Datatype types[3] = {MPI_CHAR, MPI_INT, MPI_UNSIGNED_LONG };
  t = MPI_Datatype::Create_struct(3,blens,displ,types);
	t.Set_name(type_name);
  t.Commit();
  
  MPI_Aint extent, lb;
  t.Get_extent(lb,extent);
  std::cout << "Size : " << t.Get_size() << ", extent = " << extent <<std::endl;
   
  return t;
}

struct tmap {
	int comb;
	char func[128];
} typeMap[] = {
	{ MPI_COMBINER_NAMED, "Base datatype" },
	{ MPI_COMBINER_DUP, "MPI_Type_dup" },
	{ MPI_COMBINER_CONTIGUOUS, "MPI_Type_contiguous"},
	{ MPI_COMBINER_VECTOR, "MPI_Type_vector" },
	{ MPI_COMBINER_HVECTOR, "MPI_Type_hvector OR MPI_Type_create" },
	{ MPI_COMBINER_INDEXED, "MPI_Type_indexed" },
	{ MPI_COMBINER_HINDEXED, "MPI_Type_hindexed OR MPI_Type_create_hindexed" },
	{ MPI_COMBINER_INDEXED_BLOCK, "MPI_Type_create_indexed_block" },
	{ MPI_COMBINER_STRUCT, "MPI_Type_create_struct" },
	{ MPI_COMBINER_SUBARRAY, "MPI_Type_create_subarray" },
	{ MPI_COMBINER_DARRAY, "MPI_Type_create_darray" },
	{ MPI_COMBINER_F90_REAL, "MPI_Type_create_f90_real" },
	{ MPI_COMBINER_F90_COMPLEX, "MPI_Type_create_f90_complex" },
	{ MPI_COMBINER_F90_INTEGER, "MPI_Type_create_f90_integer" },
	{ MPI_COMBINER_RESIZED, "MPI_Type_create_resized" }
};


int Combiner2Function(int combiner, std::string &s)
{
	int size = sizeof(typeMap)/sizeof(typeMap[0]);
	for(int i=0; i< size; i++){
		if( combiner == typeMap[i].comb ){
			s = typeMap[i].func;
			return 0;
		}
	}
	s = "unknown";
	return -1;
}

void print_tabs(int deep)
{
	for(int i=0; i<= deep; i++){
		printf("    ");
	}
}

void inspect_type(MPI_Datatype &t, int deep = 0)
{
	char name[1024];
	int nlen;
	t.Get_name(name,nlen);
	print_tabs(deep);
	printf("TYPE:%s\n",name);

    MPI_Aint lb, ext;
	t.Get_true_extent(lb, ext);
	print_tabs(deep);
	printf("EXTENT: %d\n", ext);

	int nints, naddrs, ndtypes, combiner;
	std::string s;
	t.Get_envelope(nints, naddrs, ndtypes, combiner);
	//printf("--------->Get envelope: ints=%d, addrs=%d, types=%d, combiner=%d\n",nints, naddrs, ndtypes, combiner);
	Combiner2Function(combiner,s);
	print_tabs(deep);
	printf("FUNC: %s\n",s.c_str());
	
	if( nints == 0 )
		return;
	
	int *ints = new int[nints];
    MPI_Aint *addrs = new MPI_Aint[naddrs];
    MPI_Datatype *dtypes = new MPI_Datatype[ndtypes];
	
	t.Get_contents(nints, naddrs, ndtypes, ints, addrs, dtypes);
/*	
	printf("--------->Get contents:\n");
	printf("--------->    ints: ");
	for(int i=0; i< nints; i++){
		printf("%d ",ints[i]);
	}
	printf("\n");

	printf("--------->    addrs: ");
	for(int i=0;i<naddrs;i++){
		printf("%d ",addrs[i]);
	}
	printf("\n");
*/
	if( ndtypes == 1 ){
		print_tabs(deep);
		if( naddrs == 0 ){
  		print_tabs(deep);
	  	printf("Placement: Continuous, size = %d\n",ints[0]);
		}else if( naddrs == 1) {
  		print_tabs(deep);
	  	printf("Placement: Block = %d, Stride = %d\n",ints[0], addrs[0]);
		} else {
  		print_tabs(deep);
	  	printf("Placement: heterogenous = {(%d,%d)",ints[1],addrs[0]);
	  	
	  	for(int i=1;i<naddrs;i++){
	  		printf(", (%d,%d)",ints[i+1], addrs[i]);
			}
			printf("}\n");
		}
		inspect_type(dtypes[0], deep+1);
	} else {
		for(int i = 0; i < ndtypes; i++){
  		print_tabs(deep);
  		printf("Field: size=%d, offs=%d. Datatype:\n",ints[i+1],addrs[i]);
			inspect_type(dtypes[i], deep+1);
	  }
	}

}

int main(int argc, char **argv)
{
    MPI_Init(argc, argv);
    MPI_Datatype t = construct_dtype();
	pRecord p1;
	
  inspect_type(t);	
	
    MPI_Datatype cont;
    MPI_Type_contiguous(100,MPI_INT,&cont);
    MPI_Type_commit(&cont);
	
	inspect_type(cont);

    MPI_Datatype hind;
	int blens[10] = { 10, 15, 4, 8, 9, 13, 20, 21, 40, 10 };
	int displ[10] = { 0, 20, 30, 40, 50, 60, 80, 110, 140, 190};
	hind = t.Create_hindexed(10, blens, displ);
	hind.Set_name("MyHINDarray");
	hind.Commit();
	inspect_type(hind);

    MPI_Datatype hvect;
	hvect = hind.Create_hvector(10, 20, 100);
	hvect.Set_name("MyHVECT");
	hvect.Commit();
	inspect_type(hvect);

/*
    MPI_Datatype ;
	char type_name[MPI_MAX_OBJECT_NAME] = "pRecord";
	int blens[3] = { 1, 6, 1};
	MPI_Aint displ[3] = {offsetof(pRecord,type),offsetof(pRecord,x1), offsetof(pRecord,prio)};
	printf("Displs= { %d, %d, %d }\n",displ[0],displ[1],displ[2]);
    MPI_Datatype types[3] = {MPI_CHAR, MPI_INT, MPI_UNSIGNED_LONG };
  t = MPI_Datatype::Create_struct(3,blens,displ,types);
	t.Set_name(type_name);
  t.Commit();


/*
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    if( rank == 0 ){
		inspect_type(t);	
		pRecord p = { 'A', 10, 11, 12, 13, 14, 15, 12345678 };
        MPI_Send(&p,1,t,1,0, MPI_COMM_WORLD);
	}else{
        MPI_Status status;
        MPI_Recv(&p1,1,t,0,0, MPI_COMM_WORLD, &status);
		printf("p: %c, %d, %d, %d, %d, %d, %d, %ld",
			p1.type, p1.x1, p1.y1, p1.z1, p1.x2, p1.y2, p1.z2, p1.prio);
	}
*/
    MPI_Finalize();
	return 0;
}
