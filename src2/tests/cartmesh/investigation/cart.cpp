#include <stdio.h>
#include <mpi.h>

#define MX 5
#define MY 3

void straight_way()
{
    MPI_comm c = MPI_COMM_WORLD;
    MPI_Comm cc;
	int dims[2] = { MX, MY }; // <--- use straight order as in real life
	bool per[2] = { true, true };
	
	cc = c.Create_cart(2, dims, per, true);
	int rank = cc.Get_rank();
	
	if( rank == 0 ){
		printf("Straight way\n");
		for(int i = 0; i < MY; i++){
			for(int j = 0; j < MX; j++){
				int tmp[2] = {j,i}; // <--- use straight order as in real life
				int r = cc.Get_cart_rank(tmp);
				printf("(%d,%d) <-> %d\t",j,i,r); // as the result mpi ranks are in haotic order
			}
			printf("\n");
		} 
	}
}

void wrong_way()
{
    MPI_Comm c = MPI_COMM_WORLD;
    MPI_Cartcomm cc;
	int dims[2] = { MY, MX }; // <--- use reverse order as in C++ array declaration
	bool per[2] = { true, true };
	
	cc = c.Create_cart(2, dims, per, true);
	int rank = cc.Get_rank();
	
	if( rank == 0 ){
		printf("Wrong way\n");
		for(int i = 0; i < MY; i++){
			for(int j = 0; j < MX; j++){
				int tmp[2] = {j,i}; // <--- use straight order as in real life
				int r = cc.Get_cart_rank(tmp);
				printf("(%d,%d) <-> %d\t",j,i,r); // as the result mpi ranks are duplicating
			}
			printf("\n");
		} 
	}

}

void convinient_way()
{
    MPI_Comm c = MPI_COMM_WORLD;
    MPI_Comm cc;
	int dims[2] = { MY, MX }; // <--- use reverse order as in C++ array declaration
	bool per[2] = { true, true };
	
	cc = c.Create_cart(2, dims, per, true);
	int rank = cc.Get_rank();
	
	if( rank == 0 ){
		printf("Convinient way\n");
		for(int i = 0; i < MY; i++){
			for(int j = 0; j < MX; j++){
				int tmp[2] = {i,j}; // <--- use reverse order as in C++ array declaration
				int r = cc.Get_cart_rank(tmp);
				printf("(%d,%d) <-> %d\t",j,i,r); // expected result
			}
			printf("\n");
		} 
	}
}

int main(int argc, char **argv)
{
    MPI_Init(argc, argv);

	straight_way();	
	wrong_way();
	convinient_way();
	
    MPI_Finalize();
	return 0;
}
