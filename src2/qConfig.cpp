#include <qConfig.h>
#include <x86arch.h>
#include <libconfig.h++>

/* Using one qConfig object per program! */
static qConfig *qConfigInstance = NULL;

void qConfig::initialize(std::string &name){
    if( qConfigInstance )
        delete qConfigInstance;
    qConfigInstance = new qConfig(name);
}

qConfig &qConfig::instance()
{
    if( unlikely(qConfigInstance == NULL) ){
        PQDEG_ABORT("Program configuration wasn't initialized!");
    }
    return *qConfigInstance;
}

/* ------------------- Class Functions -------------------- */

void qConfig::_init(MPI_Comm _comm, std::string &name)
{
    int rank;
    MPI_Comm_rank(_comm, &rank);
    comm = _comm;

    // Initialize structures
    memset(&conf, 0, sizeof(conf));
    io.diff = time;
    io.load = 0;
	io.input = "";

    defs.clear();
    ctrlv.clear();

    // Only root access config file
    if( rank == 0 ) {
        parseConfig(name);
    }

    // Exchange with workers
    bcast();

    printf("rank=%d, _G[0]=%d, _N[0]=%d\n", rank,
           conf._G[0], conf._N[0]);
}

void qConfig::printfConfig()
{
    int rank;
    MPI_Comm_rank(comm,&rank);
    if( rank == 0 ){
        printf("ParQDEG configuration:\n");
        printf("Lattice: %dx%dx%d, z0 = %d\n",
               conf._G[0], conf._G[1], conf._G[2], conf.zSurf);
        printf("Amount of deposited material: %lf\n", conf.simAmount);
        printf("MPI: %dx%d decomp, ghost = %d, step_size=%d\n",
               conf._N[0], conf._N[1], conf.gw, conf._stepSize);
        printf("Displacement:\n");
        printf("\tlocal: 1/%d steps, %d atoms\n",
               localDispFreq(), localDispPerML());
        printf("\tsurface: 1/%d steps, %d atoms, depth = %d\n",
               surfDispFreq(), surfDispPerML(), surfDispDepth());
        printf("\tfull: 1/%d steps, %d atoms\n", fullDispFreq(),fullDispPerML());

        printf("IO:\n\t1/%lf*ML deposited atoms\n", conf.io_freq.amount);
        printf("\t1/%d steps\n", conf.io_freq.steps);
        printf("\tCompression: enabled = %d, ckpts/tar = %d\n", io.compress, io.ckpt_per_tar);
        if( io.diff == time ){
            printf("\tfile name: %s<time>%s\n", io.pref.c_str(), io.suf.c_str());
        }else if( io.diff == step ){
            printf("\tfile name: %s<step>%s\n", io.pref.c_str(), io.suf.c_str());
        }else{
            printf("\tfile name: %s<amount>%s\n", io.pref.c_str(), io.suf.c_str());
        }

        printf("Contol vector size = %d\n", conf.ctrl_size);
        printf("\tstart\tspeed\tTemp\tType\n");
        for(int i=0; i < ctrlv.size(); i++){
            control_t c = ctrlv[i];
            printf("\t%.2lf\t%.2lf\t%.2lf\t%s\n", c.start, c.speed, c.temp, type2str(c.type).c_str());
        }
    }
}

void qConfig::bcast()
{
    int size = sizeof(conf);
    MPI_Bcast(&conf, size, MPI_BYTE, 0, comm);

    size = sizeof(io.load);
    MPI_Bcast(&io.load, size, MPI_BYTE, 0, comm);

    int rank;
    control_t buf[conf.ctrl_size];

    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    if( rank == 0 ){
        for(int i = 0; i < conf.ctrl_size; i++ ){
            buf[i] = ctrlv[i];
        }
    }

    MPI_Bcast(buf, sizeof(buf), MPI_BYTE, 0, comm);

    if( rank ){
        for(int i = 0; i < conf.ctrl_size; i++ ){
            ctrlv.push_back(buf[i]);
        }
    }
}

/* ---------------- libconfig parsing -----------------------*/

static int open_config(std::string name, libconfig::Config &cfg,
                       std::stringstream &error);
static int read_MPI_settings(libconfig::Config &cfg,
                             qConfig::core_config &conf,
                             std::stringstream &error);
static int read_General_settings(libconfig::Config &cfg,
                                 qConfig::core_config &conf,
                                 std::stringstream &error);
static int read_IO_settings(libconfig::Config &cfg,qConfig::io_t &io,
                            qConfig::core_config &conf,
                            std::stringstream &error);
static int read_Control_settings(libconfig::Config &cfg,
                                 std::vector<qConfig::control_t> &ctrl_list,
                                 std::stringstream &error);

int qConfig::parseConfig(std::string name)
{
    std::ifstream st;
    libconfig::Config cfg;
    std::stringstream error("");

    // Read config
    // 1. Open/setup configuration
    if( open_config(name, cfg, error) ){
        PQDEG_ABORT("%s",error.str().c_str());
    }
    cfg.setAutoConvert(true); // convert float setting to int val and so one

    // 2. Read MPI parameters
    if( read_MPI_settings(cfg, conf, error) ){
        PQDEG_ABORT("%s",error.str().c_str());
    }

    // 3. Read Lattice parameters
    if( read_General_settings(cfg, conf, error) ){
        PQDEG_ABORT("%s",error.str().c_str());
    }

    // 4. Read IO parameters
    if( read_IO_settings(cfg, io, conf, error) ){
        PQDEG_ABORT("%s",error.str().c_str());
    }

    // 5. Read control parameters
    if( read_Control_settings(cfg, ctrlv, error) ){
        PQDEG_ABORT("%s",error.str().c_str());
    }

    // Check input data
    // 1. _G[x,y,z] has to be multiple of 4
    if( (conf._G[x] % ECUBE) || (conf._G[y] % ECUBE) || (conf._G[z] % ECUBE) ) {
        PQDEG_ABORT("One of global dimensions is not a multiple "
                "of elementary cube size");
    }

    // 2. Communicator need at least size processes
    unsigned int size = conf._N[x] * conf._N[y];
    int csize;
    MPI_Comm_size(comm, &csize);
    if( size > csize - 1 ) {
        PQDEG_ABORT("Processing grid is %dx%d + master, but communicator"
                " size is only %d", conf._N[x], conf._N[y], csize);
    }

    conf.ctrl_size = ctrlv.size();
    return 0;
}

static int open_config(std::string name, libconfig::Config &cfg, std::stringstream &error)
{
    try{
        cfg.readFile(name.c_str());
    }
    catch(const libconfig::FileIOException &fioex)
    {
      error << "File I/O error: " << name;
      return -1;
    }
    catch(const libconfig::ParseException &pex)
    {
      error << "Parse error at " << pex.getFile() << ":" << pex.getLine()
                << " - " << pex.getError();
      return -1;
    }
    return 0;
}

static int get_unsigned(libconfig::Config &cfg, std::string str,
                                 unsigned int &val,
                                 std::stringstream &error)
{
    unsigned int tmp;
    if( !cfg.lookupValue (str.c_str(), tmp) ){
        error << "No \"" << str << "\" option in configuration file";
        return -1;
    }
    if( tmp <= 0 ){
        error << "Bad \"" << str << "\" option in configuration file";
        return -2;
    }
    val = tmp;
    return 0;
}

static int get_signed(libconfig::Config &cfg, std::string str,
                      int &val, std::stringstream &error)
{
    int tmp;
    if( !cfg.lookupValue (str.c_str(), tmp) ){
        error << "No \"" << str << "\" option in configuration file";
        return -1;
    }
    val = tmp;
    return 0;
}

static int get_double(libconfig::Config &cfg, std::string str,
                      double &val, std::stringstream &error)
{
    double tmp;
    if( !cfg.lookupValue (str.c_str(), tmp) ){
        error << "No \"" << str << "\" option in configuration file";
        return -1;
    }
    val = tmp;
    return 0;
}

static int
read_MPI_settings(libconfig::Config &cfg,qConfig::core_config &conf,
                  std::stringstream &error)
{
    if( !cfg.exists("MPI") || !cfg.lookup("MPI").isGroup()){
        error << "No \"MPI\" group in configuration file";
        return -1;
    }
    if( !cfg.exists("MPI.decomp") || !cfg.lookup("MPI.decomp").isGroup()){
        error << "No \"MPI.decomp\" group in configuration file";
        return -1;
    }

    if( get_signed(cfg,"MPI.decomp.dim_x", conf._N[qConfig::x], error) ){
        return -1;
    }

    if( get_signed(cfg,"MPI.decomp.dim_y", conf._N[qConfig::y], error) ){
        return -1;
    }

    int ret;

    conf.gw = 2;
    ret = get_unsigned(cfg,"MPI.decomp.ghost_cubes", conf.gw, error);
    if( ret == -2 ){
        return -1;
    }
    error.str("");

    conf.disp_freq_l = -1;
    ret = get_signed(cfg,"MPI.local_disp_freq", conf.disp_freq_l, error);
    error.str("");

    conf.disp_perml_l = 0.01;
    get_double(cfg,"MPI.local_disp_per_ml", conf.disp_perml_l, error);
    error.str("");

    conf.disp_freq_s = -1;
    ret = get_signed(cfg,"MPI.surface_disp_freq", conf.disp_freq_s, error);
    error.str("");

    conf.disp_perml_s = 0.1;
    ret = get_double(cfg,"MPI.surface_disp_per_ml", conf.disp_perml_s, error);
    error.str("");

    conf.disp_deep_s = 8;
    ret = get_unsigned(cfg,"MPI.surface_disp_deep", conf.disp_deep_s, error);
    if( ret == -2 ){
        return -1;
    }
    error.str("");

    conf.disp_freq_f = 100;
    ret = get_signed(cfg,"MPI.full_disp_freq", conf.disp_freq_f, error);
    error.str("");
    printf("full freq = %d\n",conf.disp_freq_f);

    conf.disp_perml_f = 0.2;
    ret = get_double(cfg,"MPI.full_disp_per_ml", conf.disp_perml_f, error);
    error.str("");

    return 0;
}

static int
read_General_settings(libconfig::Config &cfg,
                      qConfig::core_config &conf,
                      std::stringstream &error)
{
    if( !cfg.exists("general") || !cfg.lookup("general").isGroup()){
        error << "No \"general\" group in configuration file";
        return -1;
    }

    int ret;

    if( get_signed(cfg,"general.dim_x", conf._G[qConfig::x], error) ){
        return -1;
    }

    if( get_signed(cfg,"general.dim_y", conf._G[qConfig::y], error) ){
        return -1;
    }

    if( get_signed(cfg,"general.dim_z", conf._G[qConfig::z], error) ){
        return -1;
    }

    conf.zSurf = 20;
    if( get_signed(cfg,"general.z_surface", conf.zSurf, error) == -2 ){
        return -1;
    }
    error.str("");

/* TODO: we will use it once program hardens
    if( get_double(cfg,"general.total_time", conf.simTime, error) ){
        return -1;
    }
*/
    if( get_double(cfg,"general.total_amount", conf.simAmount, error) ){
        return -1;
    }

    if( get_unsigned(cfg,"general.step_size", conf._stepSize, error) == -2 ){
        return -1;
    }

    return 0;
}

static int read_IO_settings(libconfig::Config &cfg,qConfig::io_t &io,
                                       qConfig::core_config &conf,
                                       std::stringstream &error)
{
	// Check if IO section is presented
    if( !cfg.exists("IO") || !cfg.lookup("IO").isGroup()){
        error << "No \"IO\" group in configuration file";
        return -1;
    }

    // Read load option. If not specified consider as false
    if( !cfg.lookupValue ("IO.load", io.input) ){
		io.input = "";
    } else {
        io.load = 1;
    }

    // Check IO.file group
    if( !cfg.exists("IO.file") || !cfg.lookup("IO.file").isGroup()){
        error << "No \"IO.file\" group in configuration file";
        return -1;
    }

    // Read prefix
    if( !cfg.lookupValue ("IO.file.prefix", io.pref) ){
        io.pref = "A";
    }

    // Read suffix
    if( !cfg.lookupValue ("IO.file.suffix", io.suf) ){
        io.suf = ".xyz";
    }

    // Read suffix
    if( !cfg.lookupValue ("IO.stat_file", io.stat_fname) ){
        io.stat_fname = "pqdeg_stat.txt";
    }

    // Read file differentiation type. If not specified - time
    std::string tmp;
    if( !cfg.lookupValue ("IO.file.diff", tmp) ){
        io.diff = qConfig::time;
    }else{
        if( tmp == "time" ){
            io.diff = qConfig::time;
        }else if( tmp == "step" ){
            io.diff = qConfig::step;
        }else if( tmp == "amount" ){
            io.diff = qConfig::amount;
        }else{
            error << "Bad \"IO.file.diff\" option. May be \"time\" or \"step\"";
            return -1;
        }
    }

    // Check IO.freq group
    if( !cfg.exists("IO.freq") || !cfg.lookup("IO.freq").isGroup()){
        error << "No \"IO.freq\" group in configuration file";
        return -1;
    }

    // Read amount of deposited material between outputs
    if( !cfg.lookupValue ("IO.freq.amount", conf.io_freq.amount) ){
        error << "No \"IO.freq.amount\" setting in configuration file";
        return -1;
    }

    // Read steps between output
    if( !cfg.lookupValue ("IO.freq.steps", conf.io_freq.steps) ){
        error << "No \"IO.freq.steps\" setting in configuration file";
        return -1;
    }

    // Read steps between output
    conf.io_freq.sync = 100;
    cfg.lookupValue ("IO.freq.sync", conf.io_freq.sync);
    if( conf.io_freq.sync > conf.io_freq.steps ){
        conf.io_freq.sync = conf.io_freq.steps;
    }

    // Check IO.compress group
    if( cfg.exists("IO.compress") && cfg.lookup("IO.compress").isGroup() ){
        // Read prefix
        if( !cfg.lookupValue ("IO.compress.enable", io.compress) ){
            io.compress = 0;
        }

        // Read suffix
        if( !cfg.lookupValue ("IO.compress.ckpt_per_tar", io.ckpt_per_tar) ){
            io.ckpt_per_tar = 1;
        }
    }

    return 0;
}

static int
read_Control_settings(libconfig::Config &cfg,
                      std::vector<qConfig::control_t> &ctrlv,
                      std::stringstream &error)
{

    // Check if control section is presented
    if( !cfg.exists("control") ){
        error << "No \"control\" setting found in configuration file";
        return -1;
    }

    if( !cfg.lookup("control").isList()){
        error << "\"control\" setting must be a list of groups";
        return -1;
    }

    ctrlv.erase(ctrlv.begin(),ctrlv.end());
    const libconfig::Setting &controls = cfg.lookup("control");
    int count = controls.getLength();

    for(int i = 0; i < count; ++i)
    {
        const libconfig::Setting &ctrl = controls[i];
        qConfig::control_t c;
        std::string str;
        if(!(ctrl.lookupValue("start", c.start) && ctrl.lookupValue("speed", c.speed)
             && ctrl.lookupValue("type", str) && ctrl.lookupValue("temp", c.temp))){
            error << "Bad \"control[" << i << "]\" element in configuration file";
            ctrlv.erase(ctrlv.begin(),ctrlv.end());
            return -1;
        }
        if( str == "Si"){
            c.type = atomType::Si;
        }else if( str == "Ge" ){
            c.type = atomType::Ge;
        }else{
            c.type = atomType::None;
            if( c.speed > 0 ){
                error << "Bad \"control[" << i << "]\" element in configuration file: ";
                error << "bad deposition atom type";
            }
        }
        ctrlv.push_back(c);
    }

    return 0;
}
