#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char **argv)
{
	MPI::Init(argc,argv);
	
	MPI::Intracomm world, c;
	
	world = MPI::COMM_WORLD;
	int rank = world.Get_rank();
	int size1 = world.Get_size();
	c = world.Split( rank > 4 && rank < 10, 0 );
	
	if( !(rank > 4 && rank < 10) ){
		printf("%d: exiting!\n",rank);
		MPI::Finalize();
		exit(0);
	}
	
	int size2 = world.Get_size();
	printf("%d: Survived process. oldsize=%d, newsize=%d, newrank = %d\n",rank, size1, size2, world.Get_rank());
	
	world.Bcast
	
	MPI::Finalize();
	return 0;
}