/* 
 * File:   latPoint.h
 * Author: artpol
 *
 * Created on 11 Март 2013 г., 17:10
 */

#ifndef LATPOINT_H
#define	LATPOINT_H

#include <string.h>
#include <types.h>

#define LPSPEC "%ld"
#define LPFORMAT "(" LPSPEC "," LPSPEC "," LPSPEC ")"

class lPoint {
public:
  static const int dim = 3;
protected:
  coord_t v[dim];
public:

  lPoint() {
    for (int i = 0; i < dim; i++)
      v[i] = 0;
  }

  lPoint(coord_t x, coord_t y, coord_t z) {
    v[0] = x;
    v[1] = y;
    v[2] = z;
  }
  
  lPoint(coord_t *_v) {
    memcpy(v, _v, vsize());
  }
  

  inline coord_t &x() {
    return v[0];
  }

  inline coord_t &y() {
    return v[1];
  }

  inline coord_t &z() {
    return v[2];
  }

  inline coord_t *get_v() {
    return v;
  }

  inline void set_v(coord_t *_v) {
    memcpy(v, _v, vsize());
  }

  inline coord_t vsize() {
    return sizeof (v);
  }

  void set(coord_t x, coord_t y, coord_t z) {
    v[0] = x;
    v[1] = y;
    v[2] = z;
  }

  inline lPoint operator+(lPoint rp) {
    lPoint ret;
    ret.x() = this->x() + rp.x();
    ret.y() = this->y() + rp.y();
    ret.z() = this->z() + rp.z();
    return ret;
  }

  inline lPoint mod(lPoint rp) {
    lPoint ret;
    ret.x() = this->x() % rp.x();
    ret.y() = this->y() % rp.y();
    ret.z() = this->z() % rp.z();
    return ret;
  }

};

#endif	/* LATPOINT_H */
