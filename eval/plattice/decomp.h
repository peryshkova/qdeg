/* 
 * File:   decomp.h
 * Author: artpol
 *
 * Created on 11 Март 2013 г., 16:01
 */

#ifndef DECOMP_H
#define	DECOMP_H

#include <debug.h>
#include <mpi.h>
#include <stdlib.h>
#include <cartmesh.h>
#include <lPoint.h>

class decomp {
public:
  static const uint ecube = 4; // Elementary cube dimensions
  static const uint n = 2; // 
  typedef char relmap_t[3][3];
private:
  cartmesh<n> _mesh;
  // lattice configuration
  coord_t lcubes[n]; // lattice cubes in each dimension
  // decomposition
  coord_t offs[n], blksz[n], gwidth;

  // decomposition of data
  void distribute(coord_t cur, coord_t max, coord_t data, coord_t &blk,
      coord_t &off);

  inline int offset(int i) {
    return offs[i] * ecube;
  }

  inline int blocksize(int i) {
    return blksz[i] * ecube;
  }

  inline int _ghost(int dim) {
    return gwidth * ecube * (_mesh[dim] > 1);
  }

public:

  decomp() {
  }

  decomp(MPI_Comm comm, uint lattice[], int grid[], uint _gw = 1) {
    init(comm, lattice, grid, _gw);
  }
  void init(MPI_Comm comm, uint lattice[], int grid[], uint _gw = 1);
  void glob2local(lPoint g, lPoint &l);
  void local2glob(lPoint l, lPoint &g);
  bool is_local(lPoint p);
  bool is_neighbor(lPoint p);
  bool is_related(lPoint p, relmap_t map);
  void nbr_map(lPoint p, relmap_t nmap);
  void comm_map(relmap_t rmap, relmap_t nmap, relmap_t cmap);
  bool comm_pattern(lPoint p, relmap_t map);
  void outputMap(relmap_t map, char *prefix);

  cartmesh<n> &mesh() {
    return _mesh;
  }

  int dim() {
    return n;
  }

  inline lPoint gdims() {
    coord_t t[lPoint::dim] = {0};
    for (int i = 0; i < n; i++)
      t[i] = lcubes[i] * ecube;
    return lPoint(t);
  }

  coord_t gdim_x() {
    return lcubes[0] * ecube;
  }

  coord_t gdim_y() {
    return lcubes[1] * ecube;
  }

  inline lPoint gcubs() {
    coord_t t[lPoint::dim] = {0};
    for (int i = 0; i < n; i++)
      t[i] = lcubes[i];
    return lPoint(t);
  }

  coord_t gcub_x() {
    return lcubes[0];
  }

  coord_t gcub_y() {
    return lcubes[1];
  }

  inline lPoint ldims() {
    coord_t t[lPoint::dim] = {0};
    for (int i = 0; i < n; i++)
      t[i] = blksz[i] * ecube;
    return lPoint(t);
  }

  coord_t ldim_x() {
    return blksz[0] * ecube;
  }

  coord_t ldim_y() {
    return blksz[1] * ecube;
  }

  inline lPoint lcubs() {
    coord_t t[lPoint::dim] = {0};
    for (int i = 0; i < n; i++)
      t[i] = blksz[i];
    return lPoint(t);
  }

  coord_t lcub_x() {
    return blksz[0];
  }

  coord_t lcub_y() {
    return blksz[1];
  }

  inline lPoint ghosts() {
    coord_t t[lPoint::dim] = {0};
    for (int i = 0; i < n; i++)
      t[i] = gwidth * ecube * (_mesh[i] > 1);
    return lPoint(t);
  }

  inline coord_t ghostx() {
    return gwidth * ecube * (_mesh[0] > 1);
  }

  inline coord_t ghosty() {
    return gwidth * ecube * (_mesh[1] > 1);
  }

  inline lPoint ldims_wgh() {
    coord_t t[lPoint::dim] = {0};
    for (int i = 0; i < n; i++)
      t[i] = blksz[i] * ecube + 2 * _ghost(i);
    return lPoint(t);
  }

  inline coord_t ldimwgh_x() {
    return ecube * blksz[0] + 2 * ghostx();
  }

  inline coord_t ldimwgh_y() {
    return ecube * blksz[1] + 2 * ghosty();
  }

  inline lPoint lcubswgh() {
    coord_t t[lPoint::dim] = {0};
    for (int i = 0; i < n; i++)
      t[i] = blksz[i] + 2 * _ghost(i) / ecube;
    return lPoint(t);
  }

  inline coord_t lcubwgh_x() {
    return blksz[0] + 2 * _ghost(0) / ecube;
  }

  inline coord_t lcubwgh_y() {
    return blksz[1] + 2 * _ghost(1) / ecube;
  }
};


#endif	/* DECOMP_H */

