#include <plattice.h>
#include <qdegcfg.h>
#include <mpi.h>
#include <stdio.h>

int main(int argc, char **argv)
{
  int size, rank;
  qdegcfg qcfg;
  plattice_t<int> pl(MPI_INTEGER);
  FILE *o;
  char name[256];

  //  if (GetConfig(CONFIG_NAME, qcfg)) {
  //    EPRINTF("Cannot read configuration file: %s\n", CONFIG_NAME);
  //    exit(0);
  //  }

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  qcfg.Lx = 128;
  qcfg.Ly = 128;
  qcfg.Lz = 4;
  qcfg.z_surf = 2;
  qcfg.xdim = 4;
  qcfg.ydim = 4;

  if (pl.init(qcfg, MPI_COMM_WORLD)) {
    MPI_Finalize();
    exit(1);
  }

  if (pl.in_use()) {

    sprintf(name, "./out/out%d", rank);
    o = fopen(name, "w");
    int z;
    /*		
                    for(z = pl.startz(0); z < pl.endz(0); z++ ){
                      for(int y = pl.starty(z,0); y < pl.endy(0); y+=2){
                            fprintf(o,"{ ");
                        for(int x = pl.startx(z,y,0); x < pl.endx(0); x+=4){
                          fprintf(o,"(%d,%d,%d):%d ",x,y,z,pl(x,y,z));
                        }
                        fprintf(o,"}\n");
                      }
                      fprintf(o,"\n\n");
                    }

    /*
            int P = 1;
            while(P){
                sleep(1);
            }
     */
    for (z = pl.startz(0); z < qcfg.z_surf; z++) {
      for (int y = pl.starty(z); y < pl.endy(); y += 2) {
        for (int x = pl.startx(z, y); x < pl.endx(); x += 4) {
          pl(x, y, z) = rank + 1;
        }
      }
    }

    /*
                fprintf(o,"------------------PREPARED -----------------------\n\n");
		
                    for(int z1 = pl.startz(0); z1 < pl.endz(0); z1++ ){
                      for(int y = pl.starty(z1,0); y < pl.endy(0); y+=2){
                            fprintf(o,"{ ");
                        for(int x = pl.startx(z1,y,0); x < pl.endx(0); x+=4){
                          fprintf(o,"(%d,%d,%d):%d ",x,y,z1,pl(x,y,z1));
                        }
                        fprintf(o,"}\n");
                      }
                      fprintf(o,"\n\n");
                    }
     */

    fprintf(o, "------------------ SYNC -----------------------\n\n");

    pl.sync();


    for (z = pl.startz(0); z < pl.endz(0); z++) {
      for (int y = pl.starty(z, 0); y < pl.endy(0); y += 2) {
        fprintf(o, "{ ");
        for (int x = pl.startx(z, y, 0); x < pl.endx(0); x += 4) {
          fprintf(o, "%d ", pl(x, y, z));
        }
        fprintf(o, "}\n");
      }
      fprintf(o, "\n\n");
    }

    fclose(o);
  }

  MPI_Finalize();
  return 0;
}
