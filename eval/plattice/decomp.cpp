/* 
 * File:   decomp.cpp
 * Author: artpol
 *
 * Created on 15 Март 2013 г., 16:13
 */

#include <decomp.h>

void decomp::distribute(ulong cur, ulong max, ulong data, ulong &blk, ulong &off) {
  ulong modulo = data % max;
  blk = data / max;
  off = cur * blk;
  if (cur < modulo) {
    blk += 1;
    off += cur;
  } else {
    off += modulo;
  }
}

void decomp::init(MPI_Comm comm, uint lattice[], int grid[], uint _gw) {
  _mesh.init(comm, grid);
  gwidth = _gw; // number of ghost elementary cubes

  for (int i = 0; i < n; i++) {
    if (lattice[i] % ecube) {
      DPRINTF("Error, lattice[%d]=%d: each dimension has to be modulo 4 (elementary cube)",
          i, lattice[i]);
    }
    lcubes[i] = lattice[i] / ecube;
  }

  // Create cartesian communicator
  int crd[n];
  _mesh.coords(crd);
  for (int i = 0; i < n; i++) {
    distribute(crd[i], _mesh[i], lcubes[i], blksz[i], offs[i]);
  }
  /*
  DPRINTF("(%d,%d): (%d,%d) +/- (%d,%d)", crd[0], crd[1], offs[0], offs[1],
          blksz[0], blksz[1]);
   */
}

inline void
decomp::glob2local(lPoint g, lPoint &l) {
  coord_t *gv = g.get_v();
  l = g;
  coord_t *lv = l.get_v();
  for (int i = 0; i < n; i++) {
    lv[i] = gv[i] - offset(i);
    if (_mesh[i] > 1) {
      lv[i] += ecube;
    }
  }

  /*
  DPRINTF("(%d,%d,%d) - (%d,%d,0) = (%d,%d,%d)",
          g.x(), g.y(), g.z(),
          offset(0),offset(1),
          l.x(), l.y(), l.z());
   */
}

inline void
decomp::local2glob(lPoint l, lPoint &g) {
  coord_t *gv = g.get_v();
  coord_t *lv = l.get_v();
  for (int i = 0; i < n; i++) {
    gv[i] = lv[i] + offs[i];
    if (_mesh[i] > 1) {
      gv[i] -= ecube;
    }
  }
}

bool decomp::is_local(lPoint p) {
  coord_t *v = p.get_v();
  bool local = true;
  for (int i = 0; i < n; i++) {
    //DPRINTF("%d <= %d <= %d", ecube * offs[i], v[i], ecube * (offs[i] + blksizes[i]));
    if (!(v[i] >= offset(i) && v[i] < (offset(i) + blocksize(i))))
      local = false;
  }
  return local;
}

bool decomp::is_neighbor(lPoint p) {
  lPoint loc;
  this->glob2local(p, loc);
  bool ret = true;
  coord_t *v = loc.get_v();
  lPoint d = ldims_wgh();


  coord_t *max = d.get_v();
  for (int i = 0; i < n; i++) {
    if (!(v[i] >= 0 && v[i] < max[i]))
      ret = false;
  }
  return ret && !is_local(p);
}

bool decomp::is_related(lPoint p, relmap_t map) {
  lPoint loc;
  this->glob2local(p, loc);
  bool ret = false;
  lPoint max = gdims();
  lPoint lmax = this->ldims_wgh();
  max.z() = 1;

  DPRINTF("local: (" LPSPEC "," LPSPEC "," LPSPEC ")", loc.x(), loc.y(), loc.z());
  DPRINTF("max: (" LPSPEC ","  LPSPEC ","  LPSPEC ")", max.x(), max.y(), max.z());
  loc = loc + max;
  DPRINTF("local + MAX: (" LPSPEC "," LPSPEC "," LPSPEC ")", loc.x(), loc.y(), loc.z());
  loc = loc.mod(max);
  DPRINTF("(local + MAX)%%MAX: (" LPSPEC "," LPSPEC ","  LPSPEC ")", loc.x(), loc.y(), loc.z());

  bool mapdims[n][3];
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < 3; j++) {
      mapdims[i][j] = false;
    }
    coord_t *v = loc.get_v();
    coord_t *mv = lmax.get_v();
    if( v[i] >= 0 && v[i] < ecube ) {
      mapdims[i][0] = true;
    } else if (v[i] >= ecube && v[i] <= mv[i] - ecube) {
      mapdims[i][1] = true;
    } else if (v[i] < mv[i]) {
      mapdims[i][2] = true;
    }
  }

  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      map[i][j] = mapdims[1][i] * mapdims[0][j];
      ret = ret || map[i][j];
    }
  }
  if (ret)
    outputMap(map, "MAP:");
  return ret;
}

void decomp::nbr_map(lPoint p, relmap_t nmap) {
  lPoint loc;
  this->glob2local(p, loc);
  bool ret = false;
  lPoint max = ldims_wgh();
  max.z() = 1;

  DPRINTF("local: (" LPSPEC "," LPSPEC "," LPSPEC ")", 
      loc.x(), loc.y(), loc.z());
  DPRINTF("max: (" LPSPEC "," LPSPEC "," LPSPEC ")", 
      max.x(), max.y(), max.z());
  loc = loc + max;
  DPRINTF("local + MAX: (" LPSPEC "," LPSPEC "," LPSPEC ")", 
      loc.x(), loc.y(), loc.z());
  loc = loc.mod(max);
  DPRINTF("(local + MAX)%%MAX: (" LPSPEC "," LPSPEC "," LPSPEC ")", 
      loc.x(), loc.y(), loc.z());

  coord_t *v = loc.get_v();
  coord_t *mv = max.get_v();
  uint mapdims[n][3];
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < 3; j++) {
      mapdims[i][j] = false;
    }

    if (v[i] < 2 * ecube) {
      mapdims[i][0] = true;
    } else if (v[i] >= 2 * ecube && v[i] <= mv[i] - 2 * ecube) {
      mapdims[i][1] = true;
    } else if (v[i] < mv[i] ) {
      mapdims[i][2] = true;
    }

    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        nmap[i][j] = 0;
      }
    }
    for (int i = 0; i < 3; i++) {
      nmap[1][i] = mapdims[0][i];
      nmap[i][1] = mapdims[1][i];
    }
  }

  outputMap(nmap, "Neighbors MAP:");
}

/*
 * Communication MAP has following view:
 * 	0 1 0 
 * 	0 0 2 
 * 	0 0 0 
 * This means we get infromation from [+1][0] neighbor
 * and send it to [0][-1] neighbor
 */
void decomp::comm_map(relmap_t rmap, relmap_t nmap, relmap_t cmap) {
  int i, j;
  int flag = 0;

  if (rmap[1][1]) {
    for (i = 0; i < 3; i++) {
      for (j = 0; j < 3; j++) {
        cmap[i][j] = nmap[i][j];
        flag += cmap[i][j];
      }
    }
    return;
  }

  for (i = 0; i < 3; i++) {
    for (j = 0; j < 3; j++) {
      cmap[i][j] = rmap[i][j] + nmap[i][j];
      flag += cmap[i][j];
    }
  }
  if (cmap[0][1] == 2 || cmap[2][1] == 2) {
    cmap[1][0] = cmap[1][2] = 0;
  } else {
    int flag = 0;
    for (i = 0; i < 3; i++) {
      for (j = 0; j < 3; j++) {
        if (cmap[i][j] == 2)
          flag = 1;
      }
    }
    if (!flag) {
      cmap[0][1] *= 2;
      cmap[2][1] *= 2;
      cmap[1][0] = cmap[1][2] = 0;
    }
  }
}

bool decomp::comm_pattern(lPoint p, relmap_t map) {
  bool ret;
  relmap_t rmap, nmap;
  if (ret = is_related(p, rmap)) {
    nbr_map(p, nmap);
    comm_map(rmap, nmap, map);
    outputMap(map, "COMM MAP:");
  }
  return ret;
}

void decomp::outputMap(relmap_t map, char *prefix) {
  char str[1024] = "";
  strcat(str, prefix);
  strcat(str, "\n\t");
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      sprintf(str, "%s%d ", str, (int) map[i][j]);
    }
    sprintf(str, "%s\n\t", str);
  }
  DPRINTF("%s", str);
}
