/* 
 * File:   distrPoint.h
 * Author: artpol
 *
 * Created on 11 Март 2013 г., 17:24
 */

#ifndef DPOINT_H
#define	DPOINT_H

#include <lPoint.h>
#include <decomp.h>

class dPoint {
private:
  lPoint _local, _global;
  decomp &dcmp;

public:

  dPoint(decomp &_dcmp) : dcmp(_dcmp)
  {
  }

  dPoint(decomp &_dcmp, lPoint p) : dcmp(_dcmp)
  {
    glob(p);
  }
  
  void glob(coord_t x, coord_t y, coord_t z)
  {
    _global.set(x, y, z);
    _local = _global;
    dcmp.glob2local(_global, _local); // will touch only x & y!!
  }

  void glob(lPoint p)
  {
    _global = p;
    _local = _global;
    dcmp.glob2local(_global, _local); // will touch only x & y!!
  }

  void glob(coord_t *_v)
  {
    _global.set_v(_v);
    _local = _global;
    dcmp.glob2local(_global, _local); // will touch only x & y!!
  }

  lPoint &glob()
  {
    return _global;
  }

  void local(coord_t x, coord_t y, coord_t z)
  {
    _local.set(x, y, z);
    _global = _local;
    dcmp.local2glob(_local, _global); // will touch only x & y!!
  }

  void local(lPoint p)
  {
    _local = p;
    _global = _local;
    dcmp.local2glob(_local, _global); // will touch only x & y!!
  }

  void local(coord_t *_v)
  {
    _local.set_v(_v);
    _global = _local;
    dcmp.local2glob(_local, _global); // will touch only x & y!!
  }

  lPoint &local()
  {
    return _local;
  }

  inline bool is_local() {
    return dcmp.is_local(_global);
  }

  
  inline bool check_bounds() {
    return is_local();
  }
  
};

#endif	/* DISTRPOINT_H */

