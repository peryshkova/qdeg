#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H

#define CONFIG_NAME "qdeg.cfg"
#include <types.h>

enum atom_t { Si = 1, Ge = 2 };

class qdegcfg
{
  public:
    // MPI-related configuration: nodes per dimension
    int npdim[2];
    // Lattice configuration
    uint lsizes[3];
	  ulong z_surf;
    uchar z_type;
    // Checkpointing configuration
    double rm_start;  // time before first RASMOL file generation
	double rm_delta;     // time between RASMOL file generation
    
    // Simulation options
    double E1, E2;
	double p0, pdep;
	double T, A, B;
	double eps;
    double def_F;
    double mv_per_ml;
	double dt_ctrl;
  	double exptime;
	  ulong rndseed;
  	int dep_type;
	  int z_cap;
	  int Edef_pa_N;
    int ndef;
	  char load[100];
    ulong avg_deform_n;	

    qdegcfg();
    
};

class nodecfg
{
	public:
    ulong Lx, Ly, Lz, zsurf;
		nodecfg(){
      Lx = 0;
      Ly = 0; 
      Lz = 0;
      zsurf = 0;
    }
		
    void load(qdegcfg &cfg){
      Lx = cfg.lsizes[0];
      Ly = cfg.lsizes[1]; 
      Lz = cfg.lsizes[2];
      zsurf = cfg.z_surf;
    }
};

int GetConfig(char *fname, qdegcfg &cfg);

#endif