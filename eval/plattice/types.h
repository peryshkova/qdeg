/* 
 * File:   types.h
 * Author: artpol
 *
 * Created on 16 Январь 2013 г., 10:42
 */

#ifndef TYPES_H
#define	TYPES_H

#include <string.h>

typedef unsigned long ulong;
typedef unsigned int uint;
typedef unsigned char uchar;
typedef unsigned short ushort;
typedef ulong coord_t;
enum dumpTypes { CHAR, SHORT, INT, LONG, FLOAT, DOUBLE };

#endif	/* TYPES_H */
