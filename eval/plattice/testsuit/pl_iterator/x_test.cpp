
#include <stdio.h>
#include <plattice.h>

#define Lz 200
#define Ly 1024
#define Lx 1024

#undef ZYX
#undef ALL_Z
#undef ALL_Y
#undef ALL_X

#define ZYX       ALL_Z ALL_Y ALL_X   // перебор атомов решётки алмаза, кроме 2 верхних и 2 нижних слоёв
#define ALL_Z     for (z=2; z<Lz; z++)       // z может быть любым
#define ALL_Y     for (y=((int)z%2); y<Ly; y+=2)  // y должно иметь ту же чётность, что и z
#define ALL_X     for (x=((int)z%2)+2*(((int)z/2+(int)y/2)%2); x<Lx; x+=4)  // x идёт через 4

int main(int argc, char **argv)
{
  MPI_Init(&argc, &argv);
  init_debug(stderr);

  plattice::iterator c(Lx, false, Ly, false, Lz);
  
  c.fix(2,8);

  FILE *fp = fopen("iter.log", "w");
  for (; c < c.end(); ++c) {
    lPoint p;
    p = *c;
    fprintf(fp, "(%d,%d,%d)\n", p.x(), p.y(), p.z());
  }
  fclose(fp);

  fp = fopen("old.log", "w");

  {
    int x, y = 8, z = 2;
    ALL_X
    {
      fprintf(fp, "(%d,%d,%d)\n", x, y, z);
    }
  }

  fclose(fp);
  MPI_Finalize();
}
