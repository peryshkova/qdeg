#include <plattice.h>
#include <qdegcfg.h>
#include <mpi.h>
#include <stdio.h>

#include <debug.h>

#define Lz 200
#define Ly 1024
#define Lx 1024

#undef ZYX
#undef ALL_Z
#undef ALL_Y
#undef ALL_X

#define ZYX(sx,ex,sy,ey,sz,ez)       ALL_Z(sz,ez) ALL_Y(sy,ey) ALL_X(sx,ex)   // перебор атомов решётки алмаза, кроме 2 верхних и 2 нижних слоёв
#define ALL_Z(s,e)     for (z=s; z<e; z++)       // z может быть любым
#define ALL_Y(s,e)     for (y=s+((int)z%2); y<e; y+=2)  // y должно иметь ту же чётность, что и z
#define ALL_X(s,e)     for (x=s+((int)z%2)+2*(((int)z/2+(int)y/2)%2); x<e; x+=4)  // x идёт через 4

int main(int argc, char **argv)
{
  int size, rank;
  qdegcfg qcfg;
  FILE *o;
  char name[256];
  int x,y,z;

  //  if (GetConfig(CONFIG_NAME, qcfg)) {
  //    EPRINTF("Cannot read configuration file: %s\n", CONFIG_NAME);
  //    exit(0);
  //  }

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  
  sprintf(name,"rank%d.log",rank);
  FILE *s = fopen(name,"w");
  init_debug(s);
  
  qcfg.lsizes[0] = Lx;
  qcfg.lsizes[1] = Ly;
  qcfg.lsizes[2] = Lz;
  qcfg.z_surf = 4;
  qcfg.npdim[0] = 4;
  qcfg.npdim[1] = 2;

  if( argc >= 3 ){
    qcfg.npdim[0] = atoi(argv[1]);
    qcfg.npdim[1] = atoi(argv[2]);
  }
  
  decomp dcmp;
  dcmp.init(MPI_COMM_WORLD, (uint*)qcfg.lsizes, qcfg.npdim);

  plattice::plattice_t<int> pl;
  pl.init(qcfg, dcmp, MPI_INT);

  plattice::iterator it = pl.begin();
  int startx = 4*(dcmp.mesh()[0] > 1);
  int starty = 4*(dcmp.mesh()[1] > 1);
  int endx = dcmp.ldim_x()- 4*(dcmp.mesh()[0] > 1);
  int endy = dcmp.ldim_y()- 4*(dcmp.mesh()[1] > 1);
  ZYX(startx,endx,starty,endy,2,Lz-2){
    if( (*it).x() != x || (*it).y() != y || (*it).z() != z){
      DPRINTF("ERROR for point (" LPSPEC "," LPSPEC "," LPSPEC ") -> "
          "iter=(" LPSPEC "," LPSPEC "," LPSPEC ")",
              x, y, z, (*it).x(), (*it).y(), (*it).z());
    }
    it++;
  }

  fclose(s);
  MPI_Finalize();
  
  return 0;
}
