#include <decomp.h>
#include <qdegcfg.h>
#include <iostream>

using namespace std;

void try_point(decomp &d, lPoint p)
{
  int rank, size;
  decomp::relmap_t cmap;
  
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  int i, j;
  int myrank = d.mesh().rank();
  if (d.comm_pattern(p, cmap)) {
    char s[256] = "";
    /*
        sprintf(s, "(%d) is related:\n", rank);
        for (i = 0; i < 3; i++) {
          strcat(s, "\t");
          for (j = 0; j < 3; j++) {
            sprintf(s, "%s%d ", s, cmap[i][j]);
          }
          strcat(s, "\n");
        }
     */
    int buf = 1;
    for (i = 0; i < 3; i += 2) {
      int crd[2];
      d.mesh().coords(crd);
      if (cmap[1][i] == 2) {
        crd[0] += i - 1;
        int rank = d.mesh().coords2rank(crd);
        MPI_Status status;
        MPI_Recv(&buf, 1, MPI_INT, rank, 0, d.mesh().comm(),&status);
        buf++;        
        sprintf(s, "%sMPI_Recv(%d,%d): %d\n", s, rank, myrank, buf);
      }
    }

    for (i = 0; i < 3; i += 2) {
      int crd[2];
      d.mesh().coords(crd);
      if (cmap[i][1] == 2) {
        crd[1] += i - 1;
        int rank = d.mesh().coords2rank(crd);
        MPI_Status status;
        MPI_Recv(&buf, 1, MPI_INT, rank, 0, d.mesh().comm(),&status);
        buf++;
        sprintf(s, "%sMPI_Recv(%d,%d): %d\n", s, rank, myrank, buf);
      }
    }

    for (i = 0; i < 3; i += 2) {
      int crd[2];
      d.mesh().coords(crd);
      if (cmap[1][i] == 1) {
        crd[0] += i - 1;
        int rank = d.mesh().coords2rank(crd);
        MPI_Send(&buf, 1, MPI_INT, rank, 0, d.mesh().comm());
        sprintf(s, "%sMPI_Send(%d,%d): %d\n", s, myrank, rank, buf);
      }
    }

    for (i = 0; i < 3; i += 2) {
      int crd[2];
      d.mesh().coords(crd);
      if (cmap[i][1] == 1) {
        crd[1] += i - 1;
        int rank = d.mesh().coords2rank(crd);
        MPI_Send(&buf, 1, MPI_INT, rank, 0, d.mesh().comm());
        sprintf(s, "%sMPI_Send(%d,%d): %d\n", s, myrank, rank, buf);        
      }
    }

    printf("%s", s);
  }
}

int main(int argc, char **argv)
{
  qdegcfg qcfg;
  int rank, size;
  int coords[2];


  MPI_Init(&argc, &argv);
  init_debug(stdout);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  qcfg.lsizes[0] = 96;
  qcfg.lsizes[1] = 60;
  qcfg.lsizes[2] = 32;
  qcfg.z_surf = 8;
  qcfg.z_type = 1;
  qcfg.npdim[0] = 3;
  qcfg.npdim[1] = 3;

  if (argc >= 3) {
    qcfg.npdim[0] = atoi(argv[1]);
    qcfg.npdim[1] = atoi(argv[2]);
    if (argc >= 5) {
      qcfg.lsizes[0] = atoi(argv[3]);
      qcfg.lsizes[1] = atoi(argv[4]);
    }
  }


  decomp d;
  d.init(MPI_COMM_WORLD, (uint*)qcfg.lsizes, qcfg.npdim, 1);



  cartmesh < 2 > &m = d.mesh();
  int crank = m.rank();
  int csize = m.size();
  m.coords(coords);

  /* 
  for(int i=5; i<32;i+=8){
        for(int j=5; j< 32; j+=8){
                lPoint p(i,j,4);
                        try_point(d,p);
                        MPI_Barrier(MPI_COMM_WORLD);
                }
        }
   */

  lPoint p(29, 19, 4);
  try_point(d, p);

  MPI_Finalize();

  return 0;
}
