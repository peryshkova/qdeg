#include <stdio.h>
#include <iostream>
#define DEBUG_ON
#include<debug.h>

#include <cartmesh.h>

#define P(c,m) \
  DPRINTF("(%d,%d).rank = %d", c[0], c[1], m.coords2rank(c));

int main(int argc, char **argv)
{
  MPI_Init(&argc, &argv);
  
  init_debug(stdout);
  cartmesh<2> m;
  uint dims[] = {3,4}; 
  int coords[2];
  
  m.init(MPI_COMM_WORLD,dims);
  m.coords(coords);
  
  DPRINTF("%d(%d): (%d,%d)",m.rank(),m.size(),coords[0],coords[1]);
  
  if( m.rank() == 0 ){
    int c1[] = {0,1};
    int c2[] = {1,0};
    int c3[] = {1,1};
    int c4[] = {1,2};
    int c5[] = {2,2};
    int c6[] = {2,3};    
    P(c1,m);
    P(c2,m);
    P(c3,m);
    P(c4,m);
    P(c5,m);
    P(c6,m);    
  }
  
  MPI_Finalize();
}