#include <plattice.h>
#include <qdegcfg.h>
#include <mpi.h>
#include <stdio.h>

#include <debug.h>

using namespace plattice;

int main(int argc, char **argv)
{
  int size, rank;
  qdegcfg qcfg;
  FILE *o;
  char name[256];

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  init_debug(stderr);

  qcfg.lsizes[0] = 8;
  qcfg.lsizes[1] = 8;
  qcfg.lsizes[2] = 8;
  qcfg.z_surf = 4;
  qcfg.npdim[0] = 1;
  qcfg.npdim[1] = 1;

  if (argc >= 3) {
    qcfg.npdim[0] = atoi(argv[1]);
    qcfg.npdim[1] = atoi(argv[2]);
    if (argc >= 5) {
      qcfg.lsizes[0] = atoi(argv[3]);
      qcfg.lsizes[1] = atoi(argv[4]);
    }
  }

  if (rank == 0) {
    DPRINTF("Mesh: %dx%d, Lsizes: %dx%d",
            qcfg.npdim[0], qcfg.npdim[1], qcfg.lsizes[0], qcfg.lsizes[1]);
  }

  decomp dcmp;
  dcmp.init(MPI_COMM_WORLD, (uint*)qcfg.lsizes, qcfg.npdim);

  if (rank == 0) {
    DPRINTF("Local mesh: %ldx%ld",dcmp.ldim_x(), dcmp.ldim_y() );
  }

  plattice_t<int> pl;
  pl.init(qcfg, dcmp, MPI_INT);

  iterator it = pl.begin();
  for (; it < pl.end(); it++) {
    pl(*it) = 10*(rank + 1) + (*it).x()/4;
  }

  sprintf(name, "dump%d.before", rank);
  FILE *fp = fopen(name, "w");
  pl.dump(fp);


  pl.sync();

  sprintf(name, "dump%d.after", rank);
  fp = fopen(name, "w");
  pl.dump(fp);


  MPI_Finalize();
  return 0;
}
