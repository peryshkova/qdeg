#include <decomp.h>
#include <qdegcfg.h>
#include <iostream>
#include <dPoint.h>
#include <debug.h>

using namespace std;

int main(int argc, char **argv)
{
  qdegcfg qcfg;
  int rank, size;
  int coords[2];


  MPI_Init(&argc, &argv);
  init_debug(stdout);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  qcfg.lsizes[0] = 96;
  qcfg.lsizes[1] = 60;
  qcfg.lsizes[2] = 12;
  qcfg.z_surf = 8;
  qcfg.z_type = 1;
  qcfg.npdim[0] = 3;
  qcfg.npdim[1] = 3;

  if (argc >= 3) {
    qcfg.npdim[0] = atoi(argv[1]);
    qcfg.npdim[1] = atoi(argv[2]);
    if (argc >= 5) {
      qcfg.lsizes[0] = atoi(argv[3]);
      qcfg.lsizes[1] = atoi(argv[4]);
    }
  }

  decomp d;
  d.init(MPI_COMM_WORLD, (uint*)qcfg.lsizes, qcfg.npdim, 1);

  lPoint p1(16,8,4), p2(29,19,4), p3(62,10,4), p4(33,38,4), p5(62,38,4);
  dPoint dm[] = { dPoint(d,p1),dPoint(d,p2),dPoint(d,p3),dPoint(d,p4),dPoint(d,p5)};

  int crank = d.mesh().rank();
  int csize = d.mesh().size();


  char str[1024] = "";
  for(int i=0; i<5; i++){
        sprintf(str, "%s\tPoint: global=(" LPSPEC "," LPSPEC "," LPSPEC ")"
            "local=(" LPSPEC "," LPSPEC "," LPSPEC ")\n",
        	str,
          dm[i].glob().x(), dm[i].glob().y(), dm[i].glob().z(),
          dm[i].local().x(), dm[i].local().y(), dm[i].local().z());
  }

  DPRINTF("%s", str);

  
  MPI_Finalize();

  return 0;
}
