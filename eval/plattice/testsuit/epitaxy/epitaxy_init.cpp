#include <mpi.h>
#include <stdio.h>

#include <epitaxy.h>
#include <debug.h>

int main(int argc, char **argv)
{
  int size, rank;
  qdegcfg qcfg;
  FILE *o;
  char name[256];

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  init_debug(stderr);

  qcfg.lsizes[0] = 8;
  qcfg.lsizes[1] = 8;
  qcfg.lsizes[2] = 8;
  qcfg.z_surf = 4;
  qcfg.z_type = 1;
  qcfg.npdim[0] = 1;
  qcfg.npdim[1] = 1;
  
  lPoint lp(1,1,5);

  if (argc >= 3) {
    qcfg.npdim[0] = atoi(argv[1]);
    qcfg.npdim[1] = atoi(argv[2]);
    if (argc >= 5) {
      qcfg.lsizes[0] = atoi(argv[3]);
      qcfg.lsizes[1] = atoi(argv[4]);
    }
    if (argc >= 8) {
      lp.set(atoi(argv[5]),atoi(argv[6]),atoi(argv[7]));
    }
  }

  
  if (rank == 0) {
    DPRINTF("Mesh: %dx%d, Lsizes: %dx%d",
            qcfg.npdim[0], qcfg.npdim[1], qcfg.lsizes[0], qcfg.lsizes[1]);
  }

  epitaxy ep(MPI_COMM_WORLD, qcfg);
  DPRINTF("inuse=%d",ep.inuse());
  if( ep.inuse() )
    ep.dump("dump");
  
  MPI_Finalize();
  return 0;
}
