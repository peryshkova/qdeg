/* 
 * File:   plattice_iter.h
 * Author: artpol
 *
 * Created on 29 Март 2013 г., 15:58
 */

#ifndef PLATTICE_ITER_H
#define	PLATTICE_ITER_H

#include <mpi.h>
#include <qdegcfg.h>
#include <dlattice.h>
#include <types.h>

namespace plattice {

  class iterator {
  private:
    coord_t _x, _y, _z;
    coord_t _ghostx, _ghosty;
    coord_t _xoff, _yoff, _zoff;
    coord_t _maxx, _maxy, _maxz;
    inline void _set_y_offset();
    inline void _set_x_offset();

  public:

    inline coord_t startx() {
      return _ghostx + _xoff;
    }

    inline coord_t starty() {
      return _ghosty + _yoff;
    }

    inline coord_t startz() {
      return _zoff;
    }

    inline coord_t maxx() {
      return _maxx - _xoff - _ghostx;
    }

    inline coord_t maxy() {
      return _maxy - _yoff - _ghosty;
    }

    inline coord_t maxz() {
      return _maxz - _zoff;
    }

    iterator(coord_t maxx, coord_t ghost_x, coord_t maxy, coord_t ghost_y, coord_t maxz);
    inline void zoffs(coord_t zoff);
    inline void yoffs(coord_t yoff);
    inline void xoffs(coord_t xoff);
    inline void limit(coord_t z);
    inline void fix(coord_t z);
    inline void fix(coord_t z, coord_t y);
    inline iterator &operator++();
    inline iterator operator++(int);
    inline bool operator<(iterator rhs);
    iterator end();
    lPoint operator*();
    inline lPoint local();
  };

  // Iterators

  inline void iterator::_set_y_offset() {
    _y = _z % 2 + starty();
  }

  inline void iterator::_set_x_offset() {
    _x = startx();
    _x += (_z % 2) + 2 * ((_z / 2 + _y / 2) % 2);
  }

  iterator::iterator(coord_t maxx, coord_t ghostx,
      coord_t maxy, coord_t ghosty, coord_t maxz) {
    _maxz = maxz;
    _maxy = maxy;
    _maxx = maxx;
    _ghostx = ghostx;
    _ghosty = ghosty;
    _xoff = 0;
    _yoff = 0;
    _zoff = 2;
    _z = startz();
    _set_y_offset();
    _set_x_offset();
  }

  inline void iterator::zoffs(coord_t zoff) {
    _zoff = zoff;
    _z = zoff;
    _set_y_offset();
    _set_x_offset();
  }


  inline void iterator::yoffs(coord_t yoff) {
    _y = yoff;
    _set_x_offset();
  }

  inline void iterator::xoffs(coord_t xoff) {
    _xoff = xoff;
  }

  inline void iterator::fix(coord_t z) {
    _z = z;
    _maxz = z + 1;
    _zoff = 0;
    _set_y_offset();
    _set_x_offset();
    DPRINTF("Fix iterator: (" LPSPEC "," LPSPEC "," LPSPEC ")", _x, _y, _z );
    DPRINTF("Fix iterator (max): ( 0,0," LPSPEC ")", _maxz );
  }

  inline void iterator::limit(coord_t z) {
    _maxz = z;
  }
  
  inline void iterator::fix(coord_t z, coord_t y) {
    _z = z;
    _maxz = z + 1;
    _zoff = 0;
    _y = y;
    _maxy = y + 1;
    _set_x_offset();
    DPRINTF("Fix iterator: (" LPSPEC "," LPSPEC "," LPSPEC ")", _x, _y, _z );
    DPRINTF("Fix iterator (max): ( 0,0," LPSPEC ")", _maxz );
  }

  inline iterator & iterator::operator++() {
//    DPRINTF("Iterator (next.before): (" LPSPEC "," LPSPEC "," LPSPEC ")", _x, _y, _z );
    _x += 4;
    if (_x >= maxx()) {
      _y += 2;
      if (_y >= maxy()) {
        _z++;
        _set_y_offset();
      }
      _set_x_offset();
    }
    return *this;
  }

  inline iterator
  iterator::operator++(int v) {
    iterator it = *this;
    _x += 4;
    if (_x >= maxx()) {
      _y += 2;
      if (_y >= maxy()) {
        _z++;
        _set_y_offset();
      }
      _set_x_offset();
    }
    return it;
  }

  inline bool iterator::operator<(iterator rhs) {
     //DPRINTF("Compare (" LPSPEC "," LPSPEC "," LPSPEC ") and ("
     //LPSPEC "," LPSPEC "," LPSPEC ")", _x, _y, _z, rhs._x, rhs._y, rhs._z );

    if (this->_z < rhs._z)
      return true;
    else if (this->_z > rhs._z)
      return false;
    else {
      if (this->_y < rhs._y)
        return true;
      if (this->_y > rhs._y)
        return false;
      else {
        if (this->_x < rhs._x)
          return true;
        else
          return false;
      }
    }
  }

  inline iterator iterator::end() {
    iterator tmp = *this;
    tmp._x = 0;
    tmp._y = 0;
    tmp._z = maxz();
    //DPRINTF("(" LPSPEC "," LPSPEC "," LPSPEC ")", tmp._x, tmp._y, tmp._z );
    return tmp;
  }

  inline lPoint iterator::operator*() {
    lPoint t(_x, _y, _z);
    return t;
  }
  
  inline lPoint iterator::local() {
    lPoint t(_x - _ghostx, _y - _ghosty, _z);
    return t;
  }

};

#endif	/* PLATTICE_ITER_H */

