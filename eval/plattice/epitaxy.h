/* 
 * File:   epitaxy.h
 * Author: artpol
 *
 * Created on 6 Февраль 2013 г., 10:34
 */

#ifndef EPITAXY_H
#define	EPITAXY_H

#include <stdio.h>
#include <string>
#include <mpi.h>

#include <types.h>
#include <plattice.h>
#include <qdegcfg.h>
#include <utils.h>
#include <debug.h>
#include <lattcfg.h>
#include <decomp.h>


using namespace plattice;

class epitaxy {
  //private:
public:
  int mrank, msize;
  int wrank, wsize;
  MPI_Comm mainComm, workComm;
  plattice_t<uchar> type;
  plattice_t<ushort> conf;
  dlattcfg config;
  decomp dcmp;
  qdegcfg qcfg;
  std::string hostname;
  bool used;

  void clear() {


  }

public:

  int init_hostname() {
    char tmp[256] = {0};
    if (!gethostname(tmp, 255))
      hostname = tmp;
    else
      hostname = "";
  }

  bool prepare_comm(MPI_Comm &comm) {
    int _size, _rank;
    MPI_Comm_rank(comm, &_rank);
    MPI_Comm_size(comm, &_size);
    msize = qcfg.npdim[0] * qcfg.npdim[1] + 1; // amount we need to do the work
    if (_size < msize) {
      EPRINTF("Number of allocated processes (%d) doesn't correspond to\n"
          "requested amount: (%dx%d+1)=%d", _size, qcfg.npdim[0], qcfg.npdim[1],
          msize);
      MPI_Abort(mainComm, 0);
    }
    // 1. Create main communicator
    // remove all unnesessary processes
    used = true;
    MPI_Comm_split(MPI_COMM_WORLD, (_rank < msize), 1, &mainComm);
    MPI_Comm_rank(comm, &mrank);
    if (_rank >= msize) {
      used = false;
      return used;
    }
    MPI_Comm_rank(mainComm, &mrank);
    MPI_Comm_size(mainComm, &msize);

    // 2. Split main communicator onto master and workers communicators
    MPI_Comm_split(mainComm, !!(mrank), 1, &workComm);
    MPI_Comm_rank(workComm, &wrank);
    MPI_Comm_size(workComm, &wsize);
    return used;
  }

public:

  bool inuse() {
    return used;
  }

  epitaxy(MPI_Comm comm, qdegcfg &cfg) {
    qcfg = cfg;
    if (!prepare_comm(comm))
      return;

    init_hostname();
    if (!mrank) {
      EPRINTF("ROOT process started on %s\n", hostname.c_str());

    } else {
      EPRINTF("WORKER process #%d started on %s\n", wrank, hostname.c_str());
      dcmp.init(workComm, qcfg.lsizes, qcfg.npdim);
      type.init(qcfg, dcmp, MPI_CHAR);
      conf.init(qcfg, dcmp, MPI_SHORT);

      // initialize type lattice
      iterator it = type.begin();
      it.zoffs(0);
      it.limit(qcfg.z_surf);
      for (; it < type.end(); it++) {
        type(*it) = qcfg.z_type;
      }
      type.sync();

      // initialize config lattice
      it = conf.begin();
      for (; it < conf.end(); it++) {
        conf(*it) = config.set_config(type, *it);
      }
      conf.sync();
    }
  }

  void dump(char *dump_pref) {
    char fname[256];
    if (mrank) {
      FILE *fp;

      sprintf(fname, "%s-type.%d.out", dump_pref, wrank);
      fp = fopen(fname, "w");
      type.dump(fp);
      fclose(fp);

      sprintf(fname, "%s-conf.%d.out", dump_pref, wrank);
      fp = fopen(fname, "w");
      conf.dump(fp);
      fclose(fp);
    }
  }

    // Deposition procedures
    int exch_depPoint(int rank, atom_t &t, int &finish, lPoint & p);
    int send_depPoint(int root, atom_t t, int finish, lPoint p);
    int recv_depPoint(atom_t &t, int &finish, lPoint & p);
    int deposition_r(qdegcfg &cfg, atom_t t);
    int deposit_local(lPoint & la);
    int deposition_w(int root);
    void commit_deposition(lPoint a, atom_t t);

    void deposition(atom_t type) {
      if (mrank == 0) {
        deposition_r(qcfg, type);
      } else {
        deposition_w(0);
      }
    }
};

#endif	/* EPITAXY_H */
