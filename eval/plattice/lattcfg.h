/* 
 * File:   configuration.h
 * Author: artpol
 *
 * Created on 6 Февраль 2013 г., 11:02
 */

#ifndef CONFIGURATION_H
#define	CONFIGURATION_H

#include <plattice.h>

#include <types.h>

#define Nconfig 65536 // 2^16

using namespace plattice;

class dlattcfg{
private:
  uchar n1_config[Nconfig];
  uchar n2_config[Nconfig];
  ushort good_config[Nconfig];
public:

  dlattcfg() {
    int i, j;
    int n1_; // number of direct neighbors
    int n2_; // number of 2-hop neighbors that are CONNECTED
    int n2__; // number of non-zero 2-hop neighbors
    unsigned short int config_;
    int nb_type[dir_number];
    for (config_ = 0;; config_++) {
      fill_nb_type(nb_type, config_);
      n1_ = n2_ = n2__ = 0;
      for (i = 0; i < dir_number; i++)
        if (pr_neighbor[i] < 0 && nb_type[i] > 0)
          n1_++;
      for (i = 0; i < dir_number; i++)
        if (pr_neighbor[i] >= 0) {
          j = pr_neighbor[i];
          if (nb_type[i] > 0 && nb_type[j] > 0)
            n2_++;
          if (nb_type[i] > 0 /*&& nb_type[j]>0*/)
            n2__++;
        }
      n1_config[config_] = n1_;
      n2_config[config_] = n2__;
      good_config[config_] = (n2_ >= 2);
      if (config_ == (Nconfig - 1)) break;
    }
  }

  inline bool is_good(int config){  return good_config[config];
  }

  void fill_nb_type(int* nb_type, ushort config_) {
    ushort conf;
    int i;
    conf = config_;
    for (i = 0; i < dir_number; i++) {
      nb_type[i] = ((conf & 0x8000) != 0);
      conf <<= 1;
    }
  }

  ushort set_config(plattice_t<uchar> &type, lPoint p) {
    int dir, factor;
    uchar nb_type[dir_number];

    neighbors_t nbs;
    factor = type.lattice().neighbors(p.x(),p.y(),p.z(), nbs);

    for (dir = 0; dir < dir_number; dir++) {
      //DPRINTF("Type of ((%d,%d,%d) is %d ",nbs.x[dir]-4, nbs.y[dir]-4, nbs.z[dir], type(nbs.x[dir], nbs.y[dir], nbs.z[dir])  );
      nb_type[dir] = type(nbs.x[dir], nbs.y[dir], nbs.z[dir]);
    }

    char c[1024];
    sprintf(c,"(" LPSPEC "," LPSPEC "," LPSPEC "): ",p.x()-4,p.y()-4,p.z());
    for(int i=0;i<dir_number;i++){
        sprintf(c, "%s%d ",c,nb_type[i]);
    }
    //DPRINTF("%s",c);
    return array2config(nb_type);
  }

  ushort array2config(uchar* nb_type) {
    unsigned short int conf;
    int i;
    conf = 0;
    for (i = 0; i < dir_number; i++) {
      conf <<= 1;
      conf += (nb_type[i] != 0);
    }
    return conf;
  }
};



#endif	/* CONFIGURATION_H */

