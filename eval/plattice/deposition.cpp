#include <stdio.h>
#include <epitaxy.h>

int epitaxy::exch_depPoint(int rank, atom_t &t, int &finish, lPoint &p)
{
  static int callnum = 0;
  int buf[p.vect_elems() + 2];
  buf[0] = finish;
  buf[1] = (int)t;
  memcpy(buf + 2, p.vect(), p.vect_size());
  // 2. Broadcast deposition type and coordinates
  int ret = MPI_Bcast(buf, sizeof(buf)/sizeof(buf[0]), MPI_INTEGER, rank, MPI_COMM_WORLD);
  finish = buf[0];
  t = (atom_t)buf[1];
  p.vect(buf + 2);
  DPRINTF("#%d: finish = %d, type = %d", ++callnum, finish, t);
  return ret;
}

int epitaxy::send_depPoint(int root, atom_t t, int finish, lPoint p)
{
  int buf[p.vect_elems() + 2];
  buf[0] = finish;
  buf[1] = t;
  memcpy(buf + 2, p.vect(), p.vect_size());

  int ret = MPI_Send(buf, sizeof(buf)/sizeof(buf[0]), MPI_INTEGER, root, 0, MPI_COMM_WORLD);

  DPRINTF("%d: Send to %d, finish=%d, (%d,%d,%d)", rank, root,
         finish, p.x(), p.y(), p.z());
  return ret;
}

int epitaxy::recv_depPoint(atom_t &t, int &finish, lPoint &p)
{
  MPI_Status status;
  int buf[p.vect_elems() + 2];
  int ret = MPI_Recv(buf, sizeof(buf)/sizeof(buf[0]), MPI_INTEGER,
                     MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
  finish = buf[0];
  t = (atom_t)buf[1];
  p.vect(buf + 2);
  DPRINTF("%d: Recv from %d, finish=%d, type=%d, (%d,%d,%d)", rank, status.MPI_SOURCE,
         finish, t, p.x(), p.y(), p.z());
  return ret;
}

int epitaxy::deposition_r(qdegcfg &cfg, atom_t t)
{
  lPoint p, ls;

  ls.x() = cfg.Lx;
  ls.y() = cfg.Ly;
  ls.z() = cfg.Lz;

  int buf[p.vect_elems() + 1];
  // 1. Calculate start point of the deposition
  p.z() = ls.z();
  p.y() = (p.z() % 2) + 2 * random_(ls.y() / 2);
  p.x() = (p.z() % 2) + 2 * ((p.z() / 2 + p.y() / 2) % 2) + 4 * random_(ls.x() / 4);

  DPRINTF("SEND Start deposition coordinate: (%d,%d,%d)", p.x(), p.y(), p.z());

  int finish = 0;
  exch_depPoint(rank, t, finish, p);
  
  int i = 1;
  do {
    recv_depPoint(t, finish, p);
    lPoint p1 = p;
    // Keep point inside lattice
    p1.x() = (p1.x() + cfg.Lx) % cfg.Lx;
    p1.y() = (p1.y() + cfg.Ly) % cfg.Ly;
    
    DPRINTF("Middle (%d) deposition coordinate: (%d,%d,%d), fixed: (%d,%d,%d)", i, 
            p.x(), p.y(), p.z(), p1.x(), p1.y(), p1.z());
    i++;
    p = p1;
    
    fflush(stdout);
    
    exch_depPoint(rank, t, finish, p);
  } while (!finish);

  DPRINTF("Finish deposition. Final coordinates: (%d,%d,%d)", p.x(), p.y(), p.z());
}

int epitaxy::deposit_local(lPoint &la)
{
  lPoint tmp;
  int f = 0, i = 0, dir;
  int trials = 1000;
  
  while (!f && type.check_bounds(la) && i < trials)
  {
    if (la.z() % 2 == 0)
      dir = (random_(2) == 0) ? 1 : 2;
    else
      dir = (random_(2) == 0) ? 0 : 3;
    type.one_neighbor(la, dir, tmp);

    if (type(tmp) != 0)
    {
      dir = random_(4);
      type.one_neighbor(la, dir, tmp);
      i++;
    }

    DPRINTF("type(%d,%d,%d) = %d",tmp.x(),tmp.y(),tmp.z(), type(tmp));
    for(i=0;i<16;i++){
      lPoint t;
      type.one_neighbor(tmp, i, t);
      DPRINTF("\t\ttype(%d,%d,%d) = %d",t.x(),t.y(),t.z(), type(t));
    }

    if( type(tmp) == 0)
    {
#ifdef DEBUG_ON
      lPoint gla, gtmp;
      type.local2glob(la,gla);
      type.local2glob(tmp,gtmp);
      DPRINTF("Move from (%d,%d,%d) to (%d,%d,%d): (%d,%d,%d)->(%d,%d,%d)",
             la.x(), la.y(), la.z(), tmp.x(), tmp.y(), tmp.z(),
             gla.x(), gla.y(), gla.z(), gtmp.x(), gtmp.y(), gtmp.z());
#endif
      la = tmp;
    }

    if ( type(la) == 0 && config.is_good(cfg(la))) {
      f = 1;
      break;
    }
  }

  DPRINTF("finish = %d", !f);

  return f;
}

void epitaxy::commit_deposition(lPoint a, atom_t t)
{
  if (type.is_local(a) || type.is_neighbor(a)) {
    DPRINTF("Point a(%d,%d,%d) is: local=%d, neighbor=%d",a.x(), a.y(), a.z(), 
            type.is_local(a), type.is_neighbor(a) );
    lPoint l;
    type.glob2local(a,l);
    type(l) = (uchar)t;
    neighbors_t nbs;
    type.neighbors(l, nbs);
    for (int dir = 0; dir < dir_number; dir++) {
      lPoint p(nbs.x[dir], nbs.y[dir], nbs.z[dir]);
      lPoint gp;
      type.local2glob(p,gp);
      if( type.is_local(gp) ){
        DPRINTF("Set config for p(%d,%d,%d), gp(%d,%d,%d)",p.x(), p.y(), p.z(), gp.x(), gp.y(), gp.z() );
        cfg(p) = config.set_config(type, p);
      }
    }
  }
  
  cfg.sync();


/*
  atoms(x, y, z).type = type_of_new_atom;

  vector<lPoint> list;
  list.erase();
  list.push_back(a);

  neighbors_t nbs;
  atoms.neighbors(x, y, z, nbs);

  for (dir2 = 0; dir2 < dir_number; dir2++) {
    v_spisok(nbs.x[dir2], nbs.y[dir2], nbs.z[dir2]);
    set_config(nbs.x[dir2], nbs.y[dir2], nbs.z[dir2]);
    if (unlikely(atoms(nbs.x[dir2], nbs.y[dir2], nbs.z[dir2]).type > 0 &&
                 !good_config[atoms(nbs.x[dir2], nbs.y[dir2], nbs.z[dir2]).config])) {
      fprintf(stderr, "Error!!! bad config of atom (%d,%d,%d)\n",
              nbs.x[dir2], nbs.y[dir2], nbs.z[dir2]);
      abort();
    }
  }

  for (n = 0; n < n_spisok; n++) {
    x2 = spisok[n][0];
    y2 = spisok[n][1];
    z2 = spisok[n][2];
    calc_B0(x2, y2, z2);
  }

  axyz(x, y, z); // выставим ax,ay,az для нового атома

  n_spisok2 = n_spisok;
  for (n = 0; n < n_spisok2; n++) {
    x2 = spisok[n][0];
    y2 = spisok[n][1];
    z2 = spisok[n][2];
    atoms.neighbors(x2, y2, z2, nbs);
    for (dir3 = 0; dir3 < dir_number; dir3++) {
      v_spisok(nbs.x[dir3], nbs.y[dir3], nbs.z[dir3]);
    }
  }

  for (n = 0; n < n_spisok; n++) {
    x3 = spisok[n][0];
    y3 = spisok[n][1];
    z3 = spisok[n][2];
    set_jump_info(x3, y3, z3);
    if (n_jumps(x3, y3, z3) == 0) calc_jump_probability(x3, y3, z3);
    v_ochered_Edef(x3, y3, z3);
    spisok_flag(x3, y3, z3) = 0;
  }

  add_atom_to_spisok(x, y, z);
*/
}


int epitaxy::deposition_w(int root)
{
  atom_t t;
  lPoint a, la, a1;
  int dir;
  int factor;

  int finish = 0;
  exch_depPoint(root, t, finish, a);

  DPRINTF("GET Start coordinate: (%d,%d,%d)", a.x(), a.y(), a.z());

  do {
    if (type.is_local(a)) {
      type.glob2local(a, la);
      DPRINTF("Coordinate (%d,%d,%d) is local to me: (%d,%d,%d)", a.x(), a.y(), a.z(), la.x(), la.y(), la.z());
      finish = !(deposit_local(la));
      type.local2glob(la,a);
      DPRINTF("SEND final coordinate global: (%d,%d,%d), local: (%d,%d,%d)", a.x(), a.y(), a.z(), la.x(), la.y(), la.z());
      send_depPoint(root, t, finish, la);
    }else{
      DPRINTF("Coordinate (%d,%d,%d) is NOT local to me", a.x(), a.y(), a.z());
    }

    fflush(stdout);
    exch_depPoint(root, t, finish, a);

    DPRINTF("EXCHANGE, coordinates: (%d,%d,%d), finish = %d", a.x(), a.y(), a.z(), finish);
  } while (!finish);

  commit_deposition(a, t);
  return 0;
}
