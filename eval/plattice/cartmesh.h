/* 
 * File:   cartOps.h
 * Author: artpol
 *
 * Created on 12 Март 2013 г., 9:41
 */

#ifndef CARTOPS_H
#define	CARTOPS_H

#include <types.h>
#include <mpi.h>
#include <debug.h>

template <int n>
class cartmesh {
private:
  MPI_Comm oldComm;
  int osize, orank;
  int _size, _rank;
  MPI_Comm _comm;
  /* User prefer to have least significant dimension (like x) in dims[0], but 
   MPI_Cart_create waits for the most significant dimension (like z) there*/
  int dims[n]; // original dimensions from class user
  int mpidims[n]; // dimensions for MPI_Cart_create
  int mpicoords[n], _coords[n];

  void reverse(int src[], int dst[]);

public:

  cartmesh() {
  }

  int init(MPI_Comm c, int _dims[]);
  void coords(int c[]);
  int coords2rank(int c[]);
  void rank2coords(int r, int c[]);

  MPI_Comm &comm() {
    return _comm;
  }

  int rank() {
    return _rank;
  }

  int size() {
    return _size;
  }

  int operator [](int idx) {
    return dims[idx];
  }
  
  int  *pdims(){ return dims; }
  
};

template <int n>
void cartmesh<n>::reverse(int src[], int dst[]) {
  for (int i = 0; i < n; i++) {
    dst[i] = src[n - i - 1];
  }
}

template <int n>
int cartmesh<n>::init(MPI_Comm c, int _dims[]) {
  int periods[n];
  int chksize = 1;
  oldComm = c;
  for (int i = 0; i < n; i++) {
    periods[i] = 1;
    dims[i] = _dims[i];
    mpidims[n - i - 1] = dims[i];
    chksize *= dims[i];
  }

  MPI_Comm_size(oldComm, &osize);
  MPI_Comm_rank(oldComm, &orank);

  if (osize != chksize && !orank) {
    EPRINTF("Size of the old communicator (%d) doesn't correspond to the multiply of dimensions (%d)\n",
        osize, chksize);
    MPI_Abort(MPI_COMM_WORLD, 0);
  }


  MPI_Cart_create(oldComm, n, mpidims, periods, 1, &_comm);
  MPI_Comm_size(_comm, &_size);
  MPI_Comm_rank(_comm, &_rank);
  MPI_Cart_coords(_comm, _rank, n, mpicoords);
  reverse(mpicoords, _coords);
  return 0;
}

template <int n>
void cartmesh<n>::coords(int c[]) {
  for (int i = 0; i < n; i++) {
    c[i] = _coords[i];
  }
}

template <int n>
int cartmesh<n>::coords2rank(int c[]) {
  int tmp[n], r;
  reverse(c, tmp);
  MPI_Cart_rank(_comm, tmp, &r);
  return r;
}

template <int n>
void cartmesh<n>::rank2coords(int r, int c[]) {
  int tmp[n];
  MPI_Cart_coords(_comm, r, n, tmp);
  reverse(tmp, c);
}

#endif	/* CARTOPS_H */

