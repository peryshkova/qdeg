#include <stdio.h>
#include <plattice.h>
#include <types.h>
#include <epitaxy.h>

int main(int argc, char **argv)
{
  qdegcfg qcfg;
  epitaxy *ep;
  int rank;
    
  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  //printf("Rank=%d, pid=%d\n",rank, getpid());
  init_debug();
  
  
  qcfg.Lx = 32;
  qcfg.Ly = 32;
  qcfg.Lz = 16;
  qcfg.z_surf = 4;
  qcfg.z_type = Si;  
  qcfg.xdim = 2;
  qcfg.ydim = 2;

  ep = new epitaxy(qcfg);
  
  for(int i=0; i<1; i++){
      ep->deposition(Ge);
  }
  
  ep->dump("dump");
  MPI_Finalize();
  return 0;
}