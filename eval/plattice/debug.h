/* 
 * File:   debug.h
 * Author: artpol
 *
 * Created on 16 Январь 2013 г., 17:32
 */

#ifndef DEBUG_H
#define	DEBUG_H

#include <mpi.h>
#include <stdio.h>

#define DEBUG_ON // Define it in each file separately now!!!!

extern FILE *__parqdeg_dbgout__;
extern int __rank__;



#define EPRINTF(fmt,args...)\
{ \
   int __r__; \
   MPI_Comm_rank(MPI_COMM_WORLD, &__r__); \
   fprintf(stderr, "(%d) %s: " fmt "\n", __r__, __FUNCTION__,## args  ); \
   fflush(stderr); \
}

#define ABORT_PROG(code) { MPI_Abort(MPI_COMM_WORLD, code); }
#define DPRINTF(fmt,args...)


#ifdef DEBUG_ON

static void init_debug(FILE *out = NULL)
{
   __parqdeg_dbgout__ = out;
   MPI_Comm_rank(MPI_COMM_WORLD, &::__rank__);
   if( ::__parqdeg_dbgout__ == NULL ){
     char tmp[256];
     sprintf(tmp, "rank%d.log",::__rank__);
     ::__parqdeg_dbgout__ = fopen(tmp,"w");
     if( ::__parqdeg_dbgout__ == NULL ){
         ::MPI_Abort(MPI_COMM_WORLD,0);
     }
   }
}



#undef  DPRINTF
#define DPRINTF(fmt,args...)\
{ \
   fprintf(__parqdeg_dbgout__, "(%d) %s: " fmt "\n", __rank__ , __FUNCTION__, ## args  ); \
   fflush(__parqdeg_dbgout__); \
}

#else

static void init_debug(FILE *out = NULL)
{
}

#endif


#endif	/* DEBUG_H */

