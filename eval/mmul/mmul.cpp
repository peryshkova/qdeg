#include <stdio.h>
#include <unistd.h>
#include <time.h>

#include <pmmintrin.h>

void mmul_ser(float matrix[][4], float *x, float *res)
{
    int i,j;
    for( i=0;i<4;i++){
        res[i] = 0;
        for(j=0;j<4;j++){
            res[i] += matrix[i][j]*x[j];
        }
    }
}

void mmul_vect(float matrix[][4], float *x, float *res)
{
    int i,j;
    __m128 *m = (__m128 *)matrix;
    __m128 *v = (__m128 *)x;
    __m128 r[4];
    for(i=0;i<4;i++){
        r[i] = _mm_mul_ps(m[i], *v);
    }
    r[0] = _mm_hadd_ps(r[0], r[1]);
    r[2] = _mm_hadd_ps(r[2], r[3]);
    r[0] = _mm_hadd_ps(r[0], r[2]);
    *( (__m128*)res) = r[0];
}

int main()
{
//    float matrix1[4][4] __attribute__((aligned(64))) = { {1,0,0,0}, {0,1,0,0},{0,0,1,0},{0,0,0,1} } ;
    float matrix1[4][4] __attribute__((aligned(64))) = { {1,1,1,1}, {2,2,2,2},{1,1,1,1},{1,1,1,1} } ;
    float x[4] __attribute__((aligned(64))) = {1,2,3,4} ;
    float y[4] __attribute__((aligned(64)));

	struct timespec ts1, ts2;
	double time;
    
	clock_gettime(CLOCK_MONOTONIC,&ts1);
	for(int i = 0; i<10000000; i++){
       mmul_ser(matrix1,x,y);
	}	
	clock_gettime(CLOCK_MONOTONIC,&ts2);
	time = ts2.tv_sec - ts1.tv_sec;
	time += ts2.tv_nsec/1E9 - ts1.tv_nsec/1E9;
	printf("Serial time: %lf\n",time);

    printf("y=(");
    for(int i=0;i<4;i++){
        printf("%f ",y[i]);
        y[i] = 0;
    }
    printf(")\n");

	clock_gettime(CLOCK_MONOTONIC,&ts1);
	for(int i = 0; i<10000000; i++){
       mmul_vect(matrix1,x,y);
	}	
	clock_gettime(CLOCK_MONOTONIC,&ts2);
	time = ts2.tv_sec - ts1.tv_sec;
	time += ts2.tv_nsec/1E9 - ts1.tv_nsec/1E9;
	printf("Vector time: %lf\n",time);
    
    printf("y=(");
    for(int i=0;i<4;i++){
        printf("%f ",y[i]);
        y[i] = 0;
    }
    printf(")\n");
}