#include <stdio.h>

int main()
{
	unsigned int y, n = ~(unsigned int)0;
	
	for(y=0; y < n; y++){
		unsigned int p1 = ( (y >> 2) << 1) + ((y & 0x3) >> 1);
		unsigned int p2 = y >> 1;
		if( p2 != p1){
			printf("Not equivalent: y=%u, p1=%u, p2=%u\n", y, p1, p2);
			break;
		}
	}
}