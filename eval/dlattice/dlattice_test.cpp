#include <stdio.h>
#include "dlattice.h"
#include <unistd.h>
#include <time.h>

#define Lx  1000
#define Ly  1000
#define Lz  300

double calc_error()
{
	int i;
	struct timespec ts1, ts2;
	double time, sum = 0;

	
	for(i=0;i<1000;i++){
		clock_gettime(CLOCK_MONOTONIC,&ts1);
		clock_gettime(CLOCK_MONOTONIC,&ts2);
		time = ts2.tv_sec - ts1.tv_sec;
		time += ts2.tv_nsec/1E9 - ts1.tv_nsec/1E9;
		//printf("%.15lf\n",time);
		sum += time;
	}
	
	return sum/1000;
}


#define ZYX       ALL_Z ALL_Y ALL_X   // перебор атомов решётки алмаза, кроме 2 верхних и 2 нижних слоёв
#define ALL_Z     for (z=4; z<Lz-4; z++)       // z может быть любым
#define ALL_Y     for (y=((int)z%2)+4; y<Ly-4; y+=2)  // y должно иметь ту же чётность, что и z
#define ALL_X     for (x=((int)z%2)+2*(((int)z/2+(int)y/2)%2)+4; x<Lx-4; x+=4)  // x идёт через 4


int main()
{
	dlattice_t<int> l;
	neighbors_t nbs_s, nbs_sp;
	neighbors_t nbs_v, nbs_vp;
	int x,y,z;
	double time, sum = 0;
	struct timespec ts1, ts2;
	
	l.init(100,20,20);
	l(0,0,0) = 10;
	l(1,1,1) = 15;
	l(5,5,5) = 1;
	printf("%d, %d, %d\n",	l(0,0,0), l(1,1,1), l(5,5,5) );
	
	double err = calc_error();
	
	ZYX{
	  l.neighbors_serial(x, y, z, nbs_s, nbs_sp);
		l.neighbors_vect(x, y, z, nbs_v, nbs_vp);
		
		for(int i=0;i<16;i++){
			if( (nbs_s.x[i] != nbs_v.x[i]) || (nbs_s.y[i] != nbs_v.y[i]) || (nbs_s.z[i] != nbs_v.z[i]) ){
				printf("Error found on (%d,%d,%d), neigh #%d: orig(%d,%d,%d), vect(%d,%d,%d)\n",
					x,y,z,i,nbs_s.x[i], nbs_s.y[i], nbs_s.z[i], nbs_v.x[i],nbs_v.y[i],nbs_v.z[i]);
				abort();
			}

			if( (nbs_sp.x[i] != nbs_vp.x[i]) || (nbs_sp.y[i] != nbs_vp.y[i]) || (nbs_sp.z[i] != nbs_vp.z[i]) ){
				printf("Error found (packed) on (%d,%d,%d), neigh #%d: orig(%d,%d,%d), vect(%d,%d,%d)\n",
					x,y,z,i,nbs_sp.x[i], nbs_sp.y[i], nbs_sp.z[i], nbs_vp.x[i],nbs_vp.y[i],nbs_vp.z[i]);
				abort();
			}
		}
	}	
	
	clock_gettime(CLOCK_MONOTONIC,&ts1);
	for(int i = 0; i<10; i++){
		ZYX{
		  l.neighbors_serial(x, y, z, nbs_s,nbs_sp);
		}
	}	
	clock_gettime(CLOCK_MONOTONIC,&ts2);
	time = ts2.tv_sec - ts1.tv_sec;
	time += ts2.tv_nsec/1E9 - ts1.tv_nsec/1E9;
	printf("Serial time: %lf\n",time);

	clock_gettime(CLOCK_MONOTONIC,&ts1);
	for(int i = 0; i<10; i++){
		ZYX{
			l.neighbors_vect(x, y, z, nbs_v, nbs_vp);
		}
	}	
	clock_gettime(CLOCK_MONOTONIC,&ts2);
	time = ts2.tv_sec - ts1.tv_sec;
	time += ts2.tv_nsec/1E9 - ts1.tv_nsec/1E9;
	printf("Vector time: %lf\n",time);

}
