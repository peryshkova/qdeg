#include <stdio.h>
#include <mpi.h>
#include <unistd.h>

#define DIM 13

/*
    Test program that checks how Cartesian coordinates are
    distributed.
    It seems that MPI_Cart_create provides Fortran-style distribution.
*/

void create_file(char fname[]);
void check(char fname[]);

int main(int argc, char **argv)
{
    char fname[] = "darray_test.out";
    MPI_Init(&argc,&argv);
    
    create_file(fname);
    check(fname);

    MPI_Finalize();
}


void create_file(char fname[])
{
    MPI_File thefile;
    MPI_Datatype t;
    MPI_Comm cart;
    int i, rank, size;
    int dims[] = {2,2}, periods[] = {0, 0};
    int coords[2];
    int xsize, ysize;

    
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    
    MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 0, &cart);
    MPI_Cart_coords(cart, rank, 2, coords );
    
    xsize = DIM/dims[0] + (coords[0] < DIM%dims[0]);
    ysize = DIM/dims[1] + (coords[1] < DIM%dims[0]);

    printf("%d (%d,%d): xsize = %d, ysize = %d\n", rank, coords[0], coords[1], xsize, ysize);

    {
        int crank;
        MPI_Comm_rank(cart,&crank);
        
        int psizes[] = {2, 2};
        int gsizes[] = {DIM, DIM};
        int distribs[] = {MPI_DISTRIBUTE_BLOCK,MPI_DISTRIBUTE_BLOCK};
        //int dargs[] = {xsize,ysize};
        int dargs[] = {MPI_DISTRIBUTE_DFLT_DARG,MPI_DISTRIBUTE_DFLT_DARG};
        int tsize;
        MPI_Aint ext;
        /*
        for(i = 0; i< 2; i++){
            dargs[i] = (gsizes[i] + psizes[i] - 1)/psizes[i];
            printf("%d: dargs[%d] = %d\n",rank,i,dargs[i]);
        }
        */
        
        MPI_Type_create_darray(4, crank, 2, gsizes, distribs, dargs, psizes, MPI_ORDER_FORTRAN, MPI_INT, &t);
        // MPI_Type_create_darray(4, crank, 2, gsizes, distribs, dargs, psizes, MPI_ORDER_C, MPI_INT, &t);
        MPI_Type_commit(&t);
        
        MPI_Type_extent(t, &ext);
        MPI_Type_size(t, &tsize);
        printf("%d: ext = %d, size = %d\n",crank, ext,tsize);
    }
    
    MPI_File_open(MPI_COMM_WORLD, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY,
           MPI_INFO_NULL, &thefile);
                      
    MPI_File_set_view(thefile, 0,MPI_INT, t, "native", MPI_INFO_NULL);
    
    {
        int mas[ysize][xsize], i, j;
        
        for(i=0;i<ysize;i++){
            for(j=0;j<xsize;j++){
                mas[i][j] = rank*16*16*16 + i*16 + j;
            }
        }
        MPI_File_write_all(thefile,mas, xsize*ysize, MPI_INTEGER, MPI_STATUS_IGNORE);

        for(i=0;i<ysize;i++){
            for(j=0;j<xsize;j++){
                mas[i][j] = rank*16*16*16 + (i+ysize)*16 + j;
            }
        }
        MPI_File_write_all(thefile,mas, xsize*ysize, MPI_INTEGER, MPI_STATUS_IGNORE);

        for(i=0;i<ysize;i++){
            for(j=0;j<xsize;j++){
                mas[i][j] = rank*16*16*16 + (i+2*ysize)*16 + j;
            }
        }
        MPI_File_write_all(thefile,mas, xsize*ysize, MPI_INTEGER, MPI_STATUS_IGNORE);

        for(i=0;i<ysize;i++){
            for(j=0;j<xsize;j++){
                mas[i][j] = rank*16*16*16 + (i+3*ysize)*16 + j;
            }
        }
        MPI_File_write_all(thefile,mas, xsize*ysize, MPI_INTEGER, MPI_STATUS_IGNORE);
    }

    MPI_File_close(&thefile);
}

void check(char fname[])
{
    FILE *f = fopen(fname,"r");
    int rank_row[4] = { 0 };
    int rank_found[4] = { 0 };
    int i = 0;
    int rank;

    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    if( rank != 0 )
        return;
    

    while( !feof(f) ){
        int b;
        if( fread(&b,sizeof(int),1,f) == 0) {
            if( feof(f) )
                break;
            else{
                int error = ferror(f);
                printf("I/O error: %s\n", strerror(error));
                return;
            }
        }
            
        int rank = b>>12;
        rank_found[rank] = 1;
        int y = (b >> 4) & 0xFF;
        int x = b & 0xF;
        
        if( rank_row[rank] != y ){
            printf("Error! rank=%d, row = %d, should be %d\n",rank,y,rank_row[rank]);
            fclose(f);
            return;
        }
        printf("%d[%d,%d] ",rank,y,x);
        i++;
        if( i % DIM == 0){
            int j;
            printf("\n");
            for(j=0;j<4;j++){
                if( rank_found[j] ){
                    rank_found[j] = 0;
                    rank_row[j]++;
                    // printf("rank_row[%d] = %d\n",j, rank_row[j]);
                }
            }
        }
    }
    fclose(f);
}
