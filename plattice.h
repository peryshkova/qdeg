/* 
 * File:   plattice.h
 * Author: artpol
 *
 * Created on 16 Январь 2013 г., 10:51
 */

#ifndef PLATTICE_H
#define	PLATTICE_H

#include <mpi.h>
#include <config.h>
#include <dlattice.h>


template<typename Elem> 
class plattice_t{
private:
  int rank, size;
  int crank, csize; // Cortesian size and rank
  int dims[2];
  bool inuse;
  int coords[2];
  int Lx, Ly, Lz;  
  int Cx, Cy, Cz;
  int _lx, _ly, _lz;
  bool xcomm, ycomm;
  int zsurf;
  int offs[2];
  int bsize[2];
  MPI_Comm cart;
  dlattice_t<Elem> lat;

public:
  // lattice initialization
  int init(qdegcfg &cfg);
  int create_topo();
  void distribute_coord(int dim, int maxdim, int count, int &size, int &offs);

  // access to attributes
  inline int lx(){ return _lx; }
  inline int ly(){ return _ly; }
  inline int lz(){ return _lz; }
  inline bool in_use(){ return inuse; }
  
  inline int startz(int offs = 2){ return offs; }
  inline int endz(int offs = 2){ return _lz - offs; }
  
  inline int starty(int z, int offs = 4){ 
    return offs + ((int)z%2); 
  }
  inline int endy(int offs = 4){ 
    return _ly - offs;
  }
  
  inline int startx(int z, int y, int offs = 4){ 
    return offs + (z%2) + 2*((z/2 + y/2)%2); 
  }
  inline int endx(int offs = 4){ 
    return _lx - offs; 
  }
  
  inline Elem &operator () (uint x, uint y, uint z){ return lat(x,y,z); }
  inline Elem &l(int x, int y, int z) { return lat.l(x,y,z);  }

  // disttributed operations
  void sync();
  void sync1();
  void sync2();
  void sync3();
};

template<typename Elem> 
int plattice_t<Elem>::init(qdegcfg &cfg)
{
  if ((cfg.Lx % 4) || (cfg.Ly % 4) || (cfg.Lz % 4)) {
    EPRINTF("(Lx %% 4 != 0) OR (Ly %% 4 != 0) OR (Lz %% 4 != 0): (%d,%d,%d)\n",
            cfg.Lx, cfg.Ly, cfg.Lz);
    return -1;
  }
  
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  // distibute elementary cubes, not nodes
  Lx = cfg.Lx;
  Ly = cfg.Ly;
  Lz = cfg.Lz;
  Cx = Lx / 4;
  Cy = Ly / 4;
  Cz = Lz / 4;
  zsurf = cfg.z_surf;

  dims[0] = cfg.xdim;
  dims[1] = cfg.ydim;
  if (dims[0] * dims[1] > size) {
    EPRINTF("Init lattice: cfg.xdim*cfg.ydim > size, %d*%d > %d", dims[0], dims[1], size);
    return -1;
  }

  create_topo();
  if (inuse) {
  
    if( dims[0] > 1 ){
        xcomm = true;
        distribute_coord(coords[0], dims[0], Cx, bsize[0], offs[0]);
        _lx = 4*(bsize[0] + 2);
    }else{
        ycomm = false;
        _lx = Lx;
        bsize[0] = _lx;
        offs[0] = 0;
    }

    if( dims[1] > 1 ){
        ycomm = true;
        distribute_coord(coords[1], dims[1], Cy, bsize[1], offs[1]);
        _ly = 4*(bsize[1] + 2);
    }else{
        ycomm = false;
        _ly = Ly;
        bsize[1] = Cy;
        offs[1] = 0;
    }
    
    _lz = Cz*4;
        
    printf("%d: bsize[0]=%d, bsize[1]=%d, offs[0]=%d, offs[1]=%d\n", rank, bsize[0], bsize[1], offs[0], offs[1]);

      // Create local lattice.
      // distribute_coord defines sizes of local blocks for x & y dimensions.
      // We need ghost nodes - so actual sizes are bigger on 2 cubes in each dim.
      printf("%d: Px=%d, Py=%d, Pz=%d\n", rank, _lx, _ly, _lz);
      if( lat.init(_lx, _ly, Cz * 4) )
        return -1;
  }
  return 0;
}

template<typename Elem> 
int plattice_t<Elem>::create_topo()
{
  int ds[] = {dims[0], dims[1]}, per[] = {1, 1};

  MPI_Cart_create(MPI_COMM_WORLD, 2, ds, per, 1, &cart); // do we need error handling?
  if (rank < dims[0] * dims[1]) {
    inuse = true;
    MPI_Comm_rank(cart, &crank);
    MPI_Comm_size(cart, &csize);
    MPI_Cart_coords(cart, crank, 2, coords);
  }else{
    inuse = false;
  }
}

template<typename Elem> 
void plattice_t<Elem>::
distribute_coord(int dim, int maxdim, int count, int &size, int &offs)
{
  int modulo = count % maxdim;
  size = count / maxdim;
  offs = dim * (count / maxdim);
  if (dim < modulo) {
    size += 1;
    offs += dim;
  } else {
    offs += modulo;
  }
}

template<typename Elem> 
void plattice_t<Elem>::sync()
{
   int myrank;
   int nranks[3][3]; // left and right neighbors ranks by y axe
   int ncoords[2];
   char out[1024] = "";
   int i,j;
   MPI_Datatype x_v, y_v;
   MPI_Status status;
   
   sprintf(out,"%d:\n",nranks[1][1]);
   
   for(i=-1;i<2;i++){
        for(j=-1;j<2;j++){
            ncoords[0] = coords[0] + i;
            ncoords[1] = coords[1] + j;
            MPI_Cart_rank(cart, ncoords, &nranks[1+i][1+j]);
            sprintf(out,"%s(%d,%d)->%d\n",out,ncoords[0],ncoords[1],nranks[1+i][1+j]);
        }
   }   
   
   for(i=-1;i<2;i++){
     for(j=-1;j<2;j++){
        sprintf(out,"%s%d ",out,nranks[1+i][1+j]);
     }
     sprintf(out,"%s\n",out); 
   }
   
   printf("%s",out);
   
   // exchange x borders
   MPI_Type_vector(_ly/2*_lz/4, 4, _lx, MPI_INT, &x_v);
   MPI_Type_commit(&x_v);
   
   MPI_Sendrecv(&lat.l(4,0,0),1, x_v, nranks[0][1], 1, &lat.l(_lx-4,0,0), 1, x_v, nranks[2][1],1, cart, &status);
   MPI_Sendrecv(&lat.l(_lx-8,0,0),1, x_v, nranks[2][1], 1, &lat.l(0,0,0), 1, x_v, nranks[0][1],1, cart, &status);                   

   // exchange y borders
   MPI_Type_vector(_lz/4, _lx*2, _lx*_lx/2, MPI_INT, &y_v);
   MPI_Type_commit(&y_v);
   MPI_Sendrecv(&lat.l(0,2,0),1, y_v, nranks[1][0], 1, &lat.l(0,_ly/2-2,0), 1, y_v, nranks[1][2],1, cart, &status);
   MPI_Sendrecv(&lat.l(0,_ly/2-4,0),1, y_v, nranks[1][2], 1, &lat.l(0,0,0), 1, y_v, nranks[1][0],1, cart, &status);                   

}

// Temp

template<typename Elem> 
void plattice_t<Elem>::sync1()
{
   int myrank;
   int nranks[3][3]; // left and right neighbors ranks by y axe
   int ncoords[2];
   char out[1024] = "";
   int i,j;
   MPI_Datatype x_v, y_v;
   MPI_Status status;
   
   for(i=-1;i<2;i++){
        for(j=-1;j<2;j++){
            ncoords[0] = coords[0] + i;
            ncoords[1] = coords[1] + j;
            MPI_Cart_rank(cart, ncoords, &nranks[1+i][1+j]);
        }
   }   
   
   // exchange x borders
   MPI_Type_vector(_ly/2*_lz/4, 4, _lx, MPI_INT, &x_v);
   MPI_Type_commit(&x_v);
   
   MPI_Sendrecv(&lat.l(4,0,0),1, x_v, nranks[2][1], 1, &lat.l(_lx-4,0,0), 1, x_v, nranks[0][1],1, cart, &status);
   MPI_Sendrecv(&lat.l(_lx-8,0,0),1, x_v, nranks[2][1], 1, &lat.l(0,0,0), 1, x_v, nranks[0][1],1, cart, &status);                   

}

template<typename Elem> 
void plattice_t<Elem>::sync2()
{
   int myrank;
   int nranks[3][3]; // left and right neighbors ranks by y axe
   int ncoords[2];
   char out[1024] = "";
   int i,j;
   MPI_Datatype x_v, y_v;
   MPI_Status status;
   
   for(i=-1;i<2;i++){
        for(j=-1;j<2;j++){
            ncoords[0] = coords[0] + i;
            ncoords[1] = coords[1] + j;
            MPI_Cart_rank(cart, ncoords, &nranks[1+i][1+j]);
        }
   }   

   // exchange y borders
   MPI_Type_vector(_lz/4, _lx*2, _lx*_lx/2, MPI_INT, &y_v);
   MPI_Type_commit(&y_v);
   MPI_Sendrecv(&lat.l(0,2,0),1, y_v, nranks[1][0], 1, &lat.l(0,_ly/2-2,0), 1, y_v, nranks[1][2],1, cart, &status);

}

template<typename Elem> 
void plattice_t<Elem>::sync3()
{
   int myrank;
   int nranks[3][3]; // left and right neighbors ranks by y axe
   int ncoords[2];
   char out[1024] = "";
   int i,j;
   MPI_Datatype x_v, y_v;
   MPI_Status status;
   
   
   for(i=-1;i<2;i++){
        for(j=-1;j<2;j++){
            ncoords[0] = coords[0] + i;
            ncoords[1] = coords[1] + j;
            MPI_Cart_rank(cart, ncoords, &nranks[1+i][1+j]);
        }
   }   
   
   // exchange y borders
   MPI_Type_vector(_lz/4, _lx*2, _lx*_lx/2, MPI_INT, &y_v);
   MPI_Type_commit(&y_v);
   MPI_Sendrecv(&lat.l(0,_ly/2-4,0),1, y_v, nranks[1][0], 1, &lat.l(0,0,0), 1, y_v, nranks[1][2],1, cart, &status);                   
}


#endif	/* PLATTICE_H */

