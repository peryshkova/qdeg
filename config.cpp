#include <stdio.h>
#include <math.h>

#include <config.h>

qdegcfg::qdegcfg()
{
  xdim = ydim = 0;
  Lx = Ly = Lz = 0;
  rm_start = 0;
  rm_delta = 0; // Create RASMOL file every rm_delta seconds
  E1 = 0.35 * 11800;
  E2 = 0.15 * 11800;
  p0 = 1e13;
  pdep = 0.1 * Lx * Ly / 8.0;
  T = 670;
  A = 48.5 * 1.36e-10 * 1.36e-10 / 1.6e-19 * 11800 / 8.0;
  B = 38 * 1.36e-10 * 1.36e-10 / 1.6e-19 * 11800 / 8.0;
  eps = 0.04;
  def_F = (5 * 1.5 * 11800) / sqrt(3.0);
  mv_per_ml = 50;
  dt_ctrl = 0.1;
  exptime = 200;
  rndseed = 19;
  dep_type = 2;
  z_surf = 20; // Lz/2;
  z_cap = -100; // количество закрывающих монослоев (-100 =Ю закрывающего слоя нет)
  ndef = -1; // отрицательное n_defects означает, что берём их столько, сколько есть в файле defects.txt
  avg_deform_n = 0; // количество смещений по которым усреднять деформацию на атом, 0 - не считать  
}

int GetConfig(char *fname, qdegcfg &cfg)
{
  // open file
  // read & check everything

}
