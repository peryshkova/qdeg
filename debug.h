/* 
 * File:   debug.h
 * Author: artpol
 *
 * Created on 16 Январь 2013 г., 17:32
 */

#ifndef DEBUG_H
#define	DEBUG_H

#define DEBUG_ON

#define EPRINTF(fmt,args...) printf("%s: " fmt "\n",__FUNCTION__, ## args  )
#define DPRINTF(fmt,args...)


#ifdef DEBUG_ON

  #undef  DPRINTF
  #define DPRINTF(fmt,args...) printf("%s: " fmt "\n",__FUNCTION__, ## args  )

#endif


#endif	/* DEBUG_H */

