
#include <iostream>

#include <dLattice.h>
#include <lPoint.h>
#include <sstream>
#include <list>
#include <vector>

#include <zyxiterator.h>
#include <ziterator.h>
#include <lattcfg.h>

#include "constants.h"

int Gx, Gy, Gz;
plattice::dlattcfg cfg;

void output(char *fname, dlattice_t<atomType> &l)
{
    FILE *fp = fopen(fname, "w");
    int x, y, z;
    if(fp == NULL) {
        EPRINTF("Cannot open %s for writing", fname);
        exit(0);
    }
    unsigned long acnt = 0;

    ALL_Z(0, l.maxz() * 4) ALL_Y(0, l.maxy() * 2) ALL_X(0, l.maxx())
    {
        if(l(x, y, z) != None) {
            acnt++;
        }
    }

    fprintf(fp, "%ld %.1f\n\n", acnt, (double)0);

    ALL_Z(0, l.maxz() * 4) ALL_Y(0, l.maxy() * 2) ALL_X(0, l.maxx())
    {
        if(l(x, y, z) != None) {
            fprintf(fp, "%d ", (int)l(x, y, z));
            fprintf(fp, "%d %11.10f %d %11.10f %d %11.10f\n",
                    x, (double)0, y, (double)0, z, (double)0);
        }
    }

}

void deposit(dlattice_t<atomType> &l, lPoint &p)
{
    coord_t max[] = {p.x(), p.y(), Gz};
    ziterator it(max);
    std::stringstream buf;

    // get closer to final position
    for(it = it.end(); it >= it.begin(); it--) {
        lPoint tmp = *it;
        if(l(tmp) == None) {
            p = tmp;
        }
    }

    ushort conf = cfg.set_config(l, p);
    if(!cfg.good_config(conf)) {
        buf << "Can't deposit directly: " << std::endl;
        int startx = (p.x() / ECUBE) * ECUBE;
        int starty = (p.y() / ECUBE) * ECUBE;
        std::vector<lPoint> good_cfg;
        good_cfg.clear();
        coord_t ecube_max[] = {4, 4, 4};
        zyxiterator ecit(ecube_max);
        for(; ecit <= ecit.end(); ecit++) {
            int x = startx + (*ecit).x();
            int y = starty + (*ecit).y();
            coord_t max1[] = {x, y, p.z() + 1};
            buf << "(" << x << "," << y << "): ";
            it.init(max1);
            lPoint p1 = *(it.end());
            // explore elementary cube column
            for(it = it.end(); it >= it.begin(); it--) {
                lPoint tmp = *it;
                buf << tmp;
                if(l(tmp) == None) {
                    p1 = tmp;
                    conf = cfg.set_config(l, p1);
                    if((l(p1) == None) && cfg.good_config(conf)) {
                        buf << "+";
                    } else {
                        buf << "*";
                    }
                }
                buf << " ";
            }
            buf << std::endl;
            conf = cfg.set_config(l, p1);
            if((l(p1) == None) && cfg.good_config(conf)) {
                good_cfg.push_back(p1);
            }
        }
        buf << std::endl;

        bool debug = false; //true;
        if(debug) {
            std::cout << buf.str();
        }
        if(!good_cfg.size()) {
            std::cout << "ERROR: cannot find the place for " << p << std::endl;
        } else {
            int i = ::rand() % good_cfg.size();
            l(good_cfg[i]) = Si;
            std::cout << "Final deposition: " << good_cfg[i] << std::endl;
        }
    } else {
        std::cout << "Deposit directly into " << p << std::endl;
        l(p) = Si;
    }
}

int main(int argc, char **argv)
{
    MPI::Init(argc, argv);
    dlattice_t<atomType> l;
    neighbors_t nbs_s, nbs_sp;
    neighbors_t nbs_v, nbs_vp;
    int x, y, z, surf;
    double time, sum = 0;
    struct timespec ts1, ts2;

    std::cin >> Gx;
    std::cin >> Gy;
    std::cin >> Gz;

    std::cout << "Lattice : " << Gx << "x" << Gy << "x" << Gz << std::endl;
    l.init(Gx, Gy, Gz);

    std::cin >> surf;
    std::cout << "Initial level: " << surf << std::endl;

    std::list<lPoint> lst;
    std::list<lPoint>::iterator lit;
    lst.clear();

    std::cout << "Initialize with None" << std::endl;

    ALL_Z(0, Gz) ALL_Y(0, Gy) ALL_X(0, Gx)
    {
        l(x, y, z) = None;
        lPoint p(x, y, z);
    }

    std::cout << "Initialize z-surface" << std::endl;

    ALL_Z(0, surf) ALL_Y(0, Gy) ALL_X(0, Gx)
    {
        l(x, y, z) = Si;
        lPoint p(x, y, z);
        lst.push_front(p);
    }

    std::cout << "Check z-surface" << std::endl;
    coord_t maxx[] = {Gx, Gy, surf};
    zyxiterator git(maxx, 0);
    lit = lst.begin();
    int count = 0;
    for(; git <= git.end(); git++, lit++) {
        if(!l((*git).x(), (*git).y(), (*git).z())) {
            lPoint p = *git;
            std::cout << " FAILED: " << p << std::endl;
        }
        count++;
    }
    if(lit != lst.end()) {
        std::cout << " FAILED: size orig: " << lst.size()
                << "iter: " << count << std::endl;
    }
    lst.clear();

    for(int i = 0; i < 10000; i++) {
        int x = rand() % (l.maxx()/2);
        int y = rand() % l.maxy();
        if(y % 2 != x % 2) {
            y -= x % 2;
            if(y < 0)
                y = 0;
        }
        lPoint lp(x, y, 0);
        std::cout << "Deposit coordinates: " << lp << std::endl;
        deposit(l, lp);

        if(!(i % 100)) {
            char fname[256];
            sprintf(fname, "A0.%d.txt", i / 100);
            output(fname, l);
        }
    }


    return 0;
}
