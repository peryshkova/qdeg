
#include <iostream>

#include <pLattice.h>
#include <sstream>
#include <list>
#include <vector>

#include <zyxiterator.h>
#include <ziterator.h>
#include <lattcfg.h>

#include "constants.h"
#define COUNT 100

int Gx, Gy, Gz;

using namespace plattice;

void deposit_w(plattice_t<atomType> &p, int root);
void deposit_r(qConfig &q, int root);

/*
void output(char *fname, dlattice_t<atomType> &l);
void deposit(plattice_t<atomType> &p, dPoint &p);
*/

int main(int argc, char **argv)
{
    MPI::Init(argc, argv);
    decomp d;
    plattice_t<atomType> p;

    std::string s = "input";
    qConfig cfg(s);

    MPI::Intracomm c = MPI::COMM_WORLD;
    bool flag = c.Get_rank() < cfg.Nx() * cfg.Ny();
    MPI::Intracomm nc = c.Split(flag, 0);
    int root_rank = cfg.Nx() * cfg.Ny();
    if( root_rank >= c.Get_size()){
        printf("Not enough cpu's!\n");
        exit(0);
    }
    
    if(flag) {
        d.init(nc,cfg);
        p.init(cfg,d,MPI_INT);
        deposit_w(p, root_rank);
    } else {
        deposit_r(cfg, root_rank);
    }

    MPI::Finalize();
    return 0;
}

void deposit_r(qConfig &q, int root)
{
    for(int i = 0; i < COUNT; i++) {
        int p[2];
        p[0] = rand() % q.Gx();
        p[1] = rand() % q.Gy();
        if(p[1] % 2 != p[0] % 2) {
            p[1] -= p[0] % 2;
            if(p[1] < 0)
                p[1] = 0;
        }
        
        printf("MASTER: point = (%d,%d)\n",p[0],p[1]);
        MPI::COMM_WORLD.Bcast(p, 2, MPI_INT, root);
        sleep(1);
    }
}

void deposit_w(plattice_t<atomType> &p, int root)
{
    decomp &d = p.dcmp();
    MPI::Intracomm comm = MPI::COMM_WORLD;

    for(int i = 0; i < COUNT; i++) {
        int c[2];
        MPI::COMM_WORLD.Bcast(c, 2, MPI_INT, root);
        //printf("%d: get point - (%d,%d)\n", comm.Get_rank(),c[0],c[1]);

        dPoint dp(p.dcmp());
        dp.glob(c[0], c[1], 0);
        if(dp.is_local()) {
            //deposit(p, dp);
            printf("%d: My point: (%d,%d)\n", comm.Get_rank(),c[0],c[1]);
        }
        //p.sync();

    }
}
/*
void output(char *fname, dlattice_t<atomType> &l)
{
    FILE *fp = fopen(fname, "w");
    int x, y, z;
    if(fp == NULL) {
        EPRINTF("Cannot open %s for writing", fname);
        exit(0);
    }
    unsigned long acnt = 0;

    ALL_Z(0, l.maxz() * 4) ALL_Y(0, l.maxy() * 2) ALL_X(0, l.maxx())
    {
        if(l(x, y, z) != None) {
            acnt++;
        }
    }

    fprintf(fp, "%ld %.1f\n\n", acnt, (double)0);

    ALL_Z(0, l.maxz() * 4) ALL_Y(0, l.maxy() * 2) ALL_X(0, l.maxx())
    {
        if(l(x, y, z) != None) {
            fprintf(fp, "%d ", (int)l(x, y, z));
            fprintf(fp, "%d %11.10f %d %11.10f %d %11.10f\n",
                    x, (double)0, y, (double)0, z, (double)0);
        }
    }
}

void deposit(plattice_t<atomType> &p, dPoint &p)
{
    coord_t max[] = { p.x(), p.y(), p.Gz() };
    ziterator it(max);
    std::stringstream buf;

    // get closer to final position
    for(it = it.end(); it >= it.begin(); it--) {
        lPoint tmp = *it;
        if(l(tmp) == None) {
            p = tmp;
        }
    }

    ushort conf = cfg.set_config(l, p);
    if(!cfg.good_config(conf)) {
        buf << "Can't deposit directly: " << std::endl;
        int startx = (p.x() / ECUBE) * ECUBE;
        int starty = (p.y() / ECUBE) * ECUBE;
        std::vector<lPoint> good_cfg;
        good_cfg.clear();
        coord_t ecube_max[] = {4, 4, 4};
        zyxiterator ecit(ecube_max);
        for(; ecit <= ecit.end(); ecit++) {
            int x = startx + (*ecit).x();
            int y = starty + (*ecit).y();
            coord_t max1[] = {x, y, p.z() + 1};
            buf << "(" << x << "," << y << "): ";
            it.init(max1);
            lPoint p1 = *(it.end());
            // explore elementary cube column
            for(it = it.end(); it >= it.begin(); it--) {
                lPoint tmp = *it;
                buf << tmp;
                if(l(tmp) == None) {
                    p1 = tmp;
                    conf = cfg.set_config(l, p1);
                    if((l(p1) == None) && cfg.good_config(conf)) {
                        buf << "+";
                    } else {
                        buf << "*";
                    }
                }
                buf << " ";
            }
            buf << std::endl;
            conf = cfg.set_config(l, p1);
            if((l(p1) == None) && cfg.good_config(conf)) {
                good_cfg.push_back(p1);
            }
        }
        buf << std::endl;

        bool debug = false; //true;
        if(debug) {
            std::cout << buf.str();
        }
        if(!good_cfg.size()) {
            std::cout << "ERROR: cannot find the place for " << p << std::endl;
        } else {
            int i = ::rand() % good_cfg.size();
            l(good_cfg[i]) = Si;
            std::cout << "Final deposition: " << good_cfg[i] << std::endl;
        }
    } else {
        std::cout << "Deposit directly into " << p << std::endl;
        l(p) = Si;
    }
}

*/