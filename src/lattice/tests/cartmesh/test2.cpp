#include <stdio.h>
#include <iostream>
#define DEBUG_ON
#include<debug.h>

#include <cartmesh.h>

#define P(c,m) \
  DPRINTF("(%d,%d).rank = %d", c[0], c[1], m.coords2rank(c));

void count_mesh(int &x, int &y) {
  MPI::Intracomm c = MPI::COMM_WORLD;
  int size = c.Get_size();
  x = size;
  y = 1;
  int i = 2;
  while (x > y && i < (size / 2)) {
    if (size % i == 0) {
      x = size / i;
      y = i;
    }
    i++;
  }

  if (c.Get_rank() == 0)
    DPRINTF("MESH is (%d,%d)", x, y);
}

int main(int argc, char **argv) {
  MPI::Intracomm comm = MPI::COMM_WORLD;
  MPI_Init(&argc, &argv);
  init_debug(stdout);
  int x, y;
  count_mesh(x, y);

  cartmesh < 2 > m;
  int dims[] = {x, y};

  m.init(MPI::COMM_WORLD, dims);


  if (comm.Get_rank() == 0) {
    int matrix[y][x];
    printf("matrix:\n");
    for (int i = 0; i < y; i++) {
      for (int j = 0; j < x; j++) {
        int coords[2] = {j, i};
        matrix[i][j] = m.coords2rank(coords);
        printf("%d\t",matrix[i][j]);
      }
      printf("\n");
    }

    for (int i = 0, count = 0; i < y; i++)
      for (int j = 0; j < x; j++, count++) {
        if (matrix[i][j] != count) {
          DPRINTF("ERROR, matrix[%d][%d] = %d != %d\n", i, j, matrix[i][j],count);
          MPI::Finalize();
          exit(0);
        }
      }

    for (int count = 0, i = 0, j = 0; count < m.size(); count++) {
      int crd[2];
      m.rank2coords(count, crd);
      if (crd[0] != j || crd[1] != i) {
        DPRINTF("ERROR, rank %d (%d,%d)do not correspond to (%d,%d)\n", 
            count, crd[0],crd[1], i, j);
        MPI::Finalize();
        exit(0);
      }
      j++;
      if (j >= x) {
        i++;
        j = 0;
      }
    }
    
    DPRINTF("All tests are passed successfuly");
  }
  MPI::Finalize();
}
