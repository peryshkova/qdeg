#include <iostream>
#include <zyxiterator.h>
#include <list>

#define Lx 1024
#define Ly 1048
#define Lz 512

#define ALL_Z(s,e)     for (z = s; z < e; z++)       // z may take all values
#define ALL_Y(s,e)     for (y = ((int)z%2)+s; y < e; y+=2)  // y & z - same parity
#define ALL_X(s,e)     for (x = ((int)z%2)+2*(((int)z/2+(int)y/2)%2)+s; x < e; x+=4)

int main()
{
    coord_t maxx[] = {Lx, Ly, Lz};
    int x, y, z;
    zyxiterator t(maxx, 0);


    std::cout << "Test increment functions:" << std::endl;

    std::cout << "1. Zero Ghost nodes, postfix increment (based on prefix)" << std::endl;
    {
        // Zero ghost nodes
        ALL_Z(0, Lz) ALL_Y(0, Ly) ALL_X(0, Lx)
        {
            lPoint p = (*t);
            lPoint p1(x, y, z);

            if(x != p.x() || y != p.y() || z != p.z()) {
                std::cout << p << " <-> " << p1 << " [ERROR]" << std::endl;
            }
            t++;
        }
    }

    std::cout << "2. Ghost nodes width=4, postfix increment" << std::endl;
    { // Ghost nodes width = 4
        t.init(maxx, 4);

        ALL_Z(0, Lz) ALL_Y(4, Ly - 4) ALL_X(4, Lx - 4)
        {
            lPoint p = (*t);
            lPoint p1(x, y, z);

            if(x != p.x() || y != p.y() || z != p.z()) {
                std::cout << p << " <-> " << p1 << " [ERROR]" << std::endl;
            }
            t++;
        }
    }

    std::cout << "Test increment functions:" << std::endl;



    std::cout << "1. Zero Ghost nodes, postfix decrement (based on prefix)" << std::endl;
    {
        // Zero ghost nodes
        std::list<lPoint> straight;
        std::list<lPoint>::iterator it;
        straight.clear();

        ALL_Z(0, Lz) ALL_Y(0, Ly) ALL_X(0, Lx)
        {
            lPoint p1(x, y, z);
            straight.push_front(p1);
        }

        t.init(maxx, 0);
        t = t.end();
        it = straight.begin();
        while(t >= t.begin() && it != straight.end()) {
            lPoint p = *t, p1 = *it;
            if(p != p1) {


                std::cout << p1 << " <-> " << p << " [ERROR]" << std::endl;
            }
            it++;
            t--;
        }
    }


    std::cout << "2. Zero ghost nodes, postfix decrement (based on prefix)" << std::endl;
    {
        // Zero ghost nodes
        std::list<lPoint> straight;
        std::list<lPoint>::iterator it;
        straight.clear();

        ALL_Z(0, Lz) ALL_Y(4, Ly - 4) ALL_X(4, Lx - 4)
        {
            lPoint p1(x, y, z);
            straight.push_front(p1);

            // std::cout << p1 << " " << std::endl;
        }

        t.init(maxx, 4);
        t = t.end();
        it = straight.begin();
        while(t >= t.begin() && it != straight.end()) {
            lPoint p = *t, p1 = *it;
            if(p != p1) {
                std::cout << p1 << " <-> " << p << " [ERROR]" << std::endl;
            }
            it++;
            t--;
        }
    }
    return 0;
}
