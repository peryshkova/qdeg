#include <iostream>
#include <ziterator.h>
#include <list>

#define Lx 256
#define Ly 256
#define Lz 1024

#define ALL_Z(s,e)     for (z = s; z < e; z++)       // z may take all values
#define ALL_Y(s,e)     for (y = ((int)z%2)+s; y < e; y+=2)  // y & z - same parity
#define ALL_X(s,e)     for (x = ((int)z%2)+2*(((int)z/2+(int)y/2)%2)+s; x < e; x+=4)

int main()
{
    coord_t maxx[] = {Lx, Ly, Lz};
    ziterator t(maxx, 0);
    
    std::cout << "Zero ghost nodes" << std::endl;
    zyxiterator git(maxx, 0);
    std::cout << "1. postfix increment (based on prefix)" << std::endl;
    for(; git <= git.end() && !(*git).z(); git++) {
        lPoint gp = *git;
        lPoint ep1, ep2;
        bool flag = false;

        std::cout << gp;

        coord_t zmax[] = {gp.x(), gp.y(), Lz};
        t.init(zmax,0);
        int x, y, z;

        ALL_Z(0, Lz) ALL_Y(0, Ly) ALL_X(0, Lx)
        {
            if(x == gp.x() && y == gp.y()) {
                lPoint p = (*t);
                lPoint p1(x, y, z);

                if(x != p.x() || y != p.y() || z != p.z()) {
                    ep1 = p;
                    ep2 = p1;
                    flag = true;
                    //std::cout << p << " <-> " << p1 << " [ERROR]" << std::endl;
                }
                t++;
            }
        }
        if(!flag) {
            std::cout << " OK" << std::endl;
        } else {
            std::cout << " FAILED: " << ep2 << " <-> " << ep1 << std::endl;
        }
    }

    std::cout << "2. postfix decrement (based on prefix)" << std::endl;

    git = git.begin();
    for(; git <= git.end() && !(*git).z(); git++) {
        lPoint gp = *git;
        lPoint ep1, ep2;
        bool flag = false;

        std::cout << gp;

        int x, y, z;

        std::list<lPoint> lst;
        lst.clear();
        std::list<lPoint>::iterator it;

        ALL_Z(0, Lz) ALL_Y(0, Ly) ALL_X(0, Lx)
        {
            if(x == gp.x() && y == gp.y()) {
                lPoint p1(x, y, z);
                lst.push_front(p1);
            }
        }

        it = lst.begin();
        coord_t zmax[] = {gp.x(), gp.y(), Lz};
        t.init(zmax,0);
        t = t.end();
        while(t >= t.begin()) {
            lPoint p = *t, p1 = *it;
            if(p != p1) {
                //std::cout << p1 << " <-> " << p << " [ERROR]" << std::endl;
                ep1 = p;
                ep2 = p1;
                flag = true;
            }
            it++;
            t--;
        }

        if(flag) {
            std::cout << " FAILED: " << ep2 << " <-> " << ep1 << std::endl;
            continue;
        }

        it = lst.begin();
        t = t.end();
        while(it != lst.end()) {
            lPoint p = *t, p1 = *it;
            if(p != p1) {
                //std::cout << p1 << " <-> " << p << " [ERROR]" << std::endl;
                ep1 = p;
                ep2 = p1;
                flag = true;
            }
            it++;
            t--;
        }
        if(!flag) {
            std::cout << " OK" << std::endl;
        } else {
            std::cout << " FAILED: " << ep2 << " <-> " << ep1 << std::endl;
        }
    }

    return 0;
}

