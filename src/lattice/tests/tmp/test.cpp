#include<stdio.h>
#include<mpi.h>

int main(int argc, char ** argv)
{
	int dims[2] = { 10, 10 };
	bool periods[2] = { 1, 1 };
	MPI::Init(argc,argv);
	MPI::Intracomm c = MPI::COMM_WORLD;
	MPI::Cartcomm comm = ((MPI::Cartcomm)c).Create_cart(2, dims, periods, 1);

}