/* 
 * File:   zxyiterator.h
 * Author: artpol
 *
 * Created on 14 Июнь 2013 г., 10:51
 */

#ifndef ZITERATOR_H
#define	ZITERATOR_H

#include <zyxiterator.h>

class ziterator : public zyxiterator {
public:

    ziterator(coord_t _max[3], coord_t ghost = 0) :
    zyxiterator(_max, ghost)
    {

        __init(_max, ghost);
    }

    void __init(coord_t _max[3], coord_t ghost = 0)
    {
        map[0] = 2; // inner loop variable is z
        map[1] = 1; // middle loop is y
        map[2] = 0; // outermost loop is x
        ghostw = 0;
        for(int i = 0; i < 2; i++) {
            offs_s[i] = 0;
            max[i] = _max[i] + 1;
            x[i] = _max[i];
        }
        offs_s[map[2]] = x[map[2]];
        coord_t tmp = _start_1();
        offs_s[map[1]] = x[map[1]] - tmp;
        x[map[0]] = _start_0();
    }

    void init(coord_t _max[3], coord_t ghost = 0)
    {
        ((zyxiterator*)this)->init(_max, ghost);
        __init(_max, ghost);
    }

    ziterator(const zyxiterator &op) : zyxiterator(op)
    {
    }

    ziterator &operator =(const zyxiterator &op)
    {
        zyxiterator *t = (zyxiterator*)this;
        *t = op;
        return *this;
    }

    inline bool operator==(ziterator r)
    {
        zyxiterator *lhs = this, *rhs = &r;
        return(*lhs == *rhs);
    }

    inline bool operator<(ziterator r)
    {
        zyxiterator *lhs = this, *rhs = &r;
        return(*lhs < *rhs);
    }

    inline bool operator>(ziterator r)
    {
        zyxiterator *lhs = this, *rhs = &r;
        return(*lhs > *rhs);
    }

    inline bool operator>=(ziterator r)
    {
        zyxiterator *lhs = this, *rhs = &r;
        return(*lhs >= *rhs);
    }

    inline ziterator end()
    {
        ziterator tmp = ((zyxiterator*)this)->end();
        return tmp;
    }

    inline ziterator begin()
    {
        ziterator tmp = ((zyxiterator*)this)->begin();
        return tmp;
    }
};

#endif	/* ZITERATOR_H */

