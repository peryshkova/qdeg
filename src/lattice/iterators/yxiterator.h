/* 
 * File:   xyiterator.h
 * Author: artpol
 *
 * Created on 16 Июнь 2013 г., 12:54
 */

#ifndef YXITERATOR_H
#define	YXITERATOR_H

#include <zyxiterator.h>

class yxiterator : public zyxiterator {
public:

    yxiterator(coord_t _max[3], coord_t ghost = 0) :
    zyxiterator(_max, ghost)
    {
        __init(_max, ghost);
    }

    void __init(coord_t _max[3], coord_t ghost = 0)
    {
        ghostw = ghost;
        // fix z coordinate
        max[2] = _max[2] + 1;
        x[2] = _max[2];
        offs_s[2] = x[2];
        // setup the rest coordinatesd
        x[1] = _start_1();
        x[0] = _start_0();
    }
    
    void init(coord_t _max[3], coord_t ghost = 0){
        __init(_max, ghost);
    }

    /*
    yxiterator(const zyxiterator &op) : zyxiterator(op)
    {
        __init()
    }

    yxiterator &operator =(const zyxiterator &op)
    {
        zyxiterator *t = (zyxiterator*)this;
        *t = op;
        return *this;
    }
     */

    inline bool operator==(yxiterator r)
    {
        zyxiterator *lhs = this, *rhs = &r;
        return(*lhs == *rhs);
    }

    inline bool operator<(yxiterator r)
    {
        zyxiterator *lhs = this, *rhs = &r;
        return(*lhs < *rhs);
    }

    inline bool operator>(yxiterator r)
    {
        zyxiterator *lhs = this, *rhs = &r;
        return(*lhs > *rhs);
    }

    inline bool operator>=(yxiterator r)
    {
        zyxiterator *lhs = this, *rhs = &r;
        return(*lhs >= *rhs);
    }

    inline yxiterator end()
    {
        yxiterator tmp = ((zyxiterator*)this)->end();
        return tmp;
    }

    inline yxiterator begin()
    {
        yxiterator tmp = ((zyxiterator*)this)->begin();
        return tmp;
    }
};

#endif	/* YXITERATOR_H */

