/* 
 * File:   plattice_iter.h
 * Author: artpol
 *
 * Created on 29 Март 2013 г., 15:58
 */

#ifndef BASE_ITER_H
#define	BASE_ITER_H

#include <qTypes.h>
#include <lPoint.h>

class zyxiterator {
protected:
    coord_t x[3];
    coord_t ghostw;
    coord_t offs_s[3];
    coord_t offs_e[3];
    coord_t max[3];
    int map[3];

    inline coord_t _init(int i)
    {
        if(i == 0 || i == 1)
            return offs_s[i] + ghostw;
        else if(i == 2)
            return offs_s[i];
        return -1;
    }
    
    inline coord_t fini(int i)
    {
        if(i == 0 || i == 1)
            return offs_e[i] + ghostw;
        else if(i == 2)
            return offs_e[i];
        return -1;
    }

    inline coord_t _start_2()
    {
        return _init(map[2]);
    }

    inline coord_t _start_1()
    {
        coord_t tmp = _init(map[1]);
        tmp += x[map[2]] % 2;
        return tmp;
    }

    inline coord_t _start_0()
    {
        coord_t tmp = _init(map[0]);
        tmp += x[map[2]] % 2;
        tmp += 2 * ((x[map[2]] / 2 + x[map[1]] / 2) % 2);
        return tmp;
    }

    inline coord_t _end(int i)
    {
        coord_t tmp = max[i] - fini(i);
        return tmp;
    }

    inline coord_t _end_1()
    {
        return(_end(map[2]) - _start_2() - 1);
    }

    inline coord_t _end_2()
    {
        int i = map[1];
        coord_t steps = (_end(i) - _start_1()) / 2;
        if(!((_end(i) - _start_1()) % 2))
            steps--; // if number of steps is modulo 4
        return _start_1() + 2 * steps;
    }

    inline coord_t _end_3()
    {
        int i = map[0];
        coord_t steps = (_end(i) - _start_0()) / 4;
        if(!((_end(i) - _start_0()) % 4))
            steps--; // if number of steps is modulo 4
        return _start_0() + 4 * steps;
    }

    inline void _reset_coords()
    {
        x[map[2]] = _start_2();
        x[map[1]] = _start_1();
        x[map[0]] = _start_0();
    }

public:

    // ------------------------ Initialization --------------------------//

    zyxiterator(coord_t _max[3], coord_t ghost = 0)
    {
        init(_max,ghost);
    }
    
    inline void init(coord_t _max[3], coord_t ghost = 0)
    {
        ghostw = ghost;
        for(int i = 0; i < 3; i++) {
            offs_s[i] = offs_e[i] = 0;
            max[i] = _max[i];
            map[i] = i;
        }
        _reset_coords();
    }

    void offsets(coord_t _off[3])
    {
        for(int i = 0; i < 3; i++) {
            offs_s[i] = offs_e[i] = _off[i];
        }
        _reset_coords();
    }
    
    void soffsets(coord_t _off[3])
    {
        for(int i = 0; i < 3; i++) {
            offs_s[i] = _off[i];
        }
        _reset_coords();
    }
    
    void eoffsets(coord_t _off[3])
    {
        for(int i = 0; i < 3; i++) {
            offs_e[i] = _off[i];
        }
        _reset_coords();
    }
    
    
    
    /* use for(it=it.begin(); it <= it.end(); it++)
           for(it=it.end(); it >= it.begin(); it--) 
     */
    inline zyxiterator begin()
    {
        zyxiterator tmp = *this;
        tmp._reset_coords();
        return tmp;
    }

    inline zyxiterator end()
    {
        zyxiterator tmp = *this;

        tmp.x[map[2]] = _end(map[2]);
        tmp.x[map[1]] = 0;
        tmp.x[map[0]] = 0;
        //DPRINTF("(" LPSPEC "," LPSPEC "," LPSPEC ")", tmp._x, tmp._y, tmp._z );
        tmp--;
        return tmp;
    }

    // ------------------- Increment/Decrement ----------------------//

    virtual inline zyxiterator &operator++()
    {
        x[map[0]] += 4;
        if(x[map[0]] >= _end(map[0])) {
            x[map[1]] += 2;
            x[map[0]] = _start_0(); // drom map[0] coord            
            if(x[map[1]] >= _end(map[1])) {
                x[map[2]]++;
                x[map[1]] = _start_1();
                x[map[0]] = _start_0();
            }
        }
    }

    virtual inline zyxiterator operator++(int)
    {
        zyxiterator tmp = *this;
        ++(*this);
        return tmp;
    };

    virtual inline zyxiterator &operator--()
    {
        x[map[0]] -= 4;
        if(x[map[0]] < _start_0()) {
            x[map[1]] -= 2;
            x[map[0]] = _end_3(); // drom map[0] coord            
            if(x[map[1]] < _start_1()) {
                x[map[2]]--;
                x[map[1]] = _end_2();
                x[map[0]] = _end_3();
            }
        }
    }

    virtual inline zyxiterator operator--(int)
    {
        zyxiterator tmp = *this;
        --(*this);
        return tmp;
    };

    // ------------------- Comparision ----------------------//

    inline bool operator==(zyxiterator rhs)
    {
        //DPRINTF("Compare (" LPSPEC "," LPSPEC "," LPSPEC ") and ("
        //LPSPEC "," LPSPEC "," LPSPEC ")", _x, _y, _z, rhs._x, rhs._y, rhs._z );
        return !memcmp(x, rhs.x, sizeof(x));
    }

    inline bool operator<(zyxiterator rhs)
    {
        //DPRINTF("Compare (" LPSPEC "," LPSPEC "," LPSPEC ") and ("
        //LPSPEC "," LPSPEC "," LPSPEC ")", _x, _y, _z, rhs._x, rhs._y, rhs._z );

        if(this->x[map[2]] < rhs.x[map[2]])
            return true;
        else if(this->x[map[2]] > rhs.x[map[2]])
            return false;
        else {
            if(this->x[map[1]] < rhs.x[map[1]])
                return true;
            if(this->x[map[1]] > rhs.x[map[1]])
                return false;
            else {
                if(this->x[map[0]] < rhs.x[map[0]])
                    return true;

                else
                    return false;
            }
        }
    }

    inline bool operator>(zyxiterator rhs)
    {
        return(!this->operator<(rhs) && !this->operator ==(rhs));
    }
    
    inline bool operator>=(zyxiterator rhs)
    {
        return (this->operator>(rhs) || this->operator ==(rhs));
    }
    
    inline bool operator<=(zyxiterator rhs)
    {
        return (this->operator<(rhs) || this->operator ==(rhs));
    }

    inline lPoint operator*()
    {
        return lPoint(x[0], x[1], x[2]);
    }

    inline lPoint local()
    {
        return lPoint(x[0] - ghostw, x[1] - ghostw, x[2]);
    }

    int set_map(int _map[3])
    {
        bool check[3] = {0};
        for(int i = 0; i < 3; i++) {
            if(_map[i] < 0 || _map[i] >= 3)
                return -1;
            check[_map[i]] = true;
        }
        for(int i = 0; i < 3; i++) {
            if(check[i] == 0)
                return -1;
        }
        for(int i = 0; i < 3; i++) {
            map[i] = _map[i];
        }
    }
};


#endif	/* PLATTICE_ITER_H */

