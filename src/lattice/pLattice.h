/* 
 * File:   plattice.h
 * Author: artpol
 *
 * Created on 28 Май 2013 г., 16:21
 */

#ifndef PLATTICE_H
#define	PLATTICE_H

#include <mpi.h>
#include <qConfig.h>
#include <qTypes.h>
#include <dLattice.h>

#include <lPoint.h>
#include <dPoint.h>
#include <decomp.h>
#include <zyxiterator.h>

#include <debug.h>

namespace plattice {

    template<typename Elem>
    class plattice_t {
    private:
        decomp *_dcmp;
        qConfig *_qcfg;
        dlattice_t<Elem> lat;
        MPI_Datatype mpiType;
        MPI_Datatype dumpType, xcommType, ycommType;



        qConfig &qcfg()
        {
            return *_qcfg;
        }

    public:

        // lattice initialization

        plattice_t()
        {
        }

        void init(qConfig &__cfg, decomp &__dcmp, MPI_Datatype mpiType);

        // dump
        void dump(FILE *f);

        //-------------------------- Distributed operations --------------------------//
        void create_dumpType();
        void create_commTypes();
        void sync();
        void syncneigh(dPoint a);
        void mpidump(const char *fname);

        //------------------------ Access to class elements --------------------------//  
        
        decomp &dcmp()
        {
            return *_dcmp;
        }
        
        inline int Gx(){
            return _qcfg->Gx();
        }
        inline int Gy(){
            return _qcfg->Gy();
        }
        inline int Gz(){
            return _qcfg->Gz();
        }

        inline Elem &operator () (uint x, uint y, uint z)
        {
            return lat(x, y, z);
        }

        inline dlattice_t<Elem> &lattice()
        {
            return lat;
        }

        inline Elem &operator () (lPoint p)
        {
            return lat(p.x(), p.y(), p.z());
        }

        inline Elem &l(int x, int y, int z)
        {
            return lat.l(x, y, z);
        }

        inline void one_neighbor(lPoint a, int dir, lPoint &n)
        {
            lat.one_neighbor(a.x(), a.y(), a.z(), dir, n.x(), n.y(), n.z());
        }

        inline void neighbors(lPoint a, neighbors_t &nb)
        {
            lat.neighbors(a.x(), a.y(), a.z(), nb);
        }

        inline zyxiterator begin()
        {
            return zyxiterator(dcmp().ldimwgh_x(), dcmp().ghostx(), dcmp().ldimwgh_y(), dcmp().ghosty(),
                    qcfg().Lz());
        }

        inline zyxiterator end()
        {
            zyxiterator it(dcmp().ldimwgh_x(), dcmp().ghostx(), dcmp().ldimwgh_y(), dcmp().ghosty(),
                    qcfg().Lz());
            return it.end();
        }
    };

    template<typename Elem>
    void plattice_t<Elem>::init(qConfig &__cfg, decomp &__dcmp, MPI_Datatype mpi_t)
    {
        _dcmp = &__dcmp;
        _qcfg = &__cfg;
        // Create local lattice.
        mpiType = mpi_t;
        lPoint p = dcmp().ldims_wgh();
        if(lat.init(p.x(), p.y(), qcfg().Gz())) {
            EPRINTF("Cannot initialize lattice");
            MPI_Abort(MPI_COMM_WORLD, 0);
        }
        create_dumpType();
        create_commTypes();
    }

    template<typename Elem>
    void plattice_t<Elem>::create_dumpType()
    {
        lPoint c = dcmp().gcubs();
        // Important! this array should be initialized in MPI (and C array declaration) order 
        // (most significant dimension first) rather that C usual order: least significant value first.
        int gsizes[decomp::n] = {c.y() * 2, c.x()};
        int distribs[decomp::n] = {MPI_DISTRIBUTE_BLOCK, MPI_DISTRIBUTE_BLOCK};
        int dargs[decomp::n] = {MPI_DISTRIBUTE_DFLT_DARG, MPI_DISTRIBUTE_DFLT_DARG};

        int tsize;
        MPI_Aint ext;

        cartmesh<decomp::n> &m = dcmp().mesh();
        MPI_Type_create_darray(m.size(), m.rank(), decomp::n, gsizes, distribs, dargs,
                m.pdims(), MPI_ORDER_C, mpiType, &dumpType);
        MPI_Type_commit(&dumpType);

        MPI_Type_extent(dumpType, &ext);
        MPI_Type_size(dumpType, &tsize);
        //DPRINTF("ext = %d, size = %d\n", ext, tsize);
    }

    template<typename Elem>
    void plattice_t<Elem>::create_commTypes()
    {
        lPoint d = dcmp().ldims_wgh();
        d.z() = qcfg().Gz();

        MPI_Type_vector((d.y() / 2)*(d.z() / 4), 4, d.x(), mpiType, &xcommType);
        MPI_Type_commit(&xcommType);

        MPI_Type_vector(d.z() / 4, d.x()*2, d.x()*(d.y() / 2), mpiType, &ycommType);
        MPI_Type_commit(&ycommType);
    }

    template<typename Elem>
    void plattice_t<Elem>::sync()
    {
        int myrank;
        int nranks[3][3]; // left and right neighbors ranks by y axe
        MPI_Status status;
        int crd[2];
        cartmesh<decomp::n> &m = dcmp().mesh();

        char out[1024] = "";
        int i, j;

        m.coords(crd);
        lPoint d = dcmp().ldims_wgh();
        lPoint g = dcmp().ghosts();

        sprintf(out, "(%d,%d):\n", crd[0], crd[1]);

        for(i = -1; i < 2; i++) {
            for(j = -1; j < 2; j++) {
                m.coords(crd);
                crd[0] += i;
                crd[1] += j;
                nranks[1 + i][1 + j] = dcmp().mesh().coords2rank(crd);
                sprintf(out, "%s(%d,%d)->%d\n", out, crd[0], crd[1], nranks[1 + i][1 + j]);
            }
        }

        for(i = -1; i < 2; i++) {
            for(j = -1; j < 2; j++) {
                sprintf(out, "%s%d ", out, nranks[1 + i][1 + j]);
            }
            sprintf(out, "%s\n", out);
        }

        //printf("%s", out);

        // exchange x borders
        if(m[0] > 1) {
            //DPRINTF("lat = %p, lat.l(4,0,0)=%p, lat.l(_lx-4,0,0)=%p",
            //   &lat(0, 0, 0), &lat(4, 0, 0), &lat(dims[0] - 4, 0, 0));

            MPI_Sendrecv(&lat.l(g.x(), 0, 0), 1, xcommType, nranks[0][1], 1,
                    &lat.l(d.x() - g.x(), 0, 0), 1, xcommType, nranks[2][1], 1,
                    m.comm(), &status);

            MPI_Sendrecv(&lat.l(d.x() - 2 * g.x(), 0, 0), 1, xcommType,
                    nranks[2][1], 1, &lat.l(0, 0, 0), 1, xcommType, nranks[0][1], 1,
                    m.comm(), &status);

        }
        // exchange y borders
        // TODO: check if possible!! m.pdims()[0] = 1;

        if(m[1] > 1) {

            MPI_Sendrecv(&lat.l(0, g.y() / 2, 0), 1, ycommType, nranks[1][0], 1,
                    &lat.l(0, (d.y() - g.y()) / 2, 0), 1, ycommType,
                    nranks[1][2], 1, m.comm(), &status);

            MPI_Sendrecv(&lat.l(0, (d.y() - 2 * g.y()) / 2, 0), 1, ycommType,
                    nranks[1][2], 1, &lat.l(0, 0, 0), 1, ycommType, nranks[1][0], 1,
                    m.comm(), &status);
        }
    }

    template<typename Elem>
    void plattice_t<Elem>::syncneigh(dPoint dp)
    {
        cartmesh<decomp::n> &m = dcmp().mesh();
        decomp::relmap_t cmap;
        lPoint d = dcmp().ldims_wgh();
        lPoint g = dcmp().ghosts();
        int i, j;

        if(dcmp().comm_pattern(dp.glob(), cmap)) {
            DPRINTF("I am related");
            int crd[2];
            for(i = 0; i < 3; i += 2) {
                m.coords(crd);
                if(cmap[1][i] == 2) {
                    uint offset = (!!i)*(d.x() - g.x());
                    crd[0] += i - 1;
                    int rank = m.coords2rank(crd);
                    MPI_Status status;
                    DPRINTF("MPI_Recv(%d)", rank);
                    MPI_Recv(&lat.l(offset, 0, 0), 1, xcommType, rank, 1, m.comm(), &status);
                }
            }

            for(i = 0; i < 3; i += 2) {
                int crd[2];
                m.coords(crd);
                if(cmap[i][1] == 2) {
                    uint offset = (!!i)*(d.y() - g.y()) / 2;
                    crd[1] += i - 1;
                    int rank = m.coords2rank(crd);
                    MPI_Status status;
                    DPRINTF("MPI_Recv(%d)", rank);
                    MPI_Recv(&lat.l(0, offset, 0), 1, ycommType, rank, 1, m.comm(), &status);
                }
            }

            for(i = 0; i < 3; i += 2) {
                int crd[2];
                m.coords(crd);
                if(cmap[1][i] == 1) {
                    uint offset = g.x() + (i / 2)*(d.x() - 3 * g.x());
                    crd[0] += i - 1;
                    int rank = m.coords2rank(crd);
                    DPRINTF("MPI_Send(%d)", rank);
                    MPI_Send(&lat.l(offset, 0, 0), 1, xcommType, rank, 1, m.comm());
                }
            }

            for(i = 0; i < 3; i += 2) {
                int crd[2];
                m.coords(crd);
                if(cmap[i][1] == 1) {
                    uint offset = (g.y() + (i / 2)*(d.y() - 3 * g.y())) / 2;
                    crd[1] += i - 1;
                    int rank = m.coords2rank(crd);
                    DPRINTF("MPI_Send(%d)", rank);
                    MPI_Send(&lat.l(0, offset, 0), 1, ycommType, rank, 1, m.comm());
                }
            }
        }

    }

    template<typename Elem>
    void plattice_t<Elem>::dump(FILE *f)
    {
        /*
        int x, y, z;
        fprintf(f, "------------ Start dump------------------\n");
        int out;
        zyxiterator it(dcmp().ldimwgh_x(), 0, dcmp().ldimwgh_y(), 0,
                qcfg().Lz());
        it.zoffs(0);

        y = -1;
        z = -1;

        for(; it < it.end();) {

            if(z != (*it).z()) {
                if(z > -1)
                    fprintf(f, "\n\n");
                z = (*it).z();
            }

            if(y != (*it).y()) {
                fprintf(f, "{ ");
                y = (*it).y();
            }

            int out = (*this)(*it);
            fprintf(f, "%3d ", out);

            it++;
            if(y != (*it).y()) {
                fprintf(f, "}\n");
            }
        }
        fprintf(f, "\n-----------------End dump---------------------\n\n");
         */
    }

    template<typename Elem>
    void plattice_t<Elem>::mpidump(const char *fname)
    {
        MPI_Info info;
        MPI_File fh;
        MPI_Datatype t;
        int tsize;
        MPI_Comm comm = dcmp().mesh().comm();
        int rank;
/*
        MPI_Comm_rank(comm, &rank);
        MPI_Barrier(comm);

        DPRINTF("Open file \"%s\"", fname);

        MPI_File_open(comm, (char*) fname, MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &fh);

        DPRINTF("File \"%s\" is opened successfuly", fname);

        if(rank == 0) {
            dumpTypes t;
            if(mpiType == MPI_CHAR)
                t = CHAR;
            else if(mpiType == MPI_SHORT)
                t = SHORT;
            else if(mpiType == MPI_INT)
                t = INT;
            else if(mpiType == MPI_FLOAT)
                t = FLOAT;
            else if(mpiType == MPI_DOUBLE)
                t = DOUBLE;
            int tmp = (int) t;
            MPI_File_write(fh, &tmp, 1, MPI_INT, MPI_STATUS_IGNORE);
            ulong buf[] = {dcmp().gcub_x(), 2 * dcmp().gcub_y()};
            MPI_File_write(fh, buf, 2, MPI_LONG, MPI_STATUS_IGNORE);
        }

        //MPI_Barrier(comm);

        MPI_File_set_view(fh, sizeof(int) + 2 * sizeof(long), mpiType, dumpType,
                (char*) "native", MPI_INFO_NULL);

        int z;
        coord_t locx = dcmp().lcub_x();
        coord_t locy = dcmp().lcub_y()*2;
        zyxiterator it = (*this).begin();

        coord_t zstart = it.startz();
        coord_t zmax = it.maxz();

        for(z = zstart; z < zmax; z++) {
            it.fix(z);
            Elem buf[locy][locx];
            char outstr[1024] = "";
            int x1 = 0, y1 = 0;
            for(; it < it.end(); it++) {
                lPoint p = *it;
                lPoint p1 = it.local();
                if(rank == 0) {
                    DPRINTF("z = " LPSPEC "; buf[" LPSPEC "][" LPSPEC "] = %d", p1.z(), p1.y() / 2, p1.x() / 4, (*this)(*it));
                }
                buf[y1][x1] = (*this)(*it);
                x1++;
                if(x1 >= locx) {
                    x1 = 0;
                    y1++;
                }
                sprintf(outstr, "%s %d", outstr, buf[p1.y() / 2][p1.x() / 4]);
            }
            if(rank == 0)
                DPRINTF("---------------------------------");
            sprintf(outstr, "%s\n", outstr);
            DPRINTF("z=%d localx=%d, localy=%d\n%s",
                    z, locx, locy, outstr);
            MPI_File_write_all(fh, buf, locx*locy, mpiType, MPI_STATUS_IGNORE);
        }

        MPI_File_close(&fh);
        DPRINTF("exit");
 */
    }
};

#endif	/* PLATTICE_H */

