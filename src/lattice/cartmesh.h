/* 
 * File:   cartOps.h
 * Author: artpol
 *
 * Created on 12 Март 2013 г., 9:41
 */

#ifndef CARTMESH_H
#define	CARTMESH_H

#include <qTypes.h>
#include <mpi.h>
#include <debug.h>

template <int n>
class cartmesh {
private:
    MPI::Intracomm bcomm;
    MPI::Cartcomm _comm;


    int _size, _rank;

    /* User prefer to have least significant dimension (like x) in dims[0], but 
     MPI_Cart_create waits for the most significant dimension (like z) there*/
    int dims[n]; // original dimensions from class user
    int mpidims[n]; // dimensions for MPI_Cart_create
    int mpicoords[n], _coords[n];

    void reverse(int src[], int dst[]);

public:

    cartmesh()
    {
    }

    int init(MPI::Intracomm c, int _dims[]);
    void coords(int c[]);
    int coords2rank(int c[]);
    void rank2coords(int r, int c[]);

    MPI::Intracomm &comm()
    {
        return _comm;
    }

    int rank()
    {
        return _rank;
    }

    int size()
    {
        return _size;
    }

    int operator [](int idx) {
        return(int) dims[idx];
    }

    int *pdims()
    {
        return dims;
    }

};

template <int n>
void cartmesh<n>::reverse(int src[], int dst[])
{
    for(int i = 0; i < n; i++) {
        dst[i] = src[n - i - 1];
    }
}

template <int n>
int cartmesh<n>::init(MPI::Intracomm c, int _dims[])
{
    bool periods[n];
    int chksize = 1;
    bcomm = c;
    for(int i = 0; i < n; i++) {
        periods[i] = true;
        dims[i] = _dims[i];
        mpidims[n - i - 1] = dims[i];
        chksize *= dims[i];
    }

#ifdef DEBUG_ON
    int osize = bcomm.Get_size();
    int orank = bcomm.Get_rank();
    if(osize != chksize && !orank) {
        EPRINTF("Size of the old communicator (%d) doesn't correspond to the multiply of dimensions (%d)\n",
                osize, chksize);
        MPI::COMM_WORLD.Abort(0);
    }
#endif

    _comm = bcomm.Create_cart(n, mpidims, periods, true);
    _size = _comm.Get_size();
    _rank = _comm.Get_rank();
    _comm.Get_coords(_rank, n, mpicoords);
    reverse(mpicoords, _coords);
    return 0;
}

template <int n>
void cartmesh<n>::coords(int c[])
{
    for(int i = 0; i < n; i++) {
        c[i] = _coords[i];
    }
}

template <int n>
int cartmesh<n>::coords2rank(int c[])
{
    int tmp[n], r;
    reverse(c, tmp);
    return _comm.Get_cart_rank(tmp);
}

template <int n>
void cartmesh<n>::rank2coords(int r, int c[])
{
    int tmp[n];
    _comm.Get_coords(r, n, tmp);
    reverse(tmp, c);
}

#endif	/* CARTMESH_H */

