/* 
 * File:   configuration.h
 * Author: artpol
 *
 * Created on 6 Февраль 2013 г., 11:02
 */

#ifndef CONFIGURATION_H
#define	CONFIGURATION_H

#include <dLattice.h>

#include <qTypes.h>

#define Nconfig 65536 // 2^16 - 1

namespace plattice {

    class dlattcfg {
    private:
        bool good_conf[Nconfig];
        uchar n1_conf[Nconfig];
        uchar n2_conf[Nconfig];
    public:

        dlattcfg()
        {
            unsigned int conf;
            int nb_type[dir_number];
            int n1; // number of direct neighbors
            int n2; // number of 2-hop neighbors that are CONNECTED
            int n2_all; // number of non-zero 2-hop neighbors
            for(conf = 0; conf < Nconfig; conf++) {
                n1 = n2 = n2_all = 0;
                for(int i = 0; i < dir_number; i++)
                    if(pr_neighbor[i] < 0 && (conf & (1 << i)) )
                        n1++;
                for(int i = 0; i < dir_number; i++)
                    if(pr_neighbor[i] >= 0) {
                        int j = pr_neighbor[i];
                        if((conf & (1 << i)) && (conf & (1 << j)))
                            n2++;
                        if(conf & (1 << i))
                            n2_all++;
                    }
                n1_conf[conf] = n1;
                n2_conf[conf] = n2;
                good_conf[conf] = (n2 >= 2);
            }
        }

        uchar n1_config(ushort conf)
        {
            return n1_conf[conf];
        }

        uchar n2_config(ushort conf)
        {
            return n2_conf[conf];
        }

        bool good_config(ushort conf)
        {
            return good_conf[conf];
        }

        ushort set_config(dlattice_t<atomType> &l, lPoint &p)
        {
            int dir, x2, y2, z2, factor = 1;
            int nb_type[dir_number];
            neighbors_t nbs;
            ushort conf = 0;

            if(p.z() % 2 != 0)
                factor = -1;
            l.neighbors(p, nbs);

            for(dir = 0; dir < dir_number; dir++) {
                lPoint p1(nbs.x[dir],nbs.y[dir],nbs.z[dir]);
                conf |= (l(p1) != None) << dir;
            }
            return conf;
        }
    };

};

#endif	/* CONFIGURATION_H */

