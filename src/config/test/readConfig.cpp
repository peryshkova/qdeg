#include <mpi.h>
#include<qConfig.h>
#include <iostream>

int main(int argc, char **argv)
{
    std::string s = "input";
    std::stringstream sstream;
    MPI::Init(argc, argv);
    MPI::Intercomm comm = MPI::COMM_WORLD;
    qConfig cfg(MPI::COMM_WORLD, s);
    cfg.print(sstream);
    std::cout << sstream.str();
    MPI::Finalize();
    return 0;
}