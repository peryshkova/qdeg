/* 
 * File:   qConfig.h
 * Author: artpol
 *
 * Created on 28 Май 2013 г., 10:13
 */

#ifndef QCONFIG_H
#define	QCONFIG_H

#include <mpi.h>
#include <string>
#include <qTypes.h>
#include <debug.h>
#include <constants.h>

// Config parse functions
#include <iostream>
#include <fstream>
#include <sstream>

#define DEFAULT_CONFIG "pqdeg.cfg"

class qConfig {

    enum DIM {
        x = 0, y = 1, z = 2
    };
private:

    MPI::Intercomm comm;
    // Lattice settings
    coord_t _G[3]; // Global lattice dimensions
    //coord_t _L[3]; // Local lattice dimensions
    coord_t _N[2]; // Node grid dimensions
    coord_t _zSurf; // Initial surface level

    // Checkpoint parameters
    double fCkpt, dCkpt; // time vefore first checkpoint and between checkpoints

    // Simulation configuration

    // ----------- Parse config file -----------------------
    // TODO: add more advaced parsing in future

    inline unsigned long readcoord(std::ifstream &s, coord_t &dim)
    {
        // Read values from lines like XX = val
        // skip till ' ' 2 times and then read
        s.ignore(256, ' ');
        s.ignore(256, ' ');
        s >> dim;
        return s.fail();
    }

    int parseConfig(std::string name)
    {
        std::ifstream st;

        int err;
        st.open(name.c_str());

        if(readcoord(st, _G[x]))
            PQDEG_ABORT("Error in configuration file reading Gx");
        if(readcoord(st, _G[y]))
            PQDEG_ABORT("Error in configuration file reading Gy");
        if(readcoord(st, _G[z]))
            PQDEG_ABORT("Error in configuration file reading Gz");
        if(readcoord(st, _N[x]))
            PQDEG_ABORT("Error in configuration file reading Nx");
        if(readcoord(st, _N[y]))
            PQDEG_ABORT("Error in configuration file reading Ny");

        // 1. _G[x,y,z] has to be multiple of 4
        if((_G[x] % ECUBE) || (_G[y] % ECUBE) || (_G[z] % ECUBE)) {
            PQDEG_ABORT("One of global dimensions is not a multiple of elementary cube size");
        }

        // 2. Communicator need at least size processes
        unsigned int size = _N[x] * _N[y];
        if(size > comm.Get_size() - 1) {
            PQDEG_ABORT("Processing grid is %dx%d + master, but communicator size is only %d",
                    _N[x], _N[y], comm.Get_size());
        }
        return 0;
    }

        /*
        inline void count_dims()
        {
            int rank = comm.Get_rank();
            for(int i = 0; i < 2; i++) {
                unsigned int cubes = _G[i] / ECUBE;
                _L[i] = cubes / _N[i];
                if(rank < cubes % _N[i]) {
                    _L[i]++;
                }
                _L[i] *= ECUBE;
            }
        }
     */
    void bcast()
    {
        int gsize = sizeof(_G);
        int nsize = sizeof(_N);
        int size = nsize + gsize;
        unsigned long buf[size];


        if(comm.Get_rank() == 0) {
            // Send config data
            for(int i = 0; i < gsize; i++)
                buf[i] = _G[i];
            for(int i = gsize, j = 0; i < size; i++, j++)
                buf[i] = _N[j];
            comm.Bcast(buf, size, mpi_coord_t, 0);
        } else {
            // Receive config data
            comm.Bcast(buf, size, mpi_coord_t, 0);
            for(int i = 0; i < gsize; i++)
                _G[i] = buf[i];
            for(int i = gsize, j = 0; i < size; i++, j++)
                _N[j] = buf[i];
        }
    }
public:

    inline qConfig(std::string name)
    {
        comm = MPI::COMM_WORLD;
        if(comm.Get_rank() == 0) {
            parseConfig(name);
        }
        bcast();
    }

    void print(std::stringstream & ss)
    {
        ss << "ParQDEG configuration:" <<
                "\n\tGx = " << Gx() << ", Gy = " << Gy() << ", Gz = " <<
                Gz() << std::endl;
        ss << "\tNx = " << Nx() << ", Ny = " << Ny() << std::endl;
    }
    // Access to lattise parameters

    inline coord_t Gx()
    {
        return _G[x];
    }

    inline coord_t Gy()
    {
        return _G[y];
    }

    inline coord_t Gz()
    {
        return _G[z];
    }

    inline coord_t Nx()
    {
        return _N[0];
    }

    inline coord_t Ny()
    {
        return _N[1];
    }
};

#endif	/* QCONFIG_H */
