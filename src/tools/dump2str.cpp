#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <types.h>
#include <stdlib.h>
#include <iostream>

using namespace std;
/*
    Test program that checks how Cartesian coordinates are
    distributed.
    It seems that MPI_Cart_create provides Fortran-style distribution.
*/

template<typename Elem>
class printDump
{
public:
    void writeDump(FILE *f, dumpTypes t, bool inhex);
};

dumpTypes get_type(FILE *f)
{
    int tmp;
    int offset = fread(&tmp,sizeof(int),1,f);
    if( offset != 1 ){
        printf("Error reading mesh, offset=%d\n",offset);
        exit(0);
    }
    return (dumpTypes)tmp;

}    

int main(int argc, char **argv)
{
    char fname[] = "";
    bool inhex = false;
    
    if( argc < 2 ){
        printf("Need file name as argument\n");
        return 0;
    }

    FILE *f = fopen(argv[1],"r");
    if( f == NULL ){
        printf("Error opening file %s\n",fname);
        return 0;
    }
    
    if( argc > 2 ){
        inhex = true;
    }
    
    dumpTypes type = get_type(f);
    
    switch( type ){
    case CHAR:{
        printDump<char> dump;
        dump.writeDump(f, type, inhex);
        break;
    }
    case SHORT:{
        printDump<short> dump;
        dump.writeDump(f, type, inhex);
        break;
    }
    case INT:{
        printDump<int> dump;
        dump.writeDump(f, type, inhex);
        break;
    }
    case FLOAT:{
        printDump<float> dump;
        dump.writeDump(f, type, inhex);
        break;
    }
    case DOUBLE:{
        printDump<double> dump;
        dump.writeDump(f, type, inhex);
        break;
    }
    }
}
/*
template<typename Elem>
int printDump<Elem>::int calc_wide(FILE *f)
{
    long offs = ftell(f);
    Elem tmp;
    max = 0;
    while( !feof(f) ){
        fread(tmp,sizeof(tmp),1,f);
        int digits = 0;
        while( tmp    
    
    }
}
*/
template<typename Elem>
void printDump<Elem>::writeDump(FILE *f, dumpTypes t, bool inhex)
{
    ulong ranges[2];
    off_t offset;
    
    offset = fread(ranges,sizeof(ulong),2,f);
    if( offset != 2 ){
        printf("Error reading mesh, offset=%zu\n",(size_t)offset);
        return;
    }
    
    if( inhex )
        cout << hex;
    
    while( !feof(f) ){
      for(int i=0;i<ranges[1];i++){
        for(int j=0;j<ranges[0];j++){
            Elem b;
            if( fread(&b,sizeof(Elem),1,f) == 0) {
                if( feof(f) )
                    break;
                else{
                    int error = ferror(f);
                    printf("I/O error: %s\n", strerror(error));
                    return;
                }
            }
            
            if( t == CHAR ){
                int I = b;
                cout << I << " ";
            }else
                cout << b << " ";
        }
        cout << std::endl;
      }
      cout << std::endl;
      cout << std::endl;
    }
    fclose(f);
}
