/* 
 * File:   debug.h
 * Author: artpol
 *
 * Created on 28 Май 2013 г., 13:30
 */

#ifndef DEBUG_H
#define	DEBUG_H
#include <stdlib.h>
#include <mpi.h>

//#define DEBUG_ON // Define it in each file separately now!!!!

extern FILE *__parqdeg_dbgout__;
extern int __rank__;

#define DEBUG_IN_FILE (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)



#define EPRINTF(fmt,args...)\
{ \
   static int __r__ = MPI::COMM_WORLD.Get_rank(); \
   fprintf(stderr, "[%d] %s(%s):%d " fmt "\n", __r__, \
          DEBUG_IN_FILE, __FUNCTION__, \
          __LINE__, ## args  ); \
   fflush(stderr); \
}

#define PQDEG_ABORT(fmt,args...) { \
    EPRINTF(fmt, ## args ); \
    MPI::COMM_WORLD.Abort(0); \
}

#define DPRINTF(fmt,args...)


#ifdef DEBUG_ON

static void init_debug(FILE *out = NULL)
{
    __parqdeg_dbgout__ = out;
    __rank__ = MPI::COMM_WORLD.Get_rank();
    if(::__parqdeg_dbgout__ == NULL) {
        char tmp[256];
        sprintf(tmp, "rank%d.log", ::__rank__);
        ::__parqdeg_dbgout__ = fopen(tmp, "w");
        if(::__parqdeg_dbgout__ == NULL) {
            ::MPI_Abort(MPI_COMM_WORLD, 0);
        }
    }
}

#undef  DPRINTF
#define DPRINTF(fmt,args...)\
{ \
   if( __rank__ < 0 ){ \
     fprintf(__parqdeg_dbgout__, "[%d]:%s(%s):%d: DPRINTF is used before debug_init! Abort.\n", __rank__, DEBUG_IN_FILE , \
          __FUNCTION__, __LINE__, ## args  ); \
     MPI::COMM_WORLD.Abort(0); \
   } else {\
     fprintf(__parqdeg_dbgout__, "[%d]:%s(%s):%d: " fmt "\n", __rank__, \
          DEBUG_IN_FILE, __FUNCTION__, __LINE__, ## args  ); \
     fflush(__parqdeg_dbgout__); \
   } \
}

#else

static void init_debug(FILE *out = NULL)
{
}

#endif


#define ERR_EXIT(msg) { \
  printf( "\n---------------\n%s:%d: " msg \
  "\n---------------\n\n", DEBUG_IN_FILE, __LINE__); \
  MPI::COMM_WORLD.Abort(0); \
  exit(0); \
}

#endif	/* DEBUG_H */

