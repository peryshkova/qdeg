/* 
 * File:   qTypes.h
 * Author: artpol
 *
 * Created on 28 Май 2013 г., 10:14
 */

#ifndef QTYPES_H
#define	QTYPES_H

#include <mpi.h>

typedef unsigned long ulong;
typedef unsigned int uint;
typedef unsigned char uchar;
typedef unsigned short ushort;
typedef int coord_t;
const MPI::Datatype mpi_coord_t = MPI::LONG;

enum dumpTypes {
    CHAR, SHORT, INT, LONG, FLOAT, DOUBLE
};

enum atomType{ None, Si, Ge };

#endif	/* QTYPES_H */

